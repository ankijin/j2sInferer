import static org.simmetrics.builders.StringMetricBuilder.with;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.internal.compiler.parser.Scanner;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;

import alignment.common.HungarianAlgorithm;
import gumtree_tester.SwiftBaseListener;

public class StructureMapping2 {

	ASTNode android;
	ParserRuleContext swift;

	public StructureMapping2(ASTNode android, ParserRuleContext swift){
		this.android = android;
		this.swift = swift;
		//		SwiftParser.Rule

	}

	public void compute(){

		List<String> 		aanodes 	= new LinkedList<String>();
		List<ASTNode> 		taanodes 	= new LinkedList<ASTNode>();


		List<String> 	ssnodes 					= new LinkedList<String>();
		List<ParserRuleContext> 	tssnodes 		= new LinkedList<ParserRuleContext>();
		Scanner scanner = new Scanner();
		scanner.recordLineSeparator = true;
		char[] source = android.toString().toCharArray();
		int az = swift.start.getStartIndex();
		int bz = swift.stop.getStopIndex();
		Interval interval = new Interval(az,bz);

		final String swift_str2 = swift.start.getInputStream().getText(interval);
		String swift_str = swift_str2;
		//		StringBuffer b = new StringBuffer();
		scanner.setSource(source);
		//				scanner.getCurrentTokenString()
		SwiftBaseListener lis = new SwiftBaseListener(){
			@Override
			public void enterEveryRule(ParserRuleContext ctx) {
				// TODO Auto-generated method stub
				int az = ctx.start.getStartIndex();
				int bz = ctx.stop.getStopIndex();
				Interval interval = new Interval(az,bz);
				String context = swift.start.getInputStream().getText(interval);
//				System.out.println();
				for(String t:ssnodes){
					if(t.contains(context)) return;
				}
				if(!context.contains("var")&&!context.contains(":")&&!context.contains("?")&&!context.contains("return")&&!context.contains("=")&&!ssnodes.contains(context) && !swift_str2.equals(context)){
					
					ssnodes.add(context);
					tssnodes.add(ctx);
				}
				//				}
				super.enterEveryRule(ctx);
			}
		};
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(lis, swift);

		android.accept(new ASTVisitor() {
			@Override
			public void preVisit(ASTNode node) {
				// TODO Auto-generated method stub
				for(String t:aanodes){
					if(t.contains(node.toString())) return;
				}
				
			  if(!node.toString().contains("final")&&!node.toString().contains(";")&&!node.toString().contains("=")&&!aanodes.contains(node.toString()) && ! node.toString().equals(android.toString())){
//				if(!node.toString().equals(android)){
					aanodes.add(node.toString());
//				}
			  }
				super.preVisit(node);
				
			}
		});


		/*
		android.accept(new ASTVisitor() {
			@Override
			public boolean visit(FieldDeclaration node) {
				// TODO Auto-generated method stub
				if(node.getType()!=null){
					taanodes.add(node.getType());
					aanodes.add(node.getType().toString());
				}
				if(node.modifiers()!=null){
					List modis = node.modifiers();
					for(int j=0; j<modis.size();j++){
						taanodes.add((ASTNode)modis.get(j));
						aanodes.add(modis.get(j).toString());
					}
				}
				List frgs = node.fragments();
				for(int i=0;i<frgs.size();i++){
					VariableDeclarationFragment fg = (VariableDeclarationFragment)frgs.get(i);
					if(fg.getInitializer()!=null){
						taanodes.add(fg.getInitializer());
						aanodes.add(fg.getInitializer().toString());
					}
					if(fg.getName()!=null){
						taanodes.add(fg.getName());
						aanodes.add(fg.getName().toString());
					}
				}

				return super.visit(node);
			}

		});
		 */

		double[][] ecostMatrix = new double[aanodes.size()][ssnodes.size()];


		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		for(int i=0;i<aanodes.size();i++){
			for(int j=0;j<ssnodes.size();j++){
				ecostMatrix[i][j]= 1- emetric.compare(aanodes.get(i), ssnodes.get(j));
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		if(aanodes.size() > 0 && ssnodes.size() >0){
			ehung = new HungarianAlgorithm(ecostMatrix);
			eresult = ehung.execute();
		}

		Map<String, String> amap = new HashMap<String, String>();
		Map<String, String> smap = new HashMap<String, String>();
		int argnum = 0;
		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1){
				amap.put(aanodes.get(p), "<arg"+argnum+">");
				smap.put(ssnodes.get(eresult[p]), "<arg"+argnum+">");
				argnum++;
			}
		}




		Comparator<String> comparator = new Comparator<String>() {
			public int compare(String o1, String o2) {
				if(o1.length()>=o2.length()) return -1; 
				else return 1;
			}
		};

		SortedSet<String> keys = new TreeSet<String>(comparator);
		keys.addAll(amap.keySet());

		SortedSet<String> skeys = new TreeSet<String>(comparator);
		skeys.addAll(smap.keySet());

		//		amap = sortByValues(amap, -1);  // Ascending order
		//		smap = sortByValues(smap, -1);  // Ascending order

		Iterator<String> ait = keys.iterator();
		Iterator<String> sit = skeys.iterator();

		swift.start.getInputStream();
		if(android instanceof FieldDeclaration){
			((FieldDeclaration)android).setJavadoc(null);
		}
		String android_str = android.toString().replace("\n", "");
		System.out.println("keys"+keys);
		int aaa=0;
		while(ait.hasNext()){
			String key = ait.next();

			String expr = key;
			String arg = amap.get(key);
			//			System.err.println(astnode+" "+arg);
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\!", "\\\\!");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
			expr=expr.replaceAll("\\+", "\\\\+");
			expr=expr.replaceAll("\\-", "\\\\-");
			//			expr=expr.replaceAll("\\ ", "\\\\ ");
			expr=expr.replaceAll("/", "\\\\/");
			expr=expr.replaceAll("\\s", "\\\\s");
			expr=expr.replaceAll("\\&", "\\\\&");
			//			android_str=android_str.replaceAll("\b"+expr+"\b", arg);
			android_str=android_str.replaceFirst(expr, arg);
			System.err.println(android_str+" "+expr);
			aaa++;
		}

		int sss=0;
		System.out.println("skeys"+skeys);
		while(sit.hasNext()){
			String key = sit.next();
			String expr = key;
			String arg = smap.get(key);

			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\!", "\\\\!");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
			expr=expr.replaceAll("\\+", "\\\\+");
			expr=expr.replaceAll("\\-", "\\\\-");
			//			expr=expr.replaceAll("\\ ", "\\\\ ");
			expr=expr.replaceAll("/", "\\\\/");
			expr=expr.replaceAll("\\s", "\\\\s");
			expr=expr.replaceAll("\\&", "\\\\&");
			//			System.err.println(astnode+" "+arg);
			//			swift_str=swift_str.replaceAll("\\b"+expr+"\\b", arg);
			swift_str=swift_str.replaceFirst(expr, arg);
			System.out.println("smap	"+swift_str+" "+expr+" "+smap);
			sss++;
		}

		System.out.println("android_str	"+android+"   "+android_str);
		System.out.println("swift_str	"+swift_str2+"	"+swift_str);

		if(aaa==sss){
			BufferedWriter out;
			try {
				out = new BufferedWriter(new FileWriter("sample4.txt", true));
				//		out.write(android.toString()+"	"+swift.getText());
				out.write(android.toString()+android.getNodeType());
				out.write(swift.getText()+"\n"+swift.getRuleIndex());
				out.write(android_str.toString()+"\n");
				out.write(" "+swift_str.toString()+"\n");
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


	}
}
