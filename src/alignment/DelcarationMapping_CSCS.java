package alignment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionMethodReference;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.simmetrics.StringMetric;
//import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.StringMetrics;

import alignment.common.HungarianAlgorithm;
import alignment.common.LineCharIndex;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.ClassBodyDeclarationContext;
import antlr_parsers.javaparser.JavaParser.TypeDeclarationContext;
import migration.BaseSwiftRecorder;
import migration.BindingUnit;
import migration.StructureMappingAntlr4_for_examples;
import migration.SwiftBaseListener;
import migration.SwiftLexer;
import migration.SwiftParser;
import migration.VariableDeclarion;
import migration.SwiftParser.Class_declarationContext;
import migration.SwiftParser.Function_nameContext;
import migration.SwiftParser.Pattern_initializerContext;
import migration.SwiftParser.Top_levelContext;
import migration.SwiftParser.Variable_declarationContext;
import migration.SwiftParser.Variable_nameContext;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;

public class DelcarationMapping_CSCS {
	public static CommonTokenStream swiftcommon=null;
	public static CommonTokenStream androidcommon=null;
	public static String androidpath=null, swiftpath=null;
	public static Map<String, String> methodBinding;

	public static LinkedHashMap<LineCharIndex, String> totalBindings = new LinkedHashMap<LineCharIndex, String>();

	public static LinkedHashMap<LineCharIndex, BindingUnit> unitBindingMap = new LinkedHashMap<LineCharIndex, BindingUnit>();

	public static <T> Map<String, ParserRuleContext> getVarDeclSwift(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();

		SwiftParser.Top_levelContext root1 = null;
		try{
			ANTLRInputStream stream1 	= new ANTLRInputStream(new FileReader(path));
			Lexer lexer1 				= new SwiftLexer((CharStream)stream1);
			swiftcommon					= new CommonTokenStream(lexer1);
			SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
			root1 = parser1.top_level();
		} catch (Exception e){
			//			return nodeSwift;
		}
		BaseSwiftRecorder record= new BaseSwiftRecorder(){
			Variable_declarationContext var_decl;
			@Override
			public Object visitVariable_declaration(Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				var_decl=ctx;

				try{
					if(ctx.pattern_initializer_list()!=null&& !ctx.pattern_initializer_list().pattern_initializer().isEmpty()){
						List<Pattern_initializerContext> list = ctx.pattern_initializer_list().pattern_initializer();
						nodeSwift.put(list.get(0).pattern().identifier_pattern().getText(), var_decl);
					}
				}catch (Exception e){

				}
				return super.visitVariable_declaration(ctx);
			}


		};
		if(root1!=null)
			root1.accept(record);


		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getVarDeclAndroid(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeAndroid = new HashMap<String, ParserRuleContext>();

		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
		JavaParser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();


		//		System.out.println("parser.compilationUnit()"+parser.compilationUnit().getText());
		//				walker.walk(listner, parser.compilationUnit());

		JavaBaseListener listener = new JavaBaseListener(){
			ParserRuleContext field; 

			@Override
			public void enterClassBodyDeclaration(ClassBodyDeclarationContext ctx) {
				// TODO Auto-generated method stub
				field = ctx;
				super.enterClassBodyDeclaration(ctx);
			}
			@Override
			public void enterFieldDeclaration(antlr_parsers.javaparser.JavaParser.FieldDeclarationContext ctx) {
				// TODO Auto-generated method stub

				List<antlr_parsers.javaparser.JavaParser.VariableDeclaratorContext> list = ctx.variableDeclarators().variableDeclarator();

				//				VariableDeclaratorListContext list = ctx.variableDeclaratorList();
				//				List<VariableDeclaratorContext> ll = list.variableDeclarator();
				if(!list.isEmpty() && field !=null){
					nodeAndroid.put(list.get(0).variableDeclaratorId().getText(), field);
				}

				super.enterFieldDeclaration(ctx);
			}
		};

		/*
		j2swift.Java8BaseListener listener = new j2swift.Java8BaseListener(){
			ParserRuleContext field; 
			@Override
			public void enterFieldDeclaration(j2swift.Java8Parser.FieldDeclarationContext ctx) {
				field = ctx;

				VariableDeclaratorListContext list = ctx.variableDeclaratorList();
				List<VariableDeclaratorContext> ll = list.variableDeclarator();
				if(!ll.isEmpty()){
					nodeAndroid.put(ll.get(0).variableDeclaratorId().getText(), field);
				}

				// TODO Auto-generated method stub
				super.enterFieldDeclaration(ctx);
			}

		};
		 */
		walker.walk(listener, com);

		return nodeAndroid;
	}


	public static <T> Map<String, ParserRuleContext> getMethodDeclAndroid(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeAndroid = new HashMap<String, ParserRuleContext>();

		/*
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new j2swift.Java8Lexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		j2swift.Java8Parser parser = new j2swift.Java8Parser((new CommonTokenStream(lexer1)));
		j2swift.Java8Parser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
		 */


		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		androidcommon= new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
		JavaParser.CompilationUnitContext com = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();

		JavaBaseListener listener = new JavaBaseListener(){
			//			j2swift.Java8Parser.FieldDeclarationContext field; 
			ParserRuleContext cntx;
			@Override
			public void enterClassBodyDeclaration(ClassBodyDeclarationContext ctx) {
				// TODO Auto-generated method stub
				cntx = ctx;
				super.enterClassBodyDeclaration(ctx);
			}
			@Override
			public void enterMethodDeclaration(antlr_parsers.javaparser.JavaParser.MethodDeclarationContext ctx) {
				// TODO Auto-generated method stub
				//				ctx.m
				//				;
				if(cntx!=null)
					nodeAndroid.put(ctx.Identifier().getText(), cntx);
				super.enterMethodDeclaration(ctx);
			}
			/*
			@Override
			public void enterMethodDeclaration(MethodDeclarationContext ctx) {
				// TODO Auto-generated method stub
				nodeAndroid.put(ctx.methodHeader().methodDeclarator().Identifier().getText(), ctx);

				super.enterMethodDeclaration(ctx);
			}
			 */
		};

		walker.walk(listener, com);

		return nodeAndroid;
	}


	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}

		reader.close();
		return  fileData.toString();	
	}



	public static Map<String, ASTNode> getMethodInvocationOfAndroid(String str, String name) {
		ASTParser parser = ASTParser.newParser(AST.JLS8);

		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();


		parser.setResolveBindings(true);
		//		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		parser.setBindingsRecovery(true);

		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);

		//		String unitName = "XAxisRenderer.java";
		//		System.out.println("F	"+file.getName());
		parser.setUnitName(name);

//		String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/testsrc" }; 
		String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/antlr4.java" };
		String[] classpath = {"/Users/kijin/Desktop/android-13.jar","/Users/kijin/Downloads/android.jar","/Users/kijin/Desktop/android-17-api.jar","/Users/kijin/Downloads/android_2.jar"  };

		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
		parser.setSource(str.toCharArray());

		//		parser.
		CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		methodBinding = new HashMap<String, String>();
		cu.accept(new ASTVisitor() {

			
			private Object getLineNumber(QualifiedName node) {
				// TODO Auto-generated method stub
				return null;
			}
//			public boolean visit(org.eclipse.jdt.core.dom.MethodDeclaration node) {};)
			@Override
			public boolean visit(ExpressionMethodReference node) {
				// TODO Auto-generated method stub

				return super.visit(node);
			}
			/*
			@Override
			public void postVisit(ASTNode node) {
				// TODO Auto-generated method stub
				if(node.toString().equals("set.getHighlightCircleStrokeAlpha()")){
					System.err.println("set.getHighlightCircleStrokeAlpha()"+node.getNodeType());
				}
				super.preVisit(node);
			}
			 */
//			visit
			@Override
			public boolean visit(Initializer node) {
				// TODO Auto-generated method stub
//				node.getBody().
				return super.visit(node);
			}
			@Override
			public boolean visit(QualifiedName node) {
				// TODO Auto-generated method stub
				//				System.out.println("nodenode"+node+"  "+node.getName()+"  "+node.getQualifier());
				//				node.getQualifier();
				final int line = cu.getLineNumber(node.getStartPosition());
				ArrayList<String> vars = new ArrayList<String>();
				ArrayList<String> cons = new ArrayList<String>();
				vars.add(node.getQualifier().toString().replaceAll(" ", ""));
			
				cons.add(node.getName().toString());
				
				String mtemplate = "";
//				mtemplate +="."+node.getQualifier().toString().replaceAll(" ", "");
				
				if(node.getQualifier()!=null){
					if(Character.isUpperCase(node.toString().charAt(0))){
						mtemplate = node.toString()+".";
					}else{
						mtemplate = "<arg0>.";
					}
				}
				mtemplate += node.getName().toString();
				//					System.err.println(node.l+":"+node.getLength()+"->MethodInvocation"+mbinding);
				//					System.err.println(line+":"+cu.getColumnNumber(node.getStartPosition())+"->MethodInvocation"+mbinding);
				//					System.err.println(node.getStartPosition()+":"+node.getLength()+"->MethodInvocation"+mbinding);
				//					int line 	= cu.getLineNumber(node.getStartPosition());
				int column 	= cu.getColumnNumber(node.getStartPosition());
				LineCharIndex linechar = new LineCharIndex(line, column, line, column+node.getLength());
				linechar.example = node.toString().replaceAll(" ", "");
				String typess = "";
				if(node.resolveTypeBinding()!=null){
					typess = node.resolveTypeBinding().getName();
				}
				//				LineCharIndex linechar = new LineCharIndex(line, column, line, column+node.getLength());
				unitBindingMap.put(linechar, new BindingUnit(typess, StringUtils.join(vars, ","), StringUtils.join(cons, ","), mtemplate));
//				if(node.getParent().getNodeType()!=ASTNode.IMPORT_DECLARATION)
//				System.err.println(node.toString()+" "+mtemplate);
				return super.visit(node);
			}

			@Override
			public boolean visit(MethodInvocation node) {
				// TODO Auto-generated method stub
				//				MethodInvocation mmmm = (MethodInvocation)androidExpr;
				IMethodBinding m11=null;
				int argnum=0;
				
				//				if(node.resolveMethodBinding()!=null)
				//					mbinding = mmmm.resolveMethodBinding().toString();
				//					m11= node.resolveMethodBinding().getMethodDeclaration();
				//				String vars = "";
				ArrayList<String> vars = new ArrayList<String>();
				ArrayList<String> cons = new ArrayList<String>();
				String mbinding ="", mtemplate="";
				//				vars = vars+" "+node.getName().getIdentifier()+" ";
				//A.foo(..
				//aaa.foo(..
				if(node.getExpression()!=null)
					if(Character.isUpperCase(node.getExpression().toString().charAt(0))){
						cons.add(node.getExpression().toString());
						mtemplate += node.getExpression().toString();
					}else{
						vars.add(node.getExpression().toString());
						mtemplate += "<arg"+argnum+">";
						argnum++;
					}
				if(!node.arguments().isEmpty()){
					for(int i=0; i< node.arguments().size() ; i++){
						vars.add(node.arguments().get(i).toString().replaceAll(" ", "").replaceAll("'", "''"));
					}
				}

				if(node.resolveMethodBinding()!=null){

					m11= node.resolveMethodBinding().getMethodDeclaration();
					mbinding=m11.getDeclaringClass().getName()+".";
					if(mtemplate!="")
						mtemplate = mtemplate+".";
					cons.add(m11.getName());
					mbinding+=m11.getName()+"(";
					mtemplate += m11.getName()+"(";
					ITypeBinding[] pt = m11.getParameterTypes();
//					StringUtils.join(pt, ", ");
					ArrayList<String> types = new ArrayList<String>(), argss = new ArrayList<String>();
					for(ITypeBinding b:pt){
						types.add(b.getQualifiedName());
						argss.add("<arg"+argnum+">");
						argnum++;
					}
					mbinding+= StringUtils.join(types, ", ");
					mbinding+=")";
					mtemplate+=StringUtils.join(argss, ",");
					mtemplate+=")";
					final int line = cu.getLineNumber(node.getStartPosition());

					//					System.err.println(node.l+":"+node.getLength()+"->MethodInvocation"+mbinding);
					//					System.err.println(line+":"+cu.getColumnNumber(node.getStartPosition())+"->MethodInvocation"+mbinding);
					//					System.err.println(node.getStartPosition()+":"+node.getLength()+"->MethodInvocation"+mbinding);
					//					int line 	= cu.getLineNumber(node.getStartPosition());
//					System.out.println(node.toString()+"   "+mtemplate);
					int column 	= cu.getColumnNumber(node.getStartPosition());
					LineCharIndex linechar = new LineCharIndex(line, column, line, column+node.getLength());
					linechar.example = node.toString().replaceAll(" ", "");

					//					System.out.println("typeMethod:"+mbinding+"  "+linechar+" _"+node.toString());
					mbinding = mbinding.replaceAll(" ", "");
					methodBinding.put(line+":"+cu.getColumnNumber(node.getStartPosition()), mbinding);
					//					return mbinding;

					totalBindings.put(linechar, mbinding);
					//					System.out.println("mbinding	"+mbinding+"\n"+StringUtils.join(vars, ",")+" "+StringUtils.join(cons, ","));
					unitBindingMap.put(linechar, new BindingUnit(mbinding, StringUtils.join(vars, ","), StringUtils.join(cons, ","), mtemplate));
				}

				return super.visit(node);
			}

			@Override
			public boolean visit(SimpleName node) {
				// TODO Auto-generated method stub


				ITypeBinding typeB = node.resolveTypeBinding();
				//				node.resolveBinding()
				if(typeB !=null ){
					int line 	= cu.getLineNumber(node.getStartPosition());
					int column 	= cu.getColumnNumber(node.getStartPosition());
					LineCharIndex linechar = new LineCharIndex(line, column, line, column+node.getLength());
					linechar.example = node.toString();
					//					System.out.println("typeBBBB:"+typeB.getQualifiedName()+"  "+linechar+" _"+node.toString());
					totalBindings.put(linechar, typeB.getQualifiedName());
					//					methodBinding.put(line+":"+column, typeB.getQualifiedName());
				}
				return super.visit(node);
			}

		});

		return nodeAndroid;

	}

	public static Map<String, String> mappingVarDec(Map<String, ParserRuleContext> nodeAndroid, Map<String, ParserRuleContext> nodeSwift, boolean toPrint, String pp){

		//		alignment of VariableDeclartion
		Object[] keyA =  nodeAndroid.keySet().toArray();
		Object[]  keyS = nodeSwift.keySet().toArray();

		Map<String, String> hmapping = new HashMap<String, String>();
		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		double[][] costMatrix = new double[keyA.length][keyS.length];
		//		if(keyA.length>=keyS.length){

		for(int i=0; i< keyA.length;i++){
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());
				costMatrix[i][j]=1-score; //same: 1-1
			}
		}

		int[] result =  new int[0];
		if(keyA.length >0 &&  keyS.length>0){
			HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
			result = hung.execute();
		}
		for(int p=0; p<result.length;p++){
			if(result[p]!=-1){
				if(costMatrix[p][result[p]] < 0.25){
					//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
					hmapping.put(keyA[p].toString(), keyS[result[p]].toString());
				}
			}
			//			System.out.println(p+",, "+result[p]);
			//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
		}

		//		System.out.println("hmapping"+hmapping);


		//		}
		return hmapping;
	}

	public static <T> Map<String, ParserRuleContext> getMethodDeclSwift(String path) throws FileNotFoundException, IOException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		SwiftParser.Top_levelContext root1;
		try{

			ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
			Lexer lexer1 = new SwiftLexer((CharStream)stream1);
			SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
			root1 = parser1.top_level();
		} catch (Exception e){
			return nodeSwift;
		}

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_name(SwiftParser.Variable_nameContext ctx) {
				// TODO Auto-generated method stub

				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitVariable_name(ctx);
			}	
		};
		root1.accept(visitor);
		return nodeSwift;
	}

	static ParserRuleContext ssss=null;
	static ParserRuleContext aaaa=null;

	public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub

		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:antlr4-api_0430.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("binding_android_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingAndroid());

		stmt.executeUpdate(SQLiteJDBC.createBindingStat());

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());
		Map<String, String> repos = new HashMap<String, String>();
//				repos.put("testsrc", "mcharts/Charts-master");
		repos.put("/Users/kijin/Documents/workspace_mars/GumTreeTester/antlr4.java", "/Users/kijin/Downloads/antlr4-4.6/runtime/CSharp");

		//		repos.put("Refactoring-StringCalculator-Kata-master", "Refactoring-StringCalculator-Kata-master");
		//		repos.put("cardboard-java-master", "cardboard-swift-master");
//				repos.put("geometry-api-java-master", "geometry-api-swift-master");
		Iterator<String> repo_it = repos.keySet().iterator();

		//		getVarDeclAndroid("testsrc");

//		repos.put("testsrc", "mcharts/Charts-master");

		List<File> afiles = new LinkedList<File>(), sfiles = new LinkedList<File>() ;
//		recording.java.Filewalker.walk_folder("cardboards", afiles);
//		recording.java.Filewalker.walk_folder("/Users/kijin/Downloads/antlr4-4.6/runtime/Swift", sfiles);
		//		fw1.walkforjava("cardboard-java-master", afiles );
		//		fw1.walkforswift("cardboard-swift-master", sfiles );	

//		recording.java.Filewalker.walk_folder("/Users/kijin/Documents/RESEARCH/repos/cardboard-java-master", afiles);
//		recording.java.Filewalker.walk_folder("/Users/kijin/Documents/RESEARCH/repos/cardboard-swift-master", sfiles);

//		recording.java.Filewalker.walk_folder("Refactoring-StringCalculator-Kata-master", afiles);
//		recording.java.Filewalker.walk_folder("Refactoring-StringCalculator-Kata-master", sfiles);

//		recording.java.Filewalker.walk_folder("testsrc", afiles);
//		recording.java.Filewalker.walk_folder("mcharts/Charts-master", sfiles);
		alignment.common.Filewalker.walk_folder("/Users/kijin/Documents/workspace_mars/GumTreeTester/antlr4.java", afiles);
		alignment.common.Filewalker.walk_folder("/Users/kijin/Downloads/antlr4-4.6/runtime/CSharp", sfiles);
		int loca = 0, locs = 0;;
		int swiftSyntax [] = new int [SwiftParser.ruleNames.length];
		int androidSyntax [] = new int [JavaParser.ruleNames.length];
		System.err.println("sfile"+sfiles);
		if(1==1){
			for(File af: afiles){
				System.out.println(af);
				getMethodInvocationOfAndroid(readFileToString(af.getAbsolutePath()), af.getAbsolutePath());


				Iterator<LineCharIndex> iter_binding = unitBindingMap.keySet().iterator();
				while(iter_binding.hasNext()){
					LineCharIndex nxt = iter_binding.next();
					BindingUnit bindu = unitBindingMap.get(nxt);
					String sql_binding = SQLiteJDBC.insertTableBindingAndroid
							(bindu.template, nxt.example.replaceAll("\'", "\''"), bindu.vars.replaceAll("\'", "\''"), 
									bindu.consts.replaceAll("\'", "\''"), af.getName(), nxt.toString().replaceAll("\'", "\''"));
//					System.out.println(bindu.template);
					try{
						stmt.executeUpdate(sql_binding);
						
					}catch (SQLException e){
						System.out.println(sql_binding);
					}
				}

				
			ANTLRInputStream stream11 = new ANTLRInputStream(new FileReader(af.getAbsolutePath()));
			Lexer lexer11 = new JavaLexer((CharStream)stream11);
			androidcommon= new CommonTokenStream(lexer11);
			JavaParser parser11 = new JavaParser(new CommonTokenStream(lexer11));
			JavaParser.CompilationUnitContext com111 = parser11.compilationUnit();

			JavaBaseListener java_listner = new JavaBaseListener(){


				@Override
				public void enterEveryRule(ParserRuleContext ctx) {
					androidSyntax[ctx.getRuleIndex()]++;
					// TODO Auto-generated method stub
					super.enterEveryRule(ctx);
				}

			};


								ParseTreeWalker walker = new ParseTreeWalker();
								walker.walk(java_listner, com111);;
				 
							loca= loca+ (com111.stop.getLine()-com111.start.getLine()+1);

			}
			
		}
		DBTablePrinter.printTable(connection, "binding_android_stat");
		/*
		int total =0;
		for(int jk=0; jk< JavaParser.ruleNames.length;jk++){
			System.out.println(jk+"|"+JavaParser.ruleNames[jk]+"|"+androidSyntax[jk]);
			total = total + androidSyntax[jk];
		}
*/
//		System.out.println("total"+total);

/*
		for(File sf: sfiles){
			try{
			ANTLRInputStream stream22 	= new ANTLRInputStream(new FileReader(sf.getAbsolutePath()));
			Lexer lexer22 				= new SwiftLexer((CharStream)stream22);
			swiftcommon					= new CommonTokenStream(lexer22);

			SwiftParser parser111 = new SwiftParser(new CommonTokenStream(lexer22));

			SwiftParser.Top_levelContext root1111 = parser111.top_level();


			SwiftBaseListener swift_listner = new SwiftBaseListener(){
				@Override
				public void enterEveryRule(ParserRuleContext ctx) {
					swiftSyntax[ctx.getRuleIndex()]++;
					// TODO Auto-generated method stub
					super.enterEveryRule(ctx);
				}
			};
			ParseTreeWalker walker = new ParseTreeWalker();
			walker.walk(swift_listner, root1111);;


			root1111.start.getLine();
			root1111.stop.getLine();
			locs= locs+ (root1111.stop.getLine()-root1111.start.getLine()+1);
			}catch(Exception e){
				System.out.println(sf.getAbsolutePath());
			}
		}

		System.out.println("LOCS	"+loca+"  "+locs);
*/
/*
	total = 0;

	for(int jk=0; jk< SwiftParser.ruleNames.length;jk++){
		System.err.println(jk+"|"+SwiftParser.ruleNames[jk]+"|"+swiftSyntax[jk]);
		total = total + swiftSyntax[jk];
	}

	System.err.println("total"+total);
	*/	 
		while(repo_it.hasNext() && false){
			String and_repo =repo_it.next();
			String swft_repo = repos.get(and_repo);
			System.out.println("repo	"+and_repo+":"+swft_repo);
			alignment.common.Filewalker fw = new alignment.common.Filewalker(and_repo, swft_repo);
			//					recording.java.Filewalker fw = new recording.java.Filewalker("testsrc", "mcharts/Charts-master");
			//		recording.java.Filewalker fw = new recording.java.Filewalker("wordpress/WordPress-Android-develop", "wordpress/WordPress-iOS-develop");
			//		recording.java.Filewalker fw = new recording.java.Filewalker("reactivex/RXJava-1.x", "reactivex/RXSwift-master");
			//		recording.java.Filewalker fw = new recording.java.Filewalker("wire/wire-android-master", "wire/wire-ios-develop");
			//		recording.java.Filewalker fw = new recording.java.Filewalker("wechat/TSWeChat-master", "wechat/wechat-master");
			//				recording.java.Filewalker fw = new recording.java.Filewalker("cardboard-java-master", "cardboard-swift-master");
			//				recording.java.Filewalker fw = new recording.java.Filewalker("roundandsplit-master", "roundandsplit-master");

			//		fw1.walkforjava("roundandsplit-master", afiles );
			//		fw1.walkforswift("roundandsplit-master", sfiles );
			//		recording.java.Filewalker fw = new recording.java.Filewalker("roundandsplit-master", "roundandsplit-master");

			//		11
			//				recording.java.Filewalker fw = new recording.java.Filewalker("migrating-to-swift-from-android-master", "migrating-to-swift-from-android-master");

			//		recording.java.Filewalker fw = new recording.java.Filewalker("reimbursement-platform-for-meals-master", "reimbursement-platform-for-meals-master");
			//		reimbursement-platform-for-meals-master
			//		recording.java.Filewalker fw = new recording.java.Filewalker("SocialQ-IOS-Android-Mobile-App-master", "SocialQ-IOS-Android-Mobile-App-master");

			//		40
			//				recording.java.Filewalker fw = new recording.java.Filewalker("Refactoring-StringCalculator-Kata-master", "Refactoring-StringCalculator-Kata-master");
			//

			//		recording.java.Filewalker fw = new recording.java.Filewalker("my-ctci-master", "my-ctci-master");
			//261
			//		recording.java.Filewalker fw = new recording.java.Filewalker("cardboard-java-master", "cardboard-swift-master");
			//123
			//		recording.java.Filewalker fw = new recording.java.Filewalker("geometry-api-java-master", "geometry-api-swift-master");
			//
			//		recording.java.Filewalker fw = new recording.java.Filewalker("Android-App-master", "iOS-App-master");



			//			recording.java.Filewalker fw = new recording.java.Filewalker(and_repo, swft_repo);
			Map<File, File> classmap = fw.getClassMapping();
			alignment.common.Filewalker.staticPrintMapping(classmap);

			Map<String, ParserRuleContext> varDeclSwift 		= null;
			Map<String, ParserRuleContext> varDeclAndroid 		= null;
			Map<String, ParserRuleContext> methodDecliOS 		= null;
			Map<String, ParserRuleContext> methodDeclAndroid 	= null;


			Vector<String> queries = new Vector<String>();

			//get variable and method declarations class by class
			Iterator<File> a_classes = classmap.keySet().iterator();
			int b =0;

			while(a_classes.hasNext()){ 
				//			Math.max(a, b);

				//						if(b==5) break;
				//anchor has
				//return, remaining has syntax
				//for a in aaa {}

				File aclass_path = a_classes.next();
				File sclass_path = classmap.get(aclass_path);
				//				androidpath = aclass_path.getAbsolutePath();
				//				swiftpath = aclass_path.getAbsolutePath();
//								if(aclass_path.getName().equals("ViewPortHandler.java")){

									if(1==1){





				//				SwiftParser.ruleNames.length;


//				if(11==111){
					//					System.err.println(sclass_path.getAbsolutePath());

					try{
						totalBindings.clear();
						getMethodInvocationOfAndroid(readFileToString(aclass_path.getAbsolutePath()), aclass_path.getAbsolutePath());


						Iterator<LineCharIndex> iter_binding = unitBindingMap.keySet().iterator();
						while(iter_binding.hasNext()){
							LineCharIndex nxt = iter_binding.next();
							BindingUnit bindu = unitBindingMap.get(nxt);
							String sql_binding = SQLiteJDBC.insertTableBindingAndroid(bindu.mbinding, nxt.example.replaceAll("\'", "\''"), bindu.vars, bindu.consts, aclass_path.getName(), nxt.toString());
							try{
								stmt.executeUpdate(sql_binding);
							}catch (SQLException e){
								System.out.println(sql_binding);
							}
						}

						/*
					Iterator<LineCharIndex> iter_binding = totalBindings.keySet().iterator();
					while(iter_binding.hasNext()){
						LineCharIndex nxt = iter_binding.next();
						String sql_binding = SQLiteJDBC.insertTableBindingAndroid(totalBindings.get(nxt), nxt.example.replaceAll("\'", "\''"), "", "", aclass_path.getName(), nxt.toString());

						//						System.out.println("sql_binding"+sql_binding);
						try{
						stmt.executeUpdate(sql_binding);
						}catch (SQLException e){
							System.out.println(sql_binding);
						}
					}
						 */


						//			get variable declarations of aligned class by brief visit of ASTs
						//					varDeclSwift   			= getVarDeclSwift(sclass_path.getAbsolutePath());
						//					varDeclAndroid 			= getVarDeclAndroid(aclass_path.getAbsolutePath());
						//					methodDecliOS 			= getMethodDeclSwift(sclass_path.getAbsolutePath());
						//					methodDeclAndroid 		= getMethodDeclAndroid(aclass_path.getAbsolutePath());


						ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(aclass_path.getAbsolutePath()));
						Lexer lexer1 = new JavaLexer((CharStream)stream1);
						androidcommon= new CommonTokenStream(lexer1);
						JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
						JavaParser.CompilationUnitContext com = parser.compilationUnit();
						//					ParseTreeWalker walker = new ParseTreeWalker();



						ANTLRInputStream stream2 	= new ANTLRInputStream(new FileReader(sclass_path.getAbsolutePath()));
						Lexer lexer2 				= new SwiftLexer((CharStream)stream2);
						swiftcommon					= new CommonTokenStream(lexer2);

						SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer2));



						SwiftParser.Top_levelContext root1 = parser1.top_level();
						root1.start.getLine();
						root1.stop.getLine();
						//					ParserRuleContext  sss=null;

						SwiftBaseListener swift_listner = new SwiftBaseListener(){
							@Override
							public void enterEveryRule(ParserRuleContext ctx) {
								// TODO Auto-generated method stub
								super.enterEveryRule(ctx);
							}

							@Override
							public void enterClass_declaration(Class_declarationContext ctx) {
								// TODO Auto-generated method stub
								ssss = ctx;
								System.out.println("enterClass_declaration");
								super.enterClass_declaration(ctx);

							}
						};

						JavaBaseListener java_listner = new JavaBaseListener(){
							@Override
							public void enterEveryRule(ParserRuleContext ctx) {
								// TODO Auto-generated method stub
								super.enterEveryRule(ctx);
							}
							@Override
							public void enterTypeDeclaration(TypeDeclarationContext ctx) {
								// TODO Auto-generated method stub
								aaaa = ctx;
								System.out.println("enterTypeDeclaration");
								super.enterTypeDeclaration(ctx);
							}
						};

						ParseTreeWalker walker_swift= new ParseTreeWalker(), walker_android = new ParseTreeWalker();

						walker_swift.walk(swift_listner, root1);
						walker_android.walk(java_listner, com);


						if(aaaa!=null && ssss!=null){
//													StructureMappingAntlr4_for_stmt structure12 = new StructureMappingAntlr4_for_stmt(aaaa, ssss);
//													structure12.afilename = aclass_path.getName();
//													structure12.sfilename = sclass_path.getName();
//													structure12.waking();
							//						structure12.compute();
							//						structure12.doupdate();
							//						System.out.println("structure12.sfilename");
						}
					}catch (Exception e){
						System.out.println(sclass_path);
					}
				}


				if(11==11){
					//					System.err.println(sclass_path.getAbsolutePath());

					getMethodInvocationOfAndroid(readFileToString(aclass_path.getAbsolutePath()), aclass_path.getAbsolutePath());


					//			get variable declarations of aligned class by brief visit of ASTs
					varDeclSwift   			= getVarDeclSwift(sclass_path.getAbsolutePath());
					varDeclAndroid 			= getVarDeclAndroid(aclass_path.getAbsolutePath());
					methodDecliOS 			= getMethodDeclSwift(sclass_path.getAbsolutePath());
					methodDeclAndroid 		= getMethodDeclAndroid(aclass_path.getAbsolutePath());


					ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(aclass_path.getAbsolutePath()));
					Lexer lexer1 = new JavaLexer((CharStream)stream1);
					androidcommon= new CommonTokenStream(lexer1);
					JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));
					JavaParser.CompilationUnitContext com = parser.compilationUnit();
					//					ParseTreeWalker walker = new ParseTreeWalker();



					ANTLRInputStream stream2 	= new ANTLRInputStream(new FileReader(sclass_path.getAbsolutePath()));
					Lexer lexer2 				= new SwiftLexer((CharStream)stream2);
					swiftcommon							= new CommonTokenStream(lexer2);


					/*
					SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer2));




					SwiftParser.Top_levelContext root1 = parser1.top_level();

					//					ParserRuleContext  sss=null;

					SwiftBaseListener swift_listner = new SwiftBaseListener(){
						@Override
						public void enterClass_declaration(Class_declarationContext ctx) {
							// TODO Auto-generated method stub
							ssss = ctx;
							System.out.println("enterClass_declaration");
							super.enterClass_declaration(ctx);

						}
					};

					JavaBaseListener java_listner = new JavaBaseListener(){
						@Override
						public void enterTypeDeclaration(TypeDeclarationContext ctx) {
							// TODO Auto-generated method stub
							aaaa = ctx;
							System.out.println("enterTypeDeclaration");
							super.enterTypeDeclaration(ctx);
						}
					};

					ParseTreeWalker walker_swift= new ParseTreeWalker(), walker_android = new ParseTreeWalker();
					 */
					//					walker_swift.walk(swift_listner, root1);
					//					walker_android.walk(java_listner, com);

					/*
					if(aaaa!=null && ssss!=null){
						StructureMappingAntlr4_for_stmt structure12 = new StructureMappingAntlr4_for_stmt(aaaa, ssss);
						structure12.afilename = aclass_path.getName();
						structure12.sfilename = sclass_path.getName();
						structure12.waking();
						structure12.compute();
						structure12.doupdate();
						//						System.out.println("structure12.sfilename");
					}
					 */
					//					sclass_path.getAbsolutePath()
					Map<String, String> varmap = mappingVarDec(varDeclAndroid, varDeclSwift, true, "");
					Map<String, String> methodmap = mappingVarDec(methodDeclAndroid, methodDecliOS, true, "");
					//					System.err.println(aclass_path.getName()+":"+sclass_path.getName());
					System.out.println("varmap"+varmap);
					System.out.println("methodmap"+methodmap);
					Iterator<String> iterator_varDec = varmap.keySet().iterator();

					while(iterator_varDec.hasNext()){
						String key_a=iterator_varDec.next();
						//					varDeclAndroid.get(key_a);
						//					varDeclSwift.get(varmap.get(key_a));
//						StructureMappingAntlr4_for_examples structure11 = new StructureMappingAntlr4_for_examples(varDeclAndroid.get(key_a), varDeclSwift.get(varmap.get(key_a)));
						StructureMappingAntlr4_for_examples structure11 = new StructureMappingAntlr4_for_examples(varDeclAndroid.get(key_a), varDeclSwift.get(varmap.get(key_a)));
						
						structure11.afilename = aclass_path.getName();
						structure11.sfilename = sclass_path.getName();
						structure11.swiftCommonStream = swiftcommon;
						structure11.androidCommonStream = androidcommon;
						//						structure11.waking();
						structure11.compute(stmt);
						//						java.sql.Statement sql_stmt = connection.createStatement();
						//						connection.setAutoCommit(true);
						//			sql_stmt.executeUpdate(e12);	
						//						stmt.executeUpdate(query);
						//						System.out.println(query);
						//						DBTablePrinter.printTable(connection, "grammar_android");
						//						DBTablePrinter.printTable(connection, "grammar_swift");
					}//iteration of aligned variable declarations


					Iterator<String> iterator_methodDec = methodmap.keySet().iterator();
					while(iterator_methodDec.hasNext()){
						String key_a=iterator_methodDec.next();
						//					varDeclAndroid.get(key_a);
						//					varDeclSwift.get(varmap.get(key_a));
						StructureMappingAntlr4_for_examples structure = new StructureMappingAntlr4_for_examples(methodDeclAndroid.get(key_a), methodDecliOS.get(methodmap.get(key_a)));
						structure.afilename = aclass_path.getName();
						structure.sfilename = sclass_path.getName();
						structure.swiftCommonStream = swiftcommon;
						structure.androidCommonStream = androidcommon;
						//						structure.waking();

						structure.compute(stmt);

						//						java.sql.Statement sql_stmt = connection.createStatement();
						//						connection.setAutoCommit(true);
						//			sql_stmt.executeUpdate(e12);	
						//						stmt.executeUpdate(query);
						//						System.out.println(query);
						//						structure.doupdate();
						//						DBTablePrinter.printTable(connection, "grammar_android");
						//						DBTablePrinter.printTable(connection, "grammar_swift");

						//					b++;
					}//iteration of aligned variable declarations



					//				StructureMappingAntlr4 structure1 = new StructureMappingAntlr4();


					DBTablePrinter.printTable(connection, "grammar_android");
					DBTablePrinter.printTable(connection, "grammar_swift");
					DBTablePrinter.printTable(connection, "grammar_android_stat");
					DBTablePrinter.printTable(connection, "grammar_swift_stat");

				}
				System.out.println(queries.size());
				//				StructureMappingAntlr4 structure = new StructureMappingAntlr4();


				System.out.println(queries.size());
			}
			//			DBTablePrinter.printTable(connection, "grammar_android_stat");
			//			DBTablePrinter.printTable(connection, "grammar_swift_stat");
			//			DBTablePrinter.printTable(connection, "grammar_binding_stat");
		

			//		}
			//				System.err.println(varDeclSwift);
			//				System.out.println(varDeclAndroid);
			//				System.err.println(methodDecliOS);
			//				System.err.println(methodDeclAndroid);
			connection.close();
		}
					

	}
}
