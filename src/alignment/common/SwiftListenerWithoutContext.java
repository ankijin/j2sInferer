package alignment.common;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.antlr.runtime.RecognitionException;
import org.antlr.v4.parse.ScopeParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.tool.Attribute;
import org.antlr.v4.tool.Grammar;
import org.apache.commons.lang3.StringUtils;

import alignment.ruleinfer.StructureMappingAntlr4_for_stmt;
import antlr_parsers.common_old.CommonBaseListener;
import antlr_parsers.common_old.CommonLexer;
import antlr_parsers.common_old.CommonParser;
import antlr_parsers.common_old.CommonParser.AssignContext;
import antlr_parsers.common_old.CommonParser.Ast_nodeContext;
import antlr_parsers.common_old.CommonParser.ExprContext;
import antlr_parsers.pcommon.PCommonBaseListener;
import antlr_parsers.pcommon.PCommonLexer;
import antlr_parsers.pcommon.PCommonListener;
import antlr_parsers.pcommon.PCommonParser;
import antlr_parsers.pcommon.PCommonParser.ExpressionContext;
import common.CommonParser.AssignmentContext;
import common.CommonParser.EnclosedContext;
import common.CommonParser.Keyword_withContext;
import common.CommonParser.ParseContext;
import recording.BaseSwiftRecorder;
import recording.java.SwiftBaseListener;

public class SwiftListenerWithoutContext extends SwiftBaseListener{
	public ParserRuleContext swift;
	Interval swift_interval;
	public List<String> 		ssnodes 							= new LinkedList<String>();
	public List<OpProperty> 	op_s_nodes 							= new LinkedList<OpProperty>();
	public List<OpProperty>  	s_nodes 							= new LinkedList<OpProperty>();
	public List<OpProperty> 	common_nodes 						= new LinkedList<OpProperty>();
	public List<OpProperty> 	blacklist 							= new LinkedList<OpProperty>();
	public List<CommonOperators> 	constantlist 					= new LinkedList<CommonOperators>();
	public List<OpProperty> 	constantlist2 					= new LinkedList<OpProperty>();
	
	public List<CommonOperators> highestop =null;
	public CharStream inputstream = null;
	public List<OpProperty> op_s_precoding = new LinkedList<OpProperty>();
	String left="", right="",rright="";
	//	String left_interval=;
	public String type="none";

	public Map<Interval, String> termsMap = new LinkedHashMap<Interval, String>();
	public Map<Interval, String> termsMapOnly = new LinkedHashMap<Interval, String>();
	public List<CommonOperators> termsMapOnlys = new LinkedList<CommonOperators>();
	Interval left_interval= new Interval(0,0);

	Interval right_interval;
	public String opsequence="";
	Map<Interval, String> tokens;
	public List<String> punc_swift = new LinkedList<String>();

	public LinkedHashMap<String, Attribute> scope_list;
	public LinkedHashMap<String, List<Interval>> scope_constraint;

	public LineCharIndex leftIndex =null, blockIndex = null, rightIndex = null;



	public SwiftListenerWithoutContext(ParserRuleContext swift){

		scope_list 					= new  	LinkedHashMap<String, Attribute>();
		scope_constraint 			= new LinkedHashMap<String, List<Interval>>();

		s_nodes 					= new LinkedList<OpProperty>();
		opsequence="";
		op_s_nodes 					= new LinkedList<OpProperty>();
		op_s_precoding = new LinkedList<OpProperty>();
		tokens = new HashMap<Interval, String>();
		this.swift = swift;
		//		inputstream = swift.start.getInputStream();
		//		swift_interval = swift.getSourceInterval();
		int az = swift.start.getStartIndex();
		int bz = swift.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = swift.start.getInputStream().getText(interval);
		//		List<TerminalNode> 	tokens 					= new LinkedList<TerminalNode>();




		ANTLRInputStream stream2 = new ANTLRInputStream(context);


		Lexer plexer = new PCommonLexer((CharStream)stream2);
		antlr_parsers.pcommon.PCommonParser pparser	= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(plexer));

		antlr_parsers.pcommon.PCommonParser.ParseContext proot = pparser.parse();
		ParseTreeWalker pwalker = new ParseTreeWalker();
		//		int rootd = proot.depth();
		//		Interval interval1 = new Interval(proot.start.getStartIndex(),proot.stop.getStopIndex());
		//		String context1 = proot.start.getInputStream().getText(interval1);
		//		System.out.println("proot	"+rootd+" "+context1);





		PCommonBaseListener plistener = new PCommonBaseListener(){
			int depth = 1000;
			@Override
			public void enterAssignment(PCommonParser.AssignmentContext ctx) {


				if(ctx.depth() < depth){
					type = "AssignmentContext";
					//					System.err.println("enterA");
					depth = ctx.depth();
					left = ctx.leftAssign().getText();

					rright = ctx.rightAssign().getText().replace(";", "").replace(" ", "").replace("\n", "");
					//					System.out.println("rright"+right);

					leftIndex = new LineCharIndex(
							ctx.leftAssign().start.getLine(), 
							ctx.leftAssign().start.getCharPositionInLine(), 
							ctx.leftAssign().stop.getLine(), 
							ctx.leftAssign().stop.getStopIndex());

					rightIndex = new LineCharIndex(
							ctx.rightAssign().start.getLine(), 
							ctx.rightAssign().start.getCharPositionInLine(), 
							ctx.rightAssign().stop.getLine(), 
							ctx.rightAssign().stop.getStopIndex());
//					System.out.println("rightIndexrightIndex"+rightIndex+" "+ctx.rightAssign().getText());
					super.enterAssignment(ctx);
				}


				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 mean one lower level of paranthesis

					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
					String context = proot.start.getInputStream().getText(interval);
					OpProperty op = new OpProperty(null, context,"AssignmentContext",ctx.depth());
					op.getxt = ctx.getText();
					op.lineindex = LineCharIndex.contextToLineIndex(ctx);
					s_nodes.add(op);
				}
				super.enterAssignment(ctx);
			}

			@Override
			public void enterEnclosed(PCommonParser.EnclosedContext ctx) {

				if(ctx.depth() <= depth && ctx.outer()!=null && ctx.inner()!=null){
					type = "EnclosedContext";
//					System.err.println("EnclosedContext\n"+ctx.getText());
					depth = ctx.depth();
					left = ctx.outer().getText();
					right = ctx.inner().getText();
					left_interval =  ctx.outer().getSourceInterval();
					//					right = ctx.inner().getText();
					//					System.out.println(left_interval+"rright"+right);
					//					super.enterAssignment(ctx);

					if(ctx.outer().stop!=null && ctx.outer().start!=null)
						//						leftIndex = new LineCharIndex(
						//								ctx.outer().start.getLine(), 
						//								ctx.outer().start.getCharPositionInLine(), 
						//								ctx.outer().stop.getLine(), 
						//								ctx.outer().stop.getCharPositionInLine()+1);

						leftIndex = new LineCharIndex(
								ctx.outer().start.getLine(), 
								ctx.outer().start.getCharPositionInLine(), 
								ctx.outer().stop.getLine(), 
								ctx.outer().stop.getStopIndex());

					//					ctx.start.getLine();
					//					ctx.start.getCharPositionInLine();
					//					ctx.stop.getLine();
					//					ctx.stop.getCharPositionInLine();

					if(ctx.inner().stop!=null && ctx.inner().start!=null){
						blockIndex = new LineCharIndex(
								ctx.inner().start.getLine(), 
								ctx.inner().start.getCharPositionInLine(), 
								ctx.inner().stop.getLine(), 
								ctx.inner().stop.getStopIndex());
					}

				}
				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 mean one lower level of paranthesis
					//				if(ctx.depth()==6 &&!ctx.outer().getText().equals("")){ // 6 mean one lower level of paranthesis

					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
					String context = proot.start.getInputStream().getText(interval);

					//					s_nodes.add(new OpProperty(ctx, context,"EnclosedContext",ctx.depth()));

					OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());
					//					op.getxt = ctx.getText();
					op.getxt = ctx.getText();
					op.lineindex = LineCharIndex.contextToLineIndex(ctx);
					s_nodes.add(op);
					//					System.out.println("EnclosedContext\n:"+ctx.inner().getText());

				}
				super.enterEnclosed(ctx);
			}

			@Override
			public void enterExpression(ExpressionContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 mean one lower level of paranthesis

					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
					String context = proot.start.getInputStream().getText(interval);

					//					s_nodes.add(new OpProperty(ctx, context,"ExpressionContext",ctx.depth()));
					OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
					op.getxt = ctx.getText();
					op.lineindex = LineCharIndex.contextToLineIndex(ctx);
					s_nodes.add(op);
					//					leftIndex = null;
				}

				super.enterExpression(ctx);
			}
		};

		//		walker.walk(listener, root);

		pwalker.walk(plistener, proot);

		//		s_nodes = common_nodes;
		//		System.out.println("common_nodes"+common_nodes);
	}

	/**
	 * only identifiers	
	 */
	public Interval last = null;
	@Override
	public void visitTerminal(TerminalNode node) {
		// TODO Auto-generated method stub
		//		node.getSourceInterval();
		boolean disj = true;
		//		Interval sourceInterval = node.getSourceInterval();
		//		last = node.getSourceInterval();
		LineCharIndex lineindex=new LineCharIndex(node.getSymbol().getLine(), node.getSymbol().getCharPositionInLine(),
				node.getSymbol().getLine(),node.getSymbol().getStopIndex());
		//		System.out.println("TerminalNode"+node.getText()+" "+lineindex);
		//		Entry<Interval,String> last1 = (Entry<Interval, String>) termsMap.entrySet().toArray()[termsMap.size() -1];
		//		if()
		Iterator<Interval> common = termsMapOnly.keySet().iterator();
		if(!common.hasNext()){
			if(last!=null && last.disjoint(node.getSourceInterval())){
				termsMap.put(node.getSourceInterval(), node.getText());	
				last = node.getSourceInterval();
			}
			if(last==null){
				termsMap.put(node.getSourceInterval(), node.getText());	
				last = node.getSourceInterval();
			}
		}else{
			common = termsMapOnly.keySet().iterator();
			while(common.hasNext()){
				boolean updated = false;
				Interval iii = common.next();
				if(iii.disjoint(node.getSourceInterval())){
					if(last!=null && last.disjoint(node.getSourceInterval())){
						termsMap.put(node.getSourceInterval(), node.getText());	
						last = node.getSourceInterval();
					}
				}else {
					termsMap.remove(node.getSourceInterval());
					//					if(last!=null && last.disjoint(iii)){
					termsMap.put(iii, termsMapOnly.get(iii));
					last= iii;
					//						updated = true;
					//					}
				}
			}
		}
		//		termsMap.put(node.getSourceInterval(), node.getText());
		//		System.out.println("node.getText()"+node.getText());


		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) 
				&& StructureMappingAntlr4_for_stmt.common_operators_list.contains(node.getText())){
			//		if(StructureMappingAntlr4.common_operators_list.contains(node.getText())){
			Iterator<Interval> aaa = termsMapOnly.keySet().iterator();
			boolean disjj = true;
			while(aaa.hasNext()){
				if(!aaa.next().disjoint(node.getSourceInterval())){
					disjj = false;
				}
			}

			if(disjj){

				ParserRuleContext parentnode = (ParserRuleContext)node.getParent();
				/*
				if(node.getText().equals(".")){
					termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()));
				}
				else if(node.getText().equals(")") || node.getText().equals("(")){
					termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()-1));
				}
				else{
					termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()-1));
				}
				 */
				//				ParserRuleContext parentnode = (ParserRuleContext)node.getParent();
				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()-1));
				//				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
				termsMapOnly.put(node.getSourceInterval(), node.getText());
			}
		}

		else if (!type.equals("EnclosedContext")&& StructureMappingAntlr4_for_stmt.common_operators_list.contains(node.getText())){



			Iterator<Interval> aaa = termsMapOnly.keySet().iterator();
			boolean disjj = true;

			while(aaa.hasNext()){
				if(!aaa.next().disjoint(node.getSourceInterval())){
					disjj = false;
				}
			}

			if(disjj){
				ParserRuleContext parentnode = (ParserRuleContext)node.getParent();
				/*
				if(node.getText().equals(".")){
					termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()+1));
				}
				else if(node.getText().equals(")") || node.getText().equals("(")){
					termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()+1));
				}

				else{
					termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()+1));
				}
				 */
				//				ParserRuleContext parentnode = (ParserRuleContext)node.getParent();
				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval(), parentnode.depth()-1));
				termsMapOnly.put(node.getSourceInterval(), node.getText());
			}

		}
		/*
		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex)
				)
		{

			Attribute decl = scope_list.get(node.getText());
			if(decl!=null){
				List<Interval> list = scope_constraint.get(decl.toString());
				if(list!=null){
					list.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), list);

				}else{
					List<Interval> ll = new LinkedList<Interval>();
					ll.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), ll);

				}
			}	

		} 



		 */

		for(CommonOperators t:constantlist){
			//			System.out.println("blist"+t);
			//			System.err.println(ctx.getSourceInterval());
			if(!t.opi.disjoint(node.getSourceInterval())) {
				//				System.out.println(ctx.getText()+"	android contains	"+t.strASTNode);
				return;

			}
		}

		for(OpProperty t:constantlist2){
			//			System.out.println("blist"+t);
			//			System.err.println(ctx.getSourceInterval());
			if(!t.interval.disjoint(node.getSourceInterval())) {
				//				System.out.println(ctx.getText()+"	android contains	"+t.strASTNode);
				return;

			}
		}
		
		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex))
		{

			Attribute decl = scope_list.get(node.getText());
			if(decl!=null){
				List<Interval> list = scope_constraint.get(decl.toString());
				if(list!=null){
					list.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), list);

				}else{
					List<Interval> ll = new LinkedList<Interval>();
					ll.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), ll);

				}
				//				System.err.println("ssscope_constraint	"+scope_constraint);
			}			
			//		if(type.equals("EnclosedContext")&&!left_interval.disjoint(node.getSourceInterval())){
			//			System.err.println(node.getSourceInterval()+"node.getText()"+node.getText());

			//			else if(StructureMappingAntlr4.swift_punctuations_list.contains(node.getText())){
			//				termsMapOnlys.add(new CommonOperators("PUNC", node.getSourceInterval()));
			//			}
			//			else{
			//				termsMapOnlys.add(new CommonOperators("OTHERS", node.getSourceInterval()));
			//			}
		}

		/*
		if(!type.equals("EnclosedContext") && StructureMappingAntlr4.common_operators_list.contains(node.getText())){
			termsMapOnly.put(node.getSourceInterval(), node.getText());
			termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
		}
		else if(type.equals("EnclosedContext") && blockIndex!=null){
			if(lineindex.isDisjoint(blockIndex) && StructureMappingAntlr4.common_operators_list.contains(node.getText())){
				termsMapOnly.put(node.getSourceInterval(), node.getText());
				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
			}
		}
		 */
		//		node.getSourceInterval();
		Interval it = null;
		for(OpProperty t:op_s_nodes){
			if(t.node instanceof ParserRuleContext) 
				it = ((ParserRuleContext)t.node).getSourceInterval();
			else if(t.node instanceof TerminalNode) it = ((TerminalNode)t.node).getSourceInterval();


			if(!it.disjoint(node.getSourceInterval())) return;
			if(t.strASTNode.equals(node.getText()) && !t.interval.equals(node.getSourceInterval()) ) {
				//				System.out.println("same_scope");
				return;
			}


			if(t.strASTNode.equals(node.getText()) && t.interval.disjoint(node.getSourceInterval())) {
				//				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex)){
				//				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) && scope_list.get(node.getText())!=null){
				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex))
				{

					Attribute decl = scope_list.get(node.getText());
					if(decl!=null){
						t.same_scope.put(node.getSourceInterval(), new OpProperty(node, node.getText(), t.property));
						System.out.println("aaaadding scoping"+node.getText());
					}
					return;
				}
			}
			//			if(!StructureMappingAntlr4.swift_punctuations_list.contains(node.getText())){
			//				if(t.getxt.equals(node.getText())) System.out.println("aaaaaaa	"+node.getText());
			//			}
			//			if(!it.disjoint(node.getSourceInterval())) return;
			disj = it.disjoint(node.getSourceInterval()) & disj;
			//			System.out.println("swift contains"+t.strASTNode);
			//			t.node
			//			if(!t.strASTNode.equals(node.getText()) && !t.strASTNode.contains(node.getText())) {
			//				System.out.println("not overlapped term	"+node.getText());
			//			}
			//			if(t.interval.equals(node.getSourceInterval()))
			//				return;
		}


		//		if(!disj) return;

		if(!StructureMappingAntlr4_for_stmt.swift_keywords_list.contains(node.getText()) && !StructureMappingAntlr4_for_stmt.swift_punctuations_list.contains(node.getText())){
			//		if(disj){
			//			System.out.println("not overlapped term	"+node.getText());
			//			op_a_nodes.add(new OpProperty(node, node.getText(),""));

			if(type.equals("EnclosedContext")){
				//								System.out.println("left.contains"+leftIndex+" "+lineindex+"  "+node.getText());

				if(!leftIndex.isDisjoint(lineindex)){
					op_s_nodes.add(new OpProperty(node, node.getText(),"enclosed_outer"));
				}
				//				else if(right.contains(node.getText())){
				//					op_s_nodes.add(new OpProperty(node, node.getText(),"enclosed_inner"));
				//				}
				else{
					//					if(!node.getText().equals("<EOF>"))
					op_s_nodes.add(new OpProperty(node, node.getText(),"none"));
				}

			}

			//			if(!leftIndex.isDisjoint(lineindex)){
			//				System.out.println("AssignmentContext"+context);
			//				op_s_nodes.add(new OpProperty(ctx, context,"assign_left",ctx.depth()));

			//			}else if(!rightIndex.isDisjoint(lineindex)){

			else if(type.equals("AssignmentContext")){
				//				System.out.println("right"+right+"  "+ctx.getText());
//				System.out.println("cctright"+rightIndex+"  "+node.getText());
				if(!leftIndex.isDisjoint(lineindex)){
					//					System.out.println("AssignmentContext"+context);
					op_s_nodes.add(new OpProperty(node, node.getText(),"assign_left"));
				}else if(!rightIndex.isDisjoint(lineindex)){
					op_s_nodes.add(new OpProperty(node, node.getText(),"assign_right"));
				}
				else{
					op_s_nodes.add(new OpProperty(node, node.getText(),"none"));
				}
			}
			else{
				if(!node.getText().equals("<EOF>"))
					op_s_nodes.add(new OpProperty(node, node.getText(),"none"));
			}


		}

		super.visitTerminal(node);
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {

		// TODO Auto-generated method stub

		if(!constantlist.isEmpty())
			System.out.println("constantlist		"+constantlist);
		//		get orginal text
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context="";
		try {
			context = ctx.start.getInputStream().getText(interval);
		} catch(Exception e){
		}



		//		System.out.println("ctx.getText()	"+ctx.getText());

		/** 
		 * mapping statements, to be changed to LineCharIndex(line position)
		 */
		//if equal to .. chilren , should add line #

		LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
				ctx.getStop().getLine(),ctx.getStop().getStopIndex());
		for(OpProperty t:s_nodes){
			t.lineindex.equals(lineindex);
			if(t.getxt.equals(ctx.getText())){
				t.node = ctx;
				t.interval = ctx.getSourceInterval();
			}
		}


		//collect common operators
		//in case block type, only header parts are collected
		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) 
				&& StructureMappingAntlr4_for_stmt.common_operators_list.contains(ctx.getText())){

			Iterator<Interval> aaa = termsMapOnly.keySet().iterator();
			boolean disj = true;

			while(aaa.hasNext()){
				if(!aaa.next().disjoint(ctx.getSourceInterval())){
					disj = false;
				}
			}


			if(disj){
				//				termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval()));
				//				termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
				/*
				if(ctx.getText().equals(".")){
					termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
				} 

				else if(ctx.getText().equals(")") || ctx.getText().equals("(")){
					termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
				}

				else{
					termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
				}
				 */
				termsMapOnly.put(ctx.getSourceInterval(), ctx.getText());
				termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
			}
		}

		else if (!type.equals("EnclosedContext")){

			if(StructureMappingAntlr4_for_stmt.common_operators_list.contains(ctx.getText())){
				//				if(ctx.getText().equals("<")){
				//					System.out.println("   <<<<<<");
				//				}
				Iterator<Interval> aaa = termsMapOnly.keySet().iterator();
				boolean disj = true;
				while(aaa.hasNext()){
					if(!aaa.next().disjoint(ctx.getSourceInterval())){
						disj = false;
					}
				}
				//								if(!termsMapOnly.containsKey(ctx.getSourceInterval())){
				if(disj){
					//					termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval()));
					/*

					if(ctx.getText().equals(".")){
						termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
					} 
					else if(ctx.getText().equals(")") || ctx.getText().equals("(")){
						termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
					}
					else{
						termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
					}
					 */
					termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval(), ctx.depth()));
					termsMapOnly.put(ctx.getSourceInterval(), ctx.getText());
				}
			}
		}


		/*
		LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);
		for(OpProperty t:s_nodes){
			if(t.lineindex.equals(lineIndex)){
				t.node = ctx;	
				t.type = ctx.getRuleIndex();
			}	

		}
		 */



		for(CommonOperators t:constantlist){
			//			System.out.println("blist"+t);
			//			System.err.println(ctx.getSourceInterval());
			if(!t.opi.disjoint(ctx.getSourceInterval())) {
				//				System.out.println(ctx.getText()+"	android contains	"+t.strASTNode);
				return;

			}
		}



		//obtain declaration
		Grammar dummy = null;
		try {
			dummy = new Grammar("grammar T; a:'a';");
		} catch (RecognitionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LinkedHashMap<String, Attribute> attributes =  new LinkedHashMap<String, Attribute>();
		try{
			attributes = ScopeParser.parseTypedArgList(null, context, dummy).attributes;
		} catch(Exception e){}
		for (String arg : attributes.keySet()) {
			Attribute attr = attributes.get(arg);

			if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex)){
				if(attr!=null&& attr.name!=null &&attr.type!=null  && StringUtils.isAlphanumeric(attr.name) && StringUtils.isAlphanumeric(attr.type)){
					boolean isUpperCase = Character.isUpperCase(attr.type.charAt(0));

					if(isUpperCase || attr.type.equals("var")){
						scope_list.put(attr.name, attr);
					}
				}
			}
		}


		//		if(ctx.getText().equals(swift.getText())) {return;}

		//goto next candidates
		for(OpProperty t:blacklist){
			if(t.interval.equals(ctx.getSourceInterval())) {
				return;
			}
		}

		if(highestop!=null){
			for(CommonOperators highestop:highestop){
				if(!highestop.interval.disjoint(ctx.getSourceInterval())) {
					return;
				}
			}
			//			if(!highestop.interval.disjoint(ctx.getSourceInterval())) {
			//			return;
		}

		for(OpProperty t:op_s_nodes){

			//			disj = disj&t.interval.disjoint(ctx.getSourceInterval());
			//			System.out.println("swift contains"+t.strASTNode);
			//			if(t.strASTNode.contains(context)) return;

			if(t.interval.equals(interval)) {
				return;
			} 

			if(!t.interval.disjoint(ctx.getSourceInterval())) {
				return;
			}


			if(t.strASTNode.equals(context) && t.interval.disjoint(ctx.getSourceInterval())) {
				//				System.out.println("scope:"+context+" "+interval);



				//				LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getLine()+ctx.getStart().getCharPositionInLine(),
				//						ctx.getStop().getLine(),ctx.getStop().getLine()+ctx.getStop().getCharPositionInLine()-1);

				//				LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
				//						ctx.getStop().getLine(),ctx.getStop().getStopIndex());

				//				System.out.println("::::"+context+"  "+type+" "+leftIndex+" "+lineindex);
				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) && scope_list.get(context)!=null){

					t.same_scope.put(ctx.getSourceInterval(), new OpProperty(ctx, context, t.property));
					//										System.out.println("swft adding scoping"+ctx.getSourceInterval()+":::"+ctx.getText()+"  >>"+t);
					return;
					//					}
					//				}
				}
				//				t.same_scope.add(e);
				//			if(t.strASTNode.equals(context)|| t.node==null) {
				//				return;
			}
			//			idx++;
		}

		if(context.replaceAll(" ", "").startsWith("{") && context.replaceAll(" ", "").endsWith("}")){
			op_s_nodes.add(new OpProperty(ctx, context, "enclosed{}", ctx.depth()));
			return;
		}


		//keywords or swift punc should not .. 
		for(String key:StructureMappingAntlr4_for_stmt.swift_keywords_list){
			if(context.equals(key)) return;
		}
		for(String key:StructureMappingAntlr4_for_stmt.swift_punctuations_list){
			if(context.equals(key)) return;
		}




		if(type.equals("EnclosedContext")){
			if(left.contains(ctx.getText())){
				op_s_nodes.add(new OpProperty(ctx, context,"enclosed_outer",ctx.depth()));
			}else if(right.contains(ctx.getText())){
				//				op_s_nodes.add(new OpProperty(ctx, context,"enclosed_inner",ctx.depth()));
			}
			else{
				//				op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			}
		}

		else if(type.equals("AssignmentContext")){
			//			System.out.println("iicctright"+rightIndex+"  "+ctx.getText()+" "+lineindex);
			if(!leftIndex.isDisjoint(lineindex)){
				//				System.out.println("AssignmentContext"+context);
				op_s_nodes.add(new OpProperty(ctx, context,"assign_left",ctx.depth()));

			}else if(!rightIndex.isDisjoint(lineindex)){
				op_s_nodes.add(new OpProperty(ctx, context,"assign_right",ctx.depth()));
			}
			//			else if(swift.getText().equals(ctx.getText()) && !op_s_nodes.contains(context)){

			//			else if(!op_s_nodes.contains(context)){
			//					op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			//			}
			//			else{
			//				op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			//			}
		}
		else{
			op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
		}


		/*
		else{
			//			if(!op_s_nodes.contains(context))
			if(!context.equals("<EOF>"))
				op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
		}
		 */
		//				}
		super.enterEveryRule(ctx);
	}
}
