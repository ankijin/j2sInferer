package alignment.common;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.TerminalNode;
//import org.eclipse.jdt.core.dom.ASTNode;

//import StructureMapping.OpProperty;

public class OpProperty{
	public String property="none";
	public Object node;
	public String strASTNode="";
	public String getxt="";
	public OpProperty parent;
	public int height;
	public double sim;
	public Interval interval;
	public LineCharIndex lineindex;
	public String binding="";
//	public List<OpProperty> same_scope= new LinkedList<OpProperty>();
//	public Map<Interval, String> same_scope = new HashMap<Interval, String>();
	public Map<Interval, OpProperty> same_scope = new HashMap<Interval, OpProperty>();
	public List<String> punclist= new LinkedList<String>();
	public int type;
	public String orginTxt ="";
	public String whyBlacklist=null;
	public String toCompareStr ="";
	public OpProperty(Object ASTNode){
		this.node = ASTNode;
		
	}

	public OpProperty(Object ASTNode, String strASTNode){
		this.node = ASTNode;
//		strASTNode.con
		if(node instanceof ParserRuleContext){
			interval = ((ParserRuleContext)ASTNode).getSourceInterval();
			type = ((ParserRuleContext)ASTNode).getRuleIndex();
			orginTxt = ((ParserRuleContext)ASTNode).getText();
		}
		else if(node instanceof TerminalNode){
			interval = ((TerminalNode)ASTNode).getSourceInterval();
			orginTxt = ((TerminalNode)ASTNode).getText();
//			type = ((TerminalNode)ASTNode).
		}
		this.strASTNode = strASTNode;
		
		same_scope.clear();
		same_scope = new HashMap<Interval, OpProperty>();
		
	}

	public OpProperty(Object ASTNode,  String strASTNode, String property){
		this.node = ASTNode;
		if(node instanceof ParserRuleContext){
			
			interval = ((ParserRuleContext)ASTNode).getSourceInterval();
			type = ((ParserRuleContext)ASTNode).getRuleIndex();
			orginTxt = ((ParserRuleContext)ASTNode).getText();
//			int az = 	((ParserRuleContext)ASTNode).start.getStartIndex();
//			int bz = 	((ParserRuleContext)ASTNode).stop.getStopIndex();
//			interval = new Interval(az,bz);
		
		}
		else if(node instanceof TerminalNode){
			interval = ((TerminalNode)ASTNode).getSourceInterval();
			orginTxt = ((TerminalNode)ASTNode).getText();
//			int az = 	interval.a;
//			int bz = 	interval.b;
//			interval = new Interval(az,bz);
		}
		else{
			interval = new Interval(0, 0);
		}
		this.strASTNode = strASTNode;
		this.property = property;
		same_scope.clear();
		same_scope = new HashMap<Interval, OpProperty>();
	}
	public OpProperty(Object ASTNode,  String strASTNode, String property, int height){
		this.node = ASTNode;
		if(node instanceof ParserRuleContext){
			interval = ((ParserRuleContext)ASTNode).getSourceInterval();
			type = ((ParserRuleContext)ASTNode).getRuleIndex();
			orginTxt = ((ParserRuleContext)ASTNode).getText();
//			int az = 	((ParserRuleContext)ASTNode).start.getStartIndex();
//			int bz = 	((ParserRuleContext)ASTNode).stop.getStopIndex();
//			interval = new Interval(az,bz);
		}
		else if(node instanceof TerminalNode){
			interval = ((TerminalNode)ASTNode).getSourceInterval();
			orginTxt = ((TerminalNode)ASTNode).getText();
		}
		else{//null node
			
//			System.out.println("#####"+strASTNode+ASTNode.getClass());
//			interval = new Interval(0, 0);
		}
		this.strASTNode = strASTNode;
		this.property = property;
		this.height = height;
		same_scope.clear();
		same_scope = new HashMap<Interval, OpProperty>();
		
	}
	
	public OpProperty(Object ASTNode,  String strASTNode, String property, int height, double sim){
		this.node = ASTNode;
		if(node instanceof ParserRuleContext){
			interval = ((ParserRuleContext)ASTNode).getSourceInterval();
			type = ((ParserRuleContext)ASTNode).getRuleIndex();
			orginTxt = ((ParserRuleContext)ASTNode).getText();
		}
		else if(node instanceof TerminalNode){
			interval = ((TerminalNode)ASTNode).getSourceInterval();
			orginTxt = ((TerminalNode)ASTNode).getText();
		}
		else{
//			interval = new Interval(0, 0);
		}
		this.strASTNode = strASTNode;
		this.property = property;
		this.height = height;
		this.sim = sim;
		same_scope.clear();
		same_scope = new HashMap<Interval, OpProperty>();
	}
	
	@Override
	public String toString() { 
		/*
		// TODO Auto-generated method stub
		if(node instanceof ASTNode){
			ASTNode n = (ASTNode) node;
			return this.strASTNode+" "+":"+property+" "+height;
//			n.toString()
		}else if(node instanceof ParserRuleContext){
//			ParserRuleContext n = (ParserRuleContext) node;
			return this.strASTNode+":"+property+" "+height;
		}
		else if(node instanceof TerminalNode){
//			ParserRuleContext n = (ParserRuleContext) node;
			return this.strASTNode+":"+property+" "+height;
		}
		return super.toString(); 
		*/
//		if(!same_scope.isEmpty()) return "\n"+interval+" "+this.property+":"+strASTNode+"  @"+same_scope;
//		else
//		return "\n"+interval+" "+this.property+":"+strASTNode;
		if(property.equals("enclosed{}")) return interval+":"+"{...}";
//		else if(!same_scope.isEmpty()) return height+"-> "+interval+":"+strASTNode+"@"+same_scope;
//		else return height+"-> "+property+interval+":"+strASTNode;
		else if(!same_scope.isEmpty()) return interval+":"+strASTNode+"@"+same_scope;
//		/else if(whyBlacklist!=null) return strASTNode+"\n?"+whyBlacklist;
		else return property+":"+strASTNode+"  "+interval+"\n";
	
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return this.interval.equals(((OpProperty)obj).interval);
//		return this.strASTNode.equals(((OpProperty)obj).strASTNode);
	}
}
