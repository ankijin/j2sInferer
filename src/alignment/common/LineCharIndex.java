package alignment.common;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;

public class LineCharIndex {
	public int startLine=0, startCharP=0, endLine=0, endCharP=0;
	public String example;
	public LineCharIndex(){

	}
	public LineCharIndex(int sl, int sp, int el, int ep){
		startLine 	= sl;
		startCharP 	= sp;
		endLine 	= el;
		endCharP 	= ep;
	}
	
	/**
	 * 1:2, 2:5
	 * 1:5, 2:4
	 * @param unit
	 * @return
	 */
	public boolean isDisjoint(LineCharIndex unit){
		if(startLine <= unit.startLine && startCharP <=unit.startCharP && endLine >= unit.endLine && endCharP >=unit.endCharP) return false;
		else return true;
	}
	
	//1:10
	//2:10
	
	public boolean isIncluded(LineCharIndex unit){
		if((startLine <= unit.startLine &&  endLine == unit.endLine && endCharP == unit.endCharP) || (startCharP == unit.startCharP &&  startLine== unit.startLine && endLine <=unit.endLine)) return true;
//		if((startLine <= unit.startLine &&  endLine == unit.endLine) || (startLine== unit.startLine && endLine <=unit.endLine)) return true;
		
		else return false;
	}

	public boolean properlyContains(LineCharIndex unit) {
//		return other.a >= this.a && other.b <= this.b;
		return false;
	}
//		if((startLine <= unit.startLine &&  endLine == unit.endLine && endCharP == unit.endCharP) || (startCharP == unit.startCharP &&  startLine== unit.startLine && endLine <=unit.endLine)) return true;
//		if((startLine <= unit.startLine &&  endLine == unit.endLine) || (startLine== unit.startLine && endLine <=unit.endLine)) return true;
		
//		else return false;
//	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		if(example !=null){
		return startLine+":"+startCharP+"~"+endLine+":"+endCharP+" "+example;
		}
		return startLine+":"+startCharP+"~"+endLine+":"+endCharP;
	}
	@Override
	public boolean equals(Object obj) {
		LineCharIndex unit=(LineCharIndex)obj;
		// TODO Auto-generated method stub
		return startLine == unit.startLine && startCharP == unit.startCharP && endLine==unit.endLine && endCharP== unit.endCharP;
	}
	


	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return startLine+endCharP;
	}
	public static void main(String args []){
		//		LineCharIndex a = new LineCharIndex(1,2, 1,2);
		//		LineCharIndex b = new LineCharIndex(1,5, 2, 4);
		//		LineCharIndex c = new LineCharIndex(1,4, 1, 6);
		//		System.out.println(a.isDisjoint(c));


		String string = "abc\ndef\nghi\nABCDEFG\n";
		int c = 1;
		int line = 1, col = 1;
		int i=2;
		while (c <= i) 
		{
			if (string.charAt(c) == '\n')
			{
				++line;
				col = 1;
			} else {
				++col;
			}
			c++;
		}
		System.out.println(line+" "+col);
		System.out.println(string.substring(0, 5)+":  "+stringToLineCharIndex(string, 0,5));
	}	

	public static LineCharIndex contextToLineIndex(Object ctx){
		
		if(ctx instanceof ParserRuleContext){
			ParserRuleContext ctx1 = (ParserRuleContext)ctx;
		return new LineCharIndex(
				ctx1.getStart().getLine(), 
				ctx1.getStart().getCharPositionInLine(), 
				ctx1.getStop().getLine(), 
				ctx1.getStop().getStopIndex());
		
//		LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
//				ctx.getStop().getLine(),ctx.getStop().getStopIndex());
		}else{
			return new LineCharIndex(0,0,0,0);
		}
	}
	

	public static LineCharIndex stringToLineCharIndex(String string, int begin, int end){

		int c = 0;
		int start_line = 1, end_line=1, start_col = 0, end_col = 0;
		int i=2;
		while (c <= begin) 
		{
			if (string.charAt(c) == '\n')
			{
				++start_line;
				start_col = 0;
			} else {
				++start_col;
			}
			c++;
		}
		c=0;
		while (c < end) 
		{
			if (string.charAt(c) == '\n')
			{
				++end_line;
				end_col = 0;
			} else {
				++end_col;
			}
			c++;
		}


		
		return new LineCharIndex(start_line, start_col-1, end_line, end_col-1);
	}

}
