package alignment.common;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.lang3.ArrayUtils;
import org.sqlite.Function;
import org.stringtemplate.v4.ST;


//import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
//import com.googlecode.concurrenttrees.solver.LCSubstringSolver;

import alignment.ruleinfer.IterativeMapping;
import alignment.ruleinfer.StructureMappingAntlr4_for_stmt;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.pcommon.SimpleFormat;
import j2swift.J2Swift;
import migration.MigrationNode;
import migration.RewriterJava;
import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import net.vivin.GenericTreeTraversalOrderEnum;
import rulesapp.TemplateEDB;
import sqldb.SQLiteJDBC;
import templatechecker.DSLErrorListener;
//import templatechecker.TemplateCParser;
import templates.MatchingTemplate2;

/**
 * This class is used for migrating the test data with template table
 * There are cross-validation steps
 * For now, only line-based decoding are available. For instance the block would be empty such as if(aaa){int a;} if aaa {} however making trees is not difficult
 * @author kijin
 *
 */
public class TranslationExample {
	public static ParserRuleContext typeJ=null;
	public static int idd;

	public static final String db_name = "tables/antlr4_extra.db";

	/**
	 * used for migration
	 * @param atemplate : template
	 * @param example : testing syntax
	 * @param parent
	 * @param testingset
	 * @param tfile
	 * @param efile
	 * @return
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws SQLException
	 */

	public GenericTreeNode<MigrationNode> iterativeTemplates(String atemplate, ParserRuleContext example, GenericTreeNode<MigrationNode> parent, String [] testingset, String tfile, String efile) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException{

		RewriterJava rw = new RewriterJava();
		ParseTreeWalker walker_swift = new ParseTreeWalker();
		walker_swift.walk(rw, example);				
		int az = example.start.getStartIndex();
		int bz = example.stop.getStopIndex();
		Interval interval = new Interval(az, bz);
		String context = example.start.getInputStream().getText(interval);

		Map<String, OpProperty> subMatching 	= MatchingTemplate2.getAttributesMapLineIndex(atemplate, context ,rw.maps);
		if(subMatching.size()>0){
						System.out.println("interative	"+atemplate+"   "+subMatching);
		}
		java.util.Iterator<String> subm 		= subMatching.keySet().iterator();

		if(subMatching.size()==0) return null;
		else{
			//System.err.println(atemplate);
		}
		//in case only contexts exist
		while(subm.hasNext()){
			String arg 		= subm.next();
			OpProperty sub 	= subMatching.get(arg);
			//			System.out.println("SUBBB"+sub+"  "+sub.type);
			arg = arg.replaceAll("<", "").replaceAll(">", "");
			//			getConstant(String aConst, String [] id, String templatedb);
			//			String mm= getConstant(sub.strASTNode, testingset, "charts_template0427.db");
			//same things?
			ResultSet aaa =null;
			String atemp="", sstemp="";
			String fatemp="", fsstemp="";
			String m=null;
			boolean tt = true;
			if(sub.interval.length()>1){
				//				if(sub.interval.length()>1){
				aaa = getTemplateResultSet(sub.strASTNode, sub.type, testingset, tfile, efile);
				//				}
				int j =0;
				while(aaa.next() && tt && j==0){
					//				num++;
					//				int templateT = 0;
					atemp 	= aaa.getString("template");
					sstemp 	= aaa.getString("example");
					//					String rform = aaa.getString("rform");
					//					int			c 	= Integer.parseInt(aaa.getString("c"));
					//				stype 	= Integer.parseInt(example_resultSet.getString("PARENT_TYPE"));
					//				idd 	= Integer.parseInt(aaa.getString("ID"));
					ParserRuleContext subTree=getContext(sub.strASTNode, ((ParserRuleContext)sub.node).getRuleIndex());				
					//				System.out.println("sub.strASTNode	"+sub.strASTNode+" "+subTree.getRuleIndex());
					RewriterJava rwsub = new RewriterJava();
					ParseTreeWalker swalker_swift = new ParseTreeWalker();
					swalker_swift.walk(rwsub, subTree);
					Map<String, OpProperty> subMatching1 	= MatchingTemplate2.getAttributesMapLineIndex(atemp, sub.strASTNode ,rwsub.maps);
					//	
					//					System.out.println("@@@@	"+sub.strASTNode+"   "+c+"   "+atemp+"|   "+sstemp+" "+rform+"  "+subMatching1.size());

					if(subMatching1.size()>0){


						m="hasTemplate";
						//												tt= false;
						fatemp = atemp.replaceAll("\\s+\\<\\s+", "\\\\<").replaceAll("\\s+\\>\\s+", "\\\\>");
						fsstemp = sstemp.replaceAll("\\s+\\<\\s+", "\\\\<").replaceAll("\\s+\\>\\s+", "\\\\>");
						//			parent.getData().targetTemp;

						//just space problems..
						if((atemp.equals("<arg0> ( )") || atemp.equals("<arg0> . <arg1>") || atemp.equals("<arg0> ( <arg1> )")))
						{

						} else{
							j = 1;
							//							System.err.println("		subMatching1"+subMatching1+" "+atemp+"  "+sstemp);
							//	System.err.println("		subMatching1	"+" "+fatemp+"  "+fsstemp);
						}

					}


				}
			}

			GenericTreeNode<MigrationNode> node = new GenericTreeNode<MigrationNode>(new MigrationNode());
			node.getData().srcText 	= sub.strASTNode;
			node.getData().argID 	= arg;
			String stemp			= sub.strASTNode;

			//no constant
			if(m!=null){
				//				stemp = "<arg0><<arg1>>";
				stemp = fatemp;
				try{
					//					node.getData().srcTemp = new ST(stemp.replaceAll("\\<=", "\\<="));
					//					node.getData().targetTemp = new ST(sstemp);
					node.getData().srcTemp 		= new ST(fatemp.replaceAll("\\<=", "\\\\<=").replaceAll("\\s+\\<\\s+", "\\\\<"));
					node.getData().targetTemp 	= new ST(fsstemp.replaceAll("\\<=", "\\\\<=").replaceAll("\\s+\\<\\s+", "\\\\<"));
					//					System.out.println("fatemp"+fatemp+" "+fsstemp);
				} catch (org.stringtemplate.v4.compiler.STException e){
					System.out.println("error T"+stemp+" "+sstemp);
				}
			}
			//terminate in this node
			else{
				//node.getData().targetTemp = null;
				node.getData().targetTemp =null;
				String mm= getConstant(sub.strASTNode, testingset, tfile);
				if(mm==null){
					node.getData().targetText = sub.strASTNode;
				}else{
					node.getData().targetText = mm;
				}
			}

			//			parent.getData().targetTemp;



			parent.addChild(node);
			if(sub.node instanceof ParserRuleContext && m!=null && !stemp.equals("<arg0>") && !atemplate.equals(stemp)){		
				//resetting offset 
				ParserRuleContext subTree=getContext(sub.strASTNode, ((ParserRuleContext)sub.node).getRuleIndex());				
				RewriterJava rwsub = new RewriterJava();
				ParseTreeWalker swalker_swift = new ParseTreeWalker();
				swalker_swift.walk(rwsub, subTree);
				iterativeTemplates(stemp, subTree, node, testingset, tfile, efile);
			}

		}
		return parent;
	}

	public static ResultSet getTemplateResultSet(String example, int type_id, String [] testingset, String tfile, String efile) throws SQLException{
		//		String [] afile= {};
		ResultSet result=queryTemplateByExample(example, testingset,type_id,tfile,efile);
		return result;
	}


	public static ResultSet queryExampleResultRandom(String tableName, int num) throws SQLException{
		String query = "select TEMPLATE, AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC , ID from "+tableName+ " ORDER BY RANDOM() LIMIT "+num +";";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}


	public static String query(String tableName, int android, int swift){
		String query = "select template, example, ast_type, count from "+tableName+ " where parent_type="+swift +" order by count;";
		return query;
	}


	public static ResultSet queryExampleResult(String tableName, int android, int swift) throws SQLException{
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where ast_type="+android +" and parent_type="+swift +" order by count;";
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleResultbyID(String tableName, int id) throws SQLException{
		String query = "select * from "+tableName+ " where id="+id+ " order by count;";
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}


	public static ResultSet queryExampleResultbyTestingSet(String tableName, String testing) throws SQLException{
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in (\'"+testing+ "\') and ast_type in (19)"+ ";";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);

		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyTestingSet(String tableName, String[] afiles, int [] atype, int [] stype, String dbname) throws SQLException{

		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");
		int [] testingid={
				70
		};
		int [] testingid1={
				70
		};

		String idset =  Arrays.toString(testingid1).replace("[","").replace("]","");
		//		idset = "";
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and ast_type in (19) and parent_type in (97)"+ ";";
		String query ="";
		if(atype.length==0){

			query = "select template, example, parent_type, ast_type, count, ID, afilename, sfilename from "+tableName+ " where afilename in ("+test+ ") and  ast_type not in (0,3,19, 68) and parent_type not in (130, 133, 97, 80, 143);";

			//					query = "select template, example, parent_type, ast_type, count, ID, afilename, sfilename from "+tableName+ " where afilename in ("+test+ ") and  ast_type not in (0) and parent_type not in () and id  in (806, 807, 808, 811, 812, 813, 851);";

			//			query = "select template, example, parent_type, ast_type, count, ID, afilename, sfilename from "+tableName+ " where afilename in ("+test+ ") and  ast_type in (70) and parent_type not in (-1)"+ ";";

		}else{
			query = "select template, example, parent_type, ast_type, count, ID, afilename, sfilename from "+tableName+ " where afilename in ("+test+ ") and  ast_type in ("+Arrays.toString(atype).replace("[","").replace("]","")+") and parent_type in ("+Arrays.toString(stype).replace("[","").replace("]","")+")"+ ";";
		}
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and  ast_type in (3) and parent_type in (133,130)"+ ";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+dbname);
		//		connection2.prepareStatement( "ATTACH DATABASE 'test_record.db' AS template").execute();

		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyTestingSet(String tableName, String[] afiles, int sample_num, int [] atype, int [] stype) throws SQLException{

		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and ast_type in (19) and parent_type in (97)"+ ";";
		int num= (int) Math.round(sample_num*1);
		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and  ast_type in ("+Arrays.toString(atype).replace("[","").replace("]","")+") and parent_type in ("+Arrays.toString(stype).replace("[","").replace("]","")+")"+ " ORDER BY RANDOM() LIMIT "+num+";";

		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where afilename in ("+test+ ") and  ast_type in (3) and parent_type in (133,130)"+ ";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		//		connection2.prepareStatement( "ATTACH DATABASE 'test_record.db' AS template").execute();

		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}


	public static ResultSet queryTemplateByType(String dbName, int type) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from grammar_android_stat where ast_type="+type+ " order by count;";
		String query = "select template, example, parent_type, ast_type, count, ID from grammar_android_stat  where id=1;";

		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}


	/**
	 * return templates
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet queryTemplateByExampleID(int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";

		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record0415.db");
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT xgroup by ast_type, parent_type order by c

		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat "
				+ "\'ViewPortHandler.java\') and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 10;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("qquery\n"+query);


		//		connection2.setAutoCommit(true);




		Statement stmt2 = connection2.createStatement();
		stmt2.execute("ATTACH 'db_result/total_examples0404.db' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//				System.out.println(value);
				if (value == null){
					value = "";
				}
				Pattern pattern=Pattern.compile(expression, Pattern.DOTALL);
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});


		Function.create(connection2, "SIMPLE", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				result(SimpleFormat.getSimpleFormat(expression));
			}
		});

		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryTemplateByExample(String example, String [] afiles, int atype, String templatedb, String exampledb) throws SQLException{
		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;
		}

		String test=Arrays.toString(newa).replace("[","").replace("]","");
		test ="";
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+templatedb);
		Statement stmt2 = connection2.createStatement();
		String query = "SELECT * , sum(count) as c FROM grammar_android_stat where SIMPLE(\'"+example+"\') REGEXP rform and afilename not in ("+ test +")  and ast_type="+atype+" and template not in ('<arg0>', '<arg0> . <arg1>', '<arg0> ( <arg1> )','<arg0> . <arg1> ( )','<arg0> ( )', '<arg1> . <arg0>') and template is not '<arg0>' group by example, template order by c DESC LIMIT 20;";
		//		System.out.println(query);
		//		String query = "SELECT * , count(*) as c FROM grammar_android_stat where \'"+example+"\' REGEXP rform and afilename not in ("+ test +") group by example, template order by c DESC LIMIT 1;";
		//		String query = "SELECT * , count(*) as c FROM tem.grammar_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" group by example, template order by c DESC LIMIT 5;";
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM sum_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" group by example, template order by c DESC LIMIT 5;";
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where sfilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 5;";

		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where afilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC;";
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform FROM grammar_android_stat where afilename not in ("+ test +") and SIMPLE((SELECT template FROM exams.grammar_swift_stat where id="+id+")) REGEXP rform group by template, example;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";



		//		connection2.setAutoCommit(true);
		stmt2.execute("ATTACH '"+exampledb+"' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//				System.out.println(value);
				if (value == null){
					value = "";
				}
				Pattern pattern=Pattern.compile(expression, Pattern.DOTALL);
				//				System.out.println("pattern"+pattern+"   value  "+value+"  "+pattern.matcher(value).find());
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});


		Function.create(connection2, "SIMPLE", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				//								String value = value_text(1);
				//								System.out.println("simple\n"+SimpleFormat.getSimpleFormat(expression));
				//				if (value == null){
				//					value = "";
				//				}
				//				Pattern pattern=Pattern.compile(expression);
				result(SimpleFormat.getSimpleFormat(expression));
			}
		});
		ResultSet resultSet = stmt2.executeQuery(query);
		//		System.out.println("qquery	"+query);
		return resultSet;
	}
	/*
	queryTemplateByExampleID_union(int id, String [] afiles, int atype, String exampledb, String [] templatedb) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		StringUtils.join(null, " ");
		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");
		//		test ="";
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+templatedb);
		Statement stmt2 = connection2.createStatement();
		//		stmt2.execute("ATTACH '"+"test_record0424_antlr4.db"+"' as tem;");
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT xgroup by ast_type, parent_type order by c

		String query = "SELECT * , sum(count) as c FROM grammar_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" group by example, template order by c DESC LIMIT 5;";
		//		String query = "SELECT * , count(*) as c FROM tem.grammar_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" group by example, template order by c DESC LIMIT 5;";


		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM sum_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" group by example, template order by c DESC LIMIT 5;";
		//				String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where sfilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 5;";

		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where afilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC;";
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform FROM grammar_android_stat where afilename not in ("+ test +") and SIMPLE((SELECT template FROM exams.grammar_swift_stat where id="+id+")) REGEXP rform group by template, example;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";
		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";



		//		connection2.setAutoCommit(true);





		stmt2.execute("ATTACH '"+exampledb+"' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//								System.out.println(expression);
				if (value == null){
					value = "";
				}
				Pattern pattern=Pattern.compile(expression, Pattern.DOTALL);

				//				String testing="public void groupBars(float fromX, float groupSpace, float barSpace) {...}";
				//				testing = testing.replaceAll("\n", "").replaceAll(" ", "");
				//				if(testing.equals(value.replaceAll("\n", "").replaceAll(" ", ""))){
				//					System.out.println("pattern"+pattern+"   value  "+value+"  "+pattern.matcher(value).find());
				//				}
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});


		Function.create(connection2, "SIMPLE", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				//				String value = value_text(1);
				//				System.out.println("simple\n"+SimpleFormat.getSimpleFormat(expression));
				//				if (value == null){
				//					value = "";
				//				}
				//				Pattern pattern=Pattern.compile(expression);
				result(SimpleFormat.getSimpleFormat(expression));
			}
		});

		ResultSet resultSet = null;
		try{
			resultSet = stmt2.executeQuery(query);
		} catch ( java.sql.SQLException e){
			System.out.println("qquery	"+query);
		}
		return resultSet;
	}
	 */
	public static ResultSet queryTemplateByExampleID(int id, String [] afiles, int atype, String exampledb, String templatedb) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		StringUtils.join(null, " ");
		String [] newa = new String[afiles.length];
		int i =0;
		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}
		String test=Arrays.toString(newa).replace("[","").replace("]","");
		test ="";
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+templatedb);
		//		System.out.println("templatedb	"+templatedb);
		Statement stmt2 = connection2.createStatement();
		//		stmt2.execute("ATTACH '"+"test_record0424_antlr4.db"+"' as tem;");
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT xgroup by ast_type, parent_type order by c

		String query = "SELECT * , sum(count) as c FROM grammar_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" and template not in ('<arg0>', '<arg0> . <arg1>', '<arg0> ( <arg1> )', '<arg0> . <arg1> ( )','<arg0> ( )','<arg1> . <arg0>') group by example, template order by c DESC LIMIT 20;";
		//		System.out.println("query    "+query+" \n"+SimpleFormat.getSimpleFormat(template));
		//		String query = "SELECT * , count(*) as c FROM tem.grammar_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" group by example, template order by c DESC LIMIT 5;";


		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM sum_android_stat where (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform and afilename not in ("+ test +") and ast_type ="+atype+" group by example, template order by c DESC LIMIT 5;";
		//				String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where sfilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 5;";

		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where afilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC;";
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform FROM grammar_android_stat where afilename not in ("+ test +") and SIMPLE((SELECT template FROM exams.grammar_swift_stat where id="+id+")) REGEXP rform group by template, example;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";



		//		connection2.setAutoCommit(true);





		stmt2.execute("ATTACH '"+exampledb+"' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//				System.out.println("REGEXP			"+expression+"     "+value);
				if (value == null){
					value = "";
				}

				//				Pattern pattern=Pattern.compile("\\G"+expression+"$", Pattern.DOTALL);
				Pattern pattern=Pattern.compile(expression, Pattern.DOTALL);
				//				String testing="public void groupBars(float fromX, float groupSpace, float barSpace) {...}";
				//				testing = testing.replaceAll("\n", "").replaceAll(" ", "");
				//				if(testing.equals(value.replaceAll("\n", "").replaceAll(" ", ""))){
				//					if(pattern.matcher(value).find())
				//													System.out.println("pattern"+pattern+"   value  "+value+"  "+pattern.matcher(value).find());
				//				}
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});


		Function.create(connection2, "SIMPLE", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				//				String value = value_text(1);
				//				System.out.println("simple\n"+SimpleFormat.getSimpleFormat(expression));
				//				if (value == null){
				//					value = "";
				//				}
				//				Pattern pattern=Pattern.compile(expression);
				result(SimpleFormat.getSimpleFormat(expression));
			}
		});

		ResultSet resultSet = null;
		try{
			resultSet = stmt2.executeQuery(query);
		} catch ( java.sql.SQLException e){
			System.out.println("qquery	"+query);
		}
		return resultSet;
	}

	public static ResultSet queryTemplateByExample(String example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";

		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"test_record0415.db");
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT x

		String query = "SELECT * FROM grammar_android_stat where '"+example+"' REGEXP rform order by count;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat where id="+id+") REGEXP rform order by count DESC;";

		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println("query	"+query);


		//		connection2.setAutoCommit(true);




		Statement stmt2 = connection2.createStatement();
		//		stmt2.execute("ATTACH 'db_result/total_examples0404.db' as exams;");
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";

		Function.create(connection2, "REGEXP", new Function() {
			@Override
			protected void xFunc() throws SQLException {
				String expression = value_text(0);
				String value = value_text(1);
				//				System.out.println(value);
				if (value == null){
					value = "";
				}
				Pattern pattern=Pattern.compile(expression, Pattern.COMMENTS);
				result(pattern.matcher(value).find() ? 1 : 0);
			}
		});



		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	/**
	 * @param dbname
	 * @param afiles for cross-validation
	 * @return
	 * @throws SQLException
	 */
	public static ResultSet queryRegex(String dbname, String [] afiles) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String [] newa = new String[afiles.length];
		int i =0;

		for(String a:afiles){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}

		String test=Arrays.toString(newa).replace("[","").replace("]","");
		//				test="";
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+dbname);//group by template, example
		//		connection2.prepareStatement( "attach 'total_example.db' as tempp").execute();BY RANDOM() LIMIT x
		//		String query = "SELECT template, example, id, ast_type, parent_type, count, rform , count(*) as c FROM grammar_android_stat where afilename not in ("+ test +") and (SELECT SIMPLE(template) FROM exams.grammar_swift_stat where id="+id+") REGEXP rform group by template, example order by c DESC LIMIT 5;";

		//		String query = "SELECT id, count, rform, ast_type, parent_type, template, example, sum(count) as c FROM grammar_android_stat where afilename not in ("+test+") group by template, example order by c;";
		String query = "SELECT id, count, rform, ast_type, parent_type, template, example, sum(count) as c FROM grammar_android_stat where afilename not in ("+test+") group by template, example order by c;";
		//		String query = "SELECT * FROM grammar_android_stat where (SELECT template FROM exams.grammar_swift_stat BY RANDOM() LIMIT ) REGEXP rform order by count DESC LIMIT 1;";

		//		String query = "select ast_type, parent_type, count(*) as c from grammar_swift_stat group by ast_type, parent_type order by c DESC;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";


		Statement stmt2 = connection2.createStatement();
		ResultSet resultSet = stmt2.executeQuery(query);
		//		System.out.println("query	"+resultSet.getFetchSize()+"   "+query);

		return resultSet;
	}


	public static ResultSet queryExampleResultbyExample(String tableName, String a_example, String s_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' and example=\'"+s_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		//		System.out.println(query);
		//		test_record11.db
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}



	/*
	public static ResultSet queryExampleResultbyExample(String tableName, String example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+"train_record.db");
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}
	 */

	public static ParserRuleContext getContext(String text, int id) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(text));
		CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
		//				List<ParserRuleContext> plist = new LinkedList<ParserRuleContext>();

		JavaParser parserj = new JavaParser(javaCommonStream);
		//			Lock lock = new ReentrantLock();


		Class<? extends Parser> parserClass = null;
		ClassLoader cl 	= Thread.currentThread().getContextClassLoader();
		parserClass 	= cl.loadClass("antlr_parsers.javaparser.JavaParser").asSubclass(Parser.class);


		Method startRule = parserClass.getMethod(JavaParser.ruleNames[id]);
		ParserRuleContext tree = (ParserRuleContext)startRule.invoke(parserj, (Object[])null);
		//		RewriterJava rw = new RewriterJava();
		//		ParseTreeWalker walker_swift = new ParseTreeWalker();
		//		walker_swift.walk(rw, tree);
		//		ParserRuleContext ctxx = tree;

		return tree;
	}


	/*
	public static String getLongestCommonSubstring(Collection<String> strings) {
		LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());

		for (String s: strings) {
			solver.add(s);
		}
		return solver.getLongestCommonSubstring().toString();
	}
	 */
	public static Statement initConnection(String dbname) throws ClassNotFoundException, SQLException{
		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:"+dbname);
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingStat());

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());

		return stmt;
	}




	public static void main(String[] args) throws SQLException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {
		// TODO Auto-generated method stub
		//query examples
		//generates template to except for bindings
		//when there are no binding .. just syntax change
		//Goals: testing migration
		/*
		String[][] testingsett_chart ={
				{
					"GLStateBackup.java",
					"Distortion.java",
					"HeadTracker.java",
					"So3Util.java",
					"ScreenParams.java",	
					"Vector3d.java",
					"Eye.java",
					"DistortionRenderer.java",
					"Viewport.java",
					"OrientationEKF.java",
					"HeadMountedDisplay.java",
					"Matrix3x3d.java",
					"CardboardDeviceParams.java",
					"HeadTransform.java",
					"FieldOfView.java"
				}
		};
		 */
		//
		/*
		String[][] testingsett_chart ={
				{"BarLineChartBase.java","IMarker.java",	"IAxisValueFormatter.java"}, //fold 1
				{"ChartData.java","IHighlighter.java"}, //fold 2
				{"ViewPortHandler.java","PieChartRenderer.java","IScatterDataSet.java"}, //fold 3
				{"LineChartRenderer.java","PieRadarChartBase.java"}, //fold 4
				{"PieChart.java","CombinedData.java","HorizontalBarChartRenderer.java",			"IFillFormatter.java"	
				}, //fold 5
				{"LegendRenderer.java","AxisBase.java","CandleStickChartRenderer.java","ILineRadarDataSet.java"}, //fold 6
				{"YAxisRendererRadarChart.java","BarChartRenderer.java","XAxisRendererHorizontalBarChart.java","HorizontalBarChart.java"}, //fold 7
				{"ChartHighlighter.java","RadarChartRenderer.java","YAxisRendererHorizontalBarChart.java","CombinedChartRenderer.java","Transformer.java","IShapeRenderer.java"},	//fold 8	
				{
					"YAxis.java",
					"CombinedChart.java",
					"BarChart.java",
					"YAxisRenderer.java",
					"AxisRenderer.java",
					"BubbleChartRenderer.java",
					"MarkerImage.java",
					"AnimatedZoomJob.java",
					"ChartAnimator.java",

					"IValueFormatter.java"
				}, ////fold 9

				{
					"LineRadarDataSet.java",
					"PieRadarHighlighter.java",
					"ScatterChartRenderer.java",
					"LargeValueFormatter.java",
					"LegendEntry.java",
					"DefaultFillFormatter.java",
					"ZoomJob.java",
					"CombinedHighlighter.java",
					"LineScatterCandleRadarDataSet.java",
					"ScatterData.java",
					"XAxisRendererRadarChart.java",
					"Range.java",
					"CircleShapeRenderer.java",
					"BarHighlighter.java",
					"HorizontalBarHighlighter.java",
					"ViewPortJob.java",
					"AnimatedMoveViewJob.java",
					"BubbleData.java",
					"PieHighlighter.java",
					"RadarChart.java",
					"ComponentBase.java",
					"MoveViewJob.java",
					"CandleData.java",
					"DefaultValueFormatter.java",
					"LineChart.java",
					"Renderer.java",
					"TriangleShapeRenderer.java",
					"BarLineScatterCandleBubbleDataSet.java",
					"BubbleChart.java",
					"DefaultAxisValueFormatter.java",
					"ICandleDataSet.java",

					"MarkerView.java",
					"ScatterChart.java",
					"ScatterDataProvider.java",
					"TransformerHorizontalBarChart.java",
					"BarLineScatterCandleBubbleData.java",
					"BarLineScatterCandleBubbleDataProvider.java",
					"CandleStickChart.java",
					"IBubbleDataSet.java",
					"ILineScatterCandleRadarDataSet.java",

					"LineScatterCandleRadarRenderer.java",
					"SquareShapeRenderer.java",
					"CombinedDataProvider.java",
				}

		};
		 */

		String[][] testing_all={
				{"BarLineChartBase.java","IMarker.java",	"IAxisValueFormatter.java",
					"LineRadarDataSet.java",
					"PieRadarHighlighter.java",
					"ScatterChartRenderer.java",
					"LargeValueFormatter.java",
					"LegendEntry.java",
					"DefaultFillFormatter.java",
					"ZoomJob.java",
					"CombinedHighlighter.java",
					"LineScatterCandleRadarDataSet.java",
					"ScatterData.java",
					"XAxisRendererRadarChart.java",
					"Range.java",
					"CircleShapeRenderer.java",
					"BarHighlighter.java",
					"HorizontalBarHighlighter.java",
					"ViewPortJob.java",
					"AnimatedMoveViewJob.java",

					"PieHighlighter.java",
					"RadarChart.java",
					"ComponentBase.java",
					"MoveViewJob.java",
					"CandleData.java",
					"DefaultValueFormatter.java",
					"LineChart.java",
					"Renderer.java",
					"TriangleShapeRenderer.java",
					"BarLineScatterCandleBubbleDataSet.java",
					"BubbleChart.java",
					"DefaultAxisValueFormatter.java",
					"ICandleDataSet.java",

					"MarkerView.java",
					"ScatterChart.java",
					"ScatterDataProvider.java",
					"TransformerHorizontalBarChart.java",
					"BarLineScatterCandleBubbleData.java",
					"BarLineScatterCandleBubbleDataProvider.java",
					"CandleStickChart.java",
					"IBubbleDataSet.java",
					"ILineScatterCandleRadarDataSet.java",

					"LineScatterCandleRadarRenderer.java",
					"SquareShapeRenderer.java",
					"CombinedDataProvider.java","LineChartRenderer.java",
					"ChartData.java","IHighlighter.java", 	"YAxis.java",
					"CombinedChart.java",
					"BarChart.java",
					"BubbleData.java",
					"YAxisRenderer.java",
					"AxisRenderer.java",
					"BubbleChartRenderer.java",
					"MarkerImage.java",
					"AnimatedZoomJob.java",
					"ChartAnimator.java",

					"IValueFormatter.java","IScatterDataSet.java",

					//cards
					"GLStateBackup.java",
					"Distortion.java",
					"HeadTracker.java",
					"So3Util.java",
					"Vector3d.java",
					"Eye.java",
					"DistortionRenderer.java",
					"Viewport.java",

					//antl4				
					"BufferedTokenStream.java",
					"ArrayPredictionContext.java",
					"ParseTreeListener.java",
					"CharStream.java",
					"ATNType.java",
					"TokenFactory.java",
					"ATNDeserializer.java",
					"AmbiguityInfo.java",
					"ParserInterpreter.java",
					"TerminalNodeImpl.java",
					"ANTLRFileStream.java",
					"WritableToken.java",
					"ConsoleErrorListener.java",
					"AbstractPredicateTransition.java",
					"Tree.java",
					"TokenStream.java",
					"ProfilingATNSimulator.java",
					"FailedPredicateException.java",
					"InputMismatchException.java",
					"ContextSensitivityInfo.java",
					"BaseErrorListener.java",
					"Utils.java",
					"TokenSource.java",
					"SyntaxTree.java",
					"LexerATNSimulator.java",
					"ATN.java",
					"LexerTypeAction.java",
					"MultiMap.java",
					"InterpreterRuleContext.java",
					"DecisionState.java",
					"Chunk.java",

					"VertexDescription.java", 
					"NumberUtils.java"

				}


		};

		String[][] testingsett_chart ={
				{"BarLineChartBase.java","IMarker.java",	"IAxisValueFormatter.java",
					"LineRadarDataSet.java",
					"PieRadarHighlighter.java",
					"ScatterChartRenderer.java",
					"LargeValueFormatter.java",
					"LegendEntry.java",
					"DefaultFillFormatter.java",
					"ZoomJob.java",
					"CombinedHighlighter.java",
					"LineScatterCandleRadarDataSet.java",
					"ScatterData.java",
					"XAxisRendererRadarChart.java",
					"Range.java",
					"CircleShapeRenderer.java",
					"BarHighlighter.java",
					"HorizontalBarHighlighter.java",
					"ViewPortJob.java",
					"AnimatedMoveViewJob.java",

					"PieHighlighter.java",
					"RadarChart.java",
					"ComponentBase.java",
					"MoveViewJob.java",
					"CandleData.java",
					"DefaultValueFormatter.java",
					"LineChart.java",
					"Renderer.java",
					"TriangleShapeRenderer.java",
					"BarLineScatterCandleBubbleDataSet.java",
					"BubbleChart.java",
					"DefaultAxisValueFormatter.java",
					"ICandleDataSet.java",

					"MarkerView.java",
					"ScatterChart.java",
					"ScatterDataProvider.java",
					"TransformerHorizontalBarChart.java",
					"BarLineScatterCandleBubbleData.java",
					"BarLineScatterCandleBubbleDataProvider.java",
					"CandleStickChart.java",
					"IBubbleDataSet.java",
					"ILineScatterCandleRadarDataSet.java",

					"LineScatterCandleRadarRenderer.java",
					"SquareShapeRenderer.java",
					"CombinedDataProvider.java","LineChartRenderer.java"
				},
				{"ChartData.java","IHighlighter.java", 	"YAxis.java",
					"CombinedChart.java",
					"BarChart.java",
					"BubbleData.java",
					"YAxisRenderer.java",
					"AxisRenderer.java",
					"BubbleChartRenderer.java",
					"MarkerImage.java",
					"AnimatedZoomJob.java",
					"ChartAnimator.java",

					"IValueFormatter.java","IScatterDataSet.java"}, //fold 2

				{"PieChartRenderer.java","PieChart.java","CombinedData.java","HorizontalBarChartRenderer.java",			"IFillFormatter.java","LegendRenderer.java","AxisBase.java","CandleStickChartRenderer.java","ILineRadarDataSet.java","PieRadarChartBase.java"}, //fold 6
				{"ViewPortHandler.java", "YAxisRendererRadarChart.java","BarChartRenderer.java","XAxisRendererHorizontalBarChart.java","HorizontalBarChart.java","ChartHighlighter.java","RadarChartRenderer.java","YAxisRendererHorizontalBarChart.java","CombinedChartRenderer.java","Transformer.java","IShapeRenderer.java"},	//fold 8	




		};


		String[][] testingsett_cards ={
				{
					"GLStateBackup.java",
					"Distortion.java",
					"HeadTracker.java",
					"So3Util.java",

				},
				{
					"Vector3d.java",
					"Eye.java",
					"DistortionRenderer.java",
					"Viewport.java"
				},
				{
					"OrientationEKF.java",
					"HeadMountedDisplay.java",
					"Matrix3x3d.java",
					"CardboardDeviceParams.java"
				},
				{
					"HeadTransform.java",
					"FieldOfView.java",
					"ScreenParams.java"
				}
		};

		/*
		String[][] testingsett_cards ={
				{
					"GLStateBackup.java",
					"Distortion.java",
					"HeadTracker.java"

				},
				{
					"So3Util.java",
					"ScreenParams.java",	
				},
				{
					"Vector3d.java",
					"Eye.java"
				},
				{
					"DistortionRenderer.java",
					"Viewport.java"
				},
				{
					"OrientationEKF.java",
					"HeadMountedDisplay.java"
				},
				{
					"Matrix3x3d.java",
					"CardboardDeviceParams.java"
				},
				{
					"HeadTransform.java",
					"FieldOfView.java"
				}
		};
		 */

		//antlr4 folding
		String[][] testingsett_antlr4 ={
				{
					"BufferedTokenStream.java",
					"ArrayPredictionContext.java",
					"ParseTreeListener.java",
					"CharStream.java",
					"ATNType.java",
					"TokenFactory.java",
					"ATNDeserializer.java",
					"AmbiguityInfo.java",
					"ParserInterpreter.java",
					"TerminalNodeImpl.java",
					"ANTLRFileStream.java",
					"WritableToken.java",
					"ConsoleErrorListener.java",
					"AbstractPredicateTransition.java",
					"Tree.java",
					"TokenStream.java"			
				}, 
				{
					"ProfilingATNSimulator.java",
					"FailedPredicateException.java",
					"InputMismatchException.java",
					"ContextSensitivityInfo.java",
					"BaseErrorListener.java",
					"Utils.java",
					"TokenSource.java",
					"SyntaxTree.java",
					"LexerATNSimulator.java",
					"ATN.java",
					"LexerTypeAction.java",
					"MultiMap.java",
					"InterpreterRuleContext.java",
					"DecisionState.java",
					"Chunk.java"
				},
				{
					"Interval.java",
					"LexerInterpreter.java",
					"Triple.java",
					"RuntimeMetaData.java",
					"OrderedATNConfigSet.java",
					"IntStream.java",
					"DiagnosticErrorListener.java",
					"ATNState.java",
					"ParseTreePattern.java",
					"DecisionEventInfo.java",
					"PredicateEvalInfo.java",
					"Recognizer.java",
					"LookaheadEventInfo.java",
					"ErrorInfo.java",
					"AbstractParseTreeVisitor.java",
					"CommonToken.java",
					"ATNSimulator.java",
					"DoubleKeyMap.java",
					"SetTransition.java",
					"PredictionMode.java",
					"ErrorNode.java"
				},
				{
					"Transition.java",
					"DefaultErrorStrategy.java",
					"ParseInfo.java",
					"RuleTagToken.java",
					"TokenTagToken.java",
					"EmptyPredictionContext.java",
					"LexerAction.java",
					"ATNDeserializationOptions.java",
					"LexerActionExecutor.java",
					"ParseTreeWalker.java",
					"CommonTokenFactory.java",
					"WildcardTransition.java",
					"IntSet.java",
					"LexerActionType.java",

					"LexerATNConfig.java",
					"ParserATNSimulator.java",
					"TagChunk.java",
					"ProxyErrorListener.java",
					"Lexer.java",
					"BlockStartState.java"
				} //fold4
		};

		/*
		//antlr4 folding
		String[][] testingsett_antlr4 ={
				{
					"BufferedTokenStream.java",
					"ArrayPredictionContext.java",
					"ParseTreeListener.java",
					"CharStream.java",
					"ATNType.java",
					"TokenFactory.java",
					"ATNDeserializer.java",
					"AmbiguityInfo.java"},//fold 1
				{
						"ParserInterpreter.java",
						"TerminalNodeImpl.java",
						"ANTLRFileStream.java",
						"WritableToken.java",
						"ConsoleErrorListener.java",
						"AbstractPredicateTransition.java",
						"Tree.java",
						"TokenStream.java"			
				}, 
				{
					"ProfilingATNSimulator.java",
					"FailedPredicateException.java",
					"InputMismatchException.java",
					"ContextSensitivityInfo.java",
					"BaseErrorListener.java",
					"Utils.java",
					"TokenSource.java",
					"SyntaxTree.java"
				},
				{
					"LexerATNSimulator.java",
					"ATN.java",
					"LexerTypeAction.java",
					"MultiMap.java",
					"InterpreterRuleContext.java",
					"DecisionState.java",
					"Chunk.java"
				},
				{
					"Interval.java",
					"LexerInterpreter.java",
					"Triple.java",
					"RuntimeMetaData.java",
					"OrderedATNConfigSet.java",
					"IntStream.java",
					"DiagnosticErrorListener.java"
				},
				{
					"ATNState.java",
					"ParseTreePattern.java",
					"DecisionEventInfo.java",
					"PredicateEvalInfo.java",
					"Recognizer.java",
					"LookaheadEventInfo.java",
					"ErrorInfo.java"
				},
				{
					"AbstractParseTreeVisitor.java",
					"CommonToken.java",
					"ATNSimulator.java",
					"DoubleKeyMap.java",
					"SetTransition.java",
					"PredictionMode.java",
					"ErrorNode.java"
				},
				{
					"Transition.java",
					"DefaultErrorStrategy.java",
					"ParseInfo.java",
					"RuleTagToken.java",
					"TokenTagToken.java",
					"EmptyPredictionContext.java",
					"LexerAction.java"
				},
				{
					"ATNDeserializationOptions.java",
					"LexerActionExecutor.java",
					"ParseTreeWalker.java",
					"CommonTokenFactory.java",
					"WildcardTransition.java",
					"IntSet.java",
					"LexerActionType.java"
				},
				{
					"LexerATNConfig.java",
					"ParserATNSimulator.java",
					"TagChunk.java",
					"ProxyErrorListener.java",
					"Lexer.java",
					"BlockStartState.java"
				}
		};
		 */

		String[][] testingsett_geo = {
				{"VertexDescription.java"},
				{"NumberUtils.java"},
				{"MathUtils.java"},
				{"Point2D.java"}
		};

		//testingsett_chart = testing_all;
		/**
		 * cross projects validation
		 */

		int k = 0;
		String[] aaaa =	ArrayUtils.addAll(testingsett_chart[k], testingsett_antlr4[k]);
		aaaa = ArrayUtils.addAll(aaaa, testingsett_geo[k]);
		aaaa = ArrayUtils.addAll(aaaa, testingsett_cards[k]);
		aaaa = ArrayUtils.addAll(aaaa, testingsett_cards[k]);

		aaaa = ArrayUtils.addAll(aaaa, testingsett_antlr4[k+1]);
		aaaa = ArrayUtils.addAll(aaaa, testingsett_geo[k+1]);
		aaaa = ArrayUtils.addAll(aaaa, testingsett_cards[k+1]);
		aaaa = ArrayUtils.addAll(aaaa, testingsett_chart[k+1]);

		System.out.println("aaaa.length	"+aaaa.length);
		/*
		String[] bbbb;
		bbbb =ArrayUtils.addAll(testingsett_cards[k+1], testingsett_chart[k+1]);
		bbbb =ArrayUtils.addAll(bbbb, testingsett_antlr4[k+1]);
		bbbb = ArrayUtils.addAll(bbbb, testingsett_geo[k+1]);
//		bbbb = ArrayUtils.addAll(bbbb, testingsett_cards[k+1]);
		 */
		//		String[][] newset= {ArrayUtils.addAll(testingsett_chart[0], testingsett_chart[1])};	
		String[][] newset= {aaaa};
		//				String[] bbbb =ArrayUtils.addAll(testingsett_antlr4[0], testingsett_antlr4[1]);
		//				bbbb = ArrayUtils.addAll(bbbb, testingsett_antlr4[2]);
		//				bbbb = ArrayUtils.addAll(bbbb, testingsett_antlr4[3]);
		//		String[] bbbb =ArrayUtils.addAll(testingsett_cards[0], testingsett_cards[1]);
		//		bbbb = ArrayUtils.addAll(bbbb, testingsett_cards[2]);
		//		bbbb = ArrayUtils.addAll(bbbb, testingsett_cards[3]);
		String[] bbbb =ArrayUtils.addAll(testingsett_geo[0], testingsett_geo[1]);
		//				bbbb = ArrayUtils.addAll(bbbb, testingsett_geo[2]);
		//				bbbb = ArrayUtils.addAll(bbbb, testingsett_geo[3]);
		//		String[] bbbb =ArrayUtils.addAll(testingsett_chart[0], testingsett_chart[1]);
		//		bbbb = ArrayUtils.addAll(bbbb, testingsett_chart[2]);
		//		bbbb = ArrayUtils.addAll(bbbb, testingsett_chart[3]);
		//		String[][] newset= {bbbb};
		//		String[][] newset= {testingsett_cards[0],testingsett_cards[1], testingsett_cards[2], testingsett_cards[3]};

		//						String[][] newset= {testingsett_antlr4[0], testingsett_antlr4[1], testingsett_antlr4[2], testingsett_antlr4[3]};
		//		String[][] newset= {testingsett_chart[0], testingsett_chart[1], testingsett_chart[2], testingsett_chart[3]};

		//								String[][] newset= {testingsett_cards[0], testingsett_cards[1], testingsett_cards[2], testingsett_cards[3]};
		//		String[][] newset= {testingsett_geo[0], testingsett_geo[1], testingsett_geo[2], testingsett_geo[3]};

		//		newset = 	testingsett_chart;
		//		newset = 	testingsett_antlr4;
		//		ArrayUtils.addAll(testingsett_chart[0], testingsett_antlr4[0]);
		//		total_examples0421_cards.db db_result/total_examples0404.db
		//				final String example_file = "tables/antlr4_extra.db";
		//				final String example_file ="total0502.db";
		//						final String example_file = "total_examples0421_cards.db";
		final String example_file = "db_result/total_examples0404.db";
		//		total_examples0430_geo
		//								final String example_file = "total_examples0421_cards.db";
		//		final String example_file = "tables/total_examples0430_geo.db";
		//										final String template_file = "ABG_template0427.db"; //antlr4_extra.db
		//				final String template_file = "charts_template04272.db";


		//		final String template_file = "charts_template043022.db";

		//		final String template_file ="CAG_template0427.db";
		//		final String template_file="CAG_template0427.db";
		//										CAG_template0427.db
		//CBG for antlr4
		//String template_file="a0504.db";
		//String template_file="tables/template_total05042.db";
		//String template_file = "1t1es2dting1911111191111112121213111222922221111119.db";
		//final String template_file = "tables/template_antlr0504.db";
		//final String template_file = "tables/charts_template0427.db";
		final String template_file = "tables/template_total0511.db";
		//final String template_file = "tables/test_record0424_geo.db";
		//final String template_file = "validationgeo.db";

		/**
		 * final String template_file = "tables/template_antlr0504.db";
		 * final String template_file = "tables/test_record0424_geo.db";
		 * final String template_file = "tables/test_record0506_cards.db";
		 * final String template_file = "tables/charts_template0427.db";
		 */

		//					 .db
		//		template_antlr0504_decl.db
		//				final String template_file = "test_record0506_cards.db";
		//		final String template_file = "test_record0424_geo.db";
		//						final String template_file = "field_decl05071.db";
		//						final String template_file = "decls4440510.db";

		//								final String template_file = "template_total0504.db"; //test_record0424_antlr4.db
		//								final String template_file = "charts_template0427.db";//84
		//		final String template_file = "template_total0511.db";//84

		//				final String template_file = "test_record0424_antlr4.db";//54
		//								final String template_file = "template_antlr0504.db";
		//						final String template_file = "template_antlr40510.db";
		//		final String template_file = "template_total0511.db";

		//				final String template_file = "test_record04242222222_antlr4.db"; //test_record0424_antlr4.db
		//												final String template_file = "test_record0424_antlr4.db"; //test_record0424_antlr4.db
		//								final String template_file = "test_record04242222222_cards.db"; //test_record0424_antlr4.db
		//										final String template_file = "test_record0424_geo.db";//47
		//												final String template_file = "template_cards0505.db";//47
		//		final String [] template_files = {"test_record0424_antlr4.db", "charts_template0427.db"};
		int num		= 0;
		int hit		= 0;
		int nohit	= 0;
		//		int hit=0;
		int cc = 0;
		int total = 0;
		int atype, stype=0;

		//				int aatype[] = {19,68};
		//						int sstype[] = {130, 133};
		//						int aatype[] = {3};
		//				int sstype[] = {130, 133,97, 143};
		//				int aatype[] = {3,19, 80};

		//				int sstype[] = {97, 143};
		//				int aatype[] = {19, 80};
		//				int aatype[] = {19,68};

		//variables
		//						int aatype[] = {68, 19};
		//						int sstype[] = {80, 76};
		//						int aatype[] = {};
		//				int sstype[] = {};

		//						int [] aatype ={3};
		//						int [] sstype ={130, 133};	
		int [] aatype =	{};
		int [] sstype =	{};	

		//		int [] aatype =	{19, 68, 70,  3,  80};
		//		int [] sstype = {76,80, 130, 133, 97, 143};
		//		int [] aatype ={70};
		//		int [] sstype ={18};	

		//for methoddecl
		//						int [] aatype ={19};
		//						int [] sstype ={97,143};	

		//for var

		//		int [] aatype = {68, 19, 70};
		//		int [] sstype ={80, 76};//68	80 19	80

		//				int [] aatype ={3,19, 68};
		//				int [] sstype ={130, 133, 97, 80, 143};	
		int setnum = newset.length;
		int start  = 0;
		int fold = 0;

		//{3,19, 68}
		//{130, 133, 97, 80, 143}		

		//				int [] aatype = {3, 19};
		//				int [] sstype = {130, 133, 97, 143};//68	80 19	80
		//		sstype = at;
		//		aatype = st;
		//19,68
		//								int sstype[] ={80, 97,143};
		//				int aatype[] ={3};
		//		int sstype[] ={130, 133};
		//				int aatype[] ={3};
		//				int sstype[] ={130, 133};
		//		int aatype[] ={70};
		//		int sstype[] ={36};
		//		int sstype[] ={80};
		//		for(int id=0 ; id<testingsett_chart.length;id++){


		//		for(int id=start ; id<setnum;id++){
		for(int id=start ; id<1;id++){
			//						System.out.println("newset[id]"+newset[id].length + "  "+setnum);
			//		Map<String, String> item_map = new HashMap<String, String>();
			LinkedHashMap<TemplateEDB, TemplateEDB> item_map = new LinkedHashMap<TemplateEDB, TemplateEDB>();
			//			ResultSet example_resultSet 	=  queryExampleResultbyTestingSet("grammar_swift_stat", newset[id],aatype, sstype, example_file);
			ResultSet example_resultSet 	= queryExampleResultbyID("grammar_swift_stat", 70);
			while(example_resultSet.next()){
				cc++; //checking total number of line examples
			}
			//			System.out.println("ccccc"+cc);
			example_resultSet 	=  queryExampleResultbyID("grammar_swift_stat", 70);
//			example_resultSet 	=  queryExampleResultbyTestingSet("grammar_swift_stat", newset[id], aatype, sstype,example_file);
			cc = (int) Math.round(cc*1);
			Vector<String> migrations = new Vector<String>();
			String aaa	="";
			String bbb 	="";
			//checking irregular regex set of training set
			ResultSet regex = queryRegex(template_file,newset[id]);
			while(regex.next()){
				String raaa = regex.getString("rform");
				String tttt = regex.getString("template");
				//				String eeee = regex.getString("example");
				int c = Integer.parseInt(regex.getString("c"));
				//								int at = Integer.parseInt(regex.getString("ast_type"));
				PatternSyntaxException exc = null;
				System.err.println(c+":  "+tttt+"   "+raaa+"  ");
				try {
					Pattern.compile(raaa);
				} catch (PatternSyntaxException e) {
					exc = e;
					System.out.println("errrrrr"+raaa);
				}
				if (exc != null) {
					exc.printStackTrace();
				} else {

				}	
			}

			//	System.out.println("print end");
			int dbid =0;
			File newTextFile = new File("thetextfile.txt");

			FileWriter fw = new FileWriter(newTextFile);
			while(example_resultSet.next()){
				num++;
				int templateT = 0;
				aaa 	= example_resultSet.getString("template");
				bbb 	= example_resultSet.getString("example");
				atype 	= Integer.parseInt(example_resultSet.getString("AST_TYPE"));
				idd 	= Integer.parseInt(example_resultSet.getString("ID"));
				dbid 	= idd;
				String afile = example_resultSet.getString("afilename");

				//restore context
				aaa = aaa.replaceAll("@Override[\\s|\\t|\\n]* ", "@Override ");
				aaa= SimpleFormat.getSimpleFormat(aaa);
				JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(aaa));
				CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
				JavaParser parserj = new JavaParser(javaCommonStream);
				//			Lock lock = new ReentrantLock();


				Class<? extends Parser> parserClass = null;
				ClassLoader cl = Thread.currentThread().getContextClassLoader();
				parserClass = cl.loadClass("antlr_parsers.javaparser.JavaParser").asSubclass(Parser.class);

				/**
				 * removal the invaild templates of regex by syntax
				 */
				Method startRule = parserClass.getMethod(JavaParser.ruleNames[atype]);
				ParserRuleContext tree = (ParserRuleContext)startRule.invoke(parserj, (Object[])null);
				RewriterJava rw = new RewriterJava();
				ParseTreeWalker walker_swift = new ParseTreeWalker();
				try{
					walker_swift.walk(rw, tree);
				} catch (java.lang.NullPointerException e){
					System.out.println("null	"+aaa);
				}
				ParserRuleContext ctxx = tree;					
				int az = tree.start.getStartIndex();
				int bz = tree.stop.getStopIndex();
				Interval interval = new Interval(az, bz);
				String context = ctxx.start.getInputStream().getText(interval);
				rw.orginText = context;


				//query templates <arg0>
				J2Swift.init();
				//				System.out.println("template_file	"+template_file);

				ResultSet template_resultSet = queryTemplateByExampleID(dbid, newset[id], atype, example_file, template_file);
				int idddd = idd;
				while(template_resultSet!=null && template_resultSet.next()){
					String src_temp 	= template_resultSet.getString("template");
					String target_temp 	= template_resultSet.getString("example");
					//					String afilename 	= template_resultSet.getString("afilename");
					//					String rform 		= template_resultSet.getString("rform");


					templateT 			= Integer.parseInt(template_resultSet.getString("AST_TYPE"));
					stype 				= Integer.parseInt(template_resultSet.getString("PARENT_TYPE"));
					idd 				= Integer.parseInt(template_resultSet.getString("ID"));

					//					System.out.println("DDD	"+idd);
					//only same javatype templates
					if(templateT==atype){
						src_temp = src_temp.replaceAll("@ Override", "@Override").replaceAll("< ", "\\\\< ");
						src_temp =getStdFormTemplate(src_temp);
						item_map.put(new TemplateEDB(idd,src_temp), new TemplateEDB(idd,target_temp));
					}
				}

				//System.out.println("pop		"+context+" "+context.length());
				//get example context... 
				//query templates..
				//download all templates
				Iterator<TemplateEDB> iter = item_map.keySet().iterator();
				boolean once = true;
				String top1="";
				int countt = 0;
				String finalM  = null;
				//once: only ranked one is chosen
				while(iter.hasNext()){
					//					once = false;
					GenericTree<MigrationNode> treea 	  = new GenericTree<MigrationNode>();
					TranslationExample migrating 		  = new TranslationExample();
					TemplateEDB src_tem = iter.next();
					TemplateEDB target_tem = item_map.get(src_tem);
					GenericTreeNode<MigrationNode> root   = new GenericTreeNode<MigrationNode>(new MigrationNode());
					//escape < and >
					root.getData().srcTemp = new ST(src_tem.template.replaceAll("\\s+<\\s+", "\\\\<").replaceAll(" >=", "\\\\>= ").replaceAll("\\s+>\\s+", "\\\\>").replaceAll(" <=", "\\\\<= "));
					root.getData().srcText = context;
					try{
						//escapling < and >
						System.out.println("template "+target_tem.template+"  "+src_tem.template);
						root.getData().targetTemp  = new ST(target_tem.template.replaceAll("\\s+<\\s+", "\\\\<").replaceAll(" >= ", "\\\\>= ").replaceAll("\\.\\.<", "..\\\\<").replaceAll(" <= ", "\\\\<= ").replaceAll("\\s+>\\s+", "\\\\>"));
					}catch (org.stringtemplate.v4.compiler.STException e){
						System.out.println(e+" "+target_tem.template);

					}
					treea.setRoot(root);
					//one to many diverges
					//										GenericTreeNode<MigrationNode> rr = null;
					//										System.out.println("src_tem	"+src_tem+ "target_tem	"+target_tem.template);

					GenericTreeNode<MigrationNode> rr = migrating.iterativeTemplates(src_tem.template, tree, root, testingsett_chart[id], template_file, example_file);
				
					if(rr!=null){
						countt = 0;
						System.out.println(idd+"	>>> template "+src_tem.template+"  , "+target_tem.template);
						for(GenericTreeNode<MigrationNode> tt1:treea.build(GenericTreeTraversalOrderEnum.POST_ORDER)){
							GenericTreeNode<MigrationNode> parent = tt1.getParent();
							if(parent!=null){
								if(tt1.getData().targetTemp!=null && tt1.getData().targetTemp.getAttributes()!=null){

									java.util.Iterator<String> iterrr = tt1.getData().targetTemp.getAttributes().keySet().iterator();
									boolean nomap = false;
									while(iterrr.hasNext() && !nomap && treea.getRoot().getData().targetTemp.getAttributes()!=null){
										Object aa = treea.getRoot().getData().targetTemp.getAttributes().get(iterrr.next());
										if(aa!=null && aa.toString().equals("(  )")){
											nomap = true;
										}
									}
									//							tt1.getData().targetTemp
									//								System.out.println("parent.getData().targetTemp	"+tt1.getData().argID+"\n"+parent.getData().targetTemp.getEvents());
									//									if(parent.getData().targetTemp.getAttribute(tt1.getData().argID)!=null)
									if(!nomap){
										parent.getData().targetTemp.add(tt1.getData().argID, tt1.getData().targetTemp.render());
										//										System.out.println("tt1.getData().targetTemp"+tt1.getData().targetTemp.impl.template+"  "+tt1.getData().srcTemp.impl.template);
									}
									//parent.getData().targetTemp
									//T.add(arg0, safdaf)
								}else{
									//								System.out.println("else parent.getData().targetTemp	"+tt1.getData().argID+"\n"+tt1.getData().targetText);
									//									if(parent.getData().targetTemp.getAttribute(tt1.getData().argID)!=null)

									parent.getData().targetTemp.add(tt1.getData().argID, tt1.getData().targetText);
								}
								//								System.out.println("Traversal	"+tt1.getData().srcText+"   parent:"+tt1.getParent().getData().srcText);
							}
						}
						//						try{
						//							top1 = treea.getRoot().getData().targetTemp.render();
						java.util.Iterator<String> iterrr = treea.getRoot().getData().targetTemp.getAttributes().keySet().iterator();
						boolean nomap = false;
						while(iterrr.hasNext() && !nomap){
							Object aa = treea.getRoot().getData().targetTemp.getAttributes().get(iterrr.next());
							if(aa.toString().equals("(  )")){
								nomap = true;
							}
						}
						if(!nomap&& countt==0 ){

							finalM = treea.getRoot().getData().targetTemp.render().replaceAll("\\s*\\.s\\*", "\\.");
							//								finalM = "TESTING";
							countt=10;
						}
						if(!nomap && countt==0){
							String rendered = treea.getRoot().getData().targetTemp.render().replaceAll("\\s*\\.s\\*", "\\.").replaceAll("{...}", "{}");
							//								String rendered = "TESTING";
							DSLErrorListener schecker = IterativeMapping.checkSwiftSyntax(rendered, stype);
							if(!schecker.hasErrors() && !rendered.contains("this")){
								//									countt++;
								finalM = rendered;
							}
						}
						//						} 
						//						catch(Exception e){

						//						}

					}



					Map<String, OpProperty> subMatching = MatchingTemplate2.getAttributesMapLineIndex(src_tem.template, context ,rw.maps);
					//					Map<String, OpProperty> subMatching = null;
					//					System.err.println("subMatching\n"+subMatching);




					/**
					 * 
					 * 
					 *      ctx                                         target_temp{a1=T1,a2=T2,a3=T3}
					 *     / | \ \                                       /  |    \
					 * ctx ctx ctx ctx   <-{template, text}           term temp temp
					 *                                                      |      \
					 *													  term   term
					 *     ctx
					 *     | \ \ 
					 *   ctx ctx ctx
					 *   
					 */

					if(subMatching!=null && !subMatching.isEmpty()){
						boolean discard= false;
						//spawn a migration Tree
						//					    GenericTreeNode<MigrationNode> rootA  = new GenericTreeNode<MigrationNode>();					    
						//					    treea.setRoot(rootA);			    
						String stemplate=target_tem.template.replaceAll("\\s+<\\s+", "\\\\<");
						ST st_swift = new ST(stemplate);
						//						System.out.println("abcc	"+src_tem+"   "+target_tem+"  "+subMatching.keySet()+"  \n"+SimpleFormat.getSimpleFormat(context)+"  "+stemplate);
						//								System.err.println("rw.maps"+rw.maps);
						java.util.Iterator<LineCharIndex> kitt = rw.maps.keySet().iterator();
						//						while(kitt.hasNext()){
						//							LineCharIndex keyy = kitt.next();
						//								rw.maps.get(keyy).strASTNode;
						//																System.out.println("keyy	"+keyy+"   "+rw.maps.get(keyy).strASTNode);
						//						}
						Iterator<String> iterator = subMatching.keySet().iterator();
						//							int count =	0;
						Vector<String> checkoverlap = new Vector<String>();
						while(iterator.hasNext()){
							String key=iterator.next();
							String val = subMatching.get(key).strASTNode;
							//							System.out.println("val	"+val+" "+getConstant(val, testingsett_chart[id], "test_record04153.db"));
							//searching in context
							//								String texto = text;
							//								String text = rw.maps.get(key)!=null?rw.maps.get(key).strASTNode:"none";
							//check it has a context in source platform
							String text = val;
							//	String text = key.example;
							//	String subtree=argmap.get(key);
							String subs ="";

							//in case single token
							if(rw.maps.get(key)!=null && rw.maps.get(key).interval.length()==1){
								subs = text;
							}

							else{
								//const map.. just scan the value.
								String constantMatch=getConstant(text, testingsett_chart[id], template_file);
								if(constantMatch==null){
									subs = text;

									//need to query

								}else{
									subs = constantMatch;
								}
								//									subs = getConstant(text, testingsett[id])==null?text:getConstant(text,testingsett[id]);
							}
							//								System.out.println("getConstant("+text+")"+getConstant(text,id));
							//								if(getConstant(text, testingsett[id])==null){
							//									subs = text;
							//								}
							String arg = key;
							//							System.out.println("getConstant("+arg);
							if(arg.startsWith("<") && arg.endsWith(">") && !checkoverlap.contains(arg)){
								checkoverlap.add(arg);
								//arg0, arg1,.. for targetTemplate let <arg0> = <arg1>
								String argId = arg.replaceAll("<", "").replaceAll(">", "");
								st_swift.add(argId, subs);

								//								GenericTreeNode<MigrationNode> childB = new GenericTreeNode<MigrationNode>(new MigrationNode());
								//								MigrationNode node = new MigrationNode();
								//								node.targetTemp = st_swift;
								//								rootA.setData(node);


								//go something something(subs, parent);



							}
							//							if(rw.maps.get(key)==null && !arg.equals("{...}")){
							//									System.err.println("key"+arg+" "+key.example);
							//								discard = true;
							//							}
							if(StructureMappingAntlr4_for_stmt.java_keywords_list.contains(subs)){
								discard = true;
							}
						}
						//							if(count >0){

						//							System.out.println(aaa);
						//							System.out.println("st_android\n"+kkey+"   "+skey+"\n");
						//							System.err.println("final\n"+st_swift.render()+"\n");
						if(!discard){
							migrations.add(st_swift.render()+"\n");
							//								System.out.println("st_swift.render()\n"+st_swift.render());
						}
						//								replace( ctx, st_android.render());
						//							}
					}




				}//each template and migration result
				total++;
				//				System.out.println(idd);
				//				System.out.println(total+" J2Swift		"+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa), atype, idddd));
				//				System.err.println(">>	"+SimpleFormat.getSimpleFormat(bbb));

				if(!top1.equals("")){
					//	System.out.println(top1+ "| "+SimpleFormat.getSimpleFormat(bbb).replaceAll("\n","")+" | "+idd+" | "+SimpleFormat.getSimpleFormat(aaa)+" | "+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa).replace("...", ""), atype, idddd));
				}
				//				idddd = -1;
				String outputa = "";
				if(migrations.size()>0 && !J2Swift.misexamples.contains(dbid)){
					//					atype= -1;


					//										System.out.println(hit+" : "+(nohit+hit)+" out of "+cc);
					//					if( fold == (id)){				
					//						System.out.println("covered  | "+idddd+" | "+afile +" | "+migrations.firstElement().replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa), atype, idddd)+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", ""));
					//					System.out.println("covered  | "+idddd+" | "+afile +" | "+migrations.firstElement().replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa), atype, idddd)+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", ""));
					//					System.out.println("covered  | "+idddd+" | "+afile +" | "+migrations.firstElement().replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", ""));
					if(finalM!=null){
						//	outputa = "covered  | "+idddd+" | "+afile +" | "+finalM.replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", "")+" | "+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa), atype, idddd);
						System.out.println("covered  | "+idddd+" | "+afile +" || "+finalM.replace("\n", "")+" || "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", ""));
						//			           fw.write(outputa+"\n");

					}else{

						System.out.println("covered  | "+idddd+" | "+afile +" || "+migrations.firstElement().replace("\n", "")+" || "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", ""));

					}
					hit++;

					//					}
					//					System.out.println(migrations);
					//					System.out.println(migrations.firstElement());

				}
				if(migrations.size()==0 && !J2Swift.misexamples.contains(dbid)){
					//					atype= -1;
					//					if( fold == (id))	{
					//						System.out.println("uncovered  | "+idddd+" | "+afile +" | "+SimpleFormat.getSimpleFormat(aaa)+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", ""));
					//					System.out.println("uncovered  | "+idddd+" | "+afile +" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa), atype, idddd)+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", ""));

					//	outputa = "uncovered  | "+idddd+" | "+afile +" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", "")+" | "+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa), atype, idddd);
					System.out.println("uncovered  | "+idddd+" | "+afile +" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", ""));

					nohit++;
					//					}
					//					System.out.println("::::::	no hit	"+(nohit+hit)+" out of "+cc);

					//					System.out.println("::::::	"+SimpleFormat.getSimpleFormat(aaa));
					//					System.err.println("::::::	"+SimpleFormat.getSimpleFormat(bbb));
				}

				//				outputa = "covered  | "+idddd+" | "+afile +" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+J2Swift.getResult(SimpleFormat.getSimpleFormat(aaa), atype, idddd)+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", "");
				//	outputa = "covered  | "+idddd+" | "+afile +" | "+SimpleFormat.getSimpleFormat(bbb).replace("\n", "")+" | "+" | "+SimpleFormat.getSimpleFormat(aaa).replace("\n", "");

				//								System.out.println(outputa);
				//	fw.append(outputa+"\n"); 
				migrations.clear();
				item_map.clear();
				//					}
			}



			fw.close();
			//			System.err.println(migrations.firstElement());
			System.out.println((id+1)+" folded	"+ hit+" out of "+(hit+nohit));

			//			hit = 0;
			//			nohit = 0;


			//			total=0;
		} //itemset

		System.out.println("total	"+total+"  :"+hit);
		hit=0;
		nohit=0;
		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0413.db"), "grammar_android_stat");
		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0413.db"), "constant_stat");
		//		DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"test_record0413.db"), "grammar_swift_stat");

		//		DBTablePrinter.printTable(connection, "grammar_swift_stat");

		//		queryExampleResultRandom("", 10);
	}



	public static String getConstant(String aConst, String [] id, String templatedb) throws SQLException{
		String sConst=null;
		String [] newa = new String[id.length];
		int i =0;

		for(String a:id){
			String f="\'"+a+"\'";
			newa[i] = f;
			i++;

		}

		String test=Arrays.toString(newa).replace("[","").replace("]","");
		//		test="";
		String query ="SELECT example from constant_stat where template=\'"+aConst+"\' and ast_type is not 1 and afilename not in ("+test+") and count > 1 order by count desc LIMIT 1;";
		//		String query ="SELECT example, count(*) as c from constant_stat  where template=\'"+aConst+"\' group by template, example order by c desc LIMIT 1;";

		//				System.out.println("qqq"+query);
		Connection connection2 =DriverManager.getConnection("jdbc:sqlite:"+templatedb);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);


		while(resultSet.next()){
			//			templateOk = false;
			//			flip = true;
			//			double c1 = 1.0000, c2 = 1.0001;

			//			for(int q=0;q<=cycle && !templateOk;q++){

			//				black_a =new LinkedList<OpProperty>();
			//				black_s=new LinkedList<OpProperty>();
			//			num++;
			sConst = resultSet.getString("example");
			//			System.out.println("Integer.parseInt"+Integer.parseInt(resultSet.getString("example")));
		}
		//		if(sConst!=null)
		return sConst;
		//		else return aConst;
	}
	public static String getStdFormTemplate(String template){

		String pattern = "( )(.)( )";
		Pattern r = Pattern.compile(pattern);
		//		String aaa = "@Override public void <arg0> ( <arg1> <arg2> ) - > <arg4> {...}";
		//		CharSequence text;
		Matcher m = r.matcher(template);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			//			System.out.println("g1"+m.group(1)+ " g0"+m.group(0)+"g2"+m.group(2));
			//			System.err.println(aaa.replaceAll("( ).( )",m.group(2)));
			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
			m.appendReplacement(sb, m.group(0).replaceAll(Pattern.quote(m.group(1)), "").replaceFirst(Pattern.quote(m.group(3)), ""));
		}

		m.appendTail(sb);
		///	System.out.println(sb.toString());


		//		return sb.toString();
		return template;
	}



}
