package alignment.common;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import org.simmetrics.StringMetric;
//import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.metrics.JaroWinkler;
import org.simmetrics.simplifiers.Simplifiers;
import org.simmetrics.tokenizers.Tokenizers;
//import com.google.common.io.Files;

import com.google.common.io.Files;

import static org.simmetrics.builders.StringMetricBuilder.with;

//import com.google.common.io.Files;

public class Filewalker {


	public Filewalker(){

	}
	String _folder_a;
	String _folder_s;
	public Filewalker(String folder_a, String folder_s){

		_folder_a = folder_a;
		_folder_s = folder_s;

	}


	public void walk( String path ) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;

		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walk( f.getAbsolutePath() );
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(f.getName().contains(".java")){
					System.out.println( "JavaFile:" + f.getName() );
				}
			}
		}
	}


	public void walk( String path , LinkedList<File> files) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;


		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walk( f.getAbsolutePath() , files);
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(Files.getFileExtension(f.getAbsolutePath()).equals("java")
						|| Files.getFileExtension(f.getAbsolutePath()).equals("swift")){
					//                	  System.out.println( "JavaFile:" + f.getName() );
					//                	  System.err.println(Files.getFileExtension(f.getAbsolutePath()));
					files.add(f);
					//                	  Files.get
				}
			}
		}
	}

	
	public static void walk_folder( String path , List<File> afiles) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;


		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walk_folder( f.getAbsolutePath() , afiles);
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(Files.getFileExtension(f.getAbsolutePath()).equals("java")
					
//				}
						|| Files.getFileExtension(f.getAbsolutePath()).equals("swift")
						|| Files.getFileExtension(f.getAbsolutePath()).equals("cs")
						){
					//                	  System.out.println( "JavaFile:" + f.getName() );
					//                	  System.err.println(Files.getFileExtension(f.getAbsolutePath()));
					afiles.add(f);
					//                	  Files.get
				}
			}
		}
	}
	
	
	public static void swalk_folder( String path , List<File> afiles) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;


		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walk_folder( f.getAbsolutePath() , afiles);
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(Files.getFileExtension(f.getAbsolutePath()).equals("swift"))
				{
					//                	  System.out.println( "JavaFile:" + f.getName() );
					//                	  System.err.println(Files.getFileExtension(f.getAbsolutePath()));
					afiles.add(f);
					//                	  Files.get
				}
			}
		}
	}


	public void walkforswift( String path , LinkedList<File> files) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;


		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walkforswift( f.getAbsolutePath() , files);
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(Files.getFileExtension(f.getAbsolutePath()).equals("swift")){
//					                	  System.out.println( "swift:" + f.getName() );
					//                	  System.err.println(Files.getFileExtension(f.getAbsolutePath()));
					files.add(f);
					//                	  Files.get
				}
			}
		}
	}

	public void walkforjava( String path , LinkedList<File> files) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;


		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walkforjava( f.getAbsolutePath() , files);
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(Files.getFileExtension(f.getAbsolutePath()).equals("java")
						){
//					                	  System.out.println( "JavaFile:" + f.getName() );
					//                	  System.err.println(Files.getFileExtension(f.getAbsolutePath()));
					files.add(f);
					//                	  Files.get
				}
			}
		}
	}


	public Map<File, File> getClassMapping() throws IOException{

		LinkedList<File> android_files = new LinkedList<File>();
		LinkedList<File> swift_files = new LinkedList<File>();

		walkforjava(_folder_a, android_files);
		walkforswift(_folder_s, swift_files);

		System.out.println(android_files.size());
		System.out.println(swift_files.size());
		Map<File, File> classmapping = new HashMap<File, File>();
		Map<File, File> hclassmapping = new HashMap<File, File>();
		//		StringMetric
		//		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.

		StringMetric metric =
				with(new JaroWinkler())
				//				.simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
				//				.simplify(Simplifiers.removeAll("Chart"))
				//				.simplify(Simplifiers.removeAll("Activity"))
				.build();

		float max_score = -1;
		System.out.println(android_files);
		System.out.println(swift_files);
		Vector<Integer> iii = new Vector<Integer>();
		Vector<Integer> jjj = new Vector<Integer>();
		double[][] costMatrix = new double[android_files.size()][swift_files.size()];
		for (int i=0;i< android_files.size();i++ ) {
			File android_filePath = android_files.get(i);
//			Files.get
			
			String a = Files.getNameWithoutExtension(android_filePath.getName());
			int m=0,k=0;
			for (int j=0;j< swift_files.size();j++ ) {
				File swift_filePath = swift_files.get(j);								
				String s = Files.getNameWithoutExtension(swift_filePath.getName());


				float score = metric.compare(a, s);
				costMatrix[i][j] = 1-score;
				//				System.out.println(i+"="+a+":::"+s+"_"+score);
				if (score > max_score  && score> 0.75  && !iii.contains(i) && !jjj.contains(j)) {
					max_score = score;
					classmapping.put(android_files.get(i), swift_files.get(j));
					m=i; //store max index
					k=j;
				}
				//				System.err.println(a+" : "+s+"::"+score+"__"+max_score);
			}
			iii.add(m);
			jjj.add(k);
			//			iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));
			//prunning
			max_score = 0;


		}

		HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
		int[] result = hung.execute();
		for(int p=0; p<result.length;p++){
			if(result[p]!=-1){
				if(costMatrix[p][result[p]] < 0.1){
					//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
					hclassmapping.put(android_files.get(p), swift_files.get(result[p]));
				}
			}
			//			System.out.println(p+",, "+result[p]);
			//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
		}
		//		staticPrintMapping(classmapping);

		return hclassmapping;


	}

	public static void staticPrintMapping(Map<File, File> fmapping){

		System.out.println("class mapping: "+fmapping.size());

		Iterator<File> classmap = fmapping.keySet().iterator();
		while(classmap.hasNext()){

			File aclass_path = classmap.next();

			File sclass_path =fmapping.get(aclass_path);
			System.out.println(aclass_path.getName()+":"+sclass_path.getName());
//			System.out.println(sclass_path.getAbsolutePath());
		}
	}


	public static void main(String[] args) throws IOException {
		Filewalker fw = new Filewalker("mcharts/MPAndroidChart-master", "mcharts/Charts-master");
		//		Filewalker fw = new Filewalker("testingFileName/android", "testingFileName/swift");

		//32
		//Filewalker fw = new Filewalker("facebook/facebook-android-sdk-master", "facebook/facebook-sdk-swift-master");
		//33
		//		Filewalker fw = new Filewalker("wordpress/WordPress-Android-develop", "wordpress/WordPress-iOS-develop");
		//				Filewalker fw = new Filewalker("wire/wire-android-master", "wire/wire-ios-develop");

		Map<File, File> classmapping = fw.getClassMapping();
		staticPrintMapping(classmapping);
		/*
		LinkedList<File> android_files = new LinkedList<File>();
		LinkedList<File> swift_files = new LinkedList<File>();
//		fw.walk("facebook/facebook-android-sdk-master", android_files);
//		fw.walk("facebook/facebook-sdk-swift-master", swift_files);

		fw.walk("wechat/wechat-master", android_files);
		fw.walk("wechat/TSWeChat-master", swift_files);

//		fw.walk("wordpress/WordPress-Android-develop", android_files);
//		fw.walk("wordpress/WordPress-iOS-develop", swift_files);

//		fw.walk("wire/wire-android-master", android_files);
//		fw.walk("wire/wire-ios-develop", swift_files);

//		fw.walk("reactivex/RxJava-1.x", android_files);
//		fw.walk("reactivex/RxSwift-master", swift_files);

//		fw.walk("mcharts/MPAndroidChart-master", android_files);
//		fw.walk("mcharts/Charts-master", swift_files);

		System.out.println(android_files.size());
		System.out.println(swift_files.size());

		Map<File, File> classmapping = new HashMap<File, File>();
		StringMetric metric = StringMetrics.jaroWinkler();
		float max_score = -1;
		Vector<Integer> iii = new Vector<Integer>();
		for (int i=0;i< android_files.size();i++ ) {
			File android_filePath = android_files.get(i);



			for (int j=0;j< swift_files.size();j++ ) {
				File swift_filePath = swift_files.get(j);


				//						parseSwift(swift_filePath);
				//						System.out.println(android_files[i].getName().split(".java")[0]);
				//						System.out.println(swift_files[j].getName().split(".swift")[0]);
//				String a = android_files[i].getName().split(".java")[0];
				String a = Files.getNameWithoutExtension(android_filePath.getName());

				String s = Files.getNameWithoutExtension(swift_filePath.getName());
				//						String[] bb = "ViewPortHandler.java".split(".java");
				//						System.err.println(bb[0]);

				float score = metric.compare(a, s);

				if (score > max_score && score > 0.8 && !iii.contains(j)) {
					max_score = score;
					classmapping.put(android_files.get(i), swift_files.get(j));
				}

				iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));


			}
			//prunning
			max_score = -1;


		}
		 */
		//		staticPrintMapping(classmapping);


		StringMetric metric =
				with(new JaroWinkler())
				.simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
				.simplify(Simplifiers.removeAll("Chart"))
				.simplify(Simplifiers.removeAll("Activity"))
				.build();

		System.err.println(metric.compare("YAxisRendererRadarChart", "YAxisRendererRadarChart"));
		System.err.println(metric.compare("YAxisRenderer", "YAxisRendererRadarChart"));

	}

}