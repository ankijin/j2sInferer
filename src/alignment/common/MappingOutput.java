package alignment.common;

//import static org.hamcrest.CoreMatchers.instanceOf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.antlr.v4.runtime.ParserRuleContext;
//import org.apache.commons.collections4.ListUtils;
//import org.springframework.expression.spel.ast.OpAnd;

import alignment.ruleinfer.QueryExamples;
import sqldb.SQLiteJDBC;

public class MappingOutput implements Comparable<MappingOutput>{
	public String example_sql="", template_sql="", binding_sql="";
	public LinkedHashMap<OpProperty, OpProperty> nextMapping=new LinkedHashMap<OpProperty, OpProperty>();
	public String template_s="", template_a="";
	public String example_s="", example_a="";
	public double cost = 1;
	public boolean hasError=false;
	public Map<OpProperty, String> aamap, ssmap, aamappp, ssmappp;
	public int countC = 1;
	public String regex ="";
	
	public String sfilename ="", afilename="";
	public boolean isMethod = false; 

	public MappingOutput(LinkedHashMap<OpProperty, OpProperty> nextM, String b_sql,String e_sql, String t_sql, String t_a, String t_s, double c ){
//		nextMapping = new LinkedHashMap<OpProperty, OpProperty>();
		
		nextMapping=new LinkedHashMap<OpProperty, OpProperty>();
		aamap = new HashMap<OpProperty, String>();
		ssmap = new HashMap<OpProperty, String>();
		nextMapping = nextM;
		binding_sql = b_sql;
		template_sql  = t_sql;
		example_sql = e_sql;
		
		template_a = t_a;
		template_s = t_s;
		cost = c;
	}
	/*
	mappingout.nextMapping = nextMapping;
	mappingout.binding_sql = binding_sql;
	mappingout.example_sql = example_sql;
	mappingout.template_sql = template_sql;
	mappingout.template_a = tempa;
	mappingout.template_s= temps;
	*/
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "template: cost::"+cost+"::  "+template_a+"|"+template_s;
	}
	public boolean isNull(){
		return nextMapping.isEmpty() || template_a.equals("") || template_s.equals("");
	}
	@Override
	public int compareTo(MappingOutput o) {
		// TODO Auto-generated method stub
		//		int aa = (int) (this.cost-o.cost>0)*100;
		return ((Double)cost).compareTo(o.cost);
	}


	
	public void doupdate(Connection c){

	
		try {
			//			if(!isEqualTemplate){
			//					if(!listner_swift.hasErrors() && !listner.hasErrors() && cost!=prev_cost){
//			System.out.println("doupdate"+template_sql);

			java.sql.Statement sql_stmt = c.createStatement();
			c.setAutoCommit(true);
			//			sql_stmt.executeUpdate(e12);	
//			this.regex
			
			   PatternSyntaxException exc = null;
		    try {
		        Pattern.compile(regex);
		    } catch (PatternSyntaxException e) {
		        exc = e;
		    }
		    if (exc != null) {
		        exc.printStackTrace();
		    } else {
//		        System.out.println("Regex ok!");
				sql_stmt.executeUpdate(example_sql);
				sql_stmt.executeUpdate(template_sql);
				sql_stmt.executeUpdate(binding_sql);
				
		    }
		    


			//			System.out.println("cost:"+cost+"\n"+StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ")+"\n"+StringUtils.join(ssss.toArray()," "));
			//			prev_cost = cost;
			//			StructureMappingAntlr4.grammar_id++;

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
//			e1.printStackTrace();
			System.out.println(example_sql);
			System.out.println(template_sql);
			System.out.println(binding_sql);
			//			System.out.println(e11);
			//			System.out.println(e122);
			e1.printStackTrace();
		}

	}

	public void doupdateCheckingFail(Connection c, boolean pass){

		
		try {
			//			if(!isEqualTemplate){
			//					if(!listner_swift.hasErrors() && !listner.hasErrors() && cost!=prev_cost){
//			System.out.println("doupdate"+template_sql);

			java.sql.Statement sql_stmt = c.createStatement();
			c.setAutoCommit(true);
			//			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(example_sql);
			sql_stmt.executeUpdate(template_sql);
			sql_stmt.executeUpdate(binding_sql);

			//			System.out.println("cost:"+cost+"\n"+StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ")+"\n"+StringUtils.join(ssss.toArray()," "));
			//			prev_cost = cost;
			//			StructureMappingAntlr4.grammar_id++;

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println(example_sql);
			System.out.println(template_sql);
			System.out.println(binding_sql);
			//			System.out.println(e11);
			//			System.out.println(e122);
			e1.printStackTrace();
		}

	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		MappingOutput target = (MappingOutput)obj;
		return (template_a.equals(target.template_a) && template_s.equals(target.template_s) && target.cost==cost);
//		return template_a.equals(target.template_a) || template_s.equals(target.template_s);
	}
	public static Comparator<MappingOutput> MappingOutputComparator
	= new Comparator<MappingOutput>() {

		public int compare(MappingOutput fruit1, MappingOutput fruit2) {


			//ascending order
			return new Double(fruit1.cost).compareTo(new Double(fruit2.cost));

			//descending order
			//return fruitName2.compareTo(fruitName1);
		}

	};
	
	public boolean hasTemplateError(){
		return hasError;
	}
	static int k=1;
	
	
	public static void updateNextMapping(Connection c, LinkedHashMap<OpProperty, OpProperty> nextMapping, String aname, String sname) throws SQLException{
		java.sql.Statement sql_stmt = c.createStatement();
		c.setAutoCommit(true);
		//			sql_stmt.executeUpdate(e12);	
		Iterator<OpProperty> iterr = nextMapping.keySet().iterator();
	
		while(iterr.hasNext()){
			OpProperty a_key = iterr.next();
			OpProperty s_value = nextMapping.get(a_key);
//			LineCharIndex.contextToLineIndex(a_key);
//			LineCharIndex.contextToLineIndex(s_value);
//			String const_sql=SQLiteJDBC.insertTableConstant(QueryExamples.idd, a_key.strASTNode, a_key.type, s_value.type, s_value.strASTNode, a_key.binding,LineCharIndex.contextToLineIndex(a_key.node).toString() , "", LineCharIndex.contextToLineIndex(s_value.node).toString());
			String const_sql=SQLiteJDBC.updateTableConstant(QueryExamples.idd, a_key.strASTNode, a_key.type, s_value.type, s_value.strASTNode, aname,LineCharIndex.contextToLineIndex(a_key.node).toString() , sname, LineCharIndex.contextToLineIndex(s_value.node).toString());
//			System.out.println("const_sql"+const_sql);
//			const_sql=SQLiteJDBC.insertTableConstant(
			//	
			//System.err.println("seq		"+ListUtils.longestCommonSubsequence(a_key.strASTNode, a_key.binding));
//						System.out.println("const_sqls	"+const_sql);
			sql_stmt.executeUpdate(const_sql);	
			k++;
		}
//		sql_stmt.executeUpdate(example_sql);
//		sql_stmt.executeUpdate(template_sql);
//		sql_stmt.executeUpdate(binding_sql);
	}
	
	public static void updateNextMapping2(Connection c, LinkedHashMap<OpProperty, OpProperty> nextMapping, String aname, String sname) throws SQLException{
		java.sql.Statement sql_stmt = c.createStatement();
		c.setAutoCommit(true);
		//			sql_stmt.executeUpdate(e12);	
		Iterator<OpProperty> iterr = nextMapping.keySet().iterator();
	
		while(iterr.hasNext()){
			OpProperty a_key = iterr.next();
			OpProperty s_value = nextMapping.get(a_key);
//			LineCharIndex.contextToLineIndex(a_key);
//			LineCharIndex.contextToLineIndex(s_value);
			String const_sql=SQLiteJDBC.insertTableConstant(QueryExamples.idd, a_key.strASTNode, a_key.type, s_value.type, s_value.strASTNode, a_key.binding,LineCharIndex.contextToLineIndex(a_key.node).toString() , "", LineCharIndex.contextToLineIndex(s_value.node).toString());
//			String const_sql=SQLiteJDBC.updateTableConstant(QueryExamples.idd, a_key.strASTNode, a_key.type, s_value.type, s_value.strASTNode, aname, "<CGRect>.width()" , sname, "<__C.CGRect>.size.width");
//			String const_sql=SQLiteJDBC.updateTableConstant(QueryExamples.idd, a_key.strASTNode, a_key.type, s_value.type, s_value.strASTNode, aname, "<CGRect>.width()" , sname, "<__C.CGRect>.size.width");

//			const_sql=SQLiteJDBC.insertTableConstant(
			//	
			//System.err.println("seq		"+ListUtils.longestCommonSubsequence(a_key.strASTNode, a_key.binding));
//						System.out.println("const_sqls	"+const_sql);
			sql_stmt.executeUpdate(const_sql);	
			k++;
		}
//		sql_stmt.executeUpdate(example_sql);
//		sql_stmt.executeUpdate(template_sql);
//		sql_stmt.executeUpdate(binding_sql);
	}
	
	public static void updateNextMapping(Connection c, LinkedHashMap<OpProperty, OpProperty> nextMapping) throws SQLException{
		java.sql.Statement sql_stmt = c.createStatement();
		c.setAutoCommit(true);
		//			sql_stmt.executeUpdate(e12);	
		Iterator<OpProperty> iterr = nextMapping.keySet().iterator();
	
		while(iterr.hasNext()){
			OpProperty a_key = iterr.next();
			OpProperty s_value = nextMapping.get(a_key);
//			LineCharIndex.contextToLineIndex(a_key);
//			LineCharIndex.contextToLineIndex(s_value);
//			String const_sql=SQLiteJDBC.insertTableConstant(QueryExamples.idd, a_key.strASTNode, a_key.type, s_value.type, s_value.strASTNode, a_key.binding,LineCharIndex.contextToLineIndex(a_key.node).toString() , "", LineCharIndex.contextToLineIndex(s_value.node).toString());
			String const_sql=SQLiteJDBC.updateTableConstant(QueryExamples.idd, a_key.strASTNode, a_key.type, s_value.type, s_value.strASTNode, a_key.binding,LineCharIndex.contextToLineIndex(a_key.node).toString() , "", LineCharIndex.contextToLineIndex(s_value.node).toString());

//			const_sql=SQLiteJDBC.insertTableConstant(
			//	
			//System.err.println("seq		"+ListUtils.longestCommonSubsequence(a_key.strASTNode, a_key.binding));
//						System.out.println("const_sqls	"+const_sql);
			sql_stmt.executeUpdate(const_sql);	
			k++;
		}
//		sql_stmt.executeUpdate(example_sql);
//		sql_stmt.executeUpdate(template_sql);
//		sql_stmt.executeUpdate(binding_sql);
	}

}
