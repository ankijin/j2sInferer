package alignment.ruleinfer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Interval;

import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import alignment.common.CommonOperators;
import alignment.common.MappingOutput;
import alignment.common.OpProperty;

import alignment.ruleinfer.StructureMappingAntlr4_for_stmt.Pair;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaBaseVisitor;
import antlr_parsers.javaparser.JavaParser;
import templatechecker.DSLErrorListener;
import templatechecker.TemplateCBaseListener;
import templatechecker.TemplateCBaseVisitor;
import templatechecker.TemplateCLexer;
//import templatechecker.TemplateCListener;
import templatechecker.TemplateCParser;
import templatechecker.TemplateCParser.ArgContext;

/**
 * @author kijin
 *
 */
public class IterativeMapping {

	public static MappingOutput iteration(ParserRuleContext ctxt_java, CommonTokenStream javaCommonStream, ParserRuleContext ctxt_swft, CommonTokenStream swiftCommonStream, int cycle, String afile, String sfile) throws ClassNotFoundException{
		//		LinkedHashMap<OpProperty, OpProperty> nextmapping = new LinkedHashMap<OpProperty, OpProperty>();
		StructureMappingAntlr4_for_stmt mapping=null;
		StructureMappingAntlr4_for_stmt common_op = null;
		if(ctxt_java!=null &&ctxt_swft!=null){
			try {

				CommonOperators.init();
				common_op = new StructureMappingAntlr4_for_stmt(ctxt_java, ctxt_swft);
				common_op.swiftCommonStream 	= swiftCommonStream;
				common_op.androidCommonStream 	= javaCommonStream;
				//special matching
				if(ctxt_java.getText().charAt(ctxt_java.getText().length()-1)!=';' && !StructureMappingAntlr4_for_stmt.java_keywords_list.contains(ctxt_java.start.getText())){				
					common_op.initialize();
					//					System.out.println("constraint_a	"+common_op.constraint+"  "+common_op.common_op_alignment);
					//					System.out.println("type "+common_op.android_listener.type);
				}
				mapping = new StructureMappingAntlr4_for_stmt(ctxt_java, ctxt_swft);
				mapping.swiftCommonStream 	= swiftCommonStream;
				mapping.androidCommonStream = javaCommonStream;


			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}



			/** rule1
			 * stop when cost equals
			 */
			List<MappingOutput> outputs = new ArrayList<MappingOutput>();
			double c1 = 1.0000, c2 = 1.0001;
			//			cycle = 10;/
			boolean isDot = false;
			for(int q=0;q<=cycle && mapping!=null;q++){
				boolean templateOk = false;
				boolean flip = true;
				int asize =0, ssize=0;
				//				for(int q=0;q<=cycle && mapping!=null ;q++){
				try {
					//initialize get next candidates
					if(common_op.common_op_alignment.keySet().size()>0){
						List<CommonOperators> com_a = new LinkedList<CommonOperators>(), com_s = new LinkedList<CommonOperators>();
						List<CommonOperators> operators_src = new ArrayList<CommonOperators>(common_op.common_op_alignment.keySet());
						Collections.sort(operators_src);

						CommonOperators hop_a = operators_src.get(0);
						CommonOperators hop_s = common_op.common_op_alignment.get(hop_a);


						com_a.add(hop_a);
						com_s.add(hop_s);

						Iterator<CommonOperators> ittta = common_op.common_op_alignment.keySet().iterator();
						Iterator<CommonOperators> ittts = common_op.common_op_alignment.values().iterator();
						while(ittta.hasNext()){
							CommonOperators abb= ittta.next();
							if((!com_a.contains(abb)) && (hop_a.depth==abb.depth)){
								com_a.add(abb);
							}
							//						if(!com_a.contains(abb)){
							//							com_a.add(abb);
							//						}
						}
						//						int hop = CommonOperators.op_encoding.get(hop_s.text);
						while(ittts.hasNext()){
							CommonOperators abb= ittts.next();
							if(!com_s.contains(abb) && hop_s.depth==abb.depth){
								com_s.add(abb);

							}else{
								if(CommonOperators.op_encoding.get(hop_s.text)!=null && (CommonOperators.op_encoding.get(abb.text)!=null)){
									if(CommonOperators.op_encoding.get(hop_s.text) <= CommonOperators.op_encoding.get(abb.text)){
										com_s.add(abb);
									}
								}

							}//not same depth however if it has same ... 

						}
						//						System.out.println("com_a "+com_a+" com_s"+com_s+"\n"+CommonOperators.op_encoding);
						if(!common_op.android_listener.type.equals("EnclosedContext")){
							mapping.android_listener.highestop = com_a;
							mapping.swift_listner.highestop = com_s;
							if((com_a.get(0).text.equals(com_s.get(0).text)) && com_a.get(0).text.equals("."))
								isDot = true;
//							System.out.println("isDot = true;");
						}
					}

					List<CommonOperators> operators_src = new ArrayList<CommonOperators>(common_op.common_op_alignment.keySet());
					Collections.sort(operators_src);


					List<CommonOperators> android_terms = new ArrayList<CommonOperators>(mapping.android_listener.termsMapOnlys);
					Collections.sort(android_terms);
//					System.out.println("AAAAA"+android_terms);
					//AAAAA[(5)6..6:(, (5)8..8:), (6)4..4:.]
					// argX.argXX(..), get argXX
					if(android_terms.size()>2 && android_terms.get(0).text.equals("(") && android_terms.get(1).text.equals(")") && android_terms.get(2).text.equals(".")){
						isDot = true;
						Interval interval = android_terms.get(2).interval;
						Interval interval2 = new Interval(interval.a+1, interval.b+1);
						//						interval.a=interval.a+1; interval.b=interval.b+1;
//						System.out.println("EEEEEE"+android_terms.get(0).text+" "+android_terms.get(1).text+" "+ctxt_java.getText()+">>>		"+android_terms.get(2).text);

						List<OpProperty>  methodIdentifier_j =  new LinkedList<OpProperty>();
						JavaBaseListener lsnr = new JavaBaseListener(){
							public void visitTerminal(TerminalNode node) {

								if(node.getSourceInterval().equals(interval)){
//									System.out.println("@"+node.getSourceInterval()+" "+node.getText());
									methodIdentifier_j.add(new OpProperty(node, node.getText(),"common"));
								}
								if(node.getSourceInterval().equals(interval2)){
//									System.out.println("@"+node.getSourceInterval()+" "+node.getText());
									methodIdentifier_j.add(new OpProperty(node, node.getText(),"common"));
								}
								if(node.getSourceInterval().equals(android_terms.get(0).interval)){
//									System.out.println("@"+node.getSourceInterval()+" "+node.getText());
									methodIdentifier_j.add(new OpProperty(node, node.getText(),"common"));
								}
								if(node.getSourceInterval().equals(android_terms.get(1).interval)){
//									System.out.println("@"+node.getSourceInterval()+" "+node.getText());
									methodIdentifier_j.add(new OpProperty(node, node.getText(),"common"));
								}
							};
						};

						ParseTreeWalker walker = new ParseTreeWalker();
						walker.walk(lsnr, ctxt_java);
						mapping.android_listener.constantlist = methodIdentifier_j;
//						System.out.println("mapping.android_listener.constantlist"+mapping.android_listener.constantlist);
					}

					//get identifers of method calls
					if(mapping.android_listener.highestop!=null && mapping.android_listener.highestop.get(0)!=null && mapping.android_listener.highestop.get(0).text.equals(".")){
						Interval interval = mapping.android_listener.highestop.get(0).interval;
						interval.a=interval.a+1; interval.b=interval.b+1;
//						System.out.println("mapping.android_listener.highestop"+mapping.android_listener.highestop.get(0).text+"  "+mapping.android_listener.highestop.get(0).interval);
//						System.err.println(mapping.androidCommonStream.getText(interval));

						List<OpProperty>  methodIdentifier_j =  new LinkedList<OpProperty>();
						JavaBaseListener lsnr = new JavaBaseListener(){
							public void visitTerminal(TerminalNode node) {

								if(node.getSourceInterval().equals(interval)){
//									System.out.println("@"+node.getSourceInterval()+" "+node.getText());
									methodIdentifier_j.add(new OpProperty(node, node.getText(),"common"));
								}
							};
						};

						ParseTreeWalker walker = new ParseTreeWalker();
						walker.walk(lsnr, ctxt_java);
						mapping.android_listener.constantlist = methodIdentifier_j;
//						System.out.println("mapping.android_listener.constantlist"+mapping.android_listener.constantlist);


						//						ctxt_java.get
					}


					if(mapping.swift_listner.highestop!=null && mapping.swift_listner.highestop.get(0)!=null && mapping.swift_listner.highestop.get(0).text.equals(".")){

						Interval interval = mapping.swift_listner.highestop.get(0).interval;
						interval.a=interval.a+1; interval.b=interval.b+1;
//						System.out.println("mapping.swift_listner.highestop"+mapping.swift_listner.highestop.get(0).text+"  "+mapping.swift_listner.highestop.get(0).interval);
//						System.err.println(mapping.androidCommonStream.getText(interval));

						List<OpProperty>  methodIdentifier_j =  new LinkedList<OpProperty>();
						JavaBaseListener lsnr = new JavaBaseListener(){
							public void visitTerminal(TerminalNode node) {

								if(node.getSourceInterval().equals(interval)){
//									System.out.println("@"+node.getSourceInterval()+" "+node.getText());
									methodIdentifier_j.add(new OpProperty(node, node.getText(),"common"));
								}
							};
						};

						ParseTreeWalker walker = new ParseTreeWalker();
						walker.walk(lsnr, ctxt_swft);
						mapping.swift_listner.constantlist2 = methodIdentifier_j;
//						System.out.println("mapping.swift_listner.constantlist"+mapping.android_listener.constantlist);

					}

					//getting positions of common operators
					mapping.initialize();

					/**
					 * feed back the highest op to split the ast
					 */

					//					Iterator<CommonOperators> iterr = mapping.common_op_alignment.keySet().iterator();
					//					iterr.next();

					//					CommonOperators highestop_j = mapping.common_op_alignment.keySet().iterator().next();
					//					CommonOperators highestop_s	= mapping.common_op_alignment.get(highestop_j);
					mapping.afilename = afile;
					mapping.sfilename = sfile;

					//					Iterator<OpProperty> ita = mapping.android_listener.op_a_nodes;


//					System.out.println("and terms"+mapping.android_listener.termsMapOnlys);
//					System.out.println("swft terms"+mapping.swift_listner.termsMapOnlys);

					/*
					System.out.println("and terms"+mapping.android_listener.termsMap);
					System.out.println("swft terms"+mapping.swift_listner.termsMap);


//					System.out.println("and termsMapOnlys"+mapping.android_listener.termsMapOnlys);
//					System.out.println("swft termsMapOnlys"+mapping.swift_listner.termsMapOnlys);

					//					System.err.println("scope_constraint"+mapping.android_listener.scope_constraint);
					 */
					//					System.out.println("swft termsMapOnlys"+mapping.swift_listner.termsMap);
					asize = mapping.android_listener.termsMap.size();
					ssize = mapping.swift_listner.termsMap.size();
					//					common_op

					//					System.out.println(x);
					//					System.out.println("blcklista"+mapping.android_listener.blacklist);
					//					System.out.println("blcklists"+mapping.swift_listner.blacklist);
					
//					System.out.println("op-a"+mapping.android_listener.op_a_nodes.size()+mapping.android_listener.op_a_nodes);
//					System.out.println("op-s"+mapping.swift_listner.op_s_nodes.size()+mapping.swift_listner.op_s_nodes);

					c2 =c1;
					//					System.out.print("");




					MappingOutput output = mapping.compute();

					Iterator<OpProperty> aaaaaa = mapping.android_listener.op_a_nodes.iterator();
					List<Interval> listc 		= new LinkedList<Interval>();
					HashSet<OpProperty> set 	= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint_a = mapping.android_listener.scope_constraint;
					int count_a = 0, count_s=0;

					Iterator<String> decls = constraint_a.keySet().iterator();
					//					System.out.println("constraint"+constraint);
					while(decls.hasNext()){
						listc = constraint_a.get(decls.next());
						//						boolean isMet = true;
						//check if template have same args
						while(aaaaaa.hasNext()){
							OpProperty anode = aaaaaa.next();
							Interval nnnnn = anode.interval;
							for(int i = 0; i< listc.size();i++){

								if(!listc.get(i).equals(nnnnn) && !listc.get(i).disjoint(nnnnn)){
									//									System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set.add(anode);
									count_a++;
									if(!mapping.android_listener.blacklist.contains(anode)){
										mapping.android_listener.blacklist.add(anode);
										//										System.out.println("@@@@1"+anode);
									}
								}
							}
						}
					}//scoping constraints in java
					//don't exist

//					System.out.println("mapping.android_listener.blacklis"+mapping.android_listener.blacklist);


					List<Interval> listc_s 		= new LinkedList<Interval>();
					HashSet<OpProperty> set_s 			= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint_s = mapping.swift_listner.scope_constraint;

					Iterator<String> decls_s = constraint_s.keySet().iterator();
					//										System.out.println("constraint_s"+constraint_s);
					while(decls_s.hasNext()){
						listc_s = constraint_s.get(decls_s.next());
						//						System.out.println("listc_s"+listc_s);;
						Iterator<OpProperty> ssssss = mapping.swift_listner.op_s_nodes.iterator();
						//						boolean isMet = true;
						//check if template have same args
						while(ssssss.hasNext()){
							OpProperty snode = ssssss.next();
							Interval nnnnn = snode.interval;
							//							System.out.println("nnnnn"+nnnnn);
							for(int i = 0; i< listc_s.size();i++){

								if(!listc_s.get(i).equals(nnnnn) && !listc_s.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set_s.add(snode);
									count_s++;
									if(!mapping.swift_listner.blacklist.contains(snode)){
										mapping.swift_listner.blacklist.add(snode);
										//										System.err.println("adding"+snode+" decls");

										int h = snode.height;
										String type = snode.property;
										for(OpProperty op: mapping.swift_listner.op_s_nodes){
											if(type.equals(op.property)&& op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(op)){
												mapping.swift_listner.blacklist.add(op);
												System.err.println("aaaadding friends"+op);

											}
										}

									}
								}
							}
						}
					}//scoping constraints in swift
					//don't exist

					int unmet=0;
					Iterator<Interval> c_a = mapping.constraint.keySet().iterator();
					while(c_a.hasNext()){
						Interval i_c_a = c_a.next();
						Interval i_c_s = mapping.constraint.get(i_c_a);
						Iterator<Pair> iiit = mapping.android_to_swift_point.keySet().iterator();
						while(iiit.hasNext()){
							Pair p = iiit.next();
							if(!p.a_intval.disjoint(i_c_a) && p.b_intval.disjoint(i_c_s) && !mapping.android_listener.blacklist.contains(p.a_op)){
								mapping.android_listener.blacklist.add(p.a_op);
								p.a_op.whyBlacklist = "!p.a_intval.disjoint(i_c_a) && p.b_intval.disjoint(i_c_s)";
								count_a++;
								//								System.out.println("testing22");
							}
							else if(p.a_intval.disjoint(i_c_a) && !p.b_intval.disjoint(i_c_s) && !mapping.swift_listner.blacklist.contains(p.s_op)){
								p.s_op.whyBlacklist="p.a_intval.disjoint(i_c_a) && !p.b_intval.disjoint(i_c_s)";
								mapping.swift_listner.blacklist.add(p.s_op);
								count_s++;
								//								System.out.println("testing11");
							}
							if(!mapping.constraintMet(p.a_intval, p.b_intval))
								unmet++;
							//							Double aa = mapping.android_to_swift_point.get(p);
						}

					}


					//					for(OpProperty op: mapping.android_listener.op_a_nodes){
					//						for(OpProperty os: mapping.swift_listner.op_s_nodes){

					//						}
					//					}
					Set<String> argA = new HashSet<String>(), argS = new HashSet<String>();
					Set<String> diffArgs = new HashSet<String>();

					if(output!=null){
						c1=output.cost;
						//						c1 = output.cost;
						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						String template_a = output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", "");

						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));


						Lexer lexer1 				= new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_a));
						DSLErrorListener listner 	= new DSLErrorListener();
						lexer1.addErrorListener(listner);


						templatechecker.TemplateCParser parser 		= new TemplateCParser(new CommonTokenStream(lexer1));
						//				toskip = false;
						//				ExceptionErrorStrategy error_handler = new ExceptionErrorStrategy();
						//				parser.removeErrorListeners();
						//				parser.setErrorHandler(error_handler);

						TemplateCParser.TemplateContext comj = parser.template();
						List<String> argSwift = new LinkedList<String>(), argAndroid = new LinkedList<String>();
						comj.accept(new TemplateCBaseVisitor(){
							@Override
							public Object visitArg(ArgContext ctx) {
								// TODO Auto-generated method stub
								argAndroid.add(ctx.getText());
								argA.add(ctx.getText());

								return super.visitArg(ctx);
							}
						});

						//						if(listner.hasErrors()){
						//												System.out.println("error in"+tempa);
						//						}


						TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
						DSLErrorListener listner_swift = new DSLErrorListener();
						lexer2.addErrorListener(listner_swift);



						TemplateCParser parser2 = new TemplateCParser(new CommonTokenStream(lexer2));
						//				parser.setErrorHandler(error_handler);
						TemplateCParser.TemplateContext com = parser2.template();

						ParseTreeWalker walker_android = new ParseTreeWalker(), walker_swift = new ParseTreeWalker();

						walker_android.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argAndroid.add(ctx.getText());
								argA.add(ctx.getText());
								diffArgs.add(ctx.getText().replaceAll("\\<", "").replaceAll("\\>", ""));
							};
						}, comj);




						walker_swift.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argSwift.add(ctx.getText());
								argS.add(ctx.getText());
								String arg=ctx.getText().replaceAll("\\<", "").replaceAll("\\>", "");
								diffArgs.removeIf((String emp) -> emp.equals(arg));
							};
						}, com);


						//						ST st_android = new ST(output.template_a);
						//						while(iterator.hasNext()){
						//							String key=iterator.next();
						//							st_android.add(key, argmap.get(key));
						//						};
						String changed =null;

						if(!diffArgs.isEmpty()){
							Iterator<String> diffI = diffArgs.iterator();
							while(diffI.hasNext()){
								String aarrr=diffI.next();

								//								output.aamap.containsValue("<"+aarrr+">");
								Iterator<OpProperty> ittt = output.aamap.keySet().iterator();
								while(ittt.hasNext()){
									OpProperty key = ittt.next();
									if(output.aamap.get(key).equals("<"+aarrr+">")){
										//										changed= output.template_a.replace("<"+aarrr+">", key.strASTNode);
										//										System.out.println("aarrr"+aarrr+" "+key.strASTNode+"  "+changed);
									}
								}

							}
						}
						//						output.template_a = st_android.render();
						//						System.out.println("output.template_a"+output.template_a);
						//*						System.out.println("output.template		"+output.template_a+"    "+output.template_s);
						//						if(1==1){
						boolean templatecheck = true;
//						if(mapping.android_listener.highestop!=null && mapping.android_listener.highestop.get(0)!=null && mapping.android_listener.highestop.get(0).text.equals(".")){
							if(isDot){
								
							templatecheck = true;
//							System.out.println("TTTTEMPLATE"+output.template_a+"  "+output.template_s);
						}
						else{
							templatecheck = !listner_swift.hasErrors() && !listner.hasErrors();
						}
						//						if(!listner_swift.hasErrors() && !listner.hasErrors()){
						if(templatecheck){

							if(!(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>") )){

								//								checkJavaSyntax("if (arg0) {}", 70)
								//								for ( <arg2> <arg0> = <arg3> ; <arg0> < <arg1> ; <arg0> ++ ) {...}
								//								replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
								//								String unitt = output.template_a.replaceAll("<arg[]>", " ").replaceAll(">", "  ").replaceAll("\\.\\.\\.", "");

								//								String unitt = output.template_a.replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
								String unitt = getAtom(output.template_a).replaceAll("\\.\\.\\.", "");

								String unitt_for_s = getAtom(output.template_s).replaceAll("\\.\\.\\.", "");
								//*								System.out.println("unitt"+unitt+"  "+unitt_for_s);
								if(changed!=null){
									//									changed= changed.replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
									changed	= getAtom(changed).replaceAll("\\.\\.\\.", "");
									unitt 	= changed;
								}


								DSLErrorListener listn = checkJavaSyntax(unitt, ctxt_java.getRuleIndex());
								DSLErrorListener listn_for_s = checkSwiftSyntax(unitt_for_s, ctxt_swft.getRuleIndex());
								//*									System.out.println("listn"+listn.hasErrors()+"  "+listn_for_s.hasErrors());
								//								System.out.println(comj.getRuleIndex()+"  "+output.template_a+"	unitt"+unitt+"  "+checkJavaSyntax(unitt, ctxt_java.getRuleIndex()));
								//								if(!checkJavaSyntax(unitt, ctxt_java.getRuleIndex())&& count_a==0 && count_s==0){
								//									if(!checkJavaSyntax(unitt, ctxt_java.getRuleIndex())&& count_a==0 && count_s==0){
								if(!listn.hasErrors()&& (!listn_for_s.hasErrors()) && (count_a==0 && count_s==0)){	
									//									if((count_a==0 && count_s==0)){	
									//									if(output.template_s.replaceAll(" ","").equals(anObject))
//									System.out.println("@@@listn"+listn.hasErrors()+"  "+listn_for_s.hasErrors()+" "+count_a+" "+count_s+"  "+output);
//									System.err.println("@@@listn"+output.template_sql+"  "+output.example_sql);
									outputs.add(output);
									if(isDot) return output;
									//*										System.out.println("outputs.add(output)"+outputs.add(output));;
									cycle =1;
								}else{
									String text=listn.getError();
									String pattern = "arg[0-9]+";
									Pattern r = Pattern.compile(pattern);
									Matcher m = r.matcher(text);
									StringBuffer sb = new StringBuffer();
									while (m.find()) {
										//										System.out.println(" gggg0	"+m.group(0));
										//										System.out.println(output.aamappp);
										Iterator<OpProperty> aaaaa = output.aamappp.keySet().iterator();
										while(aaaaa.hasNext()){
											OpProperty nxt = aaaaa.next();
											String val = output.aamappp.get(nxt);
											if(val.equals(m.group(0))){
												if(!mapping.android_listener.blacklist.contains(nxt)){
													nxt.whyBlacklist="template has Error";
													mapping.android_listener.blacklist.add(nxt);
													int h = nxt.height;
													String type = nxt.property;
													for(OpProperty op: mapping.android_listener.op_a_nodes){
														if(type.equals(op.property) && op.height==h && !op.property.equals("enclosed{}")){
															if(!mapping.android_listener.blacklist.contains(op)){
																op.whyBlacklist="same height min val for anum < snum";
																mapping.android_listener.blacklist.add(op);
															}
															//															System.out.println("@@@@5"+mapping.android_listener.op_a_nodes.get(min.getKey().a));

														}
													}

												}
											}


										}
										//									    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(2)), ""));
									}

									text = listn_for_s.getError();
									m = r.matcher(text);
									while (m.find()) {
										//										System.out.println(" gggg0	"+m.group(0));
										//										System.out.println(output.ssmappp);
										Iterator<OpProperty> aaaaa = output.ssmappp.keySet().iterator();
										while(aaaaa.hasNext()){
											OpProperty nxt = aaaaa.next();
											String val = output.ssmappp.get(nxt);
											if(val.replaceAll(" ","").equals(m.group(0).replaceAll(" ",""))){

												if(!mapping.swift_listner.blacklist.contains(nxt) && nxt.interval.length()>1){
													nxt.whyBlacklist ="swifting error";
													mapping.swift_listner.blacklist.add(nxt);
												}
											}
										}
										//									    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(2)), ""));
									}

									//									arg[0-9]+
								}
								//								}
							}
						}
					}

					int anum=0;
					int snum=0;
					//					System.out.println("a-snum"+anum+" "+snum);
					//					if(mapping.android_listener.op_a_nodes.size() < mapping.swift_listner.op_s_nodes.size()){
					//					if(anum==0 || snum==0){
					anum = mapping.android_listener.op_a_nodes.size();
					snum = mapping.swift_listner.op_s_nodes.size();

					if(anum < snum){
						//							if(flip){
						//						System.out.println("android mapping	"+mapping.android_to_swift_point);
						//							System.out.println("larger"+mapping.android_listener.op_a_nodes);
						//							System.err.println("larger"+mapping.swift_listner.op_s_nodes);

						//							System.out.println("mapping	"+mapping.android_to_swift_point);
						if(mapping.android_to_swift_point.size()>0){

							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});
							if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(min.getKey().a))&& !mapping.android_listener.op_a_nodes.get(min.getKey().a).property.equals("enclosed{}")){
								mapping.android_listener.op_a_nodes.get(min.getKey().a).whyBlacklist="min val for anum < snum";
								mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(min.getKey().a));
							}
							int h = mapping.android_listener.op_a_nodes.get(min.getKey().a).height;
							String type = mapping.android_listener.op_a_nodes.get(min.getKey().a).property;
							for(OpProperty op: mapping.android_listener.op_a_nodes){
								if(type.equals(op.property) && op.height==h && !op.property.equals("enclosed{}")){
									if(!mapping.android_listener.blacklist.contains(op)){
										op.whyBlacklist="same height min val for anum < snum";
									}
								}
							}
						}
					}


					else if(anum > snum){
						//					else if(mapping.android_listener.op_a_nodes.size() > mapping.swift_listner.op_s_nodes.size()){
						//													System.out.println("swift mapping	"+mapping.android_to_swift_point);
						//													System.out.println(mapping.android_listener.op_a_nodes);

						if(mapping.android_to_swift_point.size()>0){
							Entry<Pair, Double> min = Collections.max(mapping.android_to_swift_point.entrySet(), new Comparator<Entry<Pair, Double>>() {
								public int compare(Entry<Pair, Double> entry1, Entry<Pair, Double> entry2) {
									return entry1.getValue().compareTo(entry2.getValue());
								}
							});


							//								System.out.println("|a|<|s|"+min.getKey()+"\n"+mapping.swift_listner.op_s_nodes.get(min.getKey().b)+"\n"+mapping.android_listener.op_a_nodes.get(min.getKey().a));
							//								black_s.clear();

							//							if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))){

							//							}

							if(mapping.swift_listner.op_s_nodes.get(min.getKey().b)!=null && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(min.getKey().b))&& !mapping.swift_listner.op_s_nodes.get(min.getKey().b).property.equals("enclosed{}")){
								//								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								//								System.err.println("adding op_a>op_s"+mapping.swift_listner.op_s_nodes.get(min.getKey().b));
								mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(min.getKey().b));
							}
							int h = mapping.swift_listner.op_s_nodes.get(min.getKey().b).height;
							String type = mapping.swift_listner.op_s_nodes.get(min.getKey().b).property;
							for(OpProperty op: mapping.swift_listner.op_s_nodes){
								if(type.equals(op.property)&& op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(op)){
									mapping.swift_listner.blacklist.add(op);
									//									System.err.println("aaaadding friends"+op);

								}
							}



							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);

								if(aa >=0 && ss>=0){
									int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);
									//									System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

									if(decision ==2){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											Iterator<Interval> itttt = mapping.constraint.values().iterator();
											//									   while(itttt.hasNext()){
											//										   Interval constrainttt = itttt.next();
											//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
											//											System.err.println("adding blck"+mapping.swift_listner.op_s_nodes.get(ss));
											//									   }

										}
										//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										//												}
										//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
									}
									else if(decision ==1){
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){

											mapping.android_listener.op_a_nodes.get(aa).whyBlacklist="same & decision 1";
											//											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											//											System.out.println("@@@@6"+mapping.android_listener.op_a_nodes.get(aa));

										}
									}
									else if(decision == 0){
										if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
											//											System.err.println("decision == 0			"+mapping.swift_listner.op_s_nodes.get(ss));
											mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										}
										if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
											mapping.android_listener.op_a_nodes.get(aa).whyBlacklist = "same and decision 0";
											//											mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
											//											System.out.println("@@@decision == 0");
										}
									}
								}
							}


						}
						//							black_s.clear();
						//							black_s.addAll(mapping.swift_listner.op_s_nodes);
						//							System.out.println("black_s.addAll(mapping.swift_listner.op_s_nodes)");
						//						flip = true;
					}
					else if(anum == snum && anum>0 && snum>0){



						int count =0;

						OpProperty sss1 = mapping.swift_listner.op_s_nodes.get(0);
						OpProperty aaa1 = mapping.android_listener.op_a_nodes.get(0);
						//						System.out.println("SIZEEEE"+asize+"  "+ssize);
						//						if(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>") && asize >= 1 && ssize >=1){
						if(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>")){

							//							if(output.){
							//							System.out.println("aaaa");
							char end=aaa1.strASTNode.charAt(aaa1.strASTNode.length() - 1);
							char ends=sss1.strASTNode.charAt(sss1.strASTNode.length() - 1);

							//special matching
							if(end ==';'){
								//								System.out.println("end ==';'"+((ParserRuleContext)aaa1.node).children.size());
								//								((ParserRuleContext)aaa1.node).children.size();
								if(((ParserRuleContext)aaa1.node).children.size()>2){
									if(!mapping.android_listener.blacklist.contains(aaa1)){
										aaa1.whyBlacklist="<arg0> to <arg0>";
										mapping.android_listener.blacklist.add(aaa1);
									}
									if(!mapping.swift_listner.blacklist.contains(sss1)){
										sss1.whyBlacklist="<arg0> to <arg0>";
										mapping.swift_listner.blacklist.add(sss1);
									}
								}
								else{
									if(!mapping.android_listener.blacklist.contains(aaa1)){
										aaa1.whyBlacklist="<arg0> to <arg0>";
										mapping.android_listener.blacklist.add(aaa1);
									}
								}
							}
							//plat-matching
							else{
								if(!mapping.android_listener.blacklist.contains(aaa1)){
									aaa1.whyBlacklist="<arg0> to <arg0>";
									mapping.android_listener.blacklist.add(aaa1);
								}
								if(!mapping.swift_listner.blacklist.contains(sss1)){
									sss1.whyBlacklist="<arg0> to <arg0>";
									mapping.swift_listner.blacklist.add(sss1);
								}
							}
							//							} else return null;

						}
						else{
							//							return null;



							//							mapping.swift_listner.op_s_nodes.get
							Iterator<Integer> a_keys = mapping.android_to_swift.keySet().iterator();
							while(a_keys.hasNext()){
								int aa=a_keys.next();
								int ss = mapping.android_to_swift.get(aa);
								int decision = CommonOperators.determineGoDownbyNum(mapping.android_listener.op_a_nodes.get(aa).strASTNode, mapping.swift_listner.op_s_nodes.get(ss).strASTNode);

								//								System.out.println("eq_size decision:"+decision+"   "+mapping.android_listener.op_a_nodes.get(aa)+"   "+mapping.swift_listner.op_s_nodes.get(ss));

								//								decision = -1;
								if(decision ==-1){
									//do nothiing

								}
								if(decision ==2){
									if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
										//									Iterator<Interval> itttt = mapping.constraint.values().iterator();
										//									   while(itttt.hasNext()){
										//										   Interval constrainttt = itttt.next();
										//										   if(constrainttt.properlyContains(mapping.swift_listner.op_s_nodes.get(ss).interval)&& constrainttt.length()>mapping.swift_listner.op_s_nodes.get(ss).interval.length())
										mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										//										System.err.println("decision ==2"+mapping.swift_listner.op_s_nodes.get(ss));
										//										mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
										//									   }
										int h = mapping.swift_listner.op_s_nodes.get(ss).height;
										String type = mapping.swift_listner.op_s_nodes.get(ss).property;
										for(OpProperty op: mapping.swift_listner.op_s_nodes){
											if(type.equals(op.property)&& op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(op)){
												//											mapping.swift_listner.blacklist.add(op);
												//for <arg1> in <arg2> as ! [ <arg0> ] {...}
												//												System.err.println("sssaaaadding friends"+op);

											}
										}
									}
									//												if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
									//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
									//												}
									//												System.out.println("SSSBBBB"+mapping.swift_listner.op_s_nodes.get(ss));
								}
								else if(decision ==1){
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										mapping.android_listener.op_a_nodes.get(aa).whyBlacklist="decision1";
										//										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										//										System.out.println("@@@@ 111");
									}
								}


								//has same operators
								else if(1 == 0){
									//								else if(decision == 0){
									if(!mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss)) && !mapping.swift_listner.op_s_nodes.get(ss).property.equals("enclosed{}")){
										OpProperty sss = mapping.swift_listner.op_s_nodes.get(ss);
										sss.whyBlacklist = "has samely operators";
										mapping.swift_listner.blacklist.add(sss);
										System.err.println("aaaa"+mapping.swift_listner.op_s_nodes.get(ss));
										System.out.println("height"+mapping.swift_listner.op_s_nodes);
										int h = mapping.swift_listner.op_s_nodes.get(ss).height;
										String type = mapping.swift_listner.op_s_nodes.get(ss).property;
										for(OpProperty op: mapping.swift_listner.op_s_nodes){
											if(type.equals(op.property)&& op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(op)){
												mapping.swift_listner.blacklist.add(op);
												System.err.println("aaaadding friends"+op);

											}
										}



									}
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))&& !mapping.android_listener.op_a_nodes.get(aa).property.equals("enclosed{}")){
										mapping.android_listener.op_a_nodes.get(aa).whyBlacklist="decision 0 && same";
										//										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
										//									System.out.println("@@@@1424");
									}
								}

								else{
									//								Iterator<Interval> ittt = mapping.constraint_cluster.keySet().iterator();
									Iterator<Interval> ittt = mapping.constraint.keySet().iterator();
									while(ittt.hasNext()){
										Interval aaa = ittt.next();
										//									Interval sss = mapping.constraint_cluster.get(aaa);
										Interval sss = mapping.constraint.get(aaa);
										Interval ca = mapping.android_listener.op_a_nodes.get(aa).interval;
										Interval cs = mapping.swift_listner.op_s_nodes.get(ss).interval;
										Interval diff_a=ca.differenceNotProperlyContained(aaa);
										Interval diff_s=cs.differenceNotProperlyContained(sss);
										if(!ca.disjoint(aaa) && !cs.disjoint(sss)){
											//											System.out.println("ttttt"+mapping.android_listener.op_a_nodes.get(aa)+"  "+mapping.swift_listner.op_s_nodes.get(ss)+"\n"+diff_a+" "+diff_s);;
											if(diff_a.length() < diff_s.length() && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss))){
												//												mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
												int h = mapping.swift_listner.op_s_nodes.get(ss).height;
												String type = mapping.swift_listner.op_s_nodes.get(ss).property;
												for(OpProperty op: mapping.swift_listner.op_s_nodes){
													if(type.equals(op.property)&& op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(op)){
														//														mapping.swift_listner.blacklist.add(op);
														//														System.err.println("aaaadding friends"+op);

													}
												}

											}else if(diff_a.length() == diff_s.length() && diff_s.length()!=0){
												if(mapping.swift_listner.op_s_nodes.get(ss).strASTNode.length()>mapping.android_listener.op_a_nodes.get(aa).strASTNode.length() && !mapping.swift_listner.blacklist.contains(mapping.swift_listner.op_s_nodes.get(ss))){
													mapping.swift_listner.blacklist.add(mapping.swift_listner.op_s_nodes.get(ss));
													//													System.out.println("    aaaaaaa"+mapping.swift_listner.op_s_nodes.get(ss));
													int h = mapping.swift_listner.op_s_nodes.get(ss).height;
													String type = mapping.swift_listner.op_s_nodes.get(ss).property;
													for(OpProperty op: mapping.swift_listner.op_s_nodes){
														if(type.equals(op.property)&& op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(op)){
															//															mapping.swift_listner.blacklist.add(op);
															//															System.err.println("adding friends"+op);

														}
													}



												}else if(mapping.swift_listner.op_s_nodes.get(ss).strASTNode.length()<mapping.android_listener.op_a_nodes.get(aa).strASTNode.length() && !mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(aa))){
													//													mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(aa));
												}
											}
										}
										//									;
									}
								}



							}
						}
					}//if same # of
					//					black_a = mapping.android_listener.op_a_nodes;
					//						System.out.println("black_a"+black_a);
					//						System.err.println("black_s"+black_s);

					//					black_s = mapping.swift_listner.op_s_nodes;

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(outputs.size()>0){

				MappingOutput minOutput = Collections.min(outputs);

				//										minOutput.doupdate();
				//					outputs.size();

				MappingOutput last =outputs.get(outputs.size()-1);
				//				last.doupdate();
				//				nextmapping = last.nextMapping;
				//updating next level
				//				MappingOutput.updateNextMapping(connection, nextmapping);

				//				if(minOutput.template_a.equals("<arg0> . <arg1>") && minOutput.template_s.equals("<arg0> . <arg1>")){
				//					minOutput.template_a = minOutput.example_a.replaceAll("(.+)\\.", "<arg10>.");
				//					minOutput.template_s = minOutput.example_a.replaceAll("(.+)\\.", "<arg10>.");
//				System.out.println("oooutputs		"+minOutput.template_a+"   ::  "+minOutput.template_s+"  "+minOutput.template_s+" "+minOutput.example_a);

				//				}
				return last;
			}
			else return null;
		}
		return null;
	}









	public static DSLErrorListener checkSwiftSyntax(String template, int atype) throws ClassNotFoundException{

		//		TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
		DSLErrorListener listner_swift = new DSLErrorListener();
		//		lexer2.addErrorListener(listner_swift);
		template = template.replace("- >", "->");
		antlr_parsers.swiftparser.SwiftLexer lexerj = new antlr_parsers.swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(template));
		//		lexerj.addErrorListener(listner_swift);
		CommonTokenStream swiftCommonStream = new CommonTokenStream(lexerj);
		antlr_parsers.swiftparser.SwiftParser parserj = new antlr_parsers.swiftparser.SwiftParser(swiftCommonStream);
		//		parserj.removeErrorListeners();
		parserj.addErrorListener(listner_swift);
		ParserRuleContext comj=null;

		Class<? extends Parser> jparserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		jparserClass = cl.loadClass("antlr_parsers.swiftparser.SwiftParser").asSubclass(Parser.class);

		try {
			Method jstartRule = null;

			/**
			 * get ParserRuleContexts by type ids
			 */
			jstartRule = jparserClass.getMethod(antlr_parsers.swiftparser.SwiftParser.ruleNames[atype]);
			//			sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);

			//			System.out.println("rulej"+JavaParser.ruleNames[atype]);
			//			System.out.println("rules"+SwiftParser.ruleNames[stype]);
			comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
			comj.accept(new antlr_parsers.swiftparser.SwiftBaseVisitor<>());
			//			com 	= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);
			//			System.out.println("testing	"+comj.start.getInputStream().getText(comj.getSourceInterval()));
			//			System.out.println("swift%%%%\n"+template+"\n"+listner_swift.getError());
		} catch (NoSuchMethodException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return listner_swift;
	}


	public static DSLErrorListener checkJavaSyntax(String template, int atype) throws ClassNotFoundException{

		//		TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
		DSLErrorListener java_listener = new DSLErrorListener();
		//		lexer2.addErrorListener(listner_swift);

		antlr_parsers.javaparser.JavaLexer lexerj = new antlr_parsers.javaparser.JavaLexer((CharStream)new ANTLRInputStream(template));
		//		lexerj.addErrorListener(listner_swift);
		CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
		antlr_parsers.javaparser.JavaParser parserj = new antlr_parsers.javaparser.JavaParser(javaCommonStream);
		//		parserj.removeErrorListeners();
		//		parserj.removeParseListeners();
		parserj.addErrorListener(java_listener);
		ParserRuleContext comj=null;

		Class<? extends Parser> jparserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		jparserClass = cl.loadClass("antlr_parsers.javaparser.JavaParser").asSubclass(Parser.class);

		try {
			Method jstartRule = null;

			/**
			 * get ParserRuleContexts by type ids
			 */
			jstartRule = jparserClass.getMethod(JavaParser.ruleNames[atype]);
			comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
			comj.accept(new JavaBaseVisitor<>());

			//			System.out.println("%%%%"+java_listener.getError()+"  "+template+" "+JavaParser.ruleNames[atype]+" "+atype);
		} catch (NoSuchMethodException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return java_listener;
	}
	public static String getAtom(String text){


		String pattern = "(<)arg[0-9]+>";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(text);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			//			System.out.println("g2"+m.group(2)+"g1"+m.group(1)+ " g0"+m.group(0));
			m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));

			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(2)), ""));
		}
		m.appendTail(sb); // append the rest of the contents
		//		System.out.println(sb.toString());

		text = sb.toString();
		pattern = "arg[0-9]+(>)";
		r = Pattern.compile(pattern);
		m = r.matcher(text);
		sb = new StringBuffer();
		while (m.find()) {
			//			System.out.println("g1"+m.group(1)+ " g0"+m.group(0));
			m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
		}
		m.appendTail(sb);

		return sb.toString();
	}


	public static void main(String [] args) throws ClassNotFoundException{
		/*
		System.out.println(checkJavaSyntax("if ( arg0 ) {}", 70));

		String tt="for ( <arg2> <arg0> = <arg3> ; <arg0> < <arg1> ; <arg0> ++ ) {...}";
		String unitt = tt.replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
		System.out.println(unitt);;
		String output = "public int <arg0> <arg1> throws <arg2> {...}";
		String unitt111 = output.replaceFirst("\\(<\\)arg[0-9]+\\(>\\)", "").replaceAll("\\.\\.\\.", "");
		System.out.println(unitt111);



		System.out.println(getAtom("public int <arg0> <arg1> throws <arg2> {...}"));
		 */

		String pattern = "( )(.)( )";
		Pattern r = Pattern.compile(pattern);
		String aaa = "@Override public void <arg0> ( <arg1> <arg2> ) - > <arg4> {...}";
		//		CharSequence text;
		Matcher m = r.matcher(aaa);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			System.out.println("g1"+m.group(1)+ " g0"+m.group(0)+"g2"+m.group(2));
			System.err.println(aaa.replaceAll("( ).( )",m.group(2)));
			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
			m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), "").replaceFirst(Pattern.quote(m.group(3)), ""));
		}

		m.appendTail(sb);
		System.out.println(sb.toString());

		checkJavaSyntax("public class arg0 arg1{}",3);
		//		checkSwiftSyntax("public class arg0 arg1{}",130);
		System.out.println( "getXAxis().mAxisRange".replaceAll("(.+)\\.", "<arg10>."));
		//		System.out.println(checkSwiftSyntax("super. init (arg0)", 214));
		/*
		m.appendTail(sb); // append the rest of the contents
		System.out.println(sb.toString());
		text = sb.toString();
		pattern = "arg[0-9]+(>)";
		r = Pattern.compile(pattern);
		m = r.matcher(text);
		sb = new StringBuffer();
		while (m.find()) {
			System.out.println("g1"+m.group(1)+ " g0"+m.group(0));
		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
		}
		m.appendTail(sb);
		System.out.println(sb.toString());
		 */
	}
}
