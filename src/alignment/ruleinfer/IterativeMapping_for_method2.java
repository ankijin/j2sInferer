package alignment.ruleinfer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
//import java.util.Map.Entry;
import java.util.Set;
//import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.stringtemplate.v4.ST;

import alignment.common.CommonOperators;
import alignment.common.MappingOutput;
import alignment.common.OpProperty;
import alignment.ruleinfer.StructureMappingAntlr4_for_stmt.Pair;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaBaseVisitor;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import migration.RewriterJava;
//import name.fraser.neil.plaintext.diff_match_patch.Diff;
import templatechecker.DSLErrorListener;
import templatechecker.TemplateCBaseListener;
import templatechecker.TemplateCBaseVisitor;
import templatechecker.TemplateCLexer;
//import templatechecker.TemplateCListener;
import templatechecker.TemplateCParser;
import templatechecker.TemplateCParser.ArgContext;
//import templatechecker.TemplateCParser.BlockContext;
//import templatechecker.TemplateCParser.Java_keywordContext;
//import templatechecker.TemplateCParser.Java_puncContext;
//import templatechecker.TemplateCParser.Swift_keywordContext;
//import templatechecker.TemplateCParser.Swift_puncContext;
//import templatechecker.TemplateCParser.TemplateContext;
//import templatechecker.TemplateCParser.UnitContext;
import templates.MatchingTemplate;
import templates.MatchingTemplate2;

public class IterativeMapping_for_method2 {
	public static MappingOutput iterationWithConstants(ParserRuleContext ctxt_java, CommonTokenStream javaCommonStream, ParserRuleContext ctxt_swft, CommonTokenStream swiftCommonStream, int cycle, String _abinding,String template11, String afile, String sfile)
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

		StructureMappingAntlr4_for_stmt mapping=null, common_op = null;


		String context = ctxt_java.getText();

		JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(context));
		CommonTokenStream javaCommonStream1 = new CommonTokenStream(lexerj);
		JavaParser parserj = new JavaParser(javaCommonStream1);

		Class<? extends Parser> parserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		parserClass = cl.loadClass("antlr_parsers.javaparser.JavaParser").asSubclass(Parser.class);


		Method startRule = parserClass.getMethod(JavaParser.ruleNames[ctxt_java.getRuleIndex()]);
		ParserRuleContext tree = (ParserRuleContext)startRule.invoke(parserj, (Object[])null);
		ctxt_java = tree;


		if(ctxt_java!=null &&ctxt_swft!=null){
			try {
				
				CommonOperators.init();
				common_op = new StructureMappingAntlr4_for_stmt(ctxt_java, ctxt_swft);
				common_op.swiftCommonStream 	= swiftCommonStream;
				common_op.androidCommonStream 	= javaCommonStream1;
				common_op.initialize();
				System.out.println("common_op.alignment"+common_op.common_op_alignment);
				mapping = new StructureMappingAntlr4_for_stmt(ctxt_java, ctxt_swft);
				mapping.swiftCommonStream = swiftCommonStream;
				mapping.androidCommonStream = javaCommonStream1;

			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}


			Map<String, String> argmap = MatchingTemplate.getAttributesMap(template11, ctxt_java.getText());

			//			int az = ctxt_java.start.getStartIndex();
			//			int bz = ctxt_java.stop.getStopIndex();
			//			Interval interval = new Interval(az, bz);
			//			String context = ctxt_java.start.getInputStream().getText(interval);


			context = ctxt_java.getText();


			RewriterJava rw = new RewriterJava();
			ParseTreeWalker walker_swift1 = new ParseTreeWalker();
			try{
				walker_swift1.walk(rw, tree);
			} catch (java.lang.NullPointerException e){
				System.out.println("null	"+ctxt_java);
			}
			//			ParserRuleContext ctxx = ctxt_java;					

			rw.orginText = context;
			
			Map<String, OpProperty> subMatching = MatchingTemplate2.getAttributesMapLineIndex(template11, context.replaceAll(" ", "") ,rw.maps);
			List<String> vals = new LinkedList<String>();
//			System.out.println("subMatching	"+template11+"   "+subMatching+" "+rw.maps);


			if(!argmap.isEmpty()){
//				System.err.println("temp args"+argmap);

				vals.addAll(argmap.values());
				System.out.println("u"+vals);

			}
			//			Collections.list(Collections.enumeration(
			rw = new RewriterJava();
			rw.vars =  subMatching;
			walker_swift1 = new ParseTreeWalker();
//			System.out.println("rw.vars"+rw.vars);
			try{
				walker_swift1.walk(rw, tree);
			} catch (java.lang.NullPointerException e){
				System.out.println("null	"+ctxt_java);
			}
//			System.out.println("rw.constants	"+rw.constants);
			List<OpProperty> newList = new ArrayList<OpProperty>(rw.constants);
			List<OpProperty> constSwift = new ArrayList<OpProperty>();
			newList.removeIf(e-> !StructureMappingAntlr4_for_stmt.common_operators_list.contains(e.strASTNode));
			//			rw.constants
			//			rw.constants  = Collections.sort( new ArrayList( subMatching.values() ) );.

			
			/** rule1
			 * stop when cost equals
			 */
			List<MappingOutput> outputs = new ArrayList<MappingOutput>();
			double c1 = 1.0000, c2 = 1.0001;

			for(int q=0;q<=cycle && mapping!=null;q++){
				//			for(int q=0;q<=cycle && c1!=c2 && mapping!=null;q++){
//				boolean templateOk = false;
//				boolean flip = true;
//				int asize =0, ssize=0;
				//				List<CommonOperators> com_a = new LinkedList<CommonOperators>(), com_s = new LinkedList<CommonOperators>();

				//				mapping.android_listener.gathering = vals;
				//				for(int q=0;q<=cycle && mapping!=null ;q++){
				try {
					//					System.out.println("test"+q);


					//initialize get next candidates


					List<CommonOperators> operators_src = new ArrayList<CommonOperators>(common_op.java_common_ops);
					List<CommonOperators> operators_target = new ArrayList<CommonOperators>(common_op.swift_common_ops);

					Collections.sort(operators_src);
					Collections.sort(operators_target);
					//					out: 
					//					for(int s =0, t=0 ; s< operators_src.size() && t<operators_target.size();s++){

					int s_start = 0;
					int t_start = 0;

					common_op.java_common_ops.clear();
					common_op.swift_common_ops.clear();

					//clustering
					while(!newList.isEmpty() && s_start < 5){
						out:{
						s_start++;
//						System.out.println(s_start+"operators_src"+operators_src);
//						System.out.println("operators_target"+operators_target);
//						System.out.println("newList"+newList);
						for(int s =0; s< operators_src.size() ;s++){
							CommonOperators highest_src = operators_src.get(s);
							List<CommonOperators> com_a = new LinkedList<CommonOperators>();
							List<CommonOperators> com_s = new LinkedList<CommonOperators>();
							Set<String> aaa = new HashSet<String>(), sss = new HashSet<String>();
							com_a.add(highest_src);
							aaa.add(highest_src.text);
							// label


							for(int st =0; st< operators_src.size() ;st++){

								if(highest_src.depth==operators_src.get(st).depth && !highest_src.interval.equals(operators_src.get(st).interval)){
									com_a.add(operators_src.get(st));
									aaa.add(operators_src.get(st).text);
								}
							}

							if(com_a.size()>0){

								for(int t =0; t< operators_target.size() ;t++){
									CommonOperators highest_target = operators_target.get(t);
									com_s.add(highest_target);
									sss.add(highest_target.text);
									for(int tt =0; tt< operators_target.size() ;tt++){	
										if(highest_target.depth==operators_target.get(tt).depth && !highest_target.interval.equals(operators_target.get(tt).interval)){
											com_s.add(operators_target.get(tt));
											sss.add(operators_target.get(tt).text);
										}
										//									 continue out;


									}
									if(com_s.size() >0 && aaa.containsAll(sss)){
//										System.out.println("%%%%%"+com_a+" "+aaa+"  "+com_s+"  "+sss+"\n"+operators_src);
										for(CommonOperators ca: com_a){
											common_op.java_common_ops.add(ca);
											//											Predicate<CommonOperators> personPredicate = ca-> ca.interval.equals(o);
											operators_src.removeIf((CommonOperators emp) -> emp.interval.equals(ca.interval));
											newList.removeIf(e -> !e.interval.disjoint(ca.interval) && e.strASTNode.equals(ca.text));
										}
										for(CommonOperators cs: com_s){
											operators_target.removeIf((CommonOperators emp) -> emp.interval.equals(cs.interval));
											common_op.swift_common_ops.add(cs);
											//											constSwift.add(new OpProperty(null, cs.text,cs.interval));
										}


										break out;
									}
									else{
										for(CommonOperators ca: com_a){
											common_op.java_common_ops.add(ca);
											//											Predicate<CommonOperators> personPredicate = ca-> ca.interval.equals(o);
											operators_src.removeIf((CommonOperators emp) -> emp.interval.equals(ca.interval));
											newList.removeIf(e -> !e.interval.disjoint(ca.interval) && e.strASTNode.equals(ca.text));
										}
									}
									//								if(aaa.
								}
								//							out:
								//							break;
							}
						}
					}
					//						CommonOperators highest_target = operators_target.get(t);

					}
					/*

						System.out.println("common_opcommon_op"+common_op.common_op_alignment.keySet()+":"+common_op.common_op_alignment.values());


						//initialize get next candidates


						if(common_op.common_op_alignment.keySet().size()>0){
							List<CommonOperators> com_a = new LinkedList<CommonOperators>(), com_s = new LinkedList<CommonOperators>();



//							List<CommonOperators> operators_src = new ArrayList<CommonOperators>(common_op.common_op_alignment.keySet());

//							Collections.sort(operators_src);

							CommonOperators hop_a = operators_src.get(0);
							CommonOperators hop_s = common_op.common_op_alignment.get(hop_a);


							com_a.add(hop_a);
							com_s.add(hop_s);

							Iterator<CommonOperators> ittta = common_op.common_op_alignment.keySet().iterator();
							Iterator<CommonOperators> ittts = common_op.common_op_alignment.values().iterator();
							while(ittta.hasNext()){
								CommonOperators abb= ittta.next();
								if((!com_a.contains(abb)) && (hop_a.depth==abb.depth)){
									com_a.add(abb);
								}

								//						if(!com_a.contains(abb)){
								//							com_a.add(abb);
								//						}
							}
							//						int hop = CommonOperators.op_encoding.get(hop_s.text);
							while(ittts.hasNext()){
								CommonOperators abb= ittts.next();
								if(!com_s.contains(abb) && hop_s.depth==abb.depth){
										com_s.add(abb);

								}else{
									if(CommonOperators.op_encoding.get(hop_s.text)!=null && (CommonOperators.op_encoding.get(abb.text)!=null)){
										if(CommonOperators.op_encoding.get(hop_s.text) <= CommonOperators.op_encoding.get(abb.text)){
											com_s.add(abb);
										}
									}

								}//not same depth however if it has same ... 

							}
							System.out.println("com_a "+com_a+" com_s"+com_s+"\n"+CommonOperators.op_encoding);


							if(!common_op.android_listener.type.equals("EnclosedContext")){
								mapping.android_listener.highestop = com_a;
								mapping.swift_listner.highestop = com_s;
							}
						}
					 */


					mapping.android_listener.constantlist = rw.constants;
					if(newList.isEmpty()){
						mapping.swift_listner.constantlist = common_op.swift_common_ops;
					}
					if(common_op.swift_common_ops.isEmpty() &&  !mapping.swift_listner.op_s_nodes.isEmpty()){
					OpProperty aaa = mapping.swift_listner.op_s_nodes.get(0);
						mapping.swift_listner.blacklist.add(aaa);
					}
					//					if(common_op.java_common_ops.isEmpty() || common_op.swift_common_ops.isEmpty()){
					//						mapping.java_common_ops = null;
					//						mapping.swift_common_ops = null;
					//					}else{
					mapping.java_common_ops 	= common_op.java_common_ops;
					mapping.swift_common_ops 	= common_op.swift_common_ops;
					//					}
					mapping.initialize();
//					System.out.println("mapping.constraint"+mapping.constraint);
					mapping.afilename 	= afile;
					mapping.sfilename 	= sfile;
					mapping.abinding 	= template11;

					Statement connection_s;
					try {
						connection_s = QueryExamples.conn_binding_s.createStatement();
						String sql3 = "select * from binding_swift_stat where EXAMPLE=\'"+ctxt_swft.getText().replaceAll(" ", "")+"\'";
						ResultSet res11_s = connection_s.executeQuery(sql3);
//						System.out.println("sql3sql3"+sql3);
						if(res11_s.next()){
							String binding = res11_s.getString("BINDING");
							mapping.sbinding = binding;
							mapping.abinding = _abinding;
						}else{
							mapping.sbinding = "null";
						}

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


//					System.err.println("constraint"+mapping.constraint);
//					System.err.println("aaaablcklista"+mapping.android_listener.blacklist);

//					System.out.println("blcklists"+mapping.swift_listner.blacklist);
					//					mapping.waking();

					//					mapping.constraint.clear();

					//					Iterator<OpProperty> ita = mapping.android_listener.op_a_nodes;

					//					System.out.println("temp	"+template);

					//					System.err.println("op-aaaaaaa"+mapping.android_listener.op_a_nodes);
					//					System.err.println("op-ssssssss"+mapping.swift_listner.op_s_nodes);

					/*
					Iterator<OpProperty> ios = mapping.swift_listner.op_s_nodes.iterator();
					while(ios.hasNext()){
						OpProperty nexts = ios.next();
						if(nexts.interval.length()==1 && Character.isUpperCase(nexts.strASTNode.charAt(0))){
							ios.remove();
						}
					}
					for(OpProperty ops:mapping.swift_listner.op_s_nodes){
						if(ops.strASTNode!=null &&  ops.strASTNode.length()>0 && Character.isUpperCase(ops.strASTNode.charAt(0))){
							ops.property = "constant_s";
						}
					}
					 */
					/*
					System.out.println("termsMapOnly"+mapping.android_listener.termsMap);
					System.out.println("a termsMapOnly"+mapping.android_listener.termsMapOnly);
					System.out.println("s termsMapOnly"+mapping.swift_listner.termsMap);
					System.out.println("swft termsMapOnly"+mapping.swift_listner.termsMapOnly);
					System.err.println("constraint"+mapping.constraint);
					System.err.println("aaaablcklista"+mapping.android_listener.blacklist);

					System.out.println("blcklists"+mapping.swift_listner.blacklist);

					 */

					//					mapping.constraint.clear();
					//					mapping.nextMapping.clear();
					//					if(mapping.constraint.size()>0)
					//						System.out.println("constraint"+mapping.constraint);
					//						mapping.amap_ct
					//mappings
					//						if(mapping.android_listener.op_a_nodes.size()==mapping.swift_listner.op_s_nodes.size())
					c2 =c1;
					/*
					if(variable_list !=null){
						for(String varaible:variable_list){
//							System.out.println("ttttttesting");
							for(int i=0; i< mapping.android_listener.op_a_nodes.size(); i++){
								if(mapping.android_listener.op_a_nodes.get(i).strASTNode.contains(varaible) && !mapping.android_listener.op_a_nodes.get(i).strASTNode.equals(varaible))
								{
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(i))){
										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(i));
									}
									else{

									}
								}
								mapping.android_listener.blacklist.removeIf( (OpProperty op) -> op.orginTxt.equals(varaible));
							}
						}
					}

					if(constant_list !=null){
						for(String constant:constant_list){
							for(int i=0; i< mapping.android_listener.op_a_nodes.size(); i++){
								if(mapping.android_listener.op_a_nodes.get(i).orginTxt.equals(constant)){
//									System.err.println("testing");
									OpProperty opa = mapping.android_listener.op_a_nodes.get(i);
									opa.property="constant_a";
									//								System.out.println("constant_a	"+opa);
									mapping.android_listener.op_a_nodes.set(i, opa);
								}


								if(mapping.android_listener.op_a_nodes.get(i).orginTxt.contains(constant) && !mapping.android_listener.op_a_nodes.get(i).orginTxt.equals(constant))
								{
									if(!mapping.android_listener.blacklist.contains(mapping.android_listener.op_a_nodes.get(i))){
										mapping.android_listener.blacklist.add(mapping.android_listener.op_a_nodes.get(i));
									}

								}


							}
						}
					}

					 */


					//					mapping.constraint.clear();
					//					mapping
					//get Templates
					MappingOutput output = mapping.compute();

					Iterator<OpProperty> aaaaaa = mapping.android_listener.op_a_nodes.iterator();
					List<Interval> listc 		= new LinkedList<Interval>();
					HashSet<OpProperty> set 	= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint_a = mapping.android_listener.scope_constraint;
					int count_a = 0, count_s=0;
					//					System.out.println("constraint_a	"+constraint_a);;
					Iterator<String> decls = constraint_a.keySet().iterator();
					//					System.out.println("constraint"+constraint);
					while(decls.hasNext()){
						listc = constraint_a.get(decls.next());

						// boolean isMet = true;
						// check if template have same args
						while(aaaaaa.hasNext()){
							OpProperty anode = aaaaaa.next();
							Interval nnnnn = anode.interval;
							for(int i = 0; i< listc.size();i++){

								if(!listc.get(i).equals(nnnnn) && !listc.get(i).disjoint(nnnnn)){
									//									System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set.add(anode);
									count_a++;
									if(!mapping.android_listener.blacklist.contains(anode)){
										mapping.android_listener.blacklist.add(anode);
										//										System.out.println("@@@@1"+anode);
									}
								}
							}
						}
					}//scoping constraints in java
					//don't exist




					List<Interval> listc_s 				= new LinkedList<Interval>();
					HashSet<OpProperty> set_s 			= new HashSet<OpProperty>();
					LinkedHashMap<String, List<Interval>> constraint_s = mapping.swift_listner.scope_constraint;

					Iterator<String> decls_s = constraint_s.keySet().iterator();
					//										System.out.println("constraint_s"+constraint_s);

					while(decls_s.hasNext()){
						listc_s = constraint_s.get(decls_s.next());
						//						System.out.println("listc_s"+listc_s);;
						Iterator<OpProperty> ssssss = mapping.swift_listner.op_s_nodes.iterator();
						//						boolean isMet = true;
						//check if template have same args
						while(ssssss.hasNext()){
							OpProperty snode = ssssss.next();
							Interval nnnnn = snode.interval;
							//							System.out.println("nnnnn"+nnnnn);
							for(int i = 0; i< listc_s.size();i++){

								if(!listc_s.get(i).equals(nnnnn) && !listc_s.get(i).disjoint(nnnnn)){
									//										System.out.println("nnnnn"+mapping.amap_ct.get(nnnnn));
									set_s.add(snode);
									count_s++;
									if(!mapping.swift_listner.blacklist.contains(snode)){
										mapping.swift_listner.blacklist.add(snode);
										//										System.err.println("adding"+snode+" decls");

										int h = snode.height;
										String type = snode.property;
										for(OpProperty op: mapping.swift_listner.op_s_nodes){
											if(type.equals(op.property)&& op.height==h && !op.property.equals("enclosed{}") && !mapping.swift_listner.blacklist.contains(op)){
												mapping.swift_listner.blacklist.add(op);
												//												System.err.println("aaaadding friends"+op);

											}
										}

									}
								}
							}
						}
					}//scoping constraints in swift
					//don't exist

					int unmet=0;
					Iterator<Interval> c_a = mapping.constraint.keySet().iterator();
					while(c_a.hasNext() && false){
						Interval i_c_a = c_a.next();
						Interval i_c_s = mapping.constraint.get(i_c_a);
						Iterator<Pair> iiit = mapping.android_to_swift_point.keySet().iterator();
						while(iiit.hasNext()){
							Pair p = iiit.next();
							if(!p.a_intval.disjoint(i_c_a) && p.b_intval.disjoint(i_c_s) && !mapping.android_listener.blacklist.contains(p.a_op)){
								mapping.android_listener.blacklist.add(p.a_op);
								p.a_op.whyBlacklist = "!p.a_intval.disjoint(i_c_a) && p.b_intval.disjoint(i_c_s)";
								count_a++;
								//								System.out.println("testing22");
							}
							else if(p.a_intval.disjoint(i_c_a) && !p.b_intval.disjoint(i_c_s) && !mapping.swift_listner.blacklist.contains(p.s_op)){
								p.s_op.whyBlacklist="p.a_intval.disjoint(i_c_a) && !p.b_intval.disjoint(i_c_s)";
								mapping.swift_listner.blacklist.add(p.s_op);
								count_s++;
								//								System.out.println("testing11");
							}
							if(!mapping.constraintMet(p.a_intval, p.b_intval))
								unmet++;
							//							Double aa = mapping.android_to_swift_point.get(p);
						}

					}


					//					for(OpProperty op: mapping.android_listener.op_a_nodes){
					//						for(OpProperty os: mapping.swift_listner.op_s_nodes){

					//						}
					//					}
					Set<String> argA = new HashSet<String>(), argS = new HashSet<String>();
					Set<String> diffArgs = new HashSet<String>();

					if(output!=null){
						c1=output.cost;
						//						c1 = output.cost;
						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("\\<arg[0-9]+\\>", "arg"));
						//						String template_a = output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", "");

						//						System.out.println("template a"+output.template_a.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));
						//						System.out.println("template s"+output.template_s.replace("{...}", "{}").replaceAll("<","").replaceAll(">", ""));


						Lexer lexer1 				= new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_a));
												DSLErrorListener listner 	= new DSLErrorListener();
						//						lexer1.addErrorListener(listner);
						lexer1.removeErrorListeners();

						templatechecker.TemplateCParser parser 		= new TemplateCParser(new CommonTokenStream(lexer1));
						//				toskip = false;
						//				ExceptionErrorStrategy error_handler = new ExceptionErrorStrategy();
						//				parser.removeErrorListeners();
						//				parser.setErrorHandler(error_handler);

						TemplateCParser.TemplateContext comj = parser.template();

						List<String> argSwift = new LinkedList<String>(), argAndroid = new LinkedList<String>();
						comj.accept(new TemplateCBaseVisitor(){
							@Override
							public Object visitArg(ArgContext ctx) {
								// TODO Auto-generated method stub
								argAndroid.add(ctx.getText());
								argA.add(ctx.getText());

								return super.visitArg(ctx);
							}
						});

						//						if(listner.hasErrors()){
						//												System.out.println("error in"+tempa);
						//						}


						TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
						DSLErrorListener listner_swift = new DSLErrorListener();
						//						lexer2.addErrorListener(listner_swift);

						lexer2.removeErrorListeners();

						TemplateCParser parser2 = new TemplateCParser(new CommonTokenStream(lexer2));
						//				parser.setErrorHandler(error_handler);
						TemplateCParser.TemplateContext com = parser2.template();

						ParseTreeWalker walker_android = new ParseTreeWalker(), walker_swift = new ParseTreeWalker();
						walker_android.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argAndroid.add(ctx.getText());
								argA.add(ctx.getText());
								diffArgs.add(ctx.getText().replaceAll("\\<", "").replaceAll("\\>", ""));
							};
						}, comj);




						walker_swift.walk(new TemplateCBaseListener(){
							public void enterArg(ArgContext ctx) {
								//								 System.out.println("ArgContext"+ctx.getText());
								argSwift.add(ctx.getText());
								argS.add(ctx.getText());
								String arg=ctx.getText().replaceAll("\\<", "").replaceAll("\\>", "");
								diffArgs.removeIf((String emp) -> emp.equals(arg));
							};
						}, com);


						//						ST st_android = new ST(output.template_a);
						//						while(iterator.hasNext()){
						//							String key=iterator.next();
						//							st_android.add(key, argmap.get(key));
						//						};
						String changed =null;

						if(!diffArgs.isEmpty()){
							Iterator<String> diffI = diffArgs.iterator();
							while(diffI.hasNext()){
								String aarrr=diffI.next();
								//								output.aamap.containsValue("<"+aarrr+">");
								Iterator<OpProperty> ittt = output.aamap.keySet().iterator();
								while(ittt.hasNext()){
									OpProperty key = ittt.next();
									if(output.aamap.get(key).equals("<"+aarrr+">")){
										//										changed= output.template_a.replace("<"+aarrr+">", key.strASTNode);
										//										System.out.println("aarrr"+aarrr+" "+key.strASTNode+"  "+changed);
									}
								}

							}
						}
						//						output.template_a = st_android.render();
						//						System.out.println("output.template_a"+output.template_a);
						//						System.out.println("output.template		"+output.template_a+"    "+output.template_s);
//						if(!listner_swift.hasErrors() && !listner.hasErrors()){
													if(true){	
							if(!output.template_a.equals("<arg0>") && !output.template_s.equals("<arg0>") ){
								//								checkJavaSyntax("if (arg0) {}", 70)
								//								for ( <arg2> <arg0> = <arg3> ; <arg0> < <arg1> ; <arg0> ++ ) {...}
								//								replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
								//								String unitt = output.template_a.replaceAll("<arg[]>", " ").replaceAll(">", "  ").replaceAll("\\.\\.\\.", "");

								//								String unitt = output.template_a.replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
								String unitt = getAtom(output.template_a).replaceAll("\\.\\.\\.", "");

								String unitt_for_s = getAtom(output.template_s).replaceAll("\\.\\.\\.", "");

								if(changed!=null){
									//									changed= changed.replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
									changed= getAtom(changed).replaceAll("\\.\\.\\.", "");

									unitt = changed;
								}


								DSLErrorListener listn = checkJavaSyntax(unitt, ctxt_java.getRuleIndex());
								DSLErrorListener listn_for_s = checkSwiftSyntax(unitt_for_s, ctxt_swft.getRuleIndex());

								if(!listn.hasErrors()&& (!listn_for_s.hasErrors()) && (count_a==0 && count_s==0)){
									if(output.template_a.replaceAll(" ", "").equals(template11.replaceAll(" ", "")))
										output.template_a =template11;
									outputs.add(output);
									cycle = 1;
									System.out.println("$$$$$$$$");
//									return output;
								}else{
									String text=listn.getError();
									String pattern = "arg[0-9]+";
									Pattern r = Pattern.compile(pattern);
									Matcher m = r.matcher(text);
									StringBuffer sb = new StringBuffer();
									while (m.find()) {
										//										System.out.println(" gggg0	"+m.group(0));
										System.out.println(output.aamappp);
										Iterator<OpProperty> aaaaa = output.aamappp.keySet().iterator();
										while(aaaaa.hasNext()){
											OpProperty nxt = aaaaa.next();
											String val = output.aamappp.get(nxt);
											if(val.equals(m.group(0))){
												if(!mapping.android_listener.blacklist.contains(nxt)){
													nxt.whyBlacklist="template has Error";
													mapping.android_listener.blacklist.add(nxt);
													int h = nxt.height;
													String type = nxt.property;
													for(OpProperty op: mapping.android_listener.op_a_nodes){
														if(type.equals(op.property) && op.height==h && !op.property.equals("enclosed{}")){
															if(!mapping.android_listener.blacklist.contains(op)){
																op.whyBlacklist="same height min val for anum < snum";
																mapping.android_listener.blacklist.add(op);
															}
															//															System.out.println("@@@@5"+mapping.android_listener.op_a_nodes.get(min.getKey().a));

														}
													}

												}
											}


										}
										//									    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(2)), ""));
									}

									text = listn_for_s.getError();
									m = r.matcher(text);

									while (m.find()) {
										System.out.println(" gggg0	"+m.group(0));
										System.out.println(output.ssmappp);
										Iterator<OpProperty> aaaaa = output.ssmappp.keySet().iterator();
										while(aaaaa.hasNext()){
											OpProperty nxt = aaaaa.next();
											String val = output.ssmappp.get(nxt);
											if(val.replaceAll(" ","").equals(m.group(0).replaceAll(" ",""))){

												if(!mapping.swift_listner.blacklist.contains(nxt) && nxt.interval.length()>1){
													nxt.whyBlacklist ="swifting error";
													mapping.swift_listner.blacklist.add(nxt);
												}
											}


										}
										//									    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(2)), ""));
									}

									//									arg[0-9]+
								}
								//								}
							}
						}
					}


					//					System.out.println("a-snum"+anum+" "+snum);
					//					if(mapping.android_listener.op_a_nodes.size() < mapping.swift_listner.op_s_nodes.size()){
					//					if(anum==0 || snum==0){

					/** return <>; return <>
					 * for ( <arg0> <arg1> : <arg2> ) {...}  to for <arg1> in <arg2> {...}
					 * 없으면 이대로 .. 있으면 .. 계속. best matching
					 */

					//					black_a = mapping.android_listener.op_a_nodes;
					//						System.out.println("black_a"+black_a);
					//						System.err.println("black_s"+black_s);

					//					black_s = mapping.swift_listner.op_s_nodes;

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(outputs.size()>0){

				MappingOutput minOutput = Collections.min(outputs);
				//				System.out.println("MappingOutput\n"+outputs+" "+minOutput);
				//										minOutput.doupdate();
				//					outputs.size();

				MappingOutput last =outputs.get(outputs.size()-1);
				//				last.doupdate();
				//				nextmapping = last.nextMapping;
				//updating next level
				//				MappingOutput.updateNextMapping(connection, nextmapping);

				//				if(minOutput.template_a.equals("<arg0> . <arg1>") && minOutput.template_s.equals("<arg0> . <arg1>")){
				//					minOutput.template_a = minOutput.example_a.replaceAll("(.+)\\.", "<arg10>.");
				//					minOutput.template_s = minOutput.example_a.replaceAll("(.+)\\.", "<arg10>.");
				//					System.out.println("outputs		"+minOutput.template_a+"   ::  "+minOutput.template_s+"  "+minOutput.template_s+" "+minOutput.example_a);

				//				}
				return last;
			}
			else return null;
		}
		return null;
	}









	public static DSLErrorListener checkSwiftSyntax(String template, int atype) throws ClassNotFoundException{

		//		TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
		DSLErrorListener listner_swift = new DSLErrorListener();
		//		lexer2.addErrorListener(listner_swift);
		template = template.replace("- >", "->");
		antlr_parsers.swiftparser.SwiftLexer lexerj = new antlr_parsers.swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(template));
		//		lexerj.addErrorListener(listner_swift);
		CommonTokenStream swiftCommonStream = new CommonTokenStream(lexerj);
		antlr_parsers.swiftparser.SwiftParser parserj = new antlr_parsers.swiftparser.SwiftParser(swiftCommonStream);
		//		parserj.removeErrorListeners();
		parserj.addErrorListener(listner_swift);
		ParserRuleContext comj=null;

		Class<? extends Parser> jparserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		jparserClass = cl.loadClass("antlr_parsers.swiftparser.SwiftParser").asSubclass(Parser.class);

		try {
			Method jstartRule = null;

			/**
			 * get ParserRuleContexts by type ids
			 */
			jstartRule = jparserClass.getMethod(antlr_parsers.swiftparser.SwiftParser.ruleNames[atype]);
			//			sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);

			//			System.out.println("rulej"+JavaParser.ruleNames[atype]);
			//			System.out.println("rules"+SwiftParser.ruleNames[stype]);
			comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
			comj.accept(new antlr_parsers.swiftparser.SwiftBaseVisitor<>());
			//			com 	= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);
			//			System.out.println("testing	"+comj.start.getInputStream().getText(comj.getSourceInterval()));
			//			System.out.println("swift%%%%\n"+template+"\n"+listner_swift.getError());
		} catch (NoSuchMethodException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return listner_swift;
	}


	public static DSLErrorListener checkJavaSyntax(String template, int atype) throws ClassNotFoundException{

		//		TemplateCLexer lexer2 = new TemplateCLexer((CharStream)new ANTLRInputStream(output.template_s));
		DSLErrorListener java_listener = new DSLErrorListener();
		//		lexer2.addErrorListener(listner_swift);

		antlr_parsers.javaparser.JavaLexer lexerj = new antlr_parsers.javaparser.JavaLexer((CharStream)new ANTLRInputStream(template));
		//		lexerj.addErrorListener(listner_swift);
		CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
		antlr_parsers.javaparser.JavaParser parserj = new antlr_parsers.javaparser.JavaParser(javaCommonStream);
		//		parserj.removeErrorListeners();
		//		parserj.removeParseListeners();
		parserj.addErrorListener(java_listener);
		ParserRuleContext comj=null;

		Class<? extends Parser> jparserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		jparserClass = cl.loadClass("antlr_parsers.javaparser.JavaParser").asSubclass(Parser.class);

		try {
			Method jstartRule = null;

			/**
			 * get ParserRuleContexts by type ids
			 */
			jstartRule = jparserClass.getMethod(JavaParser.ruleNames[atype]);
			//			sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);

			//			System.out.println("rulej"+JavaParser.ruleNames[atype]);
			//			System.out.println("rules"+SwiftParser.ruleNames[stype]);
			comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
			comj.accept(new JavaBaseVisitor<>());
			//			com 	= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);
			//			System.out.println("testing	"+comj.start.getInputStream().getText(comj.getSourceInterval()));
			System.out.println("%%%%"+java_listener.getError()+"  "+template+" "+JavaParser.ruleNames[atype]+" "+atype);
		} catch (NoSuchMethodException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return java_listener;
	}
	public static String getAtom(String text){


		String pattern = "(<)arg[0-9]+>";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(text);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			//			System.out.println("g2"+m.group(2)+"g1"+m.group(1)+ " g0"+m.group(0));
			m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));

			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(2)), ""));
		}
		m.appendTail(sb); // append the rest of the contents
		//		System.out.println(sb.toString());

		text = sb.toString();
		pattern = "arg[0-9]+(>)";
		r = Pattern.compile(pattern);
		m = r.matcher(text);
		sb = new StringBuffer();
		while (m.find()) {
			//			System.out.println("g1"+m.group(1)+ " g0"+m.group(0));
			m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
		}
		m.appendTail(sb);

		return sb.toString();
	}

	public static void main(String [] args) throws ClassNotFoundException{
		/*
		System.out.println(checkJavaSyntax("if ( arg0 ) {}", 70));

		String tt="for ( <arg2> <arg0> = <arg3> ; <arg0> < <arg1> ; <arg0> ++ ) {...}";
		String unitt = tt.replaceAll("<arg[0-9]+>", "arg1").replaceAll("\\.\\.\\.", "");
		System.out.println(unitt);;
		String output = "public int <arg0> <arg1> throws <arg2> {...}";
		String unitt111 = output.replaceFirst("\\(<\\)arg[0-9]+\\(>\\)", "").replaceAll("\\.\\.\\.", "");
		System.out.println(unitt111);



		System.out.println(getAtom("public int <arg0> <arg1> throws <arg2> {...}"));
		 */

		String pattern = "( )(.)( )";
		Pattern r = Pattern.compile(pattern);
		String aaa = "@Override public void <arg0> ( <arg1> <arg2> ) - > <arg4> {...}";
		//		CharSequence text;
		Matcher m = r.matcher(aaa);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			System.out.println("g1"+m.group(1)+ " g0"+m.group(0)+"g2"+m.group(2));
			System.err.println(aaa.replaceAll("( ).( )",m.group(2)));
			//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
			m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), "").replaceFirst(Pattern.quote(m.group(3)), ""));
		}

		m.appendTail(sb);
		System.out.println(sb.toString());

		checkJavaSyntax("public class arg0 arg1{}",3);
		//		checkSwiftSyntax("public class arg0 arg1{}",130);
		System.out.println( "getXAxis().mAxisRange".replaceAll("(.+)\\.", "<arg10>."));
		//		System.out.println(checkSwiftSyntax("super. init (arg0)", 214));
		/*
		m.appendTail(sb); // append the rest of the contents
		System.out.println(sb.toString());
		text = sb.toString();
		pattern = "arg[0-9]+(>)";
		r = Pattern.compile(pattern);
		m = r.matcher(text);
		sb = new StringBuffer();
		while (m.find()) {
			System.out.println("g1"+m.group(1)+ " g0"+m.group(0));
		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
//		    m.appendReplacement(sb, m.group(0).replaceFirst(Pattern.quote(m.group(1)), ""));
		}
		m.appendTail(sb);
		System.out.println(sb.toString());
		 */
	}
	/*
	public List<String> checkSwiftSyntax(String template, int type){

		swiftparser.SwiftLexer lexer1 = new swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(bbb));
		CommonTokenStream swiftCommonStream = new CommonTokenStream(lexer1);
		swiftparser.SwiftParser parser = new swiftparser.SwiftParser(swiftCommonStream);


		JavaLexer lexerj = new JavaLexer((CharStream)new ANTLRInputStream(aaa));
		CommonTokenStream javaCommonStream = new CommonTokenStream(lexerj);
		JavaParser parserj = new JavaParser(javaCommonStream);
		ParserRuleContext com = null, comj=null;

		Class<? extends Parser> jparserClass = null, sparserClass = null;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		jparserClass = cl.loadClass("javaparser.JavaParser").asSubclass(Parser.class);
		sparserClass = cl.loadClass("swiftparser.SwiftParser").asSubclass(Parser.class);

		try {
			Method jstartRule = null, sstartRule =null;


			jstartRule = jparserClass.getMethod(JavaParser.ruleNames[atype]);
			sstartRule = sparserClass.getMethod(SwiftParser.ruleNames[stype]);

			System.out.println("rulej"+JavaParser.ruleNames[atype]);
			System.out.println("rules"+SwiftParser.ruleNames[stype]);
			comj 	= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
			com 	= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);

		} catch (NoSuchMethodException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	 */

}
