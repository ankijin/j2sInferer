package alignment.ruleinfer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;

//import org.apache.commons.lang.StringUtils;
//import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
//import com.googlecode.concurrenttrees.solver.LCSubstringSolver;

import alignment.common.LineCharIndex;
import alignment.common.MappingOutput;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.swiftparser.SwiftParser;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;

/**
 * Query examples from table and infer templates sequentially in table and iteratively in ast of the example
 * @author kijin
 *
 */
public class QueryExamples {
	public static Connection connection;
	public static Connection conn_binding;
	public static Connection conn_binding_s;
	private int i2;

	/*
	static int []testset= {
			1345,
			1347,
			1358,
			1358,
			1367,
			1385,
			1438,
			1440,
			1442,
			1444,
			1447,
			1448,
			1476,
			1480
	};
	 */

	static int [] testset= {			
	};
	/*
	static int []testset= {
			843,
			1062,
			2174,
			2126,
			1540,
			1621,
			266,
			1180,
			1405,
			72,
			2477,
			2436,
			705,
			1976,
			2198,
			2485,
			963,
			1202,
			388,
			837,
			488,
			700,
			2431,
			2322,
			343,
			2154,
			268,
			2188,
			169,
			1064,
			98,
			1877,
			669,
			333,
			766,
			2079,
			751,
			868,
			1613,
			2375,
			659,
			704,
			1983,
			851,
			2481,
			403,
			139,
			1445,
			1879,
			1088
	};
	 */
	public static int idd;
	private static boolean flip = true;
	//		public static final String db_name = "test_record11.db";
	//	public static final String db_name = "test_record0208.db";

	public static  String db_name = "ttt.db";
	//		public static String db_name = "test_record0210.db";
	//		public static final String db_name = "train_record7.db";
	//		public static final String db_name = "test_record_charts.db";

	private static boolean templateOk = false;
	public static String query(String tableName, int android, int swift){
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, count from "+tableName+ " where parent_type="+swift +" order by count;";
		return query;
	}


	public static ResultSet queryExampleResult(String tableName, int android, int swift) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		StringUtils.join(null, " ");
		String test=Arrays.toString(testset).replace("[","").replace("]","");
		String query = "select template, example, parent_type, ast_type, count, ID, AFILENAME, SFILENAME from "+tableName+ " where ast_type="+android +" and parent_type="+swift +" and id not in ("+test+");";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleSetResult(String tableName, int[] android, int[] swift) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		StringUtils.join(null, " ");
		String test=Arrays.toString(testset).replace("[","").replace("]","");
		String at=Arrays.toString(android).replace("[","").replace("]","");
		String st=Arrays.toString(swift).replace("[","").replace("]","");
		String query = "select template, example, parent_type, ast_type, count, ID, AFILENAME, SFILENAME from "+tableName+ " where ast_type in ("+ at +") and parent_type in ("+st +");";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}


	public static ResultSet queryExampleSetResultExceptFor(String tableName, int[] android, int[] swift) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		StringUtils.join(null, " ");
		String test=Arrays.toString(testset).replace("[","").replace("]","");
		String at=Arrays.toString(android).replace("[","").replace("]","");
		String st=Arrays.toString(swift).replace("[","").replace("]","");
		String query = "select template, example, parent_type, ast_type, count, ID, AFILENAME, SFILENAME from "+tableName+ " where ast_type not in ("+ at +") and parent_type not in ("+st +");";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleSetResultType(String tableName, int android, int swift) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		StringUtils.join(null, " ");
		String test=Arrays.toString(testset).replace("[","").replace("]","");
		String query = "select template, example, parent_type, ast_type, count, ID, AFILENAME, SFILENAME from "+tableName+ " where ast_type=3 and (parent_type in (130,133));";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}


	public static ResultSet queryExampleResultAll(String tableName) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select TEMPLATE, AST_TYPE, PARENT_TYPE,  EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC , ID from "+tableName+ ";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleResultRandom(String tableName, int num) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select TEMPLATE, AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC , ID from "+tableName+ " ORDER BY RANDOM() LIMIT "+num +";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}

	public static ResultSet queryExampleResultRandom(String tableName, int a, int s, int num) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select TEMPLATE, AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC , ID from "+tableName+ " where ast_type="+a+" and parent_type= "+s+" ORDER BY RANDOM() LIMIT "+num +";";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}


	public static ResultSet queryExampleResultbyID(String tableName, int id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where id="+id+ " order by count;";
		String query = "select * from "+tableName+ " where id="+id+ " order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyIDs(String tableName, int[] id) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where id="+id+ " order by count;";

		String test=Arrays.toString(id).replace("[","").replace("]","");
		String query = "select * from "+tableName+ " where id in ("+test+ ") order by count;";

		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;
	}

	public static ResultSet queryExampleResultbyID(String tableName, String ids) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		//		String query = "select template, example, parent_type, ast_type, count, ID from "+tableName+ " where id="+id+ " order by count;";
		String query = "select * from "+tableName+ " where id="+ids+ " order by count;";

		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println("query	"+query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);
		return resultSet;
	}


	public static ResultSet queryExampleResultbyExample(String tableName, String a_example, String s_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' and example=\'"+s_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}

	public static ResultSet queryExampleResultbyExample(String tableName, String a_example) throws SQLException{
		//		String query = "select template, example, ast_type from grammar_swift_stat where parent_type= 36 order by count";
		String query = "select template, example, ast_type, parent_type, count from "+tableName+ " where template=\'"+a_example +"\' order by count;";
		//		String query = "select template, example, ast_type, count from "+tableName+ " where id=30 order by count;";
		System.out.println(query);
		//		test_record11.db
		Connection connection2 = DriverManager.getConnection("jdbc:sqlite:"+db_name);
		connection2.setAutoCommit(true);
		Statement stmt2 = connection2.createStatement();
		//		String sql2="select template, example from grammar_android_stat where ast_type= 70 and parent_type =6 and count = 15;";
		ResultSet resultSet = stmt2.executeQuery(query);

		return resultSet;

	}



	/*


	public static String getLongestCommonSubstring(Collection<String> strings) {
		LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());

		for (String s: strings) {
			solver.add(s);
		}
		return solver.getLongestCommonSubstring().toString();
	}
	 */


	public static void main(String[] args) throws SQLException, ClassNotFoundException, FileNotFoundException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// TODO Auto-generated method stub

		//query examples
		//generates template
		//when there are no binding .. just syntax change
		//		db_name = "test_record_for.db";

		//		connection = DriverManager.getConnection("jdbc:sqlite:"+"t1est_record0424_geo.db");
		//		connection = DriverManager.getConnection("jdbc:sqlite:"+"t1est_record0424_geo.db");
		//				connection = DriverManager.getConnection("jdbc:sqlite:"+"template_charts40510.db");
		//		connection = DriverManager.getConnection("jdbc:sqlite:"+"061260testing.db");
		//		connection = DriverManager.getConnection("jdbc:sqlite:"+"swi1pe0711211111112_template.db");
		//		connection = DriverManager.getConnection("jdbc:sqlite:"+"11field_decl05071.db");
		//		Connection connection = DriverManager.getConnection("jdbc:sqlite:"+"charts_extra22.db");



		Class.forName("org.sqlite.JDBC");
		//		final String recoredTable 	= "validationgeo.db";
		//		db_name 					= "db_result/total_examples0404.db";
		//						db_name = "tables/charts_extra.db";//not this
		//				db_name = "tables/antlr4_examples.db";


		db_name = "tables/antlr4_extra.db";
		db_name = "antlr4_with_m.db";

		//		db_name = "tables/total_examples0430_geo.db";
		//		db_name = "examples/example_fase2.db";
		//		final String recoredTable 	= "validating_examples5.db";
		final String recoredTable ="mtest2_antlr4.db";
		//		final String recoredTable ="antlr4_vals_with_method.db";
		//				db_name = "tables/geometry-api_0328.db";

		connection = DriverManager.getConnection("jdbc:sqlite:"+recoredTable);
		connection.setAutoCommit(true);
		Statement stmt 	= connection.createStatement();


		/**
		 * binding for android repos, used for gathering apis seperately
		 */
		//		conn_binding = DriverManager.getConnection("jdbc:sqlite:"+"cards0509.db");
//		conn_binding = DriverManager.getConnection("jdbc:sqlite:"+"tables/antlr4-api_0430.db");
		
		//		conn_binding 	= DriverManager.getConnection("jdbc:sqlite:"+"tables/ttt07134_view.db");
		conn_binding 	= DriverManager.getConnection("jdbc:sqlite:"+"tables/charts-api_0430.db");
//		System.out.println(conn_binding);
		conn_binding_s 	= DriverManager.getConnection("jdbc:sqlite:"+"tables/charts-binding_t.db");
		conn_binding.setAutoCommit(true);
		conn_binding_s.setAutoCommit(true);

		//		Statement stmt_conn_binding = conn_binding.createStatement();
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("constant_stat"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("unsucess_stat"));

		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		stmt.executeUpdate(SQLiteJDBC.createConstantStat());
		stmt.executeUpdate(SQLiteJDBC.createMethod());
		stmt.executeUpdate(SQLiteJDBC.createUnsucessStat());
		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());
		//		stmt.executeUpdate(SQLiteJDBC.createBindingStat());

		int atype 	= 19, stype	= 80;
		//						int atype = 3, stype=130;


		//db_name ="tables/example_viewport.db";
		//		db_name ="docopt0711.db";
		//		db_name ="swipe0711.db";

		/*
		if(1==21){
			DBTablePrinter.printTable(DriverManager.getConnection("jdbc:sqlite:"+"example_viewport.db"),"grammar_swift_stat");
			DBTablePrinter.printTable(connection, "grammar_android_stat");
			DBTablePrinter.printTable(connection, "grammar_swift_stat");
			DBTablePrinter.printTable(connection, "constant_stat");
			DBTablePrinter.printTable(connection, "unsucess_stat");
			return;
		}
		 */
		//				db_name = "total_examples0421_cards2.db";
		//		db_name = "total_examples0504_card.db";
		//				db_name = "test_record0424_geo.db";
		//		db_name = "total0502.db";
		//		db_name = "testing.db";
		//				db_name = "total0502.db";
		//	db_name = "record_mar_6_for.db";
		//		19	9768	76
		//		int [] at = {70};
		//		int [] st = {6};
		//		int [] at = {68, 19, 70, 3};
		//		int [] st ={80, 76, 130};//68	80 19	80
		//		ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 904);
		//								int [] at = {3, 19};
		//								int [] st ={130, 133, 97, 143};
		//				int [] at = {3, 19};
		//				int [] st ={130, 133, 97, 143};//68	80 19	80
		//				int [] at = {3};
		//				int [] st ={130, 133};
		//				int [] at ={3,19,68,70};
		//		int [] st = {97,130, 133, 143};




		/**
		 * VariableDeclarations
		 */
		//		int [] at = {68, 19, 70};
		//		int [] st = {80, 76};//68	80 19	80

		//		int [] at = {68, 19, 70};
		//		int [] st = {80, 76};//68	80 19	80

		//		int [] at = {70};//statments
		//		int [] st ={18};//statements
		//		int [] at = {70};//statments
		//		int [] st ={18};//statements
		//68	80 19	80


		int [] at =	{19, 68, 70,  3,  80};
		int [] st = {76,80, 130, 133, 97, 143};
		//all
		//				int [] at ={3,19, 68};
		//				int [] st ={130, 133, 97, 80, 143};	
		//		int at[] = {70};
		//		int st[] = {36};
		//		int sstype[] = {97, 143};
		//		int aatype[] = {19, 80};
		//		int [] at = {68, 19, 70};
		//		int [] st ={80, 76};//68	80 19	80
		//				ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 297);// 155111

		String EXAMPLE_TABLE = "grammar_swift_stat";




		/**
		 * phase2-1: query example
		 */
		//		at={70};
		//		st={180};
		//		ResultSet resultSet2 =  queryExampleSetResult(EXAMPLE_TABLE, at, st);

		//				ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 87);
		//				ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 60);
		//		ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 85);
		//		ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 27);
		//		ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 150);
		//		ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 230);
		//		ResultSet resultSet2 =  queryExampleResultbyID("grammar_swift_stat", 369);

		//query all examples
		//		ResultSet resultSet2 = queryExampleResultAll(EXAMPLE_TABLE);
		ResultSet resultSet2 = queryExampleResultAll("method_table");
		int num 	= 0;
		//		int count 	= 0;
		String aaa="", bbb="";
		//iteration of ailgned examples
		while(resultSet2.next()){
			templateOk 		= false;
			flip 			= true;
			num++;
			aaa 			= resultSet2.getString("template");
			bbb 			= resultSet2.getString("example");
			String afile 	= resultSet2.getString("AFILENAME");
			String sfile 	= resultSet2.getString("SFILENAME");
			atype 			= Integer.parseInt(resultSet2.getString("AST_TYPE"));
			stype 			= Integer.parseInt(resultSet2.getString("PARENT_TYPE"));

			idd 			= Integer.parseInt(resultSet2.getString("ID"));
			//			int ccc 		= Integer.parseInt(resultSet2.getString("count"));
			//			count 			= count + ccc;

			/**
			 * phase2-2: getST(Lj), getST(Ls) by its parser and types of string
			 */				
			antlr_parsers.swiftparser.SwiftLexer lexer1 	= new antlr_parsers.swiftparser.SwiftLexer((CharStream)new ANTLRInputStream(bbb));
			CommonTokenStream swiftCommonStream 			= new CommonTokenStream(lexer1);
			antlr_parsers.swiftparser.SwiftParser parser 	= new antlr_parsers.swiftparser.SwiftParser(swiftCommonStream);

			JavaLexer lexerj 								= new JavaLexer((CharStream)new ANTLRInputStream(aaa));
			CommonTokenStream javaCommonStream 				= new CommonTokenStream(lexerj);
			JavaParser parserj 								= new JavaParser(javaCommonStream);
			ParserRuleContext com = null, comj=null;

			Class<? extends Parser> jparserClass = null, sparserClass = null;
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			jparserClass = cl.loadClass("antlr_parsers.javaparser.JavaParser").asSubclass(Parser.class);
			sparserClass = cl.loadClass("antlr_parsers.swiftparser.SwiftParser").asSubclass(Parser.class);


			try {

				Method jstartRule 	= null, sstartRule =null;
				jstartRule 			= jparserClass.getMethod(JavaParser.ruleNames[atype]);
				sstartRule 			= sparserClass.getMethod(SwiftParser.ruleNames[stype]);

				comj 				= (ParserRuleContext)jstartRule.invoke(parserj, (Object[])null);
				com 				= (ParserRuleContext)sstartRule.invoke(parser, (Object[])null);
				//		System.out.println("aaaaa"+new LineCharIndex().contextToLineIndex(com));
			} catch (NoSuchMethodException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (SecurityException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			if(comj!=null &&com!=null){
				/**
				 * 	phase2-2: iteratively generating templates
				 */
				//input: a pair of matching lines between Java and Swift
				//enqueueing for queue_j, queue_s
				TemplateWalk tempWalk = new TemplateWalk(comj, javaCommonStream, com, swiftCommonStream);
				tempWalk.afile = afile;
				tempWalk.sfile = sfile;

				//walking output: mappings of string templates and args
				MappingOutput result = tempWalk.walk();
				//template fails, then store it in debugging table; unsuccessful table
				if(result==null){
					int tt = QueryExamples.idd;
					String sssaaa = SQLiteJDBC.insertTableUS(tt,aaa, comj.getRuleIndex(), com.getRuleIndex(), bbb, "", "", "", "");
					java.sql.Statement sql_stmt = QueryExamples.connection.createStatement();
					QueryExamples.connection.setAutoCommit(true);
					sql_stmt.executeUpdate(sssaaa);
				}
			}
			DBTablePrinter.printTable(connection, "grammar_android_stat");
			DBTablePrinter.printTable(connection, "grammar_swift_stat");
			//						DBTablePrinter.printTable(connection, "constant_stat");
			//						DBTablePrinter.printTable(connection, "unsucess_stat");
			//			DBTablePrinter.printTable(connection, "method_table");
		}

		//		System.out.println(num);
		//		System.out.println(count);
		DBTablePrinter.printTable(connection, "grammar_android_stat");
		DBTablePrinter.printTable(connection, "grammar_swift_stat");
		DBTablePrinter.printTable(connection, "constant_stat");
		DBTablePrinter.printTable(connection, "method_table");	
		connection.close();
	}

}
