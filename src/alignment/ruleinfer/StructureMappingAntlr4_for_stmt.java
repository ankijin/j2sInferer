package alignment.ruleinfer;

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.antlr.v4.runtime.CommonTokenStream;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.tool.Attribute;
import org.apache.commons.lang3.StringUtils;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;
import org.simmetrics.metrics.NeedlemanWunch;
import org.stringtemplate.v4.ST;


import alignment.DelcarationMapping;
import alignment.common.CommonOperators;
import alignment.common.CostLCS;
import alignment.common.HungarianAlgorithm;
import alignment.common.JavaWithoutContext;
import alignment.common.LineCharIndex;
import alignment.common.MappingOutput;
import alignment.common.MigrationExamples;
import alignment.common.OpProperty;
import alignment.common.SwiftListenerWithoutContext;
import antlr_parsers.javaparser.JavaParser;
import migration.SwiftParser;
import sqldb.SQLiteJDBC;

import templates.MatchingTemplate2;

public class StructureMappingAntlr4_for_stmt {
	public boolean 					templateSolver 						= false;
	public Map<Integer, Integer> 	android_to_swift 					= new HashMap<Integer, Integer>();
	public Map<Pair, Double> 		android_to_swift_point 				= new HashMap<Pair, Double>();

	public CommonTokenStream 		swiftCommonStream					= null;
	public CommonTokenStream 		androidCommonStream					= null;

	public Map<Interval, Interval> 	constraint 							= new HashMap<Interval, Interval>();
	//	public Map<CommonOperators, CommonOperators> 	common_op_alignment = new HashMap<CommonOperators, CommonOperators>();
	public Map<CommonOperators, CommonOperators> common_op_alignment 					= new HashMap<CommonOperators, CommonOperators>();
	public Map<Interval, Interval> 	constraint_cluster 					= new HashMap<Interval, Interval>();
	public Map<Interval, List<Interval>> constraintRefAndroid 			= new HashMap<Interval, List<Interval>>();

	
	Map<OpProperty, String> aamap 					= new HashMap<OpProperty, String>();
	Map<OpProperty, String> aamapppp 				= new HashMap<OpProperty, String>();
	Map<OpProperty, String> ssmap 					= new HashMap<OpProperty, String>();
	Map<OpProperty, String> ssmapppp 				= new HashMap<OpProperty, String>();

	public String afilename, sfilename;
	public String abinding="", sbinding="";
	public String example_sql="", template_sql="", binding_sql="";

	//ssmap, aamap, nextMapping, template_a, template_s



	public static String[] java_keywords={
			"abstract",	"continue",	"for",	"new",	"switch",
			"assert", "default", "package", "synchronized",
			//			"boolean",	
			"do",	"if",	"private",	"this",
			"break",
			//			"double",	
			"implements",	"protected",	"throw",
			"byte",	"else",	"import",	"public",	"throws",
			"case",	"enum", 	"instanceof",	"return",	"transient",
			"catch",	"extends",	
			//						"int",	
			"short",	"try",
			//			"char",	
			"final", 
			"interface", "static",	
			"void",
			"class", "finally",	
			//			"long",	
			"strictfp",	"volatile",
			//			"float",	
			"native",	"super",	"while",
			"@Override","Override","@"
	};

	public static String[] common_keywords={
			"return", "if","else","class","for","while"
	};


	boolean toskip = false;

	public static List<String> java_keywords_list=Arrays.asList(java_keywords);

	public static String[] java_punctuations={"(",")", "{","}","<=", ">=",
			"[","]", 
			";","=",">","<","&&","&",":","==",",","-","+","/","*","?",".",">=","!=",".","<=","++","+=","()", "!", "==", "||","-="};

	public static List<String> java_punctuations_list=Arrays.asList(java_punctuations);

	public static String[] swift_keywords={"associatedtype", "class", "deinit", "enum", "to","Hashable",
			"extension", "fileprivate", "func", "import", "NSObject","private final", "internal var","public private(set) var", "publicprivate(set)var", "public private(set)",
			"init", "inout", "internal", "let", "open", "private(set)", "fileprivate final","publicprivate(set)","public private(set) var",
			"operator", "private", "protocol", "self","init","public static func","public static","internal func",
			"public", "static", "struct", "subscript", "typealias", "throws","public final var","internal final","public final", "convenience", "public convenience",
			"var", "break", "case", "continue", "default", "defer", "public final","public final func",
			"do", "else", "fallthrough", "for", "guard", "if", "in", "try", "class func","final",
			"repeat", "return", "switch", "where", "while", "override", "public override func", "public override var", "public override","private func", "internal override", "internal override func","as","public func","private var","public var","stride", "through", "by","private weak var","weak","private weak","super"};
	//	"private var","public var","public func"};

	public static List<String> swift_keywords_list=Arrays.asList(swift_keywords); 
	public static String[] swift_punctuations={"(",")", "{","}","[","]", ";","=","->",":","<",">","&&","&","-","..<","..",",","!","?",">=","!=", "& &",".","+","??","==","||", "-=", "+=","=<","*","/","<=","()","_","|","++","<="};
	public static List<String> swift_punctuations_list=Arrays.asList(swift_punctuations);

	//	public static String[] common_operators={"+","-","/","*",".","<",">", "= =","=="};
	public static String[] common_operators={"+","-","/","*",".","!",",","||","<=","==",">","<","?",":","[","]","+=","[]", "=","-=", "(",")","&&",">="};
	public static String[] common_operators_bi={"+","-","/","*",".","!",",","||","<=","==",">","<","?",":","[","]","="};
	//	public static String[] common_operators={"+","-","/","*",".",",","?",":","!",">","<","(",")"};
	public static List<String> common_operators_list = Arrays.asList(common_operators);

	public static int testnum = 0;
	public static int grammar_id=0;
	public static int ggrammar_id=0;
	ParserRuleContext android;
	ParserRuleContext swift;
	public SwiftListenerWithoutContext swift_listner;
	//	public JavaListenerWithoutContext wocontext;
	public JavaWithoutContext android_listener;

	Map<Interval, String> amap_ct 	= new HashMap<Interval, String>();
	Map<Interval, String> smap_ct 	= new HashMap<Interval, String>();

	private boolean stopped 	= false;
	public TokenStreamRewriter rewriter;
	public CommonTokenStream tokens;
	public List<CommonOperators> java_common_ops;
	public List<CommonOperators> swift_common_ops;
	public StructureMappingAntlr4_for_stmt(ParserRuleContext android, ParserRuleContext swift) throws FileNotFoundException, IOException{

		this.android 	= android;
		this.swift 		= swift;

		/**
		 * get main constraints, fed back to the .. more levels
		 * skipping common sequence operators in {...}
		 * checking common start end for better mapping 
		 */


		//		swift_listner.op_s_nodes.clear();
		//		android_listener.op_a_nodes.clear();

		swift_listner 			= new SwiftListenerWithoutContext(swift);
		android_listener 		= new JavaWithoutContext(android);

	}
	double cost = 0;
	double prev_cost = 0;
	int prev_id =0;

	String anchors = "";

	/**
	 * clear contents
	 * spawning up constraints
	 */
	public void initialize(){


		//		System.out.println("initialize");


		cost = 0;
		android_to_swift_point.clear();
		android_to_swift.clear();

		ssmap.clear();
		aamap.clear();
		aamapppp.clear();
		ssmapppp.clear();
		ssmap		= new HashMap<OpProperty, String>();
		aamap		= new HashMap<OpProperty, String>();
		aamapppp	= new HashMap<OpProperty, String>();
		ssmapppp	= new HashMap<OpProperty, String>();

		swift_listner.termsMap.clear();
		swift_listner.scope_list.clear();
		swift_listner.scope_list = new LinkedHashMap<String, Attribute>();
		android_listener.op_a_nodes.clear();
		swift_listner.op_s_nodes.clear();
		android_listener.termsMap.clear();
		swift_listner.termsMapOnlys.clear();
		swift_listner.termsMapOnly.clear();
		android_listener.termsMapOnly.clear();
		android_listener.termsMapOnlys.clear();
		//		android_listener.termsMapOnlys
		android_listener.blockIndex = null;
		swift_listner.blockIndex = null;
		swift_listner.last = null;
		//		android_listener.leftIndex = null;
		//		swift_listner.leftIndex = null;

		amap_ct.clear();
		smap_ct.clear();

		example_sql = "";
		template_sql=""; 
		binding_sql="";

		//		nextMapping.clear();
		android_listener.scope_constraint.clear();
		swift_listner.scope_constraint.clear();



		ParseTreeWalker walker_swift = new ParseTreeWalker();
		ParseTreeWalker walker_android = new ParseTreeWalker();


		walker_swift.walk(swift_listner, swift);
		walker_android.walk(android_listener, android);
//		System.out.println("op_a_nodes"+android.getSourceInterval()+"  "+android_listener.op_a_nodes);
//		System.out.println("op_s_nodes"+swift.getSourceInterval()+"  "+swift_listner.op_s_nodes);
//		System.out.println("@@android_listener.termsMapOnlys"+android_listener.termsMapOnlys+"  "+swift_listner.termsMapOnlys);
//		android_listener.
		
		constraint.clear();

		common_op_alignment.clear();

		Map<Interval, String> terms = android_listener.termsMap;
		Map<Interval, String> terms_swft = swift_listner.termsMap;

		if(java_common_ops==null && swift_common_ops==null){
		//		List<CommonOperators> lefttoright = ListUtils.longestCommonSubsequence(android_listener.termsMapOnlys, swift_listner.termsMapOnlys);
		java_common_ops= new LinkedList<CommonOperators>(); 
		swift_common_ops = new LinkedList<CommonOperators>();


		//		System.out.println("initialize");
		//tokenList
		List<Interval> list = new ArrayList<Interval>(terms.keySet());
		for(CommonOperators c:android_listener.termsMapOnlys){
			List<Interval> aklist = new ArrayList<Interval>(android_listener.termsMap.keySet());
			int k=aklist.indexOf(c.interval);
			Interval before= null;
			Interval after = null;
			if(k>0){
				before=aklist.get(k-1);
			}
			if(aklist.size()>k+1){
				after=aklist.get(k+1);
			}

			int i=list.indexOf(c.interval);
			if(c.text.equals(",") && before!= null){
				CommonOperators common = new CommonOperators(",", c.interval.union(before), c.interval);
				common.depth = c.depth;
			
				java_common_ops.add(common);
			}
			else if(c.text.equals("!") && after!= null){
				//				CommonOperators common = new CommonOperators("!", c.interval.union(list.get(i+1)));
				CommonOperators common = new CommonOperators("!", c.interval.union(after), c.interval);
				common.depth = c.depth;
				java_common_ops.add(common);
			}
			else if(c.text.equals("[") && after!= null){
				//				CommonOperators common = new CommonOperators("!", c.interval.union(list.get(i+1)));
				CommonOperators common = new CommonOperators("[", c.interval, c.interval);
				common.depth = c.depth;
				java_common_ops.add(common);
			}
			else if(c.text.equals("]") && after!= null){
				//				CommonOperators common = new CommonOperators("!", c.interval.union(list.get(i+1)));
				CommonOperators common = new CommonOperators("]", c.interval, c.interval);
				common.depth = c.depth;
				java_common_ops.add(common);
			}
			else { 

				if((before!= null && common_operators_list.contains(androidCommonStream.getText(before)))||(after!= null&& after.equals("{") && common_operators_list.contains(androidCommonStream.getText(after))))
				{
					CommonOperators common = new CommonOperators(terms.get(list.get(i)), c.interval, c.interval);
					common.depth = c.depth;
					java_common_ops.add(common);

				} else{
					Interval intval = null;

					if(before!=null){
						if(after!=null){
							intval = c.interval.union(before).union(after);
						} else{
							intval = c.interval.union(before);
						}
					}
					if(intval!=null){
						CommonOperators common = new CommonOperators(terms.get(list.get(i)), intval, c.interval);
						common.depth = c.depth;
						java_common_ops.add(common);
					}

				}
			}
		}

		//		System.out.println("initialize");

		/*
		for(CommonOperators c:android_listener.termsMapOnlys){
			int i=list.indexOf(c.interval);
			if(c.text.equals(",")){

				CommonOperators common = new CommonOperators(",", c.interval.union(list.get(i-1)));
				//				last=last.union(list.get(i+1));
				newlefttoright.add(common);
				j++;
			}
			else{

				if(i>0){
					if(last==null) last = list.get(i-1);
					//						System.out.println("last1	"+Math.abs(last.a-list.get(i-1).a));
					if(Math.abs(last.b-list.get(i-1).a) <=margin){
						//update last
						last=last.union(list.get(i-1));
						//							System.out.println("last1	"+last);
					} else{

						CommonOperators common = new CommonOperators(ops_android, last);
						//					common.ops_intervals = oplist;
						newlefttoright.add(common);

						//					newlefttoright.add(new CommonOperators(ops_android, last));
						last = list.get(i-1);
						ops_android ="";
						//					oplist.clear();
						//							System.out.println("last1<	"+last);
					}
					//						newlefttoright.add(new CommonOperators(terms.get(list.get(i-1)), list.get(i-1))); //left
					//						last  = list.get(i-1);


					if(Math.abs(last.b-list.get(i).a) <=margin){
						last=last.union(list.get(i));
						ops_android +=terms.get(list.get(i));
						//					oplist.add(c.interval);

						//						System.out.println("last2	"+last);
					} else{
						CommonOperators common = new CommonOperators(ops_android, last);
						//					common.ops_intervals = oplist;
						newlefttoright.add(common);
						ops_android ="";
						//					oplist.clear();
						last = list.get(i);
						//						System.out.println("last2<	"+last);
					}

					if(list.size()>i+1 && Math.abs(last.b-list.get(i+1).a) <=margin){
						last=last.union(list.get(i+1));


					} else{
						//					newlefttoright.add(new CommonOperators(ops_android, last));
						CommonOperators common = new CommonOperators(ops_android, last);
						//					common.ops_intervals = oplist;
						newlefttoright.add(common);
						ops_android ="";
						//					oplist.clear();
						if(list.size()>i+1)
							last = list.get(i+1);
					}

					j++;
					if(j==lefttoright.size()){
						//					newlefttoright.add(new CommonOperators(ops_android, last));
						CommonOperators common = new CommonOperators(ops_android, last);
						//					common.ops_intervals = oplist;
						newlefttoright.add(common);
					}
				}
			}
		}
		 */	
		//		oplist.clear();
		//		List<CommonOperators> righttoleft = ListUtils.longestCommonSubsequence(swift_listner.termsMapOnlys, android_listener.termsMapOnlys);

		list.clear();
		list = new ArrayList<Interval>(terms_swft.keySet());



		//		System.out.println("initialize");

		for(CommonOperators c:swift_listner.termsMapOnlys){
			//		for(CommonOperators c:righttoleft){

			int i=list.indexOf(c.interval);


			List<Interval> sklist = new ArrayList<Interval>(swift_listner.termsMap.keySet());
			int k=sklist.indexOf(c.interval);
			Interval before= null;
			Interval after = null;
			//			System.out.println("sklist.size()"+sklist.size()+" "+k);
			if(k>0){
				before=sklist.get(k-1);
			}
			//size 3, ..K+1=3
			//Index: 4, Size: 4
			//size 4 k+1=4
			if(sklist.size()>k+1){
				after=sklist.get(k+1);
			}
			//			Interval before=new Interval(c.interval.a-1, c.interval.b-1);
			//			Interval after=new Interval(c.interval.a+1, c.interval.b+1);

			if(c.text.equals(",")){
				if(before!=null){
					CommonOperators common = new CommonOperators(",", c.interval.union(before), c.interval);
					common.depth = c.depth;
					//				CommonOperators common = new CommonOperators(",", c.interval.union(list.get(i-1)));
					swift_common_ops.add(common);
				}
			}

			else if(c.text.equals("!") && after!=null){
				//				CommonOperators common = new CommonOperators("!", c.interval.union(list.get(i+1)));
				CommonOperators common = new CommonOperators("!", c.interval.union(after), c.interval);
				common.depth = c.depth;

				swift_common_ops.add(common);
			}
			else if(c.text.equals("[")){
				//				CommonOperators common = new CommonOperators("!", c.interval.union(list.get(i+1)));
				CommonOperators common = new CommonOperators("[", c.interval, c.interval);
				common.depth = c.depth;
				swift_common_ops.add(common);
			}
			else if(c.text.equals("]")){
				//				CommonOperators common = new CommonOperators("!", c.interval.union(list.get(i+1)));
				CommonOperators common = new CommonOperators("]", c.interval, c.interval);
				common.depth = c.depth;
				swift_common_ops.add(common);
			}

			else{ // $.$


				//				if(swift_punctuations_list.contains(swiftCommonStream.getText(list.get(i-1)))||swift_punctuations_list.contains(swiftCommonStream.getText(list.get(i+1))))
				if((before!=null && common_operators_list.contains(swiftCommonStream.getText(before)))||(after!=null && common_operators_list.contains(swiftCommonStream.getText(after)) && swiftCommonStream.getText(after).equals("{")))

				{

					CommonOperators common = new CommonOperators(terms.get(list.get(i)), c.interval, c.interval);
					common.depth = c.depth;
					swift_common_ops.add(common);

				} else{
					//					if(i>0){
					//						Interval intval = c.interval.union(list.get(i-1)).union(list.get(i+1));
					Interval intval =null;
					if(before!=null){
						if(after!=null){
							//							intval = c.interval.union(before).union(after);
							intval = c.interval.union(before).union(after);
						} else{
							intval = c.interval.union(before);
						}
					}


					if(intval!=null){
						CommonOperators common = new CommonOperators(terms_swft.get(list.get(i)), intval, c.interval);
						common.depth = c.depth;
						boolean isadj = false;
						swift_common_ops.add(common);
					}
				}
			}


		}

		
		}

		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		double[][] aecostMatrix = new double[java_common_ops.size()][swift_common_ops.size()];
		CostLCS[][] ccMatrix = new CostLCS[java_common_ops.size()][swift_common_ops.size()];


		//optimal alignment of operator sequence
		//common sub sequence of operator sequence
//		System.err.println("android_listener.termsMapOnlys"+android_listener.termsMapOnlys);
//		System.err.println("swift_listner.termsMapOnlys"+swift_listner.termsMapOnlys);
//		System.err.println("swift_common_ops"+swift_common_ops);
//		System.err.println("java_common_ops"+java_common_ops);
//		System.err.println("swift_common_ops"+swift_common_ops);
		for(int ai=0; ai<java_common_ops.size();ai++){
			CommonOperators a_common = java_common_ops.get(ai);
			String a_ops=a_common.text;
			String a_ctx = "a", s_ctx = "s";

			for(int si=0; si<swift_common_ops.size();si++){
				CommonOperators s_common = swift_common_ops.get(si);
				String s_ops=s_common.text;
				//				System.out.println("shifting	"+a_ops+"  "+s_ops);
				if(a_ops.equals(s_ops)){
					//					System.out.println("common ops"+a_ops+" "+s_ops+" "+a_common.interval+" "+s_common.interval);
					a_ctx = androidCommonStream.getText(a_common.interval);
					s_ctx = swiftCommonStream.getText(s_common.interval);
					aecostMatrix[ai][si]= Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx));
					ccMatrix[ai][si] = new CostLCS(Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx)), a_common.interval, s_common.interval);

				} else{
					aecostMatrix[ai][si]= 1;
					ccMatrix[ai][si] = new CostLCS(1, a_common.interval, s_common.interval);

				}
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		//			int[] eresult_copy = new int[0];
		if(java_common_ops.size() > 0 && swift_common_ops.size() >0){
			ehung = new HungarianAlgorithm(aecostMatrix);
			eresult = ehung.execute();
			//				eresult_copy = ehung.execute();
		}

		//				constraint.put(new Interval(3,3), new Interval(4,5));
		//				constraint.put(new Interval(2,2), new Interval(9,9));
		//				constraint.put(new Interval(3,3), new Interval(11,11));
		anchors = "";
		for(int p=0; p<eresult.length;p++){


			//			if(eresult[p]!=-1 && aecostMatrix[p][eresult[p]] !=1){
			if(eresult[p]!=-1){
				//					System.out.println("android_listener.termsMapOnlys"+android_listener.termsMapOnlys);
				//					System.out.println("swift_listner.termsMapOnlys"+swift_listner.termsMapOnlys);
				//				System.out.println("final"+ newlefttoright.get(p)+"  "+newrighttoleft.get(eresult[p]));
				//				System.out.println("finalops_intervals"+ newlefttoright.get(p).ops_intervals);
				//				System.out.println("swiftfinalops_intervals"+ newrighttoleft.get(p).ops_intervals);
				List<Interval> alist = new LinkedList<Interval>();
				//				List<Commo> alist_commonops = new LinkedList<Interval>();
				List<Interval> slist = new LinkedList<Interval>();
				List<CommonOperators> calist = new LinkedList<CommonOperators>();
				List<CommonOperators> cslist = new LinkedList<CommonOperators>();


				for(CommonOperators c:android_listener.termsMapOnlys){
					//					androidCommonStream.getText(ccMatrix[p][eresult[p]].minIntvlA)+":"+swiftCommonStream.getText(ccMatrix[p][eresult[p]].minItvlS);
					if(ccMatrix[p][eresult[p]].minIntvlA.properlyContains(c.interval)){
						alist.add(c.interval);
						calist.add(c);
					}
				}
				for(CommonOperators cs:swift_listner.termsMapOnlys){
					//					System.out.println("cscs	"+cs.text);
					if(ccMatrix[p][eresult[p]].minItvlS.properlyContains(cs.interval)){
						//					
						slist.add(cs.interval);
						cslist.add(cs);
					}
				}


				//				newlefttoright[2..8:==||<=]   [3..3:==, 5..5:||, 7..7:<=]
				//						newrighttoleft[]   [4..5:==, 9..10:||, 14..15:<=]


				for(int jjj=0; jjj< alist.size() && slist.size()>jjj; jjj++){
					String aop = androidCommonStream.getText(alist.get(jjj));
					String sop = swiftCommonStream.getText(slist.get(jjj));

					if(aop.equals(sop.replaceAll(" ", ""))){
						constraint.put(alist.get(jjj), slist.get(jjj));
						common_op_alignment.put(calist.get(jjj), cslist.get(jjj));
//						System.out.println("aop	"+ aop+" "+sop+" 	"+calist.get(jjj)+"  "+cslist.get(jjj));
					}
					//					CommonOperators oppa = calist.get(jjj);
					//					CommonOperators opps = cslist.get(jjj);




					//					int i=list.indexOf(c.interval);


					List<Interval> sklist = new ArrayList<Interval>(swift_listner.termsMap.keySet());
					List<Interval> aklist = new ArrayList<Interval>(android_listener.termsMap.keySet());
					int ak=aklist.indexOf(alist.get(jjj));
					//					System.out.println("aklist"+aklist+"  "+ak+"  "+oppa.opi);
					Interval abefore=null, aafter=null, sbefore=null, safter=null;
					if(ak>0){
						abefore=aklist.get(ak-1);
					}
					if(aklist.size()>ak+1){
						aafter=aklist.get(ak+1);
					}
					int sk=sklist.indexOf(slist.get(jjj));
					if(sk>0){
						sbefore=sklist.get(sk-1);
					}
					if(sklist.size()>sk+1){
						safter=sklist.get(sk+1);
					}

					//					Interval before=new Interval(c.interval.a-1, c.interval.b-1);
					//					Interval after=new Interval(c.interval.a+1, c.interval.b+1);

					if(aop.equals(",") &&  sop.equals(",") && abefore!=null && sbefore !=null){
						//						constraint.put(abefore, sbefore);
					}

					else if(aop.equals("!") &&  sop.equals("!")&& aafter!=null && safter !=null){
						//						constraint.put(aafter, safter);
					}

					else if(aop.equals(sop)){ // $.$


						//				if(swift_punctuations_list.contains(swiftCommonStream.getText(list.get(i-1)))||swift_punctuations_list.contains(swiftCommonStream.getText(list.get(i+1))))
						if((sbefore!=null && common_operators_list.contains(swiftCommonStream.getText(sbefore)))||(safter!=null && common_operators_list.contains(swiftCommonStream.getText(safter)) && !swiftCommonStream.getText(safter).equals("{") ))

						{
						} else{
							if(aafter!=null && safter!=null && !swiftCommonStream.getText(safter).equals("{")){
								//								constraint.put(aafter, safter);
							}
							if(abefore!=null && sbefore!=null){
								//								constraint.put(abefore, sbefore);
							}
						}
					}








					/*
					for(CommonOperators a:newlefttoright){
						for(CommonOperators s:newrighttoleft){
							if(!a.interval.disjoint(alist.get(jjj)) && !s.interval.disjoint(slist.get(jjj))){
//								constraint.put(a.interval, s.interval);
							}
						}
					}
					 */
					//					}
					/*
					if(androidCommonStream.getText(alist.get(jjj)).equals(".") && swiftCommonStream.getText(slist.get(jjj)).equals(".")){
						constraint.put(new Interval(alist.get(jjj).a+1,alist.get(jjj).a+1), new Interval(slist.get(jjj).a+1,slist.get(jjj).a+1));

						if(swiftCommonStream.getText(new Interval(slist.get(jjj).a-1,slist.get(jjj).a-1)).equals("?"))
							constraint.put(new Interval(alist.get(jjj).a-1,alist.get(jjj).a-1), new Interval(slist.get(jjj).a-2,slist.get(jjj).a-2));
						else
							constraint.put(new Interval(alist.get(jjj).a-1,alist.get(jjj).a-1), new Interval(slist.get(jjj).a-1,slist.get(jjj).a-1));
					 */

					//						System.out.println("cccccc	"+androidCommonStream.getText(new Interval(alist.get(jjj).a-1,alist.get(jjj).a+1))+" "+
					//								swiftCommonStream.getText(new Interval(slist.get(jjj).a-1,slist.get(jjj).a+1))
					//								);
					//					}

				}
				//				

				//				System.out.println("constraint	"+constraint);

				//				System.out.println("finalcc	"+ccMatrix[p][eresult[p]]+"  "+androidCommonStream.getText(ccMatrix[p][eresult[p]].minIntvlA)+"    "+
				//						swiftCommonStream.getText(ccMatrix[p][eresult[p]].minItvlS));
				//				constraint.put(ccMatrix[p][eresult[p]].minIntvlA, ccMatrix[p][eresult[p]].minItvlS);
				constraint_cluster.put(ccMatrix[p][eresult[p]].minIntvlA, ccMatrix[p][eresult[p]].minItvlS);

				anchors += " "+androidCommonStream.getText(ccMatrix[p][eresult[p]].minIntvlA)+":"+swiftCommonStream.getText(ccMatrix[p][eresult[p]].minItvlS);
			}
		}
		//		System.out.println("terms"+android_listener.termsMap);
		//		System.err.println("terms"+swift_listner.termsMap);

		//				constraint.put(new Interval(1, 1), new Interval(5, 5));
		//				constraint.put(new Interval(3, 3), new Interval(9, 9));
		//				constraint.put(new Interval(3, 3), new Interval(12, 12));
		//				constraint.put(new Interval(5, 5), new Interval(16, 16));
		/*
				constraint.put(new Interval(3, 3), new Interval(7, 7));
				constraint.put(new Interval(5, 5), new Interval(9, 9));
				constraint.put(new Interval(7, 7), new Interval(11, 11));
		 */
		//		constraint.put(new Interval(1, 1), new Interval(1, 1));
		//		constraint.put(new Interval(3, 3), new Interval(3, 3));
		//		constraint.put(new Interval(5, 5), new Interval(5, 5));
		//		constraint.put(new Interval(7, 7), new Interval(7, 7));
		//		const 0..8    4..12
		//		next_cmXBounds.range+mXBounds.min-z  _xBounds.range+_xBounds.min-z
		//		terms_android	{1..1=., 3..3=+, 5..5=., 7..7=-}
		//		terms_swft	{1..1=., 3..3=+, 5..5=., 7..7=+, 9..9=., 11..11=-}
		//		constraint.put(new Interval(4, 4), new Interval(3, 3));
		//		constraint.put(new Interval(17, 17), new Interval(12, 12));

		//				constraint.put(new Interval(22, 30), new Interval(21, 29));
		//		16..26
		Iterator<Interval> constraint_keys = constraint.keySet().iterator();
//		System.out.println("constraint"+constraint);
		while(constraint_keys.hasNext()){
			Interval next_c = constraint_keys.next();
			//			System.out.println("const "+next_c+"    "+constraint.get(next_c));
			//						System.out.println("next_c"+androidCommonStream.getText(next_c)+"  "+swiftCommonStream.getText(constraint.get(next_c)));
		}
		//		List<Interval> listc = new LinkedList<Interval>();
		//		listc.add(new Interval(5, 5));
		//		listc.add(new Interval(14, 14));
		//		listc.add(new Interval(29, 29));

		//		constraintRefAndroid.put(new Interval(5, 5), listc);
		//		System.out.println();
	}


	/**
	 * With constraint map, checking relation interval-a and interval-s
	 * @param a
	 * @param s
	 * @return
	 */
	public boolean constraintMet(Interval a, Interval s){
		Iterator<Interval> a_anchors = constraint.keySet().iterator();
		boolean isMet = true;
		//3..5=>12..16 
		//2..2=7..7, 4..4=14..14
		//3:yValueSum -> .. 9..12:CGFloat(yValueSum)
		//value / yValueSum * mMaxAngle 
		/**
		 /  /
		 * * 
		value/yValueSum  ) / CGFloat

		yValueSum*mMaxAngle  ) * _maxAngle
		 */
		//value / yValueSum * mMaxAngle
		//CGFloat(value) / CGFloat(yValueSum) * _maxAngle
		while(a_anchors.hasNext()){
			Interval a_anchor=a_anchors.next();
			Interval s_anchor=constraint.get(a_anchor);
			if(a_anchor.disjoint(a) && s_anchor.disjoint(s)){
				isMet = isMet && true;
			}
			else if(a_anchor.disjoint(a) && !s_anchor.disjoint(s)){return false;}
			else if(!a_anchor.disjoint(a) && s_anchor.disjoint(s)){return false;}
			else if(!a_anchor.disjoint(a) && !s_anchor.disjoint(s)){
				isMet = isMet && true;
			}
			/*
			else{
				boolean a_valid = true, s_valid = true;
				if(a.length()> a_anchor.length()){
					a_valid = a.properlyContains(a_anchor);
				}else{
					a_valid = a_anchor.properlyContains(a);
				}

				if(s.length()> s_anchor.length()){
					s_valid = s.properlyContains(s_anchor);
				}else{
					s_valid = s_anchor.properlyContains(s);
				}
				isMet = a_valid && s_valid && isMet;
				//				isMet = a.properlyContains(a_anchor) && s.properlyContains(s_anchor) && isMet;
				//				isMet = a_anchor.properlyContains(a) && s_anchor.properlyContains(s) && isMet;
			}
			 */
		}
		return isMet;
	}
	/*
	public boolean 
(Interval a, Interval s){
		Iterator<Interval> a_anchors = constraint.keySet().iterator();
		boolean isMet = true;
		while(a_anchors.hasNext()){
			Interval a_anchor=a_anchors.next();
			Interval s_anchor=constraint.get(a_anchor);
			if(a_anchor.disjoint(a) && s_anchor.disjoint(s)){
				isMet = isMet && true;
			} else if((!a_anchor.disjoint(a) || !s_anchor.disjoint(s))){
				isMet = a.properlyContains(a_anchor) && s.properlyContains(s_anchor) && isMet;
				//				isMet = a_anchor.properlyContains(a) && s_anchor.properlyContains(s) && isMet;
			}
		}
		return isMet;
	}
	 */
	//	public void compute() throws FileNotFoundException, IOException{
	public MappingOutput compute() throws FileNotFoundException, IOException{

		//		waking();

		int az = swift.start.getStartIndex(), bz = swift.stop.getStopIndex();
		Interval swift_interval = new Interval(az,bz);

		int az1 = android.start.getStartIndex(), bz1 = android.stop.getStopIndex();
		Interval android_interval = new Interval(az1,bz1);

		//				System.out.println("android_listener.a_nodes"+android_listener.a_nodes);
		//				System.err.println("swift_listner.s_node"+swift_listner.s_nodes);
		//children mappings
		//resolution {} blocks, = assignments and expressions
		if(android_listener.a_nodes.size()>0 && swift_listner.s_nodes.size()>0){

			//matching and save them
			//a_nodes or s_nodes stores statements
			double[][] aecostMatrix = new double[android_listener.a_nodes.size()][swift_listner.s_nodes.size()];

			StringMetric emetric =
					with(new JaroWinkler())
					.build();

			for(int i=0;i<android_listener.a_nodes.size();i++){
				for(int j=0;j<swift_listner.s_nodes.size();j++){
					if( android_listener.a_nodes.get(i).property.equals(swift_listner.s_nodes.get(j).property)) {
						//					if( android_listener.a_nodes.get(i).property.equals(swift_listner.s_nodes.get(j).property) && toCommon(android_listener.a_nodes.get(i).strASTNode,swift_listner.s_nodes.get(j).strASTNode)>=0.0) {
						aecostMatrix[i][j]	= Math.min(0.999, 1- emetric.compare(android_listener.a_nodes.get(i).strASTNode, swift_listner.s_nodes.get(j).strASTNode));
					}else{
						aecostMatrix[i][j] 	= 1;
					}


				}
			}

			HungarianAlgorithm ehung = null;
			int[] eresult = new int[0];
			//			int[] eresult_copy = new int[0];
			if(android_listener.a_nodes.size() > 0 && swift_listner.s_nodes.size() >0){
				ehung 	= new HungarianAlgorithm(aecostMatrix);
				eresult = ehung.execute();

				//				eresult_copy = ehung.execute();
			}


			for(int p=0; p<eresult.length;p++){
				if(eresult[p]!=-1 && eresult[p]<1){
					//				if(1==1){

					//					String a_exam = android_listener.a_nodes.get(p).strASTNode;
					//					String s_exam = swift_listner.s_nodes.get(eresult[p]).strASTNode;
					if(
							android_listener.a_nodes.get(p).node!=null 
							&& android_listener.a_nodes.get(p).node instanceof ParserRuleContext 
							&& swift_listner.s_nodes.get(eresult[p]).node!=null 
							&& swift_listner.s_nodes.get(eresult[p]).node instanceof ParserRuleContext
							)
					{

						StructureMappingAntlr4_for_stmt mapping = new StructureMappingAntlr4_for_stmt((ParserRuleContext) android_listener.a_nodes.get(p).node, (ParserRuleContext)swift_listner.s_nodes.get(eresult[p]).node);
						mapping.afilename = afilename;
						mapping.sfilename = sfilename;
						mapping.androidCommonStream = this.androidCommonStream;
						mapping.swiftCommonStream 	= this.swiftCommonStream;
						//						System.out.println("examples"+mapping.android.getText()+" "+mapping.swift.getText());

						//						mapping.waking();
						//						mapping.compute();
					}
				}
			}

		} /**enclosed's children mappings using by {{...}}, = **/




		double[][] ecostMatrix = new double[android_listener.op_a_nodes.size()][swift_listner.op_s_nodes.size()];
		for(int k=0; k<android_listener.op_a_nodes.size();k++){
			Arrays.fill(ecostMatrix[k], 1);
		}

		//				MappingOutput mappingout /= new MappingOutput();
		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		//				StringMetric emetric = new Levenshtein();
		//		StringMetric emetric = new NeedlemanWunch();
		//				new StringMetrics.needlemanWunch();



		//				Cache<String, Set<String>> tokenCache;
		//				StringMetric emetric = 
		//						with(new CosineSimilarity<String>())
		//						.simplify(Simplifiers.toLowerCase())
		//				.simplify(Simplifiers.removeNonWord())
		//				.cacheStrings(stringCache)
		//						.tokenize(Tokenizers.qGram(1))
		//										.cacheTokens(tokenCache)
		//						.build();

		for(int i=0;i<android_listener.op_a_nodes.size();i++){
			for(int j=0;j<swift_listner.op_s_nodes.size();j++){
//				boolean isNumA = NumberUtils.isNumber(android_listener.op_a_nodes.get(i).strASTNode);
//				boolean isNumS = NumberUtils.isNumber(swift_listner.op_s_nodes.get(j).strASTNode);


				if(    
						//						!swift_listner.op_s_nodes.get(j).property.equals("!none")	&& 
						//						!android_listener.op_a_nodes.get(i).property.equals("!none") && 
						android_listener.op_a_nodes.get(i).property.equals(swift_listner.op_s_nodes.get(j).property)
						//						&& toCommon(android_listener.op_a_nodes.get(i).strASTNode,swift_listner.op_s_nodes.get(j).strASTNode)>=0

						) {


					if((android_listener.op_a_nodes.get(i).interval.length()==1) && (swift_listner.op_s_nodes.get(j).strASTNode.contains(" "))){
						ecostMatrix[i][j] 	= 1;	
					}
					else{

						if(constraintMet(android_listener.op_a_nodes.get(i).interval, swift_listner.op_s_nodes.get(j).interval)){
							ecostMatrix[i][j]	= Math.min(0.999, 1- emetric.compare(android_listener.op_a_nodes.get(i).strASTNode, swift_listner.op_s_nodes.get(j).strASTNode));

//							if(isNumA==isNumS==true){
								//							android_listener.op_a_nodes.get(i).strASTNode = swift_listner.op_s_nodes.get(j).strASTNode;
								//							ecostMatrix[i][j]	=  0;
//							}

							//						System.out.println("met_ecostMatrix[i][j]"+ecostMatrix[i][j]+"  "+android_listener.op_a_nodes.get(i)+"  "+swift_listner.op_s_nodes.get(j));
						}//if anchors(+/*-.{}= constraint met the syntax mapping score not to maxima, followed by the String similarity
						else{
							ecostMatrix[i][j] 	= 1;
							//						System.out.println("unmet_ecostMatrix[i][j]"+ecostMatrix[i][j]+"  "+android_listener.op_a_nodes.get(i)+"  "+swift_listner.op_s_nodes.get(j));
						}//else the syntax mapping score not to maxima, cross-level anchoring
					}
				}else{
					ecostMatrix[i][j] 	= 1;
				}
			}
		} //cost matrix ecostMatrix[i][j]


		//		int NUM_K_HUNG = 30;
		int NUM_K_HUNG = 1;





		if(android_listener.op_a_nodes.size() == 0 || swift_listner.op_s_nodes.size() == 0) {
//			System.out.println("zeror??");;
			return null;
		}


		int[] eresult 		= new int[0];
		List<int[]> rowsols = new LinkedList<int[]>();

		//		if(android_listener.a_nodes.size() > 0 && swift_listner.s_nodes.size() >0){
		HungarianAlgorithm ehung = new HungarianAlgorithm(ecostMatrix);
		eresult = ehung.execute();

		//				eresult_copy = ehung.execute();
		//		}

		//		if(android_listener.op_a_nodes.size()<=swift_listner.op_s_nodes.size()){
		//			try{
		//				rowsols = Murty.solve(ecostMatrix,NUM_K_HUNG);
		//			} catch (java.lang.ArrayIndexOutOfBoundsException e){
		//				e.printStackTrace();
		//			}


		Comparator<OpProperty> comparator = new Comparator<OpProperty>() {
			public int compare(OpProperty o1, OpProperty o2) {
				if(o1.interval.length()>o2.interval.length()) return 1; 
				else if(o1.interval.length()==o2.interval.length()){
					if(o1.interval.a <= o2.interval.a) return 1;
					else return -1;
				}
				else return -1;
			}
		};

		//			System.out.println("rowsols.size()	"+rowsols.size());
		int k =0;
		//			boolean notfoundA = true, notfoundS = true;
		//iterations
		//			for (int r =0; r<rowsols.size() && notfoundA && notfoundS; r++) {

		//			for (int r =0; r<rowsols.size(); r++) {
		//				eresult = rowsols.get(r);
		cost = 0.0;
		double matchingPoint = 0.0;
		for (int i = 0; i < eresult.length; i++) {

			if(eresult[i]!=-1){
				android_to_swift.put(i, eresult[i]);
				//				android_to_swift_point.put(new Pair(i, eresult[i], android_listener.op_a_nodes.get(i).interval, swift_listner.op_s_nodes.get(eresult[i]).interval), ecostMatrix[i][eresult[i]]);
				OpProperty aap1 = android_listener.op_a_nodes.get(i);
				OpProperty ssp1 = swift_listner.op_s_nodes.get(eresult[i]);
				//				if(android_listener.op_a_nodes.get(i).interval.length()==1)

				/*
				StringMetric metric = 
						with(new CosineSimilarity<String>())
						.simplify(Simplifiers.toLowerCase())
						//				.simplify(Simplifiers.removeNonWord())
						//				.cacheStrings(stringCache)
						.tokenize(Tokenizers.qGram(2))
						//				.cacheTokens(tokenCache)
						.build();
				 */


				double ccccc= Math.min(0.999, 1- emetric.compare(android_listener.op_a_nodes.get(i).strASTNode, swift_listner.op_s_nodes.get(eresult[i]).strASTNode));

				android_to_swift_point.put(new Pair(i, eresult[i], android_listener.op_a_nodes.get(i).interval, swift_listner.op_s_nodes.get(eresult[i]).interval,aap1, ssp1), ccccc);
				//				System.out.println("aap1	"+ aap1+"  "+ssp1+  " "+ecostMatrix[i][eresult[i]]);

				//				Pair ppp = new Pair(i, eresult[i], android_listener.op_a_nodes.get(i).interval, swift_listner.op_s_nodes.get(eresult[i]).interval), 
				//						aap1, swift_listner.op_s_nodes.get(eresult[i]));
				//				android_to_swift_point.put(ppp
				//						, ecostMatrix[i][eresult[i]]);

			}
		}


		SortedSet<OpProperty> akeys = new TreeSet<OpProperty>(comparator);
		SortedSet<OpProperty> skeys = new TreeSet<OpProperty>(comparator);



		//			Map<String, String> smap 		= new HashMap<String, String>();

		LinkedHashMap<OpProperty, OpProperty> nextMapping = new LinkedHashMap<OpProperty, OpProperty>();

		Map<Interval, String> terms = android_listener.termsMap;
		Map<Interval, String> terms_swft = swift_listner.termsMap;

		for(int p=0; p<eresult.length;p++){
			//					if(eresult[p]!=-1){
			//						System.out.println("rank "+k+" ecostMatrix"+"["+p+"]"+"["+eresult[p]+"]="+ecostMatrix[p][eresult[p]]+" "+android_listener.op_a_nodes.get(p).strASTNode+"  "+android_listener.op_a_nodes.get(p).property+"  "+swift_listner.op_s_nodes.get(eresult[p]).strASTNode+"  "+swift_listner.op_s_nodes.get(eresult[p]).property);
			//					}

			if(eresult[p]!=-1){
				//				cost +=ecostMatrix[p][eresult[p]];
			} //get mapping cost 
		} //iteration of ...

		//				MappingOutput mappingout = new MappingOutput();
		int argnum = 0;
		int num_m = 0;
		List<Interval> listi = new LinkedList<Interval>();

		Iterator<Integer> ittt = android_to_swift.keySet().iterator();
		while(ittt.hasNext()){
			int aa = ittt.next();
			int ss = android_to_swift.get(aa);

			if(ecostMatrix[aa][eresult[aa]]<1){

				String a_p=android_listener.op_a_nodes.get(aa).property;
				String s_p=swift_listner.op_s_nodes.get(ss).property;

				if(a_p.equals("enclosed{}") && s_p.equals("enclosed{}")){
					android_to_swift_point.remove(new Pair(aa, ss));
					//						amap.put(a, "{...}");
					aamap.put(android_listener.op_a_nodes.get(aa), "{...}");
					//					amap_ct.put(wocontext.op_a_nodes.get(aa).interval, "{...}");
					//						smap.put(s, "{...}");
					aamapppp.put(android_listener.op_a_nodes.get(aa), "{...}");
					ssmap.put(swift_listner.op_s_nodes.get(ss), "{...}");
					ssmapppp.put(swift_listner.op_s_nodes.get(ss), "{...}");

				}

				else{



					if(!android_listener.op_a_nodes.get(aa).same_scope.isEmpty()){
						Iterator<Interval> scopes_a = android_listener.op_a_nodes.get(aa).same_scope.keySet().iterator();
						//						System.out.println("scope_a"+android_listener.op_a_nodes.get(aa).same_scope.values());
						while(scopes_a.hasNext()){
							Interval sc = scopes_a.next();
							amap_ct.put(android_listener.op_a_nodes.get(aa).same_scope.get(sc).interval, "<arg"+argnum+">");
							aamap.put(android_listener.op_a_nodes.get(aa).same_scope.get(sc), "<arg"+argnum+">");
							aamapppp.put(android_listener.op_a_nodes.get(aa).same_scope.get(sc), "arg"+argnum+"");

							//							System.out.println("aamap.put++"+android_listener.op_a_nodes.get(aa).same_scope.get(sc)+" :"+aamap);
						}
					}
					//if the same scopes in the single statement, then .. linking it.

					matchingPoint += emetric.compare(android_listener.op_a_nodes.get(aa).strASTNode, swift_listner.op_s_nodes.get(ss).strASTNode);
					aamap.put(android_listener.op_a_nodes.get(aa), "<arg"+argnum+">");
					aamapppp.put(android_listener.op_a_nodes.get(aa), "arg"+argnum+"");
					//						amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
					//						smap.put(s, "<arg"+argnum+">");

					amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
					ssmap.put(swift_listner.op_s_nodes.get(ss), "<arg"+argnum+">");
					ssmapppp.put(swift_listner.op_s_nodes.get(ss), "arg"+argnum+"");

					smap_ct.put(swift_listner.op_s_nodes.get(ss).interval, "<arg"+argnum+">");
					nextMapping.put(android_listener.op_a_nodes.get(aa),swift_listner.op_s_nodes.get(ss));
					//					cost += ecostMatrix[aa][eresult[aa]];

					cost += Math.min(0.999, 1- emetric.compare(android_listener.op_a_nodes.get(aa).strASTNode, swift_listner.op_s_nodes.get(ss).strASTNode));
					num_m++;
					Iterator<Interval> scopes = swift_listner.op_s_nodes.get(ss).same_scope.keySet().iterator();
					while(scopes.hasNext()){
						Interval sc = scopes.next();
						ssmap.put(swift_listner.op_s_nodes.get(ss).same_scope.get(sc), "<arg"+argnum+">");
						ssmapppp.put(swift_listner.op_s_nodes.get(ss).same_scope.get(sc), "arg"+argnum+"");
					}



					argnum++;
				}
				listi.add(android_listener.op_a_nodes.get(aa).interval);


				if(android_listener.op_a_nodes.get(aa).node instanceof ParserRuleContext && swift_listner.op_s_nodes.get(ss).node instanceof ParserRuleContext){
					ParserRuleContext anode = (ParserRuleContext)android_listener.op_a_nodes.get(aa).node;
					ParserRuleContext snode = (ParserRuleContext)swift_listner.op_s_nodes.get(ss).node;
					//							StructureMappingAntlr4 mapping = new StructureMappingAntlr4(anode, snode);
					nextMapping.put(android_listener.op_a_nodes.get(aa), swift_listner.op_s_nodes.get(ss));
					//							mapping.waking();
					//							mapping.compute();
					//							cost += ecostMatrix[aa][eresult[aa]];

				} 
			}

		}


		for(int p=0; p<eresult.length;p++){
			boolean disj= true;
			//			ParserRuleContext anode = (ParserRuleContext)wocontext.op_a_nodes.get(p).node;
			//			String anodeText = wocontext.op_a_nodes.get(p).strASTNode;
			Interval anodeInterval = android_listener.op_a_nodes.get(p).interval;
			for(int i=0; i<listi.size();i++){
				//								System.out.println("checking"+anode.getText()+""+listi.get(i).disjoint(anode.getSourceInterval())+" ");
				disj = disj & listi.get(i).disjoint(anodeInterval);
			}
			//			constraint.keySet()

			Iterator<Interval> a_anchors = constraint.keySet().iterator();
			boolean isMet = true;
			//3..5=>12..16 
			//2..2=7..7, 4..4=14..14
			//3:yValueSum -> .. 9..12:CGFloat(yValueSum)
			//value / yValueSum * mMaxAngle 
			/**
			 /  /
			 * * 
			value/yValueSum  ) / CGFloat

			yValueSum*mMaxAngle  ) * _maxAngle
			 */
			//value / yValueSum * mMaxAngle
			//CGFloat(value) / CGFloat(yValueSum) * _maxAngle
			while(a_anchors.hasNext()){
				Interval cons = a_anchors.next();
				if(!cons.disjoint(android_listener.op_a_nodes.get(p).interval)){
					disj = false;
				}
			}

			if(disj && !android_listener.op_a_nodes.get(p).property.equals("constant_a")){
				//				System.out.println("@@@"+"<arg"+argnum+">"+android_listener.op_a_nodes.get(p));
				//					amap.put(android_listener.op_a_nodes.get(p).strASTNode, "<arg"+argnum+">");
				aamap.put(android_listener.op_a_nodes.get(p), "<arg"+argnum+">");
				aamapppp.put(android_listener.op_a_nodes.get(p), "arg"+argnum+"");

				amap_ct.put(android_listener.op_a_nodes.get(p).interval, "<arg"+argnum+">");

				if(!android_listener.op_a_nodes.get(p).same_scope.isEmpty()){
					Iterator<Interval> scopes_a = android_listener.op_a_nodes.get(p).same_scope.keySet().iterator();
					//					System.out.println("scope_a"+android_listener.op_a_nodes.get(aa).same_scope.values());
					while(scopes_a.hasNext()){
						Interval sc = scopes_a.next();
						amap_ct.put(android_listener.op_a_nodes.get(p).same_scope.get(sc).interval, "<arg"+argnum+">");
						aamap.put(android_listener.op_a_nodes.get(p).same_scope.get(sc), "<arg"+argnum+">");
						aamapppp.put(android_listener.op_a_nodes.get(p).same_scope.get(sc), "arg"+argnum+"");
						//						System.out.println("aamap.put++"+android_listener.op_a_nodes.get(aa).same_scope.get(sc)+" :"+aamap);
					}
				}


				argnum++;
			}
		}


//						System.out.println("aamap"+aamap);
//						System.out.println("ssmap"+ssmap);
		akeys.addAll(aamap.keySet());
		skeys.addAll(ssmap.keySet());

		Iterator<OpProperty> aait = akeys.iterator();
		Iterator<OpProperty> ssit = skeys.iterator();
		//			String android_ctxt = android_str;

		//								System.out.println("aamap\n"+aamap);
		//				System.out.println("terms_android	"+android_listener.termsMapOnly);
		//				System.out.println("op_s\n"+swift_listner.op_s_nodes);
		//								System.out.println("ssmap\n"+ssmap);
		//				System.out.println("terms_swft	"+swift_listner.termsMapOnly);

		//								System.out.println("terms	"+terms);

		while(aait.hasNext()){

			OpProperty key = aait.next();

			//				String expr = key;
			String arg = aamap.get(key);

			Iterator<Interval> itttt = terms.keySet().iterator();
			boolean isFirst = false;
			while(itttt.hasNext()){
				Interval next = itttt.next();
				//						if(!next.disjoint(key.interval) && !isFirst){
				//												terms.put(next, arg);
				//												isFirst = true;
				//						}
				if(!next.disjoint(key.interval) ){
					//					terms.put(next, arg);
					if(!isFirst){terms.put(next, arg); isFirst = true;}
					else{itttt.remove();}
				}
			}

			//			key.interval;
			//			buf.replace(start, end, str);

		} //making template for android
		terms_swft = new HashMap<Interval, String>();
		//				terms_swft.keySet().removeAll(swift_listner.termsMap.keySet());
		//				swift_listner.termsMap.putAll(terms_swft);
		//				terms_swft.keySet().removeAll(swift_listner.termsMap.keySet());
		terms_swft = swift_listner.termsMap;
		//				terms_swft.forEach(swift_listner.termsMap::putIfAbsent);
		//		System.out.println("before:"+terms_swft+"\n"+skeys);
		while(ssit.hasNext()){
			OpProperty key = ssit.next();
			Iterator<Interval> scopes =null;
			//				String expr = key;
			String arg = ssmap.get(key);
			//
			Iterator<Interval> tokens = terms_swft.keySet().iterator();
			boolean isFirst = false;
			while(tokens.hasNext()){
				Interval next = tokens.next();
				//						if(!next.disjoint(key.interval) && !isFirst){
				//					terms.put(next, arg);
				//					isFirst = true;
				//						}
				if(!next.disjoint(key.interval) ){
					//					terms.put(next, arg);

					//adding <arg#>
					if(!isFirst){
						terms_swft.put(next, arg); isFirst = true;
					}
					else{
						tokens.remove();
					}

				}
			}


		} //making template for swift

		//		StringUtils.join(terms.values());
		ArrayList<String> aaaa = new ArrayList<String>(terms.values());
		ArrayList<String> ssss = new ArrayList<String>(terms_swft.values());



		String tempa=StringUtils.join(aaaa.toArray()," ");
		String temps=StringUtils.join(ssss.toArray()," ");


		//		System.out.println("op_s_nodes\n"+swift_listner.op_s_nodes+"  "+ssmapcom);
//		System.out.println(cost+"	"+ cost/(num_m) +"m point : "+ matchingPoint+"   "+matchingPoint/(ssmap.size())+"   "+QueryExamples.idd+"	templateeee\n"+tempa+"\n"+temps.replace("<EOF>", ""));
		aait=aamap.keySet().iterator();

		String binding = null;
		try{
			if(DelcarationMapping.methodBinding!=null)
				binding=DelcarationMapping.methodBinding.get(android.start.getLine()+":"+android.start.getCharPositionInLine());
		} catch (Exception e){
			//					e.printStackTrace();
		}
		temps = temps.replace(". . <", "..<").replace("<EOF>", "").replace("- >","->").replace("private ( set )", "private(set)").replace("! =", "!=").replace("= =", "==").replace(" + +", "++");
		String rform_a_c="";
		if(binding !=null){
			//					binding_sql 	= SQLiteJDBC.updateTableBinding(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
			binding_sql = SQLiteJDBC.updateTableBinding2(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval),
					afilename, LineCharIndex.contextToLineIndex(android).toString(),
					sfilename, LineCharIndex.contextToLineIndex(swift).toString()
					);

			//					template_sql 	= SQLiteJDBC.updateTableGrammarAndroid(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval));
			//					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval));

			String template_a=StringUtils.join(aaaa.toArray()," ");
			String rform_a = template_a.replaceAll("\\s\\.\\s", "\\\\.").replaceAll("\\s*\\(\\s*", "\\\\s*\\\\(\\\\s*").replaceAll("\\s*\\)\\s*", "\\\\s*\\\\)\\\\s*").replaceAll("\\s*\\[\\s*", "\\\\[").replaceAll("\\s*\\]\\s*", "\\\\]").replace(" ;", ";").replaceAll("\\s*\\/\\s*", "\\\\/").replaceAll("\\**\\*","\\*").replaceAll("\\s*\\?\\s*", "\\\\?").replaceAll("\\s*\\!\\s*", "\\\\!").replaceAll("\\s*\\{\\.\\.\\.\\}", "\\{...\\}").replaceAll("\\s*,\\s*", "\\\\s*,\\\\s*").replaceAll("\\s*\\+\\s*", "\\\\s*\\\\+\\\\s*");				//			String rform_a =template_a;
			rform_a_c =MatchingTemplate2.getRform(MigrationExamples.getStdFormTemplate(rform_a)).replaceAll("\\\\E", "").replaceAll("\\\\Q", "").replaceAll("@ Override", "@Override");
			//			rform_a_c=rform_a_c.replace(" ;", ";");
			rform_a_c=rform_a_c.replaceAll(" \\< ", "\\\\<").replaceAll(" \\>", "\\\\>");
			rform_a_c = "\\G"+rform_a_c+"$";
//			System.out.println("rform_a_c"+rform_a_c);
			//			template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, template_a, this.android.getRuleIndex(), swift.getRuleIndex(), StringUtils.join(ssss.toArray()," ").replace("<EOF>", "").replace("- >","->"), 
			//					rform_a_c
			//					);
			String template_a_sql = template_a;
			String template_s_sql = temps;
//			if(template_a_sql.equals("<arg0> . <arg1>") && template_s_sql.equals("<arg0> . <arg1>")){
//				template_a_sql = android.start.getInputStream().getText(android_interval).replaceAll("(.+)\\.", "<arg10>.");
//				template_s_sql = swift.start.getInputStream().getText(swift_interval).replaceAll("(.+)\\.", "<arg10>.");
//			}

			
			template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, template_a_sql, this.android.getRuleIndex(), swift.getRuleIndex(), template_s_sql, 
					rform_a_c, afilename, sfilename, abinding, sbinding
					);

//			System.out.println("template_sql\n"+template_sql);
			example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
					anchors, LineCharIndex.contextToLineIndex(android).toString(),
					sfilename, LineCharIndex.contextToLineIndex(swift).toString());
		}
		else{
			String template_a=StringUtils.join(aaaa.toArray()," ");
			String rform_a = template_a.replaceAll("\\s\\.\\s", "\\\\.").replaceAll("\\s*\\(\\s*", "\\\\s*\\\\(\\\\s*").replaceAll("\\s*\\)\\s*", "\\\\s*\\\\)\\\\s*").replaceAll("\\s*\\[\\s*", "\\\\[").replaceAll("\\s*\\]\\s*", "\\\\]").replace(" ;", ";").replaceAll("\\s*\\/\\s*", "\\\\/").replaceAll("\\**\\*","\\*").replaceAll("\\s*\\?\\s*", "\\\\?").replaceAll("\\s*\\!\\s*", "\\\\!").replaceAll("\\s*\\{\\.\\.\\.\\}", "\\{...\\}").replaceAll("\\s*,\\s*", "\\\\s*,\\\\s*").replaceAll("\\s*\\+\\s*", "\\\\s*\\\\+\\\\s*");				//			String rform_a =template_a;
			//			System.err.println("rform_a"+rform_a);
			rform_a_c =MatchingTemplate2.getRform(MigrationExamples.getStdFormTemplate(rform_a)).replaceAll("\\\\E", "").replaceAll("\\\\Q", "").replaceAll("@ Override", "@Override");
			//			rform_a_c=rform_a_c.replace(" ;", ";");
			//			System.out.println("rrrform_a_c"+rform_a_c);	
			rform_a_c=rform_a_c.replaceAll(" \\< ", "\\\\<").replaceAll(" \\>", "\\\\>").replaceAll(" \\]", "\\\\]");
			rform_a_c = "\\G"+rform_a_c+"$";
			//			System.out.println("rrrrrform_a_c"+rform_a_c);
			//			template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, template_a, this.android.getRuleIndex(), swift.getRuleIndex(), StringUtils.join(ssss.toArray()," ").replace("<EOF>", "").replace("- >","->"), 
			//					rform_a_c
			//					);

			String template_a_sql = template_a;
			String template_s_sql = temps;
//			if(template_a_sql.equals("<arg0> . <arg1>") && template_s_sql.equals("<arg0> . <arg1>")){
//				template_a_sql = android.start.getInputStream().getText(android_interval).replaceAll("(.+)\\.", "<arg10>.");
//				template_s_sql = swift.start.getInputStream().getText(swift_interval).replaceAll("(.+)\\.", "<arg10>.");
//			}

			
			template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, template_a_sql, this.android.getRuleIndex(), swift.getRuleIndex(), template_s_sql, 
					rform_a_c, afilename, sfilename, abinding, sbinding);

//			System.out.println("template_sql\n"+template_sql);
			example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
					StringUtils.join(aaaa.toArray()," ").replaceAll(" \\.", "\\."), anchors,
					StringUtils.join(ssss.toArray()," ").replaceAll(" \\.", "\\."), nextMapping.values().toString().replaceAll("\n", ""));

		}


		if(tempa.equals("<arg0>") && temps.equals("<arg0>")){
			template_sql  = "";
			//					template_sql  = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, android.start.getInputStream().getText(android_interval).replaceAll("\n", ""), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
//			System.out.println(android.getText()+"	<aaaa>"+swift_listner.termsMap);

		}



		grammar_id++;
		//		MappingOutput output = new MappingOutput(nextMapping, binding_sql, example_sql, template_sql, tempa, temps.replace("<EOF>", ""), cost/((aamap.size()+ssmap.size())*0.5));
		MappingOutput output = new MappingOutput(nextMapping, binding_sql, example_sql, template_sql, tempa, temps, cost);

		if(num_m >0){
			num_m=1;
			output = new MappingOutput(nextMapping, binding_sql, example_sql, template_sql, tempa, temps, cost/num_m);
		}

		output.aamap 		= aamap;
		output.ssmap 		= ssmap;
		output.countC 		= android_listener.terms.size();
		output.aamappp 		= aamapppp;
		output.ssmappp 		= ssmapppp;
		output.regex 		= rform_a_c;
		output.afilename 	= this.afilename;
		output.sfilename 	= this.sfilename;
		output.example_s 	= swift.start.getInputStream().getText(swift_interval);
		output.example_a 	= android.start.getInputStream().getText(android_interval);
		return output;
	}

	public static List<String> getMatchingStrings(List<String> list, String regex) {

		ArrayList<String> matches = new ArrayList<String>();

		Pattern p = Pattern.compile(regex);

		for (String s:list) {
			if (p.matcher(s).matches()) {
				matches.add(s);
			}
		}

		return matches;
	}

	public static Set<Character> toSet(String s) {
		Set<Character> ss = new HashSet<Character>(s.length());
		for (char c : s.toCharArray())
			ss.add(Character.valueOf(c));
		return ss;
	}


	public static Set<String> toSet(List<String> s) {
		Set<String> ss = new HashSet<String>(s.size());
		for (String c : s)
			ss.add(c);
		return ss;
	}


	public static double toCommon(List<String> and, List<String> swft) {
		Set<String> aset = toSet(and);
		aset.retainAll(toSet(swft));

		return (double)aset.size()/(double)and.size();
	}

	public static double toCommon(String andS, String swftS) {
		List<String> lst = matchingList(andS);
		List<String> cplst = matchingList(andS);
		List<String> swlst = matchingList(swftS);
		List<String> cpswlst = matchingList(swftS);
		//		System.err.println(lst);
		//		System.err.println(swlst);
		int s = lst.size();
		int ss = swlst.size();
		//		Set<String> aset = toSet(lst);
		//		aset.retainAll(toSet(matchingList(swftS)));
		lst.retainAll(matchingList(swftS));
		//		System.out.println(lst);
		swlst.retainAll(cplst);
		//		System.out.println(swlst);
		if(s==0 && ss==0){
			return 1.0;
		}
		//		else if((double)lst.size()==0){
		//			return 0.0;
		//		}
		double s1 = s==0? 0:(double)lst.size()/(double)s;
		double s2 = ss==0? 0:(double)swlst.size()/(double)ss;
		//		System.out.println(s1);
		//		System.err.println(s2);
		return Math.min(s1, s2);
	}

	public static List<String> matchingList(String str){

		Pattern p = Pattern.compile("\\d+");
		List<String> numbers = new ArrayList<String>();
		Matcher m = p.matcher(str);
		while (m.find()) {  
			//			numbers.add(m.group());
		}   




		try {
			Pattern regex = Pattern.compile("[+*/&><={}]|(?<=\\s)-|if|else|while");
			Matcher regexMatcher = regex.matcher(str);
			while (regexMatcher.find()) {
				numbers.add(regexMatcher.group());
			} 
		} catch (PatternSyntaxException ex) {
			// Syntax error in the regular expression
		}


		//		System.err.println(str+": num"+numbers);
		return numbers;
	}

	public void doupdate(){

		Connection c;
		try {
			//			if(!isEqualTemplate){
			//					if(!listner_swift.hasErrors() && !listner.hasErrors() && cost!=prev_cost){

			//			c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			c = DriverManager.getConnection("jdbc:sqlite:var_0309.db");

			java.sql.Statement sql_stmt = c.createStatement();
			c.setAutoCommit(true);
			//			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(example_sql);
			sql_stmt.executeUpdate(template_sql);
			sql_stmt.executeUpdate(binding_sql);
			//			System.out.println("cost:"+cost+"\n"+StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ")+"\n"+StringUtils.join(ssss.toArray()," "));
			prev_cost = cost;
			grammar_id++;

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println("err in sql"+example_sql);
			System.out.println("err in sql"+template_sql);
			System.out.println("err in sql"+binding_sql);
			//			System.out.println(e11);
			//			System.out.println(e122);
			e1.printStackTrace();
		}

	}

	
	public class Pair{
		public int a,b;
		public Interval a_intval, b_intval;
		public OpProperty a_op, s_op;
		public Pair(int a, int b){
			this.a=a;
			this.b=b;
		}
		public Pair(int a, int b, Interval ai, Interval bi){
			this.a=a;
			this.b=b;
			this.a_intval = ai;
			this.b_intval = bi;
		}

		public Pair(int a, int b, Interval ai, Interval bi, OpProperty ao, OpProperty so){
			this.a=a;
			this.b=b;
			this.a_intval = ai;
			this.b_intval = bi;
			this.a_op = ao;
			this.s_op = so;
		}


		@Override
		public boolean equals(Object obj) {
			// TODO Auto-generated method stub
			Pair p=(Pair)obj;
			return a==p.a && b==p.b;
		}

		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return a*100+b*1;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return a+":"+b;
		}
	}
}
