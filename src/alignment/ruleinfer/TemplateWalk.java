package alignment.ruleinfer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
//import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
//import java.util.Map;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;

import alignment.common.MappingOutput;
import alignment.common.OpProperty;
//import sqldb.SQLiteJDBC;
//import templates.MatchingTemplate;
import sqldb.SQLiteJDBC;

public class TemplateWalk {

	private static final int String = 0;
	MappingOutput mappingOutput;
	ParserRuleContext ctx_j, ctx_s;
	CommonTokenStream jcommon, scommon;
	public String afile="", sfile="";
	int k=0;
	//	public 
	public TemplateWalk(ParserRuleContext ctxt_java, CommonTokenStream javaCommonStream, ParserRuleContext ctxt_swft, CommonTokenStream swiftCommonStream){
		ctx_j 	= ctxt_java;
		ctx_s 	= ctxt_swft;
		jcommon = javaCommonStream;
		scommon = swiftCommonStream;

	}
	public TemplateWalk(){

	}

	public TemplateWalk(MappingOutput output){
		mappingOutput = output;
	}

	/**
	 * making template and walking down
	 * 
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */

	public MappingOutput walk() throws SQLException, ClassNotFoundException, FileNotFoundException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

		MappingOutput output = null;
		k ++;		
//		Statement stmt_conn_binding = QueryExamplesTest.conn_binding.createStatement();
//		String sql2 				= "select * from binding_android_stat where example=\'"+ctx_j.getText().replaceAll(" ", "")+"\'";
//		ResultSet res11 			= stmt_conn_binding.executeQuery(sql2);
		//		ctx_j.getRuleIndex();
		//For special care:  binding case: this is a method. seperately store it. we think it as flat-matching
		//a.method(aa,bb)->   '.' '(' ',' ')'    , a.method(c:aa,d:bb), '.' '(''c'':' ','d'':' ')'
		//35  180
		if(ctx_j.getRuleIndex()==35 && ctx_s.getRuleIndex() == 180){
			System.out.println("HIT		"+ctx_j.getText()+"  "+ctx_s.getText());
		}
/*		
		if(res11.next() & false){
			//			System.out.println(x);
			System.out.println(sql2+"\n"+ctx_j.getRuleIndex()+"  "+ctx_s.getRuleIndex());
			java.sql.Statement sql_stmt = QueryExamplesTest.connection.createStatement();
			QueryExamplesTest.connection.setAutoCommit(true);
			String savemethod 			= SQLiteJDBC.insertTableMethod(ctx_j.getText(), ctx_j.getRuleIndex(), ctx_s.getRuleIndex(), ctx_s.getText(), afile, "", sfile, "");

			try{
				sql_stmt.executeUpdate(savemethod);
			}catch(java.sql.SQLException e){
				e.printStackTrace();
			}
			//			stmt.executeUpdate(SQLiteJDBC.createMethod());

			//			System.err.println("API				"+ctx_j.getText().replaceAll(" ", ""));			
			String varss = res11.getString("vars");
			String[] vvv = varss.split(",");
			//			String conss = res11.getString("CONSTS");
			//			String[] ccc = conss.split(",");
			String temp = res11.getString("TEMPLATE");

			//			List<String> vars = new LinkedList<String>();
			//			vars=Arrays.asList(vvv);
			//			List<String> consts = new LinkedList<String>();
			//			consts = Arrays.asList(ccc);
			//			List<String> svars = new LinkedList<String>();
			//			List<String> sconsts = new LinkedList<String>();

			if(temp!=null){
								System.out.println("temptemptemp"+temp+"  "+" "+varss);	
				//				public static MappingOutput iterationWithConstants(ParserRuleContext ctxt_java, CommonTokenStream javaCommonStream, ParserRuleContext ctxt_swft, CommonTokenStream swiftCommonStream, 
				//				int cycle, String _abinding,String template11, String afile, String sfile)				
				output = IterativeMapping_for_method.iterationWithConstants(ctx_j, jcommon, ctx_s, scommon, 10,varss, temp, afile, sfile);				
				if(output!=null){
					output.isMethod = true;
				}
			}else{
			}

			//			System.out.println("ctx_j"+ctx_j.getText()+"  "+ctx_s.getText());

		}
*/

		//no binding, making templates
//		else{
			if(ctx_j!=null && ctx_s!=null){
				try{
					output = IterativeMapping.iteration(ctx_j, jcommon, ctx_s, scommon, 8, afile, sfile);
//					System.out.println("$$$$"+output);
				} catch (Exception e){

				}
			}
//		}


		if(output!=null && !(output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>"))){
			output.doupdate(QueryExamplesTest.connection); //save templates and examples chosen
			Iterator<OpProperty> next_mapping_keys 		= output.nextMapping.keySet().iterator();
			LinkedHashMap<OpProperty, OpProperty> copy 	= new LinkedHashMap<OpProperty, OpProperty>();

			//more Parameterize, go next level
			while(next_mapping_keys.hasNext()){
				OpProperty next_element_src 	= next_mapping_keys.next();
				OpProperty next_element_target  = output.nextMapping.get(next_element_src);
				if(!next_element_src.property.equals("enclosed{}"))
					copy.put(next_element_src, next_element_target);
				if(next_element_src.node instanceof ParserRuleContext && next_element_target.node instanceof ParserRuleContext && !next_element_src.property.equals("enclosed{}")){
					//					if(!output.template_a.equals("<arg0>") && output.template_s.equals("<arg0>")){
					TemplateWalk templatewalk = new TemplateWalk((ParserRuleContext)next_element_src.node, jcommon, (ParserRuleContext)next_element_target.node, scommon);
					templatewalk.afile = afile;
					templatewalk.sfile = sfile;
					//go next level walking
					templatewalk.walk();
				}

			}

			if(output.isMethod){
				MappingOutput.updateNextMapping2(QueryExamplesTest.connection, copy, output.afilename, output.sfilename);
				//				System.err.println("output"+output);
			}else{
				//parameterize output
				MappingOutput.updateNextMapping2(QueryExamplesTest.connection, copy, output.afilename, output.sfilename);
				MappingOutput.updateNextMapping(QueryExamplesTest.connection, copy, output.afilename, output.sfilename);
			}
			return output;
		}

		/**
		 * (T1, T2, <arg0>, <arg0>) means that no mapping any more. it will be stored in the constant DB. 
		 */
		return output;
	}
}
