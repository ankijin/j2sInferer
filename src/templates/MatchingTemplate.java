package templates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ParserRuleContext;
//import org.eclipse.jdt.core.dom.ASTNode;
//import org.eclipse.jdt.core.dom.ASTVisitor;
//import org.eclipse.jdt.core.dom.Expression;
import org.stringtemplate.v4.ST;

import alignment.common.LineCharIndex;

public class MatchingTemplate {

	public static Map<String, String> getTokenValues(String pattern, String str) {
		Map<String, String> res = new HashMap<String, String>();
		List<String> identifiers = new ArrayList<String>();

		Pattern dollarRgx = Pattern.compile("\\$\\{([a-zA-Z0-9]+?)\\}");
		Matcher dollarMatcher = dollarRgx.matcher(pattern);
		StringBuilder convRgxBd = new StringBuilder();
		int lastEnd = 0;
		while(dollarMatcher.find()) {
			String before = pattern.substring(lastEnd, dollarMatcher.start());
			convRgxBd.append(Pattern.quote(before));
			convRgxBd.append("(.+?)");
			lastEnd = dollarMatcher.end();
			identifiers.add(dollarMatcher.group(1));
		}
		String after = pattern.substring(lastEnd, pattern.length());
		convRgxBd.append(Pattern.quote(after));

		Pattern convRgx = Pattern.compile(convRgxBd.toString());
		Matcher convMatcher = convRgx.matcher(str);
		if(convMatcher.matches()) {
			for(int i = 0; i < convMatcher.groupCount(); i++) {
				String id = identifiers.get(i);
				//	            res.put(id, convMatcher.group(i+1));
				res.put(convMatcher.group(i+1), id);
			}
		}
		return res;
	}


	public static List<String> getTokenValuesList(String pattern, String str) {
		List<String> res = new LinkedList<String>();
		List<String> identifiers = new ArrayList<String>();

		Pattern dollarRgx = Pattern.compile("\\$\\{([a-zA-Z0-9]+?)\\}");
		Matcher dollarMatcher = dollarRgx.matcher(pattern);
		StringBuilder convRgxBd = new StringBuilder();
		int lastEnd = 0;
		while(dollarMatcher.find()) {
			String before = pattern.substring(lastEnd, dollarMatcher.start());
			convRgxBd.append(Pattern.quote(before));
			convRgxBd.append("(.+?)");
			lastEnd = dollarMatcher.end();
			identifiers.add(dollarMatcher.group(1));
		}
		String after = pattern.substring(lastEnd, pattern.length());
		convRgxBd.append(Pattern.quote(after));

		Pattern convRgx = Pattern.compile(convRgxBd.toString());
		Matcher convMatcher = convRgx.matcher(str);
		if(convMatcher.matches()) {
			for(int i = 0; i < convMatcher.groupCount(); i++) {
				String id = identifiers.get(i);
				//	            res.put(id, convMatcher.group(i+1));
				//	            res.put(convMatcher.group(i+1), id);
				res.add(convMatcher.group(i+1));
			}
		}
		return res;
	}



	public static List<String> getAttValuesList(String pattern, String str) {
		List<String> res = new LinkedList<String>();
		List<String> identifiers = new ArrayList<String>();

		Pattern dollarRgx = Pattern.compile("\\<([a-zA-Z0-9]+?)\\>|\\{...\\}");
		Matcher dollarMatcher = dollarRgx.matcher(pattern);
		StringBuilder convRgxBd = new StringBuilder();
		int lastEnd = 0;
		while(dollarMatcher.find()) {
			String before = pattern.substring(lastEnd, dollarMatcher.start());
			
//			before=before.replaceAll(" ", "\\\\E\\\\s*\\\\Q");
//			System.out.println("before"+before);
			convRgxBd.append(Pattern.quote(before).replaceAll(" ", "\\\\E\\\\s+\\\\Q"));
//			convRgxBd.append(before);
			
//			before=before.replaceAll("\\(", "\\\\(");
//			before=before.replaceAll("\\)", "\\\\)");
//			before=before.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
//			before=before.replaceAll("\\*", "\\\\*");
//			before=before.replaceAll("\\!", "\\\\!");
//			before=before.replaceAll("\\[", "\\\\[");
//			before=before.replaceAll("\\]", "\\\\]");
//			before=before.replaceAll("\\}", "\\\\}");
//			before=before.replaceAll("\\{", "\\\\{");
			/*
			expr=expr.replaceAll("\\+", "\\\\+");
			expr=expr.replaceAll("\\-", "\\\\-");
			//			expr=expr.replaceAll("\\ ", "\\\\ ");
			expr=expr.replaceAll("/", "\\\\/");
			//			expr=expr.replaceAll("\\s", "\\\\s");
			expr=expr.replaceAll("\\&", "\\\\&");
			expr=expr.replaceAll("\\&&", "\\\\&&");
			expr=expr.replaceAll("\\>", "\\\\>");
			expr=expr.replaceAll("\\<", "\\\\<");

			expr=expr.replaceAll("\\|", "\\\\|");

			expr=expr.replaceAll("\\:", "\\\\:");
			expr=expr.replaceAll("\\?", "\\\\?");
			expr=expr.replaceAll("\\^", "\\\\^");
			*/
			
//			convRgxBd.append("(.+?)");
			convRgxBd.append("(.+)");
			lastEnd = dollarMatcher.end();
			identifiers.add(dollarMatcher.group(1));
		}
		String after = pattern.substring(lastEnd, pattern.length());
//		System.out.println("after"+after);
		convRgxBd.append(Pattern.quote(after).replaceAll(" ", "\\\\E\\\\s+\\\\Q"));
		

		
//		convRgxBd.append(after);
		System.out.println("convRgxBd.toString()"+convRgxBd.toString());
		Pattern convRgx = Pattern.compile(convRgxBd.toString());
		Matcher convMatcher = convRgx.matcher(str);
		if(convMatcher.matches()) {
			for(int i = 0; i < convMatcher.groupCount(); i++) {
				String id = identifiers.get(i);
				//	            res.put(id, convMatcher.group(i+1));
				//	            res.put(convMatcher.group(i+1), id);
				res.add(convMatcher.group(i+1));
				int s = convMatcher.start(i+1);
				int e = convMatcher.end(i+1);
//				System.out.println("convMatcher	"+str.substring(s, e)+"::::"+s+"  "+e+" "+LineCharIndex.stringToLineCharIndex(str, s, e));
			}
		}
		return res;
	}
	
	
	public static List<LineCharIndex> getAttValuesListLineIndex(String pattern, String str) {
		List<LineCharIndex> res = new LinkedList<LineCharIndex>();
		List<String> identifiers = new ArrayList<String>();

		Pattern dollarRgx = Pattern.compile("\\<([a-zA-Z0-9]+?)\\>|\\{...\\}");
		Matcher dollarMatcher = dollarRgx.matcher(pattern);
		StringBuilder convRgxBd = new StringBuilder();
		int lastEnd = 0;
		while(dollarMatcher.find()) {
			String before = pattern.substring(lastEnd, dollarMatcher.start());
			
//			before=before.replaceAll(" ", "\\\\E\\\\s*\\\\Q");
//			System.out.println("before"+before);
			convRgxBd.append(Pattern.quote(before).replaceAll(" ", "\\\\E\\\\s+\\\\Q"));
//			convRgxBd.append(before);
			
//			before=before.replaceAll("\\(", "\\\\(");
//			before=before.replaceAll("\\)", "\\\\)");
//			before=before.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
//			before=before.replaceAll("\\*", "\\\\*");
//			before=before.replaceAll("\\!", "\\\\!");
//			before=before.replaceAll("\\[", "\\\\[");
//			before=before.replaceAll("\\]", "\\\\]");
//			before=before.replaceAll("\\}", "\\\\}");
//			before=before.replaceAll("\\{", "\\\\{");
			/*
			expr=expr.replaceAll("\\+", "\\\\+");
			expr=expr.replaceAll("\\-", "\\\\-");
			//			expr=expr.replaceAll("\\ ", "\\\\ ");
			expr=expr.replaceAll("/", "\\\\/");
			//			expr=expr.replaceAll("\\s", "\\\\s");
			expr=expr.replaceAll("\\&", "\\\\&");
			expr=expr.replaceAll("\\&&", "\\\\&&");
			expr=expr.replaceAll("\\>", "\\\\>");
			expr=expr.replaceAll("\\<", "\\\\<");

			expr=expr.replaceAll("\\|", "\\\\|");

			expr=expr.replaceAll("\\:", "\\\\:");
			expr=expr.replaceAll("\\?", "\\\\?");
			expr=expr.replaceAll("\\^", "\\\\^");
			*/
			
			convRgxBd.append("(.+?)");
			lastEnd = dollarMatcher.end();
			identifiers.add(dollarMatcher.group(1));
		}
		String after = pattern.substring(lastEnd, pattern.length());
//		System.out.println("after"+after);
		convRgxBd.append(Pattern.quote(after).replaceAll(" ", "\\\\E\\\\s+\\\\Q"));
		

		
//		convRgxBd.append(after);
//		System.out.println("convRgxBd.toString()"+convRgxBd.toString());
		Pattern convRgx = Pattern.compile(convRgxBd.toString());
		Matcher convMatcher = convRgx.matcher(str);
		if(convMatcher.matches()) {
			for(int i = 0; i < convMatcher.groupCount(); i++) {
				String id = identifiers.get(i);
				//	            res.put(id, convMatcher.group(i+1));
				//	            res.put(convMatcher.group(i+1), id);
//				res.add(convMatcher.group(i+1));
				int s = convMatcher.start(i+1);
				int e = convMatcher.end(i+1);
				res.add(LineCharIndex.stringToLineCharIndex(str, s, e));
				//Integer::::5  12 1:5~1:11
//				System.out.println("convMatcher	"+str.substring(s, e)+"::::"+s+"  "+e+" "+LineCharIndex.stringToLineCharIndex(str, s, e));
			}
		}
		return res;
	}
	/*
	public static List<Expression> getAttValuesList(String pattern, Expression str) {
		List<Expression> res = new LinkedList<Expression>();
		List<String> identifiers = new ArrayList<String>();

		Pattern dollarRgx = Pattern.compile("\\<([a-zA-Z0-9]+?)\\>");
		Matcher dollarMatcher = dollarRgx.matcher(pattern);
		StringBuilder convRgxBd = new StringBuilder();
		int lastEnd = 0;
		while(dollarMatcher.find()) {
			String before = pattern.substring(lastEnd, dollarMatcher.start());
			convRgxBd.append(Pattern.quote(before));
			convRgxBd.append("(.+)");
			lastEnd = dollarMatcher.end();
			identifiers.add(dollarMatcher.group(1));
		}
		String after = pattern.substring(lastEnd, pattern.length());
		convRgxBd.append(Pattern.quote(after));

		Pattern convRgx = Pattern.compile(convRgxBd.toString());
		Matcher convMatcher = convRgx.matcher(str.toString());
		if(convMatcher.matches()) {
			for(int i = 0; i < convMatcher.groupCount(); i++) {
				String id = identifiers.get(i);
				//	            res.put(id, convMatcher.group(i+1));
				//	            res.put(convMatcher.group(i+1), id);
				String val=convMatcher.group(i+1);
				str.accept(new ASTVisitor() {
					@Override
					public void preVisit(ASTNode node) {
						// TODO Auto-generated method stub
						if(node instanceof Expression){
							if(((Expression)node).toString().equals(val)){
								res.add((Expression)node);
							}
						}
						super.preVisit(node);
					}
				});
				
				
//				res.add(convMatcher.group(i+1));
			}
		}
		return res;
	}
	
	*/
	/*
	public static List<ParserRuleContext> getAttValuesList(String pattern, ParserRuleContext ctx) {
		List<ParserRuleContext> res = new LinkedList<ParserRuleContext>();
		List<String> identifiers = new ArrayList<String>();

		Pattern dollarRgx = Pattern.compile("\\<([a-zA-Z0-9]+?)\\>");
		Matcher dollarMatcher = dollarRgx.matcher(pattern);
		StringBuilder convRgxBd = new StringBuilder();
		int lastEnd = 0;
		while(dollarMatcher.find()) {
			String before = pattern.substring(lastEnd, dollarMatcher.start());
			convRgxBd.append(Pattern.quote(before));
			convRgxBd.append("(.+)");
			lastEnd = dollarMatcher.end();
			identifiers.add(dollarMatcher.group(1));
		}
		String after = pattern.substring(lastEnd, pattern.length());
		convRgxBd.append(Pattern.quote(after));
		
		Pattern convRgx = Pattern.compile(convRgxBd.toString());
		Matcher convMatcher = convRgx.matcher(ctx.toString());
		if(convMatcher.matches()) {
			for(int i = 0; i < convMatcher.groupCount(); i++) {
				String id = identifiers.get(i);
			
				//	            res.put(id, convMatcher.group(i+1));
				//	            res.put(convMatcher.group(i+1), id);
				String val=convMatcher.group(i+1);

				convMatcher.group(i+1);
				
//				res.add(convMatcher.group(i+1));
			}
		}
		return res;
	}
	*/

	public static String getClassInfo(String name){
		Pattern dollarRgx = Pattern.compile("\\(.+?\\)");
		Matcher dollarMatcher = dollarRgx.matcher(name);
		while(dollarMatcher.find()) {
			System.out.println(dollarMatcher);
		}
		return name;
	}

	public static List<String> getGeneralVars(List<String> vals){

		String prev = "";
		String arg = "arg";
		int argn = 0;
		List<String> nlst = new LinkedList<String>();

		prev = vals.get(0);
		for(String v:vals){
			if(v.equals(prev)){
				nlst.add(arg+argn);
			}else{
				argn++;
				nlst.add(arg+argn);				
			}
			prev = v;
		}
		//		System.err.println(nlst);
		return nlst;
	}

	public static String getGeneralVarsStr(String pattern, String str){

		List<String> vals = getTokenValuesList(pattern,str);


		String prev = "";
		String arg = "arg";
		String result = "";
		int argn = 0;
		List<String> nlst = new LinkedList<String>();

		if(vals.size()==0) return "";
		prev = vals.get(0);
		for(String v:vals){
			if(v.equals(prev)){
				nlst.add(arg+argn);
			}else{
				argn++;
				nlst.add(arg+argn);				
			}
			prev = v;
		}
		//		System.err.println(nlst);
		String joined = String.join(",", nlst);
		return joined;
	}

	/**
	 * 
	 * @param generalVars 	[arg0, arg1, arg1]
	 * @param vars 			[_chartWidth, _contentRect, _contentRect]
	 * @return nameMap 		[arg0:_chartWidth,arg1:_contentRect]
	 * 
	 */

	public static Map<String, String> getNameMap(List<String> generalVars, List<String> vars){
		Map<String, String> nameMap = new HashMap<String, String>();
		if(generalVars.size()!=vars.size()){
			return nameMap;
		}
		for(int i=0; i<generalVars.size();i++){
			//			generalVars.get(i);vars.get(i);
			nameMap.put(generalVars.get(i), vars.get(i));
		}

		return nameMap;
	}

	// "${CGFloat}-${CGRect}.size.width-${CGRect}.origin.x"
	// [arg0, arg1, arg1]
	// NameMaps, {arg0:AAA, arg1:BBB}
/*
	public static String findMatching(String swift_expr_temp, List<String> list, Map<String, String> map){

		Pattern dollarRgx 		= Pattern.compile("\\$\\{([a-zA-Z0-9]+?)\\}");		
		Matcher dollarMatcher 	= dollarRgx.matcher(swift_expr_temp);
		String swift = swift_expr_temp;
		List<String> patterns = new ArrayList<String>();
		while(dollarMatcher.find()){
			String str = dollarMatcher.group(0);
			str= str.replace("${", "\\$\\{").replace("}", "\\}");
			patterns.add(str);
		}

		if(list.size()!=patterns.size()) return swift_expr_temp;

		for(int i=0;i<patterns.size();i++){
			swift = swift.replaceFirst(patterns.get(i), map.get(list.get(i)));
		}
		return swift;
	}
*/
	public static List<String> getArgsAttributes(String template){

		Pattern dollarRgx1 		= Pattern.compile("\\<([a-zA-Z0-9]+?)\\>");
		Matcher dollarMatcher1 	= dollarRgx1.matcher(template);
//		System.out.println("dollarMatcher1"+dollarMatcher1);
		List<String> atts = new LinkedList<String>();
		while(dollarMatcher1.find()){
			atts.add(dollarMatcher1.group(0).replaceAll("\\<", "").replaceAll("\\>", ""));
		}
		return atts;
	}

	public static Map<String, String> getAttributesMap(String template, String expr){

		Map<String, String> attmap = new HashMap<String, String>();

		List<String> values = getAttValuesList(template, expr);
//		System.out.println("values"+values);
		List<String> atts = getArgsAttributes(template);


		if(atts.size()==values.size()){
			for(int a=0;a<atts.size();a++){
				attmap.put(atts.get(a), values.get(a));
			}
		}

		return attmap;
	}
	
	
	public static Map<LineCharIndex,String> getAttributesMapLineIndex(String template, String expr){

		Map<LineCharIndex,String> attmap = new HashMap<LineCharIndex,String>();

		List<LineCharIndex> values = getAttValuesListLineIndex(template, expr);
		System.out.println("values"+values);
		List<String> atts = getArgsAttributes(template);


		if(atts.size()==values.size()){
			for(int a=0;a<atts.size();a++){
				attmap.put(values.get(a), atts.get(a));
			}
		}
		return attmap;
	}
	
	public static Map<String, String> getAttributesMapCtx(String template, ParserRuleContext expr){

		Map<String, String> attmap = new HashMap<String, String>();

		List<String> values = getAttValuesList(template, expr.getText());
//		System.out.println("values"+values);
		List<String> atts = getArgsAttributes(template);


		if(atts.size()==values.size()){
			for(int a=0;a<atts.size();a++){
				attmap.put(atts.get(a), values.get(a));
			}
		}

		return attmap;
	}
	
	/*
	public static Map<String, Expression> getAttributesMap(String template, Expression expr){

		Map<String, Expression> attmap = new HashMap<String, Expression>();

		List<Expression> values = getAttValuesList(template, expr);
		List<String> atts = getArgsAttributes(template);


		if(atts.size()==values.size()){
			for(int a=0;a<atts.size();a++){
				attmap.put(atts.get(a), values.get(a));
			}
		}

		return attmap;
	}
	
*/
	
	

	public static void main(String[] args) {
		/*
		// TODO Auto-generated method stub
		//mContentRect.left                                                    | ${RectF}.left  

		//		Map<String, String> vals = getTokenValues("${float} - ${RectF}.right","mChartWidth - mContentRect.right");

		List<String> vals = getTokenValuesList("${CGFloat}-${CGRect}.size.width-${CGRect}.origin.x","_chartWidth-_contentRect.size.width-_contentRect.origin.x");

		System.out.println("getAttr"+getAttValuesList("<arg0>.set(<arg1>)", "rect.set(1.0f)"));

		Pattern dollarRgx1 		= Pattern.compile("\\<([a-zA-Z0-9]+?)\\>");

		System.out.println(getArgsAttributes("<arg0>.set(<arg1>)"));


		System.out.println(vals);
		Iterator<String> aaaa = vals.iterator();
		String prev = "";
		String arg = "arg";
		int argn = 0;
		List<String> lst = new LinkedList<String>();
		List<String> nlst = new LinkedList<String>();

		String rrr=getGeneralVarsStr("${CGFloat}-${CGRect}.size.width-${CGRect}.origin.x", "_chartWidth-_contentRect.size.width-_contentRect.origin.x");
		System.out.println(rrr);
		String[] aaa = rrr.split("\\,");
		//		System.err.println("arg0".split("\\,"));
		List<String> bbb = Arrays.asList(aaa);
		System.err.println(bbb);
		System.out.println(getNameMap(bbb,vals ));



		
		System.out.println("CGFloat(1.0)".replaceAll("\\([^\\(]*\\)", ""));

		String expr = "${CGFloat}-${CGRect}.size.width-${CGRect}.origin.x";
		String ccc = "${CGFloat}";
		ccc= ccc.replace("${", "\\$\\{").replace("}", "\\}");
		//		ccc= ccc.replace("}", "\\}");
		System.out.println(expr.replaceFirst(ccc, "arg0"));


		Pattern dollarRgx 		= Pattern.compile("\\$\\{([a-zA-Z0-9]+?)\\}");

		//		System.out.println(expr.replace("${CGRect}", "arg0"));
		Matcher dollarMatcher 	= dollarRgx.matcher(expr);
		while(dollarMatcher.find()){
			String str = dollarMatcher.group(0);
			System.out.println(str);
		}
		
		List<String> eee = new ArrayList<String>();
		eee.add("arg0");
		eee.add("arg1");
		eee.add("arg1");
		Map<String, String> map = new HashMap<String, String>();
		map.put("arg0", "AAA");
		map.put("arg1", "BBB");
		System.out.println(findMatching("${CGFloat}-${CGRect}.size.width-${CGRect}.origin.x", eee,map ));
*/
		//		expr= expr.replaceAll("\\b("+local_var_name+")\\b","\\${"+localtypeMap.get(local_var_name)+"}");
		String test="if 	(mChartHeight > 0 && mChartWidth > 0) \n{	;		}";
		//for (<arg2> <arg0>:<arg1>){<arg3>}
//		System.err.println(test);
		//how to ignore space ..
//		System.out.println(getAttributesMap("if (<arg0>) {...}".replaceAll("\\.\\.\\.", "\\<arg10\\>"), test));

		Map<String, String> argmap = MatchingTemplate.getAttributesMap("for (<arg0> <arg1> : <arg2>) {...}".replaceAll("\\.\\.\\.", "\\<arg10\\>"), "for (Integer num : numbers) {            sum += num;        }");

		
		
		System.out.println("argmap"+argmap);
		Iterator<String> iterator = argmap.keySet().iterator();
//		ST st_android = new ST("for <arg0> in <arg1> {<arg3>}");
		ST st_android = new ST("if <arg0> {...}".replaceAll("\\.\\.\\.", "\\<arg10\\>"));
//		if <arg0>        {...}
		//			ST st_android2 = new ST("<arg0> var <arg2>: <arg1> = <arg3>");

		while(iterator.hasNext()){
			String key=iterator.next();
			st_android.add(key, argmap.get(key));
			//				st_android2.add(key, argmap.get(key));
			System.err.println("key"+key);
		}

		System.out.println("st_android\n"+st_android.render());
		
		String words = "@Override public <arg0> <arg1>(...) {...}         ";
		words = words.replaceAll("\\.\\.\\.", "\\<arg10\\>");
		System.out.println(words);
//		System.out.println(getTokenValues("${float}-${RectF}.right","mChartWidth - mContentRect.right"));
//		System.out.println(getTokenValues("for(${arg2} ${arg0}:${arg1}){${arg3}}",test));
	}

}
