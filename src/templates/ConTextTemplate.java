package templates;

import java.io.StringReader;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.STGroupString;



public class ConTextTemplate {
	static String NL = "\n";
	static STGroup group = new STGroupDir("/Users/kijin/Documents/workspace_mars/GumTreeTester/templates");

	public static String varDeclAndroid(String modifier, String type, String name, String init){

		ST st_android = group.getInstanceOf("vardecl_java");
	
		st_android.add("modi", modifier);
		st_android.add("type", type);
		st_android.add("name", name);
		st_android.add("value", init);

		return st_android.render();
	}

	public static String enclosing(String str){
		return "\""+str+"\"\n";
	}

	public void testNestedIterationWithArg() throws Exception {
		STGroup group = new STGroup();
		group.defineTemplate("test", "users", "<users:{u | <u.id:{id | <id>=}><u.name>}>!");
		ST st = group.getInstanceOf("test");
		//	        st.add("users", new TestCoreBasics.User(1, "parrt"));
		//	        st.add("users", new TestCoreBasics.User(2, "tombu"));
		//	        st.add("users", new TestCoreBasics.User(3, "sri"));
		String expected = "1=parrt2=tombu3=sri!";
		String result = st.render();
		//	        assertEquals(expected, result);
	}

	public static String varDeclSwift(String modifier, String type, String name, String init){
		ST st_swift = group.getInstanceOf("vardecl_swift");

		st_swift.add("modi", modifier);
		st_swift.add("type", type);
		st_swift.add("name", name);
		st_swift.add("value", init);
		return st_swift.render();
	}

	public static String methodDeclAndroid(String modifier, String type, String name, String init){
		ST st_swift = group.getInstanceOf("methoddecl_java");

		st_swift.add("modi", modifier);
		st_swift.add("type", type);
		st_swift.add("name", name);
		st_swift.add("value", init);
		return st_swift.render();
	}

	public static String methodDeclSwift(String modifier, String type, String name, String init){
		ST st_swift = group.getInstanceOf("methoddecl_swift");

		st_swift.add("modi", modifier);
		st_swift.add("type", type);
		st_swift.add("name", name);
		st_swift.add("value", init);
		return st_swift.render();
	}


	public static void main(String[] args){

		System.err.println(ConTextTemplate.varDeclAndroid("public final", "Rect", "orginX", "new Rect()"));
		System.out.println(ConTextTemplate.varDeclSwift  ("public",       "var",  "orginX", "Rect()"));
		System.err.println(ConTextTemplate.methodDeclAndroid("public final", "Rect",    "orginX", "left.x"));
		System.out.println(ConTextTemplate.methodDeclSwift  ("public",       "CGFloat", "orginX", "rect.orgin.x"));

		ST vardecl_java = new ST("<modi> <type> <name> = <value>;");
		vardecl_java.add("modi", "public");
		vardecl_java.add("type", "Matrix");
		vardecl_java.add("name", "mMatrx");
		vardecl_java.add("value", "new Matrix()");
		System.out.println(vardecl_java.render());

		STGroup group = new STGroup();
		group.defineTemplate("test", "names", "<names:{n | <n>};separator=\",\">!");
		ST st = group.getInstanceOf("test");
		st.add("names", "Ter");
		st.add("names", "Tom");
		st.add("names", "Sumana");
		String expected = "TerTomSumana!";
		String result = st.render();
		System.out.println(result);
		//        assertEquals(expected, result);

		ST test_multi = new ST(
				"<type> <name>(<args; separator=\",\">)\n"
				+ "{\n"
				+ "\t<statements; separator=\";\n\">;"
				+ "\n\t<return_s>;\n"
				+ "}");

		String aaaaa = 				"<type> <name>(<args; separator=\",\">)\n"
				+ "{\n"
				+ "\t<statements; separator=\";\n\">;"
				+ "\n\t<return_s>;\n"
				+ "}";
		
		test_multi = new ST(aaaaa);
		
		test_multi.add("type", "int");
		test_multi.add("name", "getOffsetX");
		test_multi.add("args", "int a");
		test_multi.add("args", "float j");
		test_multi.add("statements", "a +=1");
		test_multi.add("statements", "j =j-1+a");
		test_multi.add("return_s", "return s");

		ST test = new ST("eeee = <eee>");
		
		System.out.println(test_multi.render());
/*
		ST test_params_swift = new ST("let <name> = <expr>");
//		Object cc = test_params_swift.get
		//STGroup();
//		String templates = "method(type,name,args) ::= <<
//<type> <name>(<args; separator=",">) {
//  <statements; separator="\n">
//}
//>>"; // templates from above
		// Use the constructor that accepts a Reader
//		STGroup group = new STGroup();
//		StringTemplate t = group.getInstanceOf("vardef");
//		t.setAttribute("type", "int");
//		t.setAttribute("name", "foo");
//		System.out.println(t);
		
		STGroup ggroup = new STGroupDir("/Users/kijin/Documents/workspace_mars/GumTreeTester/android_template");
		ST aaa = ggroup.getInstanceOf("params");
		aaa.add("types", "int");
		aaa.add("names", "i");
		aaa.add("types", "float");
		aaa.add("names", "j");
		
		System.out.println(aaa.render());
		ST aaa1 = ggroup.getInstanceOf("method");
		aaa1.add("type", "int");
		aaa1.add("name", "foo");
	
		aaa1.add("args", "int k");
		aaa1.add("args", "int j");
		aaa1.add("args", "int p");
		aaa1.add("statements", "mMaxScaleY = yScale;");
		aaa1.add("statements", "mMaxScaleY = yScale;");
		aaa1.add("statements", "mMaxScaleY = yScale;");
		aaa1.add("statements", "mMaxScaleY = yScale;");
		aaa1.add("return_s", "return 1f;");
		System.out.println(aaa1.render());
		methoddecl_java(modi, type, name, value) ::= "<modi> <type> <name>(){return <expr(value)>;}"
		*/
		String ggg = "";
//		String ggg = "expression(expr) ::="+enclosing("<if(expr)><expr><endif>");
//		ggg+="statement";
//		ggg+="return_statement(expression)::="+enclosing("return <expression>");
		ggg+="statements(statements)::="+enclosing("<statements:{n | <n>}>");
		ggg+="return_statement(expression)::="+enclosing("return <expression>");
		ggg+="if_statements(condition_clause, statements, else_clause)::="+enclosing("if <condition_clause>"
				+ "{<statements:{n | <n>}>} <else_clause>"
				+ "{\n"
				+ "<statements:{n | <n>}>"
				+ "\n}");

//		ggg+="statements(statement)::=<<"
//				+ "<statement; separator=\"\\n\">"
//				+ ">>";
	
//		if_statement : 'if' condition_clause code_block else_clause? ;
//		else_clause : 'else' code_block | 'else' if_statement  ;
		
		System.out.println(ggg);
		STGroup groupa = new STGroupString(ggg);
		ST stmts= groupa.getInstanceOf("if_statements");
		
		stmts.add("statements", "int a");
		stmts.add("statements", "int b");
		stmts.add("statements", "int c");
		stmts.add("condition_clause", "a>b");
		System.err.println(stmts.render());
		/*
		STGroup groupa = new STGroup();
		groupa.defineTemplate("expression", "<if(expr)><expr><endif>");
		groupa.defineTemplate("return_statement", "expression", "return <expression>");
//		group.defineTemplate("test", "names", "<names:{n | <n>};separator=\",\">!");
		groupa.defineTemplate("statement", "return_statement", "<return_statement:{n | <n>};separator=\";\n\">;");
		groupa.defineTemplate("statements", "return_statement", "<return_statement:{n | <n>};separator=\";\n\">;");
		
		ST expr = groupa.getInstanceOf("expression");
		ST return_expr = groupa.getInstanceOf("return_statement");
		ST statements = groupa.getInstanceOf("statements");
	//	expr.add("expr", "x==y");
//		expr.addAggr("expr", "x==y");
		return_expr.add("expression", expr);
		
		statements.add("return_statement", return_expr);
		statements.add("return_statement", return_expr);
		statements.add("return_statement", "return x==y");
//		st.add("names", "Tom");
//		st.add("names", "Sumana");
		*/
		/*
		STGroup gggg = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/newtemp/java.stg");

//		System.out.println(statements.render());
 * 
 */
		/*
		String aaaaaa = "<type> <name>(<parameter; separator=,>)\n"
				+ "{\n"
				+ "\t<return_statements; separator=\";\n\">;"
				+ "}\n";
		
//		ggg+=aaaaaa;
		*/
		/*
		STGroup group_test = new STGroupString(ggg); 
		ST aaa = group_test.getInstanceOf("expression");
		
		aaa.add("expr", "x==y");
		ST aaa1 = group_test.getInstanceOf("return_statement");
		aaa1.add("expression", aaa);
//		ST aaa2 = group_test.getInstanceOf("statements");
//		aaa2.add("statement", aaa);
		System.out.println(aaa1.render());
		*/
		
		STGroup gggg = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/newtemp/java.stg");

		ST program = gggg.getInstanceOf("program");
		program.add("globals", "int a");
		program.add("functions", "void foo1() {}");
		program.add("functions", "void foo2() {}");
		program.add("functions", "void foo3() {}");
		
		System.out.println(program.render());
	}
}
