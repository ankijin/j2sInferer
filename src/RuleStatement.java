import java.util.Map;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import recording.MappingElement;

public class RuleStatement extends ASTVisitor{
	Map<MappingElement, MappingElement> mappingElements;
	//			Set names = new HashSet();
	String swiftstr ="(generated)";

	public void setMap(Map<MappingElement, MappingElement> map){
		mappingElements = map;	
	}

	public boolean visit(FieldDeclaration node) {
		//				SimpleName name = node.getName();
		//			System.out.println("FieldDeclaration\n");
		//			System.out.println(node.fragments());
		//			System.out.println(node.modifiers()+""+node.getType());
		return true; // do not continue 
	}

	public void endVisit(FieldDeclaration node) {
	//  System.out.println("end FieldDeclaration\n");
//		System.out.println(swiftstr);
	};

	public boolean visit(org.eclipse.jdt.core.dom.Modifier node) {
		//			System.out.println("Modifier\n"+node.getNodeType());
		if(node.getKeyword().toString().equals("final")){
			return false;
		}
		
		MappingElement e = new MappingElement(node.getKeyword().toString(), node.getNodeType());
		//			
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;	
	};

	public boolean visit(org.eclipse.jdt.core.dom.PrimitiveType node) {
		MappingElement e = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getNodeType());
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	}
	/**
	 * copy variable name
	 */

	@Override
	public boolean visit(QualifiedName node) {
		// TODO Auto-generated method stub

		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	}

	public boolean visit(SimpleName node) {

		swiftstr +=node.getIdentifier();
		if(node.getParent().getNodeType()!=43)
			swiftstr +="=";
		else{
			swiftstr +=" ";
		}

		return true;
	}
	
	@Override
	public boolean visit(ArrayCreation node) {
		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	}

	
	public boolean visit(org.eclipse.jdt.core.dom.ClassInstanceCreation node) {
		
		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	};
	
	/*		
		public void endVisit(VariableDeclarationFragment node) {
			System.out.println("END VariableDeclarationFragment\n");
			System.out.println(node.resolveBinding());
			System.out.println(node.getName());
			System.out.println(node.getFlags());
//			System.out.println(node.);
			System.out.println(node.getInitializer());
//			return true;
		};


	 */

	public boolean visit(org.eclipse.jdt.core.dom.Assignment node) {
		//				System.out.println("Assignment\n");
		return true;
	}


	public void endVisit(VariableDeclarationFragment node) {
		//				System.out.println("END VariableDeclarationFragment\n");
		//				System.out.println(node.getExtraDimensions());
		//				System.out.println(node.getInitializer());
		//				swiftstr +="=";

	}

	public boolean visit(org.eclipse.jdt.core.dom.VariableDeclarationExpression node) {
		//				System.out.println("END VariableDeclarationExpression\n");
		return true;
	}

	public boolean visit(org.eclipse.jdt.core.dom.NumberLiteral node) {
		MappingElement e = new MappingElement(node.getToken().toString(), node.getNodeType());
		//				System.out.println("NumberLiteral"+mapping.get(node.getToken().toString()));
		//				System.out.println(node.getToken().toString()+node.getFlags());
		//			System.err.println(mappingElements);

		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";


		return true;
	}




}
