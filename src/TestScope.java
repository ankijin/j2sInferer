

import org.antlr.runtime.RecognitionException;
import org.antlr.v4.Tool;
import org.antlr.v4.misc.Utils;
import org.antlr.v4.parse.ScopeParser;
import org.antlr.v4.tool.Attribute;
import org.antlr.v4.tool.Grammar;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestScope {

    
	static String[] argPairs = {
			"",                                 "",
			" ",                                "",
			"int i",                            "i:int",
			"int[] i, int j[]",                 "i:int[], j:int []",
			"Map<A,B>[] i, int j[]",          	"i:Map<A,B>[], j:int []",
			"Map<A,List<B>>[] i",	          	"i:Map<A,List<B>>[]",
			"int i = 34+a[3], int j[] = new int[34]",
			"i:int=34+a[3], j:int []=new int[34]",
			"char *[3] foo = {1,2,3}",     	    "foo:char *[3]={1,2,3}", // not valid C really, C is "type name" however so this is cool (this was broken in 4.5 anyway)
			"String[] headers",					"headers:String[]",
			"for(String[] aaaa)",				"headers:String[]",
			// C++
			"std::vector<std::string> x",       "x:std::vector<std::string>", // yuck. Don't choose :: as the : of a declaration

			// python/ruby style
			"i",                                "i",
			"i,j",                              "i, j",
			"i\t,j, k",                         "i, j, k",

			// swift style
			"x: int",                           "x:int",
			"x :int",                           "x:int",
			"pppp pppp :int",                           "x:int",
			"x:int",                            "x:int",
			"x:int=3",                          "x:int=3",
			"aaa aaa:String, bbb:AAAA", 		 "x:int=3",
			"r:Rectangle=Rectangle(fromLength: 6, fromBreadth: 12)", "r:Rectangle=Rectangle(fromLength: 6, fromBreadth: 12)",
			"p:pointer to int",                 "p:pointer to int",
			"a: array[3] of int",               "a:array[3] of int",
			"a \t:\tfunc(array[3] of int)",     "a:func(array[3] of int)",
			"x:int, y:float",                   "x:int, y:float",
			"x:T?, f:func(array[3] of int), y:int", "x:T?, f:func(array[3] of int), y:int",

			// go is postfix type notation like "x int" but must use either "int x" or "x:int" in [...] actions
			"float64 x = 3",                    "x:float64=3",
			"map[string]int x",                 "x:map[string]int",
	};


	static String[] argPairs2 = {
//			"for j in _xBounds.min.stride(through: _xBounds.range + _xBounds.min, by: 1)        {  }",     "????",
//			"through: _xBounds.range",    "!!!????",
//			"func(pppp pppp :Integer) {}",                                "???",
//			"func(pppp pppp :Integer)",                                "??",
//			"int j", "correct",
//			"int j = mXBounds.min", "correct",	
//			"pppp pppp :Integer", "correct", 
			"context context: CGContext", "????"
//			"for (int j = mXBounds.min; j <= mXBounds.range + mXBounds.min; j++) {}",                                "??",
//			"pppp pppp :Integer=1",             "correct",
//			"j++",             "nono",
//			"for (int j = mXBounds.min; j <= mXBounds.range + mXBounds.min; j++) {            final BubbleEntry entry = dataSet.getEntryForIndex(j);  ", "??"
	};

	String input;
	String output;

	public TestScope(String input, String output) {
		this.input = input;
		this.output = output;
		//		for (int j = mXBounds.min; j <= mXBounds.range + mXBounds.min; j++) {}
	}



	@Test
	public void testArgs() throws Exception {
		Grammar dummy = new Grammar("grammar T; a:'a';");
		//	    ScopeParser.parseTypedArgList(s, errMgr)
		LinkedHashMap<String, Attribute> attributes = ScopeParser.parseTypedArgList(null, input, dummy).attributes;
		List<String> out = new ArrayList<>();
		for (String arg : attributes.keySet()) {
			Attribute attr = attributes.get(arg);
			out.add(attr.toString());
		}
		String actual = Utils.join(out.toArray(), ", ");
		//	    assertEquals(output, actual);
	}

	//	@Parameterized.Parameters(name="{0}")
	public static Collection<Object[]> getAllTestDescriptors() {
		List<Object[]> tests = new ArrayList<>();
		for (int i = 0; i < argPairs2.length; i+=2) {
			String arg = argPairs2[i];
			String output = argPairs2[i+1];
			tests.add(new Object[]{arg,output});
		}
		return tests;
	}
	public static void main(String args []) throws RecognitionException{
		
	
		Tool tool = new Tool();
		//		Grammar dummy = tool.loadGrammar("/Users/kijin/Documents/repo/tools/grammars-v4/java/Java.g4");
		Collection<Object[]> test = getAllTestDescriptors();
		Iterator<Object[]> aaaa = test.iterator();
		LinkedHashMap<String, Attribute> attributes_new = new LinkedHashMap<String, Attribute> ();
		while(aaaa.hasNext()){
			Object[] next = aaaa.next();
			Grammar dummy = new Grammar("grammar T; a:'a';");
			LinkedHashMap<String, Attribute> attributes = ScopeParser.parseTypedArgList(null, (String)next[0], dummy).attributes;
//			ScopeParser.
			//			LinkedHashMap<String, Attribute> attributes = ScopeParser.parseTypedArgList(null, (String)next[0], dummy).attributes;
			
			List<String> out = new ArrayList<>();
			for (String arg : attributes.keySet()) {
				Attribute attr = attributes.get(arg);
				System.out.println(attr.decl);
				System.out.println(attr.type+" "+attr.name+" "+attr.initValue);
//				if(StringUtils.isAlphanumeric(attr.name) && StringUtils.isAlphanumeric(attr.type) && !attr.initValue.contains(";")){
//					out.add(attr.decl);
										System.out.println(attr.decl);
//										System.err.println(ScopeParser.parse(null, attr.decl, ';', dummy).attributes);
//					System.out.println(attr.type+" "+attr.name+" "+attr.initValue);
//					attributes_new.put(arg, attr);
					//					System.err.println((String)next[1]);
//				}
			}
			//			String actual = Utils.join(out.toArray(), ", ");
			//			System.out.println(actual);


		}
		
//		System.out.println(attributes_new);

		//		LinkedHashMap<String, Attribute> attributes = ScopeParser.parseTypedArgList(null, "aaa aaa:String, bbb:AAAA;", dummy).attributes;

		//		System.out.println(attributes);

		//		System.out.println(TestScope.getAllTestDescriptors());

		/*
//	    Grammar dummy = tool.loadGrammar("/Users/kijin/Documents/repo/tools/grammars-v4/java/Java.g4");
		Grammar dummy = new Grammar("grammar T; for");
	    System.out.println(dummy.ast.getText());
//	    dummy.fileName="/Users/kijin/Documents/repo/tools/grammars-v4/java/Java.g4";
//	    ScopeParser.parseTypedArgList(s, errMgr)
	    ScopeParser p = new ScopeParser();
	    System.err.println(ScopeParser.parse(null, "Map<A,B>[] i, j", ',', dummy));
//	    ScopeParser.
	    LinkedHashMap<String, Attribute> attributes = ScopeParser.parseTypedArgList(null, "Map<A,B>[] i, j", dummy).attributes;
	    List<String> out = new ArrayList<>();
	    for (String arg : attributes.keySet()) {
		    Attribute attr = attributes.get(arg);
		    out.add(attr.toString());
	    }
	    String actual = Utils.join(out.toArray(), ", ");
	    System.out.println(actual);
		 */
	}
}
