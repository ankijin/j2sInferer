import java.util.LinkedList;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.Type;

import gumtree_tester.SwiftParser;

//import SwiftParser.Expression_elementContext;
//import SwiftParser.Primary_expressionContext;

//import SwiftParser.Prefix_expressionContext;

//import SwiftParser.External_parameter_nameContext;

public class VariableTokenMapping {
	public Expression androidExpr;
	public ParserRuleContext swiftExpr;

	public VariableTokenMapping(Expression aExpr, ParserRuleContext sExpr){
		androidExpr = aExpr;
		swiftExpr = sExpr;
	}

	public void compute(){
		List<VarStore> avarlist = new LinkedList<VarStore>();
		List<VarStore> svarlist = new LinkedList<VarStore>();

		androidExpr.accept(new ASTVisitor() {

			
			@Override
			public void preVisit(ASTNode node) {
				// TODO Auto-generated method stub
				
				
//				String str= printT(node);
//				avarlist.add(new VarStore(node.toString()));
				super.preVisit(node);
			}
			
			String printT(ASTNode n){
				
			      if (n instanceof Name) return ((Name) n).getFullyQualifiedName();
			        if (n instanceof Type) return n.toString();
			        if (n instanceof Modifier) return n.toString();
			        if (n instanceof StringLiteral) return ((StringLiteral) n).getEscapedValue();
			        if (n instanceof NumberLiteral) return ((NumberLiteral) n).getToken();
			        if (n instanceof CharacterLiteral) return ((CharacterLiteral) n).getEscapedValue();
			        if (n instanceof BooleanLiteral) return ((BooleanLiteral) n).toString();
			        if (n instanceof InfixExpression) return ((InfixExpression) n).getOperator().toString();
			        if (n instanceof PrefixExpression) return ((PrefixExpression) n).getOperator().toString();
			        if (n instanceof PostfixExpression) return ((PostfixExpression) n).getOperator().toString();
			        if (n instanceof Assignment) return ((Assignment) n).getOperator().toString();
			        if (n instanceof TextElement) return n.toString();
			        if (n instanceof TagElement) return ((TagElement) n).getTagName();
					return "";
			}
			
			@Override
			public boolean visit(SimpleName node) {
				// TODO Auto-generated method stub
				if(node.resolveTypeBinding()!=null)
				avarlist.add(new VarStore(node.toString(), node.resolveTypeBinding().getName()));
				else{
					avarlist.add(new VarStore(node.toString()));
				}
				return super.visit(node);
			}
			@Override
			public boolean visit(QualifiedName node) {
				// TODO Auto-generated method stub
//				node.resolveTypeBinding();
				if(node.resolveTypeBinding()!=null)
				avarlist.add(new VarStore(node.toString(), node.resolveTypeBinding().getName()));
				else{
					avarlist.add(new VarStore(node.toString()));
				}
				return super.visit(node);
			}

		});

		swiftExpr.accept(new BaseSwiftRecorder(){
			
			void visitTest(){
				
			}
			
			
			@Override
			public Object visitExpression_element(SwiftParser.Expression_elementContext ctx) {
//				ctx.identifier();
//				ctx.expression();
				svarlist.add(new VarStore(ctx.getText()));
//				if(ctx.expression()!=null)
//					svarlist.add(new VarStore(ctx.expression().getText()));
				// TODO Auto-generated method stub
				return super.visitExpression_element(ctx);
			}
			@Override
			public Object visitPrefix_expression(SwiftParser.Prefix_expressionContext ctx) {
				svarlist.add(new VarStore(ctx.getText()));
//				ctx.postfix_expression()
				if(ctx.postfix_expression()!=null){
//					ctx.postfix_expression().
				}
				// TODO Auto-generated method stub
				return super.visitPrefix_expression(ctx);
			}
			@Override
			public Object visitTerminal(TerminalNode node) {
//				svarlist.add(new VarStore(node.getText()));
				
				// TODO Auto-generated method stub
				return super.visitTerminal(node);
			}
			
			@Override
			public Object visitPrimary_expression(SwiftParser.Primary_expressionContext ctx) {
//				svarlist.add(new VarStore(ctx.getText()));
				// TODO Auto-generated method stub
				return super.visitPrimary_expression(ctx);
			}
			
			@Override
			public Object visitIdentifier(SwiftParser.IdentifierContext ctx) {
				// TODO Auto-generated method stub
//				svarlist.add(new VarStore(ctx.getText()));
				if(ctx.getParent() instanceof SwiftParser.Postfix_expressionContext){
//					svarlist.add(new VarStore(ctx.getText()));
				}
				return super.visitIdentifier(ctx);
			}
			@Override
			public Object visitExternal_parameter_name(SwiftParser.External_parameter_nameContext ctx) {
				// TODO Auto-generated method stub
				return null;
			}
			
		});
		System.out.println("tokenvars");
		System.out.println(avarlist);
		System.out.println(svarlist);

	}

	public class VarStore{
		public String varToken="";
		public String varType="";

		public VarStore(String v, String t){
			varToken = v;
			varType = t;
		}
		public VarStore(String v){
			varToken = v;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return varType+":"+varToken;
		}
	}
}
