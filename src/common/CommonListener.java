package common;

// Generated from Common.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CommonParser}.
 */
public interface CommonListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CommonParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(CommonParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(CommonParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(CommonParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(CommonParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(CommonParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(CommonParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(CommonParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(CommonParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#leftAssign}.
	 * @param ctx the parse tree
	 */
	void enterLeftAssign(CommonParser.LeftAssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#leftAssign}.
	 * @param ctx the parse tree
	 */
	void exitLeftAssign(CommonParser.LeftAssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#rightAssign}.
	 * @param ctx the parse tree
	 */
	void enterRightAssign(CommonParser.RightAssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#rightAssign}.
	 * @param ctx the parse tree
	 */
	void exitRightAssign(CommonParser.RightAssignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierFunctionCall(CommonParser.IdentifierFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierFunctionCall(CommonParser.IdentifierFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printlnFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterPrintlnFunctionCall(CommonParser.PrintlnFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printlnFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitPrintlnFunctionCall(CommonParser.PrintlnFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterPrintFunctionCall(CommonParser.PrintFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitPrintFunctionCall(CommonParser.PrintFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assertFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterAssertFunctionCall(CommonParser.AssertFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assertFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitAssertFunctionCall(CommonParser.AssertFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sizeFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterSizeFunctionCall(CommonParser.SizeFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sizeFunctionCall}
	 * labeled alternative in {@link CommonParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitSizeFunctionCall(CommonParser.SizeFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#enclosed}.
	 * @param ctx the parse tree
	 */
	void enterEnclosed(CommonParser.EnclosedContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#enclosed}.
	 * @param ctx the parse tree
	 */
	void exitEnclosed(CommonParser.EnclosedContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#outer}.
	 * @param ctx the parse tree
	 */
	void enterOuter(CommonParser.OuterContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#outer}.
	 * @param ctx the parse tree
	 */
	void exitOuter(CommonParser.OuterContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#inner}.
	 * @param ctx the parse tree
	 */
	void enterInner(CommonParser.InnerContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#inner}.
	 * @param ctx the parse tree
	 */
	void exitInner(CommonParser.InnerContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#keyword_with}.
	 * @param ctx the parse tree
	 */
	void enterKeyword_with(CommonParser.Keyword_withContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#keyword_with}.
	 * @param ctx the parse tree
	 */
	void exitKeyword_with(CommonParser.Keyword_withContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#keyword_after}.
	 * @param ctx the parse tree
	 */
	void enterKeyword_after(CommonParser.Keyword_afterContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#keyword_after}.
	 * @param ctx the parse tree
	 */
	void exitKeyword_after(CommonParser.Keyword_afterContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDecl(CommonParser.FunctionDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDecl(CommonParser.FunctionDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#idList}.
	 * @param ctx the parse tree
	 */
	void enterIdList(CommonParser.IdListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#idList}.
	 * @param ctx the parse tree
	 */
	void exitIdList(CommonParser.IdListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#exprList}.
	 * @param ctx the parse tree
	 */
	void enterExprList(CommonParser.ExprListContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#exprList}.
	 * @param ctx the parse tree
	 */
	void exitExprList(CommonParser.ExprListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtExpression(CommonParser.GtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtExpression(CommonParser.GtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumberExpression(CommonParser.NumberExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumberExpression(CommonParser.NumberExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(CommonParser.IdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(CommonParser.IdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterModulusExpression(CommonParser.ModulusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitModulusExpression(CommonParser.ModulusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(CommonParser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(CommonParser.NotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyExpression(CommonParser.MultiplyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyExpression(CommonParser.MultiplyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtEqExpression(CommonParser.GtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtEqExpression(CommonParser.GtEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(CommonParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(CommonParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterStringExpression(CommonParser.StringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitStringExpression(CommonParser.StringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNullExpression(CommonParser.NullExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNullExpression(CommonParser.NullExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code in1putExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIn1putExpression(CommonParser.In1putExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code in1putExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIn1putExpression(CommonParser.In1putExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtEqExpression(CommonParser.LtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtEqExpression(CommonParser.LtEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtExpression(CommonParser.LtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtExpression(CommonParser.LtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpression(CommonParser.BoolExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpression(CommonParser.BoolExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotEqExpression(CommonParser.NotEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotEqExpression(CommonParser.NotEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDivideExpression(CommonParser.DivideExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDivideExpression(CommonParser.DivideExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(CommonParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(CommonParser.OrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryMinusExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpression(CommonParser.UnaryMinusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryMinusExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpression(CommonParser.UnaryMinusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code powerExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPowerExpression(CommonParser.PowerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code powerExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPowerExpression(CommonParser.PowerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqExpression(CommonParser.EqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqExpression(CommonParser.EqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInExpression(CommonParser.InExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInExpression(CommonParser.InExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddExpression(CommonParser.AddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddExpression(CommonParser.AddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubtractExpression(CommonParser.SubtractExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubtractExpression(CommonParser.SubtractExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTernaryExpression(CommonParser.TernaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ternaryExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTernaryExpression(CommonParser.TernaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inputExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInputExpression(CommonParser.InputExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inputExpression}
	 * labeled alternative in {@link CommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInputExpression(CommonParser.InputExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(CommonParser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(CommonParser.KeywordContext ctx);
}