grammar Common;		
parse
 : (enclosed | keyword_with | expression| assignment | functionCall | statement) EOF
 ;

block
 : (keyword_with | enclosed | expression| assignment | functionCall | statement )*
 ;

statement
 : assignment
 | functionCall
 | expression
 | Identifier+
 | Identifier '[' Identifier? ']'
 | Identifier '(' Identifier? ')'
 | idList
 ;


assignment
 : leftAssign '=' rightAssign
 ;

leftAssign :
 Identifier+ 
;

rightAssign :
 Identifier+
;

functionCall
 : Identifier '(' exprList? ')' #identifierFunctionCall
 | Println '(' expression? ')'  #printlnFunctionCall
 | Print '(' expression ')'     #printFunctionCall
 | Assert '(' expression ')'    #assertFunctionCall
 | Size '(' expression ')'      #sizeFunctionCall
 ;

enclosed
 : outer '{' inner '}'
 ;
outer : (Identifier|expression)*
 ;
inner : block ;
keyword_with
 : keyword+ (statement|block) ;
keyword_after
 : (block keyword)|(statement keyword) ;

functionDecl
 : Def Identifier '(' idList? ')' block End
 ;


idList
 : Identifier (',' Identifier)*
 ;

exprList
 : expression (',' expression)*
 ;

expression
 : '-' expression                           #unaryMinusExpression
 | '!' expression                           #notExpression
 | expression '^' expression                #powerExpression
 | expression '*' expression                #multiplyExpression
 | expression '/' expression                #divideExpression
 | expression '%' expression                #modulusExpression
 | expression '+' expression                #addExpression
 | expression '-' expression                #subtractExpression
 | expression '>=' expression               #gtEqExpression
 | expression '<=' expression               #ltEqExpression
 | expression '>' expression                #gtExpression
 | expression '<' expression                #ltExpression
 | expression '==' expression               #eqExpression
 | expression '!=' expression               #notEqExpression
 | expression '&&' expression               #andExpression
 | expression '||' expression               #orExpression
 | expression '?' expression ':' expression #ternaryExpression
 | expression In expression                 #inExpression
 | Number                                   #numberExpression
 | Identifier                       #identifierExpression
 | String                         #stringExpression
 | Bool                                     #boolExpression
 | Null                                     #nullExpression
 | Input '(' String? ')'                    #inputExpression
 | expression '[' expression? ']'           #in1putExpression
 ;



Println  : 'println';
Print    : 'print';
Input    : 'input';
Assert   : 'assert';
Size     : 'size';
Def      : 'def';
For      : 'for';
While    : 'while';
To       : 'to';
Do       : 'do';
End      : 'end';
In       : 'in';
Null     : 'null';

keyword : ('if'|'else'|'return');

Or       : '||';
And      : '&&';
Equals   : '==';
NEquals  : '!=';
GTEquals : '>=';
LTEquals : '<=';
Pow      : '^';
Excl     : '!';
GT       : '>';
LT       : '<';
Add      : '+';
Subtract : '-';
Multiply : '*';
Divide   : '/';
Modulus  : '%';
OBrace   : '{';
CBrace   : '}';
OBracket : '[';
CBracket : ']';
OParen   : '(';
CParen   : ')';
SColon   : ';';
Assign   : '=';
Comma    : ',';
QMark    : '?';
Colon    : ':';

Bool
 : 'true' 
 | 'false'
 ;

Number
 : Int ('.' Digit*)?
 ;

Identifier
 : [a-zA-Z_0-9'!'':''->'';'] [a-zA-Z_0-9'!'':''->'';''?']* ('['|']')*
 ;

Keywords
 : ':'|'->' 
 ;

String
 : ["] (~["\r\n] | '\\\\' | '\\"')* ["]
 | ['] (~['\r\n] | '\\\\' | '\\\'')* [']
 ;
Comment
 : ('//' ~[\r\n]* | '/*' .*? '*/') -> skip
 ;
Space
 : [ \t\r\n\u000C] -> skip
 ;
fragment Int
 : [1-9] Digit*
 | '0'
 ;
  
fragment Digit 
 : [0-9]
 ;