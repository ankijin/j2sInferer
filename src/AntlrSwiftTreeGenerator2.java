
import com.github.gumtreediff.gen.TreeGenerator;
import com.github.gumtreediff.gen.antlr.AbstractAntlrTreeGenerator;
import com.github.gumtreediff.gen.jdt.AbstractJdtVisitor;
import com.github.gumtreediff.tree.TreeContext;

import gumtree_tester.SwiftParser;

//import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.Tree;
//import org.antlr.tool.GrammarAST;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.runtime.Token;
//import org.antlr.runtime.TokenRewriteStream;
//import org.antlr.runtime.debug.ParseTreeBuilder;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
//import org.antlr.v4.runtime.tree.
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
//import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.tree.ParseTree;
//import org.eclipse.jdt.internal.formatter.Token;
//import org.antlr.runtime.tree.Tree;
import org.antlr.v4.runtime.tree.SyntaxTree;

import java.io.IOException;
import java.io.Reader;
//import java.util.List;
//import org.antlr.v4.runtime.*;


public class AntlrSwiftTreeGenerator2 extends TreeGenerator{	
	SwiftParser parser;
	
	
/*
    @Override

    protected CommonTree getStartSymbol(Reader r) throws RecognitionException, IOException {
    	ANTLRInputStream stream = new ANTLRInputStream(r);
    	//ANTLRStringStream stream = new ANTLRReaderStream(r);

        Lexer lexer = new SwiftLexer((CharStream)stream);
        parser = new SwiftParser(new CommonTokenStream(lexer));
        
        
      //  parser.
   //    ParseTree tree = parser.top
    //    CommonTree aaa = getSubtrees(tree);
        ParseTree top =parser.top_level();
        System.err.println(top.toStringTree());
     //   org.antlr.runtime.tree.ParseTree aaa = (org.antlr.runtime.tree.ParseTree) top;
     //  parser.top_level().getRuleContext();
       // System.err.println(parser.getRuleNames()[41]+parser.getRuleIndex(parser.getRuleNames()[41]));
      //  System.err.println(parser.getTokenType("SwiftParser$DeclarationContext"));
        CommonTree commonTree = new CommonTree ();
        
      //  setRecursiveDescendants(commonTree, top); 
        CommonTree ct = getSubtrees(top);
        System.out.println(ct.toStringTree());
        return ct;
    }
     
 */   
    private CommonTree getSubtrees(ParseTree tree){
        CommonTree commonTree = new CommonTree();
        return setRecursiveDescendants(commonTree, tree);
    }

    private CommonTree setRecursiveDescendants(CommonTree commonTree,ParseTree parseTree){
        int n = parseTree.getChildCount();
            for (int i = 0; i < n; i++) {
         //   System.out.println(parseTree.accept(new SwiftVisitor()));
           
          //  System.out.println();
            String name = parseTree.getChild(i).getClass().getSimpleName().toLowerCase().replace("context", "");
            int type = parser.getRuleIndex(parseTree.getChild(i).
            		getClass().getSimpleName().toLowerCase().replace("context", ""));
            
            	org.antlr.runtime.CommonToken token = new org.antlr.runtime.CommonToken(type, parseTree.getChild(i).toString());
            	CommonTree root = new CommonTree(token);
                commonTree.insertChild(i, root);
                setRecursiveDescendants(commonTree, parseTree.getChild(i));
            }
            return commonTree;
    }
 


	@Override
	protected TreeContext generate(Reader arg0) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	

}
