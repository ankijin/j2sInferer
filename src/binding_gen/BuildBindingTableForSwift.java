package binding_gen;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
//import org.apache.commons.lang3.StringUtils;

import alignment.common.NumLine;
import migration.SwiftBaseListener;
import migration.SwiftLexer;
import migration.SwiftParser;
import migration.SwiftParser.Binary_expressionContext;
import migration.SwiftParser.ExpressionContext;
import migration.SwiftParser.Function_call_expressionContext;
//import migration.SwiftParser.Function_nameContext;
import migration.SwiftParser.Prefix_expressionContext;
import migration.SwiftParser.Self_expressionContext;
import migration.SwiftParser.Top_levelContext;
import migration.SwiftParser.TypeContext;
import migration.SwiftParser.Variable_nameContext;
import sqldb.DBTablePrinter;
import sqldb.SQLiteJDBC;

public class BuildBindingTableForSwift {

	public static ResultSet getResult(String filename, int line, int column){
		ResultSet res11 = null;
		try{

			Class.forName("org.sqlite.JDBC");
			Connection connection = DriverManager.getConnection("jdbc:sqlite:charts-binding_t.db");
			//connection.setAutoCommit(true);
			Statement stmt = connection.createStatement();
			//stmt.executeUpdate(SQLiteJDBC.dropTable("binding_swift_stat"));

			String sql2 = "select * from binding_swift_stat where line="+line+" and column="+column;
			res11 = stmt.executeQuery(sql2);
			/*
			//if binding case
			while(res11.next() ){
				String varss = res11.getString("type");
//				String[] vvv = varss.split(",");
//				String conss = res11.getString("CONSTANTS");
//				String[] ccc = conss.split(",");
//				String temp = res11.getString("TEMPLATE");
				System.out.println(res11.getString("example")+" "+res11.getString("type")+"  "+ res11.getString("line")+"  "+ res11.getString("column"));
			}
			 */

		}catch(Exception e){

		}
		return res11;
	}
	public static void main(String[] args) throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
		// TODO Auto-generated method stub

		Class.forName("org.sqlite.JDBC");
		//		Connection connection = DriverManager.getConnection("jdbc:sqlite:antlr4-api_0430.db");s

		Connection connection = DriverManager.getConnection("jdbc:sqlite:charts-binding_t.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("binding_swift_stat"));

		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android_stat"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift_stat"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_binding_stat"));
		//		stmt.executeUpdate(SQLiteJDBC.dropTable("binding_android_stat"));

		//		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroidStat());
		//		stmt.executeUpdate(SQLiteJDBC.creategrammarSwiftStat());
		//		stmt.executeUpdate(SQLiteJDBC.createBindingStat());
		stmt.executeUpdate(SQLiteJDBC.createBindingSwift());

		//		stmt.executeUpdate(SQLiteJDBC.createBindingStat());

		//		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		//		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());

		ANTLRInputStream stream22 						= new ANTLRInputStream(new FileReader("/Users/kijin/Desktop/Charts-master/Charts/Classes/Utils/ViewPortHandler.swift"));
		Lexer lexer22 									= new SwiftLexer((CharStream)stream22);
		//		CommonTokenStream swiftcommon					= new CommonTokenStream(lexer22);

		SwiftParser parser111 = new SwiftParser(new CommonTokenStream(lexer22));

		SwiftParser.Top_levelContext root1111 = parser111.top_level();

//		Map<NumLine, String> numlinemap = new HashMap<NumLine, String>();
		Map<NumLine, ParserRuleContext> numlinemap = new HashMap<NumLine, ParserRuleContext>();
		SwiftBaseListener swift_listner = new SwiftBaseListener(){
			/*
			@Override
			public void enterEveryRule(ParserRuleContext ctx) {
//				swiftSyntax[ctx.getRuleIndex()]++;
				int l=ctx.getStart().getLine();
				int c=ctx.getStart().getCharPositionInLine();
//				ctx.getStart()
				NumLine aa = new NumLine(l,c);
				if(!numlinemap.containsKey(aa))
					numlinemap.put(aa, ctx.getText());
				// TODO Auto-generated method stub
				super.enterEveryRule(ctx);
			}
			 */

//			enterexpression
			@Override
			public void enterPrefix_expression(Prefix_expressionContext ctx) {
				// TODO Auto-generated method stub
				NumLine aa = new NumLine(ctx.start.getLine(),ctx.start.getCharPositionInLine()+1);
				numlinemap.put(aa, ctx);
				super.enterPrefix_expression(ctx);
			}
			
			@Override
			public void enterBinary_expression(Binary_expressionContext ctx) {
				// TODO Auto-generated method stub
				NumLine aa = new NumLine(ctx.start.getLine(),ctx.start.getCharPositionInLine()+1);
				numlinemap.put(aa, ctx);
				super.enterBinary_expression(ctx);
			}
			
			@Override
			public void enterVariable_name(Variable_nameContext ctx) {
				// TODO Auto-generated method stub
				NumLine aa = new NumLine(ctx.start.getLine(),ctx.start.getCharPositionInLine()+1);
				numlinemap.put(aa, ctx);
				super.enterVariable_name(ctx);
			}

			//			enterexpress
			@Override
			public void enterExpression(ExpressionContext ctx) {
				// TODO Auto-generated method stub
//				System.err.println("ExpressionContext		"+ctx.getText()+"  "+ctx.start.getLine()+", "+ctx.start.getCharPositionInLine()+1);

				NumLine aa = new NumLine(ctx.start.getLine(),ctx.start.getCharPositionInLine()+1);
				numlinemap.put(aa, ctx);
				super.enterExpression(ctx);
			}
			@Override
			public void enterSelf_expression(Self_expressionContext ctx) {
				// TODO Auto-generated method stub
				NumLine aa = new NumLine(ctx.start.getLine(),ctx.start.getCharPositionInLine()+1);
				numlinemap.put(aa, ctx);
				super.enterSelf_expression(ctx);
			}
			
			@Override
			public void enterType(TypeContext ctx) {
				// TODO Auto-generated method stub
				NumLine aa = new NumLine(ctx.start.getLine(),ctx.start.getCharPositionInLine()+1);
				numlinemap.put(aa, ctx);
				super.enterType(ctx);
			}
			
			@Override
			public void enterFunction_call_expression(Function_call_expressionContext ctx) {
				// TODO Auto-generated method stub
//				System.out.println("Function_call_expressionContext		"+ctx.getText()+"  "+ctx.start.getLine()+", "+ctx.start.getCharPositionInLine()+1);
				List<ParseTree> params = ctx.parenthesized_expression().children;
				NumLine aa = new NumLine(ctx.start.getLine(),ctx.start.getCharPositionInLine()+1);
				numlinemap.put(aa, ctx);
				super.enterFunction_call_expression(ctx);
			}
			/*
			@Override
			public void enterFunction_name(Function_nameContext ctx) {
				System.err.println("Function_nameContext		"+ctx.getText());
				// TODO Auto-generated method stub
				super.enterFunction_name(ctx);
			}
			 */
		};
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(swift_listner, root1111);;

//		System.err.println("\n"+numlinemap.keySet());

		try(BufferedReader br = new BufferedReader(new FileReader("/Users/kijin/Desktop/Charts-master/binding/ViewPortHandler.binding"))) {
			for(String line; (line = br.readLine()) != null; ) {
				// process the line.
				String[] data = line.split(";");
				String a = data[4];
				String r_type =null;
				String m_binding = null;
				//		    	System.out.println(line.split(";"));
				if(data[4].contains(":")){
					a = data[4].split(":")[1].replace("__", "").replaceAll(" ", "");

				}
				if(data[4].contains("->")){
					r_type=data[4].split("->")[1];
					m_binding = data[4].split("->")[0].replaceAll(" ", "");
				}
				//	System.out.println(data[0]+" "+Integer.parseInt(data[1].replaceAll(" ", ""))+" "+Integer.parseInt(data[2].replaceAll(" ", ""))+" "+data[3]+" "+a);

				//		    	" FILENAME TEXT," +
				//		    	" LINE INT," +
				//		    	" COLUMN INT, " +
				//		    	" EXAMPLE TEXT," +
				//		    	" TYPE TEXT," +
				//		    	" BINDING TEXT," +
				//		    	r_type
				String sql_binding ="";
				if(r_type!=null){
					sql_binding = SQLiteJDBC.insertTableBindingSwift
							(data[0].replaceAll(" ", ""), Integer.parseInt(data[2].replaceAll(" ", "")), Integer.parseInt(data[1].replaceAll(" ", "")), "", data[3].replaceAll("\\s+",""), m_binding.replaceAll("\\s+",""),"");
				}else{
					sql_binding = SQLiteJDBC.insertTableBindingSwift
							(data[0].replaceAll(" ", ""), Integer.parseInt(data[2].replaceAll(" ", "")), Integer.parseInt(data[1].replaceAll(" ", "")), "", data[3].replaceAll("\\s+",""), a.replaceAll("\\s+",""), "");

//	s				sql_binding = SQLiteJDBC.insertTableBindingSwift
//							(data[0].replaceAll(" ", ""), Integer.parseInt(data[2].replaceAll(" ", "")), Integer.parseInt(data[1].replaceAll(" ", "")), null, data[3].replaceAll("\\s+",""), m_binding.replaceAll("\\s+",""),null);
	
				}
				//
				try{
					stmt.executeUpdate(sql_binding);

				}catch (SQLException e){
					e.printStackTrace();
					System.out.println("SQLException	"+sql_binding);
				}
			}
			DBTablePrinter.printTable(connection, "binding_swift_stat");
			// line is not visible here.


			List<String> sqls = new LinkedList<String>();
			String sql2 = "select * from binding_swift_stat";
			ResultSet res21 = stmt.executeQuery(sql2);
			String sqll ="";
			while(res21.next() ){
				String line = res21.getString("line");
				String column = res21.getString("column");
				String example = res21.getString("BINDING");
				String type = res21.getString("TYPE");
				
				int id = Integer.parseInt(res21.getString("id"));
				if(numlinemap.get(new NumLine(Integer.parseInt(line), Integer.parseInt(column)))!=null){
					ParserRuleContext aaa = numlinemap.get(new NumLine(Integer.parseInt(line), Integer.parseInt(column)));					
					String bbb = aaa.getText();
//					if(StringUtils.countMatches(example,":")>1){
//						aaa = aaa+" "+example.replaceAll("\\)","\\\\)").replaceAll("\\(","\\\\(").replaceAll("(_)?:", ":?(.+?),?");					
//					}else{
//					    aaa = example.replaceAll("\\)","\\\\)").replaceAll("\\(","\\\\(").replaceAll("(_)?:", ":?(.+),").replace(",\\)", "\\)");
//					}
					if(bbb.startsWith(type) && bbb.length()>type.length()){    
						example = bbb.replaceFirst(type, example);
						
					}
					example = example.replaceAll("(in_C.+?)\\)", "\\)");
					sqll ="update binding_swift_stat set EXAMPLE=\'"+bbb+"\', BINDING=\'"+example+"\', CONSTS=\'"+type.replaceAll("\\)","\\\\)").replaceAll("\\(","\\\\(").replaceAll("(_)?:", ":?(.+),?").replace(",?:?", ",:?")+"\' where ID = "+id;
					sqls.add(sqll);
//					System.out.println(sqll);
				}
				
				//				

				//System.out.println(Integer.parseInt(line)+"  "+Integer.parseInt(column)+" "+numlinemap.get(new NumLine(Integer.parseInt(line), Integer.parseInt(column)-1)));
				//				numlinemap.get(new NumLine(Integer.parseInt(line), Integer.parseInt(column)));
				/*
				ResultSet res11 = getResult("ViewPortHandler.swift", Integer.parseInt(line), Integer.parseInt(column));
				//if binding case
				//			while(res11.next() ){
				if(res11.next() ){
					//					String varss = res11.getString("type");
					//				String[] vvv = varss.split(",");
					//				String conss = res11.getString("CONSTANTS");
					//				String[] ccc = conss.split(",");
					//				String temp = res11.getString("TEMPLATE");
					System.err.println(res11.getString("example")+" "+res11.getString("type")+"  "+ res11.getString("line")+"  "+ res11.getString("column"));
				}
				 */

			}
			for(String str: sqls){
				stmt.executeUpdate(str);
				System.out.println(str);
			}

			//			System.err.println(sqll);
			DBTablePrinter.printTable(connection, "binding_swift_stat");

			/*
			String t1 = "limitTransAndScale(matrix:content:)";
						t1="isInBoundsTop(_:)";
//						t1 = "max(_:_:)";
//			System.out.println(t1.replaceAll("\\)","\\\\)").replaceAll("\\(","\\\\(").replaceAll("(_)?:", ":?(.+),?").replace(",?\\)", "\\)"));
			System.out.println(t1.replaceAll("\\)","\\\\)").replaceAll("\\(","\\\\(").replaceAll("(_)?:", ":?(.+),").replace(",\\)", "\\)"));
			*/
		}
	}
	//        _contentRect.size.height = _chartHeight - offsetBottom - offsetTop


}
