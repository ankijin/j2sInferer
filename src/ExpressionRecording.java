import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;

import gumtree_tester.SwiftParser;

public class ExpressionRecording extends BaseSwiftRecorder{

	public String aaa = null, sss=null;
	public Expression and=null;
	Map<String, String> mm = new HashMap<String, String>();
	
	public ExpressionRecording(Expression android){
		and = android;
	}

	


	//				@Override
	//				public Object visitExpression(SwiftParser.ExpressionContext ctx) {
	// TODO Auto-generated method stub
	//					expr_store_s.add(ctx.getText());
	//					return super.visitExpression(ctx);
	//				}
	
	@Override
	public Object visitFunction_call_expression(SwiftParser.Function_call_expressionContext ctx) {
		// TODO Auto-generated method stub
		if(and instanceof MethodInvocation){
			MethodInvocation method = (MethodInvocation) and;
		if(method.getExpression()!=null)
//			mm.put(method.getExpression().toString(), method.getExpression().resolveTypeBinding().getName());
			
			aaa = method.toString();
			sss = ctx.getText();
		}
		return super.visitFunction_call_expression(ctx);
	}

	@Override
	public Object visitBinary_expression(SwiftParser.Binary_expressionContext ctx) {
		// TODO Auto-generated method stub
		if(ctx.binary_operator()!=null && ctx.binary_operator().getText().equals("=") && and instanceof Assignment){
			Assignment aa = (Assignment) and;
			aaa = aa.getRightHandSide().toString();
			sss = ctx.prefix_expression().getText();
			aa.getRightHandSide().accept(new ASTVisitor() {
				@Override
				public boolean visit(SimpleName node) {
					// TODO Auto-generated method stub
//					mm.put(node.toString(),node.resolveTypeBinding().getName());
					return super.visit(node);
				}
			});
		}
		return super.visitBinary_expression(ctx);
	}
	
}
