package recording;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class RecordVarDeclAndroid extends ASTVisitor{


	public LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	public LinkedList<String> recordStr = new LinkedList<String> ();

	public boolean visit(org.eclipse.jdt.core.dom.Modifier node) {
		//		System.out.println("Modifier\n"+node.getNodeType()+node.getKeyword()+node.getKeyword().toString().equals("final"));
		if(node.getKeyword().toString().equals("final")){
			return false;
		}
		MappingElement e = new MappingElement(node.getKeyword().toString(), node.getNodeType());
		record.add(e);
		recordStr.add(node.getKeyword().toString());
		//			
		//	System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		return false;	
	};

	public boolean visit(org.eclipse.jdt.core.dom.PrimitiveType node) {
		MappingElement e = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getNodeType());
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		record.add(e);
		recordStr.add(node.getPrimitiveTypeCode().toString());
		
		return false;
	}
	/**
	 * copy variable name
	 */
	public boolean visit(SimpleName node) {

		MappingElement e = new MappingElement(node.getIdentifier().toString(), node.getNodeType());
		//		System.out.println("SimpleName"+node.getNodeType()+node);

		record.add(e);
		recordStr.add(node.getIdentifier().toString());
		return true;
	}

	@Override
	public boolean visit(ArrayCreation node) {
		// TODO Auto-generated method stub
		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		//		System.out.println("QualifiedName"+node.toString()+node.getNodeType());
		record.add(e);
		recordStr.add(node.toString());
		return false;
	}

	@Override
	public boolean visit(QualifiedName node) {
		// TODO Auto-generated method stub

		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		//		System.out.println("QualifiedName"+node.toString()+node.getNodeType());
		record.add(e);
		recordStr.add(node.toString());
		return false;
	}

	@Override
	public boolean visit(SimpleType node) {
		// TODO Auto-generated method stub
		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		record.add(e);
		recordStr.add(node.toString());
		return false;
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {
		// TODO Auto-generated method stub

		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		record.add(e);
		recordStr.add(node.toString());
		return false;
	}

	public boolean visit(org.eclipse.jdt.core.dom.NumberLiteral node) {
		MappingElement e = new MappingElement(node.getToken().toString(), node.getNodeType());
		record.add(e);
		recordStr.add(node.getToken().toString());
		return true;
	}

}
