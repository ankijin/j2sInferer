package recording;
// Generated from Swift.g4 by ANTLR 4.5.3
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.eclipse.jdt.core.dom.ASTNode;

import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.TreeContext;

import recording.MappingElement;


/**
 * This class provides an empty implementation of {@link SwiftVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class RecordVarDeclSwift<T> extends AbstractParseTreeVisitor<T> implements SwiftVisitor<T> {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
//	SwiftParser parser;
//	private Map<ParserRuleContext, ITree> trees = new HashMap();
//	protected Map<ITree, ParserRuleContext> mappings = new HashMap();
//	private TreeContext context = new TreeContext();
	public Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();

	LinkedList<MappingElement> record = new LinkedList<MappingElement> ();

	
	@Override
	public T visitTerminal(TerminalNode node) {
		//		if(node.getParent() instanceof SwiftParser.Return_statementContext)
//		System.out.println("TerminalNode"+node.getText());
		return super.visitTerminal(node);
	}
	@Override
	public T visit(ParseTree tree) {
		// TODO Auto-generated method stub
		//	System.out.println("visit"+tree.toStringTree());

		return super.visit(tree);
	}

/*
	private ITree buildTree(ParserRuleContext ctx)  {
		ITree t = context.createTree(ctx.getRuleIndex(), ITree.NO_LABEL, "asdfasf");
		//   t.setPos(ctx.getStart().getStartIndex());
		ParserRuleContext aa;

		ParserRuleContext bb = new ParserRuleContext();



		//   t.setLength(ctx.getStop().getStopIndex()-ctx.getStart().getStartIndex());

		// System.out.println("typeLabel"+parser.ruleNames[ctx.getRuleIndex()]);
		trees.put(ctx, t);
		mappings.put(t, ctx);

		return t;
	}
*/

	@Override public T visitTop_level(SwiftParser.Top_levelContext ctx) { 

//		context = new TreeContext();
//		trees = new HashMap<>();
//		mappings  = new HashMap<>();
		//	System.err.println("visitTop_level"+ctx.getRuleIndex()+	parser.ruleNames[ctx.getRuleIndex()]);
//		 
//		context.setRoot(t);
		//		  
		//	System.out.println("tree"+t.toTreeString());
		////		 String id= ctx.ID().getText();  // id is left-hand side of '='
		//		 int value = visit(ctx.);

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatement(SwiftParser.StatementContext ctx) { 
		//	System.err.println("visitStatement"+ctx.getRuleIndex()+	parser.ruleNames[ctx.getRuleIndex()]);
		 
		  
		//System.out.println(ctx.getText());
		//	System.err.println(ctx.getText());

		//		ctx.to

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatements(SwiftParser.StatementsContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLoop_statement(SwiftParser.Loop_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFor_statement(SwiftParser.For_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFor_init(SwiftParser.For_initContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFor_in_statement(SwiftParser.For_in_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWhile_statement(SwiftParser.While_statementContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCondition_clause(SwiftParser.Condition_clauseContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCondition_list(SwiftParser.Condition_listContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCondition(SwiftParser.ConditionContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCase_condition(SwiftParser.Case_conditionContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOptional_binding_condition(SwiftParser.Optional_binding_conditionContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOptional_binding_head(SwiftParser.Optional_binding_headContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOptional_binding_continuation_list(SwiftParser.Optional_binding_continuation_listContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOptional_binding_continuation(SwiftParser.Optional_binding_continuationContext ctx) { 
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRepeat_while_statement(SwiftParser.Repeat_while_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBranch_statement(SwiftParser.Branch_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIf_statement(SwiftParser.If_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitElse_clause(SwiftParser.Else_clauseContext ctx) { 

		 
		  

		 

		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGuard_statement(SwiftParser.Guard_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSwitch_statement(SwiftParser.Switch_statementContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSwitch_cases(SwiftParser.Switch_casesContext ctx) { 
		 
		  

		 
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override 
	public T visitSwitch_case(SwiftParser.Switch_caseContext ctx) { 
		 
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCase_label(SwiftParser.Case_labelContext ctx) { 
		 
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCase_item_list(SwiftParser.Case_item_listContext ctx) { 

		 
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDefault_label(SwiftParser.Default_labelContext ctx) { 

		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWhere_clause(SwiftParser.Where_clauseContext ctx) { 

		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWhere_expression(SwiftParser.Where_expressionContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 


		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLabeled_statement(SwiftParser.Labeled_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatement_label(SwiftParser.Statement_labelContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLabel_name(SwiftParser.Label_nameContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitControl_transfer_statement(SwiftParser.Control_transfer_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBreak_statement(SwiftParser.Break_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitContinue_statement(SwiftParser.Continue_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFallthrough_statement(SwiftParser.Fallthrough_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitReturn_statement(SwiftParser.Return_statementContext ctx) { 
		 
		 

		//		int count = ctx.getChildCount();
		//		for(int i=0;i<count;i++){
		//			System.err.println("visitReturn_statement"+ctx.getChild(i).getText());
		//			System.err.println(ctx.getChild(i).getClass().getName());
		//		}
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAvailability_condition(SwiftParser.Availability_conditionContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAvailability_arguments(SwiftParser.Availability_argumentsContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAvailability_argument(SwiftParser.Availability_argumentContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitThrow_statement(SwiftParser.Throw_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDefer_statement(SwiftParser.Defer_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDo_statement(SwiftParser.Do_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCatch_clauses(SwiftParser.Catch_clausesContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCatch_clause(SwiftParser.Catch_clauseContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCompiler_control_statement(SwiftParser.Compiler_control_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBuild_configuration_statement(SwiftParser.Build_configuration_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBuild_configuration_elseif_clauses(SwiftParser.Build_configuration_elseif_clausesContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBuild_configuration_elseif_clause(SwiftParser.Build_configuration_elseif_clauseContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBuild_configuration_else_clause(SwiftParser.Build_configuration_else_clauseContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBuild_configuration(SwiftParser.Build_configurationContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPlatform_testing_function(SwiftParser.Platform_testing_functionContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOperating_system(SwiftParser.Operating_systemContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArchitecture(SwiftParser.ArchitectureContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLine_control_statement(SwiftParser.Line_control_statementContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLine_number(SwiftParser.Line_numberContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFile_name(SwiftParser.File_nameContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGeneric_parameter_clause(SwiftParser.Generic_parameter_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGeneric_parameter_list(SwiftParser.Generic_parameter_listContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGeneric_parameter(SwiftParser.Generic_parameterContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRequirement_clause(SwiftParser.Requirement_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRequirement_list(SwiftParser.Requirement_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRequirement(SwiftParser.RequirementContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConformance_requirement(SwiftParser.Conformance_requirementContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSame_type_requirement(SwiftParser.Same_type_requirementContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGeneric_argument_clause(SwiftParser.Generic_argument_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGeneric_argument_list(SwiftParser.Generic_argument_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGeneric_argument(SwiftParser.Generic_argumentContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDeclaration(SwiftParser.DeclarationContext ctx) { 
		//	System.err.println("DeclarationContext"+ctx.getRuleIndex()+parser.ruleNames[ctx.getRuleIndex()]);
		 
		  

		 
		//	System.out.println(p.toShortString());
		//		System.out.println(p.toTreeString());

		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDeclarations(SwiftParser.DeclarationsContext ctx) { 

		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTop_level_declaration(SwiftParser.Top_level_declarationContext ctx) { 

		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCode_block(SwiftParser.Code_blockContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitImport_declaration(SwiftParser.Import_declarationContext ctx) { 

		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitImport_kind(SwiftParser.Import_kindContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitImport_path(SwiftParser.Import_pathContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitImport_path_identifier(SwiftParser.Import_path_identifierContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstant_declaration(SwiftParser.Constant_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPattern_initializer_list(SwiftParser.Pattern_initializer_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPattern_initializer(SwiftParser.Pattern_initializerContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInitializer(SwiftParser.InitializerContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */

	@Override public T visitVariable_declaration_head(SwiftParser.Variable_declaration_headContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.err.println("visitVariable_declaration_head\n"+ctx.getChild(1));
//		record.add(new MappingElement(ctx.getChild(1).getText(), 0));
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariable_name(SwiftParser.Variable_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGetter_setter_block(SwiftParser.Getter_setter_blockContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGetter_clause(SwiftParser.Getter_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSetter_clause(SwiftParser.Setter_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSetter_name(SwiftParser.Setter_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGetter_setter_keyword_block(SwiftParser.Getter_setter_keyword_blockContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitGetter_keyword_clause(SwiftParser.Getter_keyword_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSetter_keyword_clause(SwiftParser.Setter_keyword_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWillSet_didSet_block(SwiftParser.WillSet_didSet_blockContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWillSet_clause(SwiftParser.WillSet_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDidSet_clause(SwiftParser.DidSet_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypealias_declaration(SwiftParser.Typealias_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypealias_head(SwiftParser.Typealias_headContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypealias_name(SwiftParser.Typealias_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypealias_assignment(SwiftParser.Typealias_assignmentContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_head(SwiftParser.Function_headContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_signature(SwiftParser.Function_signatureContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_result(SwiftParser.Function_resultContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_body(SwiftParser.Function_bodyContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitParameter_clauses(SwiftParser.Parameter_clausesContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitParameter_clause(SwiftParser.Parameter_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitParameter_list(SwiftParser.Parameter_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitParameter(SwiftParser.ParameterContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExternal_parameter_name(SwiftParser.External_parameter_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLocal_parameter_name(SwiftParser.Local_parameter_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDefault_argument_clause(SwiftParser.Default_argument_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnum_declaration(SwiftParser.Enum_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnion_style_enum(SwiftParser.Union_style_enumContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnion_style_enum_members(SwiftParser.Union_style_enum_membersContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnion_style_enum_member(SwiftParser.Union_style_enum_memberContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnion_style_enum_case_clause(SwiftParser.Union_style_enum_case_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnion_style_enum_case_list(SwiftParser.Union_style_enum_case_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnion_style_enum_case(SwiftParser.Union_style_enum_caseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnum_name(SwiftParser.Enum_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnum_case_name(SwiftParser.Enum_case_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_style_enum(SwiftParser.Raw_value_style_enumContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_style_enum_members(SwiftParser.Raw_value_style_enum_membersContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_style_enum_member(SwiftParser.Raw_value_style_enum_memberContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_style_enum_case_clause(SwiftParser.Raw_value_style_enum_case_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_style_enum_case_list(SwiftParser.Raw_value_style_enum_case_listContext ctx) {
		 
		//	t.setLabel(ctx.Identifier().getText());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_style_enum_case(SwiftParser.Raw_value_style_enum_caseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_assignment(SwiftParser.Raw_value_assignmentContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRaw_value_literal(SwiftParser.Raw_value_literalContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStruct_declaration(SwiftParser.Struct_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStruct_name(SwiftParser.Struct_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStruct_body(SwiftParser.Struct_bodyContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClass_declaration(SwiftParser.Class_declarationContext ctx) { 

		//System.err.println("visitClass_declaration"+ctx.getRuleIndex()+parser.ruleNames[ctx.getRuleIndex()]);
		//.println(ctx);
		 
		  

		 
		//System.out.println("visitClass_declaration"+p.toShortString()+p.getChildren());

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClass_name(SwiftParser.Class_nameContext ctx) { 
		//	System.err.println("visitClass_name"+ctx.getRuleIndex()+parser.ruleNames[ctx.getRuleIndex()]);

		 
		  

		 
		//	  
		//	System.out.println("QQQQQQQQ"+p.toTreeString());
		//		 


		return visitChildren(ctx); 
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClass_body(SwiftParser.Class_bodyContext ctx) {
		//System.err.println("visitClass_body"+ctx.getRuleIndex()+parser.ruleNames[ctx.getRuleIndex()]);
		 
		  

		 

		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_declaration(SwiftParser.Protocol_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_name(SwiftParser.Protocol_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_body(SwiftParser.Protocol_bodyContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_member_declaration(SwiftParser.Protocol_member_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_member_declarations(SwiftParser.Protocol_member_declarationsContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_property_declaration(SwiftParser.Protocol_property_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_method_declaration(SwiftParser.Protocol_method_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_initializer_declaration(SwiftParser.Protocol_initializer_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_subscript_declaration(SwiftParser.Protocol_subscript_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_associated_type_declaration(SwiftParser.Protocol_associated_type_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInitializer_declaration(SwiftParser.Initializer_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInitializer_head(SwiftParser.Initializer_headContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInitializer_body(SwiftParser.Initializer_bodyContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDeinitializer_declaration(SwiftParser.Deinitializer_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExtension_declaration(SwiftParser.Extension_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExtension_body(SwiftParser.Extension_bodyContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSubscript_declaration(SwiftParser.Subscript_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSubscript_head(SwiftParser.Subscript_headContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSubscript_result(SwiftParser.Subscript_resultContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOperator_declaration(SwiftParser.Operator_declarationContext ctx) {
		 
	 
		//		System.err.println(ctx.getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrefix_operator_declaration(SwiftParser.Prefix_operator_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.err.println("visitPrefix_operator_declaration"+ctx.getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostfix_operator_declaration(SwiftParser.Postfix_operator_declarationContext ctx) {
		 
		//	 
		//		System.err.println("visitPOST"+t.getLabel());
		//		ctx.get
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInfix_operator_declaration(SwiftParser.Infix_operator_declarationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInfix_operator_attributes(SwiftParser.Infix_operator_attributesContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrecedence_clause(SwiftParser.Precedence_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrecedence_level(SwiftParser.Precedence_levelContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssociativity_clause(SwiftParser.Associativity_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssociativity(SwiftParser.AssociativityContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDeclaration_modifier(SwiftParser.Declaration_modifierContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.out.println("visitDeclaration_modifier\n"+ctx.getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDeclaration_modifiers(SwiftParser.Declaration_modifiersContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		record.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		//ctx.getParent():variable_declration_head 
		//var
		record.add(new MappingElement(ctx.getParent().getChild(1).getText(), 0));
	
		 return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAccess_level_modifier(SwiftParser.Access_level_modifierContext ctx) {
		 
	 
		//System.out.println("visitAccess_level_modifier"+ctx.getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPattern(SwiftParser.PatternContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.out.println("PatternContext\n"+ctx.getText());
		record.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWildcard_pattern(SwiftParser.Wildcard_patternContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIdentifier_pattern(SwiftParser.Identifier_patternContext ctx) {

		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		 


		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitValue_binding_pattern(SwiftParser.Value_binding_patternContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTuple_pattern(SwiftParser.Tuple_patternContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTuple_pattern_element_list(SwiftParser.Tuple_pattern_element_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTuple_pattern_element(SwiftParser.Tuple_pattern_elementContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnum_case_pattern(SwiftParser.Enum_case_patternContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOptional_pattern(SwiftParser.Optional_patternContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpression_pattern(SwiftParser.Expression_patternContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAttribute(SwiftParser.AttributeContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAttribute_name(SwiftParser.Attribute_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAttribute_argument_clause(SwiftParser.Attribute_argument_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAttributes(SwiftParser.AttributesContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBalanced_tokens(SwiftParser.Balanced_tokensContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBalanced_token(SwiftParser.Balanced_tokenContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpression(SwiftParser.ExpressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.err.println("visitExpression"+ctx.getText());
		  

		  
		//		return null; }
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpression_list(SwiftParser.Expression_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrefix_expression(SwiftParser.Prefix_expressionContext ctx) {
		 
		//		System.err.println("visitPrefix "+ctx.getText());
	 
		//		for(int i=0;i<ctx.getChildCount();i++){
		//			System.err.println("child: "+ctx.getChild(i).getText()+"type: "+ctx.getChild(i).getClass().getName());
		//		}
		//	 
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIn_out_expression(SwiftParser.In_out_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());

		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTry_operator(SwiftParser.Try_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBinary_expression(SwiftParser.Binary_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBinary_expressions(SwiftParser.Binary_expressionsContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConditional_operator(SwiftParser.Conditional_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType_casting_operator(SwiftParser.Type_casting_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimary_expression(SwiftParser.Primary_expressionContext ctx) {
		 
		//	 
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitImplicit_member_expression(SwiftParser.Implicit_member_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLiteral_expression(SwiftParser.Literal_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArray_literal(SwiftParser.Array_literalContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArray_literal_items(SwiftParser.Array_literal_itemsContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArray_literal_item(SwiftParser.Array_literal_itemContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDictionary_literal(SwiftParser.Dictionary_literalContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDictionary_literal_items(SwiftParser.Dictionary_literal_itemsContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDictionary_literal_item(SwiftParser.Dictionary_literal_itemContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSelf_expression(SwiftParser.Self_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSuperclass_expression(SwiftParser.Superclass_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSuperclass_method_expression(SwiftParser.Superclass_method_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSuperclass_subscript_expression(SwiftParser.Superclass_subscript_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSuperclass_initializer_expression(SwiftParser.Superclass_initializer_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClosure_expression(SwiftParser.Closure_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClosure_signature(SwiftParser.Closure_signatureContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCapture_list(SwiftParser.Capture_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCapture_list_items(SwiftParser.Capture_list_itemsContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCapture_list_item(SwiftParser.Capture_list_itemContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCapture_specifier(SwiftParser.Capture_specifierContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitParenthesized_expression(SwiftParser.Parenthesized_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpression_element_list(SwiftParser.Expression_element_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpression_element(SwiftParser.Expression_elementContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWildcard_expression(SwiftParser.Wildcard_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSelector_expression(SwiftParser.Selector_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_call_with_closure_expression(SwiftParser.Function_call_with_closure_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInitializer_expression_with_args(SwiftParser.Initializer_expression_with_argsContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFunction_call_expression(SwiftParser.Function_call_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSubscript_expression(SwiftParser.Subscript_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExplicit_member_expression1(SwiftParser.Explicit_member_expression1Context ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());


		//		System.err.println("visitExplicit_member_expression1 "+ctx.getText());
		//		for(int i=0;i<ctx.getChildCount();i++){
		//			System.err.println("child: "+ctx.getChild(i).getText());
		//		}


		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExplicit_member_expression2(SwiftParser.Explicit_member_expression2Context ctx) {
		 
		//		t.setLabel(ctx.identifier().getText());
		//		System.err.println("visitExplicit_member_expression2 "+ctx.identifier().getText()+", "+ctx.getClass().getTypeName());

		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInitializer_expression(SwiftParser.Initializer_expressionContext ctx) { 
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExplicit_member_expression3(SwiftParser.Explicit_member_expression3Context ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDynamic_type_expression(SwiftParser.Dynamic_type_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostfix_operation(SwiftParser.Postfix_operationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.err.println("visitPostfix_operation"+ctx.getText()+", getchild 0 "+ctx.getChild(0).getText());
		//		System.err.println("visitPostfix_operation "+ctx.getText());
	 
		//		for(int i=0;i<ctx.getChildCount();i++){
		//			System.err.println("child: "+ctx.getChild(i).getText());
		//		}
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimary(SwiftParser.PrimaryContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.err.println("visitPrimary"+ctx.getText()+", getchild 0 "+ctx.getChild(0).getText());

		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostfix_self_expression(SwiftParser.Postfix_self_expressionContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArgument_names(SwiftParser.Argument_namesContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArgument_name(SwiftParser.Argument_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTrailing_closure(SwiftParser.Trailing_closureContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType(SwiftParser.TypeContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType_annotation(SwiftParser.Type_annotationContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType_identifier(SwiftParser.Type_identifierContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType_name(SwiftParser.Type_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTuple_type(SwiftParser.Tuple_typeContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTuple_type_body(SwiftParser.Tuple_type_bodyContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTuple_type_element_list(SwiftParser.Tuple_type_element_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTuple_type_element(SwiftParser.Tuple_type_elementContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitElement_name(SwiftParser.Element_nameContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_composition_type(SwiftParser.Protocol_composition_typeContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_identifier_list(SwiftParser.Protocol_identifier_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProtocol_identifier(SwiftParser.Protocol_identifierContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType_inheritance_clause(SwiftParser.Type_inheritance_clauseContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType_inheritance_list(SwiftParser.Type_inheritance_listContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClass_requirement(SwiftParser.Class_requirementContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());

		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIdentifier(SwiftParser.IdentifierContext ctx) { 
		
//		visitChildre
		//System.err.println("visitIdentifier"+ctx.getRuleIndex()+parser.ruleNames[ctx.getRuleIndex()]);

		//	System.out.println(ctx.Identifier() instanceof TerminalNode);
		//		System.out.println(ctx.Identifier());
		nodeSwift.put(ctx.Identifier().toString(), ctx.getParent().getParent().getParent().getParent().getParent());
		//		System.out.println(ctx.getParent().getParent().getParent().getParent().getParent().getText());
		 
		//System.out.println("tree	"+t.toTreeString());
		  

		 

		//System.out.println("QQQQQQQQ"+p.toTreeString());


		return visitChildren(ctx); }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIdentifier_list(SwiftParser.Identifier_listContext ctx) { 

		 
		//t.setLabel(ctx.identifier());
		  

		 
		return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitContext_sensitive_keyword(SwiftParser.Context_sensitive_keywordContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssignment_operator(SwiftParser.Assignment_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.out.println("Assignment_operator\n"+ctx.getParent().getChild(1).getText());
		record.add(new MappingElement(ctx.getParent().getChild(1).getText(), 0));
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitNegate_prefix_operator(SwiftParser.Negate_prefix_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBuild_AND(SwiftParser.Build_ANDContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBuild_OR(SwiftParser.Build_ORContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArrow_operator(SwiftParser.Arrow_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRange_operator(SwiftParser.Range_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSame_type_equals(SwiftParser.Same_type_equalsContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBinary_operator(SwiftParser.Binary_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrefix_operator(SwiftParser.Prefix_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		//		System.err.println("Prefix_operatorContext"+ctx.getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostfix_operator(SwiftParser.Postfix_operatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOperator(SwiftParser.OperatorContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOperator_character(SwiftParser.Operator_characterContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitOperator_head(SwiftParser.Operator_headContext ctx) {
		 
	 
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDot_operator_head(SwiftParser.Dot_operator_headContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDot_operator_character(SwiftParser.Dot_operator_characterContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLiteral(SwiftParser.LiteralContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitNumeric_literal(SwiftParser.Numeric_literalContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBoolean_literal(SwiftParser.Boolean_literalContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitNil_literal(SwiftParser.Nil_literalContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInteger_literal(SwiftParser.Integer_literalContext ctx) { 
		 
		ctx.Binary_literal();
		//		t.setLabel(ctx.Identifier().getText());
	 
		//		System.out.println("visitInteger_literal"+ctx.getText());
		  

		  return visitChildren(ctx); }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitString_literal(SwiftParser.String_literalContext ctx) {
		 
		//		t.setLabel(ctx.Identifier().getText());
		  

		  return visitChildren(ctx); }
}