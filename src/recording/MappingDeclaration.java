package recording;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringJoiner;

import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import mapping.MappingElementExpr;
import mapping.MappingMethods;
import sqldb.SQLiteJDBC;

public class MappingDeclaration<T>{
	FieldDeclaration android;
	SwiftParser.Variable_declarationContext swift;
	public static Map<MappingElement, MappingElement> mappingElements1 = new HashMap<MappingElement, MappingElement>();
	public static Map<String, ASTNode> varDeclAndroid = new HashMap<String, ASTNode>();
	public static Map<String, ParserRuleContext> varDeclSwift = new HashMap<String, ParserRuleContext>();

	static int sid;

	public String getType(FieldDeclaration f, String name){

		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		f.accept(recordAndroid);
		//		System.out.println("FieldDeclaration part\n"+recordAndroid.record);
		//		System.out.println(recordAndroid.record.get(1));
		//		System.out.println(recordAndroid.record.get(2));
		//		System.out.println(name);
		if(name.equals(recordAndroid.record.get(2).label)){

			//			System.out.println("MATCHED"+name);
			return recordAndroid.record.get(1).label;
		}
		return null;
	}


	public String getTypeSwift(SwiftParser.Variable_declarationContext swift, String name){

		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		//		System.out.println("Variable_declarationContext part\n"+recordSwift.record);
		//		System.out.println(recordSwift.record.get(1));
		//		System.out.println(recordSwift.record.get(2));
		//		System.out.println(name);
		if(name.equals(((MappingElement)recordSwift.record.get(2)).label.toString())){
			//			System.out.println("MATCHED"+name);
			return ((MappingElement)recordSwift.record.get(3)).label;
		}
		return null;
	}





	public MappingDeclaration(FieldDeclaration a, SwiftParser.Variable_declarationContext s){
		android = a;
		swift 	= s;
	}
	Map<Integer, Integer> map_types	= new HashMap<Integer, Integer>();
	public MappingDeclaration(){
		map_types.put(25, 18);
		map_types.put(21, 76);
		map_types.put(41, 36);
	}

	String params_android="";
	String stms_android ="";

	public void setElementMethodDecl(ASTNode a, ParserRuleContext s) throws SQLException{
		Map<String, String> recordAndroidParams	= new HashMap<String, String>();
		Map<String, String> recordSwiftParams	= new HashMap<String, String>();
		LinkedList<MappingElement> recordStatement = new LinkedList<MappingElement> ();

		LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
		a.accept(new ASTVisitor() {


			//record modifier
			@Override
			public boolean visit(Modifier node) {
				MappingElement e = new MappingElement(node.getKeyword().toString(), node.getNodeType());
				record.add(e);
				return super.visit(node);
			}
			//record type
			public boolean visit(org.eclipse.jdt.core.dom.PrimitiveType node) {
				if(node.getParent().getNodeType()==31){//context: function decl
					//					System.out.println("org.eclipse.jdt.core.dom.PrimitiveType");
					MappingElement e = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getParent().getNodeType());
					record.add(e);
				}
				//				MappingElement e = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getNodeType());
				//				record.add(e);
				return false;
			}

			//record methodName
			public boolean visit(SimpleName node) {
				if(node.getParent().getNodeType()==31){
					//					System.out.println("visitSimpleName	"+node.getParent().getNodeType());
					MappingElement e = new MappingElement(node.getIdentifier().toString(), node.getNodeType());
					record.add(e);

				}
				//				MappingElement e = new MappingElement(node.getIdentifier().toString(), node.getNodeType());
				//				record.add(e);
				return true;
			}
			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
				if(!node.parameters().isEmpty())
					params_android = node.parameters().toString().replace("\n", " ");
				stms_android = node.getBody().toString().replace("\n", "").replace("{", "").replace("}", "");
				return super.visit(node);
			}
			//record return statement
			public boolean visit(org.eclipse.jdt.core.dom.ReturnStatement node) {
				//				Expression sss = node.getExpression();
				//				node.getParent().getNodeType();
				MappingElement e = new MappingElement(node.toString(), node.getNodeType());
				recordStatement.add(e);

				//				e = new MappingElement(node.getExpression().toString(), node.getNodeType());
				e = new MappingElement(node.toString(), node.getNodeType());
				record.add(e);
				//System.out.println("ReturnStatement\n"+node.getExpression());
				return true;
			};

			@Override
			public boolean visit(ExpressionStatement node) {
				// TODO Auto-generated method stub
				MappingElement e = new MappingElement(node.toString(), node.getNodeType());
				recordStatement.add(e);
				return super.visit(node);
			}

			@Override
			public boolean visit(SingleVariableDeclaration node) {
				//				System.out.println("SingleVariableDeclaration"+node);
				recordAndroidParams.put(node.getName().getIdentifier(), node.getType().toString());
				return super.visit(node);
			}


			@Override
			public boolean visit(IfStatement node) {
				// TODO Auto-generated method stub
				//				System.out.println("IfStatement\n"+node);
				MappingElement e = new MappingElement(node.toString(), node.getNodeType());
				recordStatement.add(e);
				return super.visit(node);
			}


		});//record android method

		Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		Statement stmt = c.createStatement();

		//		System.out.println("recordAndroidParams\n"+recordAndroidParams);

		Iterator<String> pa_iter = recordAndroidParams.keySet().iterator();

		/*
		while(pa_iter.hasNext()){
			String key = pa_iter.next();
			String value = recordAndroidParams.get(key);

			String sql = "INSERT OR IGNORE INTO "+"var_android (ID,VARNAME,TYPE) " +
					"VALUES ("+
					id + ","+
					"\'"+key+"\',"+ 
					"\'"+value+"\'"+ 
					");";
			stmt.executeUpdate(sql);
			id++;
		}

		 */



		RecordMethodSwift rrr = new RecordMethodSwift<>();
		s.accept(rrr);


		Iterator<String> ps_iter = rrr.recordSwiftParams.keySet().iterator();

		System.out.println("RECORDED Android\n"+record+"\n"+recordStatement);
		System.out.println("RECORDED Swift\n"+rrr.record+"\n"+	rrr.recordStatement);


		//		System.out.println("########"+Main.mapping(recordAndroidParams, rrr.recordSwiftParams));

		Map<String, String> mm_va = MappingMethods.mapping(recordAndroidParams, rrr.recordSwiftParams);

		Iterator<String> dddd = mm_va.keySet().iterator();


		int aaaaa = rrr.recordStatement.size();
		int bbbbb = recordStatement.size();


		while(dddd.hasNext()){
			String ak= dddd.next();
			String sk =  mm_va.get(ak);


			String sqla = "INSERT OR IGNORE INTO "+"var_android (ID,VARNAME,TYPE) " +
					"VALUES ("+
					id + ","+
					"\'"+ak+"\',"+ 
					"\'"+recordAndroidParams.get(ak)+"\'"+ 
					");";

			String sqls = "INSERT OR IGNORE INTO "+"var_swift (ID,VARNAME, INIT, ANDROID) " +
					"VALUES ("+
					id + ","+
					"\'"+sk+"\',"+ 
					"\'"+rrr.recordSwiftParams.get(sk)+"\', "+ 
					id + " "+
					");";

			String tableName = "var_swift";
			String varname = ak;
			String modi ="";
			String type = "";
			String init = recordAndroidParams.get(ak);


			stmt.executeUpdate(sqla);
			stmt.executeUpdate(sqls);
			//				System.out.println(sqls);
			id++;


		}

		if (record.size()==4 && rrr.record.size()==5){	
			mappingElements1.put(record.get(0), (MappingElement)rrr.record.get(0));

			String a1 = record.get(0).label;
			String s1 =  ((MappingElement) rrr.record.get(0)).label;

			//			System.out.println("MappingElementExpr.getVarName");
			String exprA = MappingElementExpr.getVarName(record.get(3).label);
			String exprA1 = record.get(3).label;
			//			System.out.println(MappingElementExpr.getVarName(record.get(3).label));
			String exprS = MappingElementExpr.getVarName(((MappingElement) rrr.record.get(4)).label);
			String exprS1 = ((MappingElement) rrr.record.get(4)).label;
			//			System.err.println("ExprA"+exprA);
			//			System.err.println("ExprS"+exprS);
			mappingElements1.put(record.get(1), (MappingElement)rrr.record.get(3));
			String a2 = record.get(1).label;
			String s3 = ((MappingElement)rrr.record.get(3)).label;

			String a3 = record.get(2).label;
			String s2 = ((MappingElement)rrr.record.get(2)).label;

			//			System.out.println("TYPE CHECK A"+amapping.get(MappingElementExpr.getVarName(exprA)).getType());
			//			System.out.println("TYPE CHECK S"+smapping.get(MappingElementExpr.getVarName(exprS)).getInit());
			//			System.out.println("TYPE CHECK A"+MappingElementExpr.getVarName(exprA));
			//			System.out.println("TYPE CHECK S"+MappingElementExpr.getVarName(exprS));
			//			System.out.println(amapping);
			//			System.out.println(smapping);
			//PLDI, ICSE, ECOOP, FSE
			//			RECORDED Android
			//			[public, float, offsetTop, mContentRect.top]
			//			RECORDED Swift
			//			[public, func, offsetTop, CGFloat, _contentRect.origin.y]

			//			String exprAA= "RectF"+":"+MappingElementExpr.getMethod(record.get(3).label);
			//			String exprSS= "CGRect()"+":"+MappingElementExpr.getMethod(((MappingElement) rrr.record.get(4)).label);

			//			String exprAA= amapping.get(MappingElementExpr.getVarName(exprA)).getType()+":"+MappingElementExpr.getMethod(record.get(3).label);
			//			String exprSS= smapping.get(MappingElementExpr.getVarName(exprS)).getInit()+":"+MappingElementExpr.getMethod(((MappingElement) rrr.record.get(4)).label);

			//			String a4 = exprAA;
			//			String s4 = exprSS;
			String a4;
			String s4;
			a4 = exprA1;
			s4 = exprS1;
			//			mappingElements1.put(new MappingElement(exprAA), new MappingElement(exprSS));
			mappingElements1.put(new MappingElement(a4), new MappingElement(s4));

			//			System.out.println(MappingElementExpr.getVarName(((MappingElement) rrr.record.get(4)).label));

			//			mappingExpression(record.get(3).label, ((MappingElement) rrr.record.get(4)).label);

			//			System.out.println("replace"+a4+a4.replace('.', ':'));
			String a44= "test";
			/*			
			a44=a4.replace("mContentRect", "{Rect}");
			a44=a4.replace("mChartHeight", "{float}");
			a44=a4.replace("mChartWidth", "{float}");

			LinkedList<String> list = new LinkedList<String>();
			list.add("mContentRect");
			list.add("mChartHeight");
			list.add("mChartWidth");

			Iterator<String> it = list.iterator();

			//			while(it.hasNext()){
			//				if(a4.contains(it.next()) and a4.equals(mContentRect)){
			//					a4 = a4.replace("mContentRect", "{Rect}");
			//			`	}
			//			}

			if(a4.contains("mContentRect")){
				a4=a4.replaceAll("mContentRect", "{Rect}");
				if(a4.contains("mChartHeight")){
					a4=a4.replaceAll("mChartHeight", "{float}");
				}
			}
			if(a4.contains("mChartHeight")){
				a4=a4.replaceAll("mChartHeight", "{float}");
			}

			if(a4.contains("mChartWidth")){
				a4=a4.replaceAll("mChartWidth", "{float}");
			}

			if(s4.contains("_contentRect")){
				s4=s4.replaceAll("_contentRect", "{CGRect()}");
				if(s4.contains("mChartHeight")){
					s4=s4.replaceAll("mChartHeight", "{float}");
				}
			}
			if(s4.contains("_chartHeight")){
				s4=s4.replaceAll("_chartHeight", "{CGFloat()}");
			}

			if(s4.contains("_chartWidth")){
				s4=s4.replaceAll("_chartWidth", "{CGFloat()}");
			}
			 */

			String i1 = SQLiteJDBC.insertTableMethod("MethodDecl_Android", mid, a3,a1,a2,a4);
			String i11 = SQLiteJDBC.insertTableMethod("method_android", mid, a3,a1,a2,exprA1);
			String i12 = SQLiteJDBC.insertTableMethodSwift("method_swift", mid, s2,s1,s3,exprS1,mid);
			String i2 = SQLiteJDBC.insertTableMethodSwift("MethodDecl_Swift", mid, s2,s1,s3,s4,mid);
			String i113 = SQLiteJDBC.insertTableMMethod("mmethod_android", mid, a3,a1,a2,params_android, stms_android);
			String i123 = SQLiteJDBC.insertTableMMethodSwift("mmethod_swift", mid, s2,s1,s3,rrr.params,rrr.stmts,mid);

			//				System.out.println("i123"+i123);


			String se1 = "SELECT DISTINCT var_android.VARNAME, var_android.TYPE "+
					" FROM var_android";

			ResultSet rrrr = stmt.executeQuery(se1);
			int var_num=0;
			String var_seq ="";
			StringJoiner joiner = new StringJoiner(",");
			StringJoiner joiner2 = new StringJoiner(",");
			String exprAA = exprA1;
			while(rrrr.next()){
				var_seq ="avar_"+var_num;
				String varname=rrrr.getString("VARNAME");
				String typename=rrrr.getString("TYPE");
				if(exprAA.contains(varname)){
					SQLiteJDBC.matchingVarSequence(joiner, exprAA, varname);
					exprAA = exprAA.replace(varname, "{"+typename+"}");
					joiner2.add(var_seq);
					var_num++;
				}
			}
			String e11 = SQLiteJDBC.insertTableExprAndroid2(mid, exprAA, joiner.toString(), joiner2.toString());
			stmt.executeUpdate(e11);




			se1 = "SELECT DISTINCT var_swift.VARNAME, var_swift.INIT "+
					" FROM var_swift";


			rrrr = stmt.executeQuery(se1);
			var_num=0;
			joiner = new StringJoiner(",");
			joiner2 = new StringJoiner(",");
			String exprSS = exprS1;
			while(rrrr.next()){
				var_seq ="svar_"+var_num;
				String varname=rrrr.getString("VARNAME");
				String typename=rrrr.getString("INIT");
				//				System.err.println("SSQL"+varname);
				//				System.err.println("SSQL"+typename);
				if(exprSS.contains(varname)){
					int k= SQLiteJDBC.matchingVarSequence(joiner, exprSS, varname);
					exprSS = exprSS.replace(varname, "{"+typename+"}");
					while(k>0){
						joiner2.add(var_seq);
						k--;
					}
					var_num++;
				}
				//				if(e2.contains(varname)){
				//					joiner = matchingVarSequence(joiner, e1, varname);
				//					e2 = e2.replace(varname, "{"+typename+"}");
				//				}

			}
			//			System.err.println(exprSS);
			//			System.out.println("Joiner	"+joiner.toString());
			e11 = SQLiteJDBC.insertTableExprSwift2(mid, exprSS, joiner.toString(),joiner2.toString(),mid);
			stmt.executeUpdate(e11);




			se1 = "SELECT DISTINCT expr_android.SEQ_VARS, expr_swift.S_SEQ_VARS, expr_android.SEQ_VARS_ABS, expr_swift.S_SEQ_VARS_ABS"+
					" FROM expr_android, expr_swift "
					+ "where expr_swift.ANDROID=expr_android.ID";

			rrrr = stmt.executeQuery(se1);
			String[] list_aa = null, list_aaaa=null, list_ss=null, list_ssss=null;
			//			ArrayList<String> aaa = new ArrayList<String>(Arrays.asList(list_aa));


			while(rrrr.next()){
				String aa = rrrr.getString("SEQ_VARS");
				String ss = rrrr.getString("S_SEQ_VARS");
				String aaaa = rrrr.getString("SEQ_VARS_ABS");
				String ssss = rrrr.getString("S_SEQ_VARS_ABS");
				list_aa = aa.split(",");
				list_ss = ss.split(",");
				list_aaaa = aaaa.split(",");
				list_ssss = ssss.split(",");

				//				System.err.println("SQL"+aa +aaaa);
				//				System.err.println("SQL"+ss +ssss);

			}


			ArrayList<String> aalist = new ArrayList<String>(Arrays.asList(list_aa));
			ArrayList<String> sslist = new ArrayList<String>(Arrays.asList(list_ss));
			ArrayList<String> aaaalist = new ArrayList<String>(Arrays.asList(list_aaaa));
			ArrayList<String> sssslist = new ArrayList<String>(Arrays.asList(list_ssss));

			//			System.out.println(aalist);
			//			System.out.println(sslist);

			//			System.out.println(aaaalist);
			//			System.out.println(sssslist);


			se1 = "SELECT DISTINCT var_swift.VARNAME, var_android.VARNAME "+
					" FROM var_android, var_swift "
					+ "where var_swift.ANDROID=var_android.ID";

			rrrr = stmt.executeQuery(se1);
			joiner2 = new StringJoiner(",");
			StringJoiner joiner3;
			while(rrrr.next()){
				String aa = rrrr.getString(2);
				String ss = rrrr.getString(1);
				//				System.out.println("SQL	"+aa+"	"+ss+"  :"+sslist);
				for(int i=0;i<sslist.size();i++){
					if(ss.equals(sslist.get(i)) && aalist.indexOf(aa)!=-1){
						String mapping = aaaalist.get(aalist.indexOf(aa));
						joiner2.add(mapping);
					}
				}
				//				sssslist.get(sslist.indexOf(ss));
				//				aaaalist.get(aalist.indexOf(aa));
				//				System.out.println("SQL"+aalist.get(aaaalist.indexOf(aa))+":"+sslist.get(sssslist.indexOf(ss)));
			}

			//			System.out.println("mapping test "+joiner2.toString());
			//			stmt.execute(dropTable("expr_swift"));
			//			stmt.execute(dropTable("expr_android"));
			//			stmt.executeUpdate(createExprTables());
			//			stmt.executeUpdate(createExprTablesSwift());

			e11 = SQLiteJDBC.insertTableExprSwift3(mid, joiner2.toString());
			stmt.executeUpdate(e11);






			//			System.out.println(i1);
			//			System.out.println(i2);
			stmt.executeUpdate(i1);
			stmt.executeUpdate(i2);		
			stmt.executeUpdate(i11);	
			stmt.executeUpdate(i12);		
			stmt.executeUpdate(i113);
			stmt.executeUpdate(i123);	
			mid++;


		}
		//		c.close();
	}

	static LinkedList<VarDeclElement> alist = new LinkedList<VarDeclElement>();
	static LinkedList<VarDeclElement> slist = new LinkedList<VarDeclElement>();

	static Map<String, VarDeclElement> amapping = new HashMap<String, VarDeclElement>();
	static Map<String, VarDeclElement> smapping = new HashMap<String, VarDeclElement>();
	static Map<String, String> mappingKeysVarDecl = new HashMap<String, String>();
	static int id = 1;
	static int mid = 1;
	public <T> void mappingVarDecl() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException{
		//		int id = 1;
		//		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader("charts/swift/ChartViewPortHandler.swift"));


		//		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		//		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));

		RecordVarDeclSwift recordSwift = new RecordVarDeclSwift<>();
		swift.accept(recordSwift);

		RecordVarDeclAndroid recordAndroid = new RecordVarDeclAndroid();
		android.accept(recordAndroid);
		//		System.out.println("android's recording part\n"+recordAndroid.record);
		//		System.out.println("swift's recording part\n"+recordSwift.record);

		int na = recordAndroid.record.size();
		int ns = recordSwift.record.size();

		//		Class.forName("org.sqlite.JDBC");

		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		//		connection.setAutoCommit(false);
		//			System.out.println("Opened database successfully");


		//      System.out.println(insertTable(1, "_nameX","public","Rect","new Rect()"));

		Statement stmt = connection.createStatement();


		if(na==ns && na==4){
			mappingElements1.put(recordAndroid.record.get(0), (MappingElement)recordSwift.record.get(0));
			mappingElements1.put(recordAndroid.record.get(1), (MappingElement)recordSwift.record.get(1));
			//			mappingElements1.put(recordAndroid.record.get(2), (MappingElement)recordSwift.record.get(2));
			mappingElements1.put(recordAndroid.record.get(3), (MappingElement)recordSwift.record.get(3));

			//			System.err.println("mappingElements\n"+mappingElements1);

			//				System.out.println(recordAndroid.recordStr);
			String a1 = recordAndroid.recordStr.get(0);
			String a2 = recordAndroid.recordStr.get(1);
			String a3 = recordAndroid.recordStr.get(2);
			String a4 = recordAndroid.recordStr.get(3);
			alist.add(new VarDeclElement(a1, a2, a3, a4));
			amapping.put(a3, new VarDeclElement(a1, a2, a3, a4));
			String s1 = ((MappingElement) recordSwift.record.get(0)).label;
			String s2 = ((MappingElement) recordSwift.record.get(1)).label;
			String s3 = ((MappingElement) recordSwift.record.get(2)).label;
			String s4 = ((MappingElement) recordSwift.record.get(3)).label;
			slist.add(new VarDeclElement(s1, s2, s3, s4));

			smapping.put(s3, new VarDeclElement(s1, s2, s3, s4));

			mappingKeysVarDecl.put(a3, s3);


			String i1 = SQLiteJDBC.insertTable("VarDecl_Android", id, a3,a1,a2,a4);
			String i2 = SQLiteJDBC.insertTable("VarDecl_Swift", id, s3,s1,s2,s4);


			String i3 = SQLiteJDBC.insertTable("var_android", id, a3,a1,a2,a4);
			String i4 = SQLiteJDBC.insertTableSwift("var_swift", id, s3,s1,s2,s4,id);

			//			System.out.println(i1);
			//			System.out.println(i2);
			stmt.executeUpdate(i1);
			stmt.executeUpdate(i2);			

			//			System.out.println(i3);
			//			System.out.println(i4);
			stmt.executeUpdate(i3);
			stmt.executeUpdate(i4);	

			id++;
			System.out.println(id+":"+a3+","+s3);
		}

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[android:"+android.toString()+", swift:"+swift.getText()+"]";
	}
}
