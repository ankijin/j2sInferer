package recording.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;

public class RecordingTestMain {

	public static String classfileaName="";
	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}

		reader.close();
		return  fileData.toString();	
	}

	static ASTRewrite rewriter;
	public static void main(String[] args) throws IOException{
		/*

		Filewalker fw = new Filewalker("mcharts/MPAndroidChart-master", "mcharts/Charts-master");
		Map<File, File> maps = fw.getClassMapping();
		Set<File> androidcodes = maps.keySet();
		Iterator<File> iter = androidcodes.iterator();
		int k=0;
		while(iter.hasNext()){
			k++;
			String javaFile = iter.next().getAbsolutePath();
			if(k>50){
				System.out.println(javaFile);
				String str= readFileToString(javaFile);
				//			System.out.println(str);
				ASTParser parser = ASTParser.newParser(AST.JLS8);
				parser.setSource(str.toCharArray());
				parser.setKind(ASTParser.K_COMPILATION_UNIT);
				//			Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
				final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
				System.err.println(cu.toString());
				cu.accept(new Android2Swift());
			}
		}
		 */

		//		ASTParser parser = ASTParser.newParser(AST.JLS8);

		//		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);

		//		parser.setSource(str.toCharArray());
		//		CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		//		cu.accept(new Android2Swift());


		//		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		//		CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		//
		//		File file = null;
		String [] files ={
				//				"XAxis.java",
				"TestingM1.java",
				//				"XAxisRenderer.java",
				//				"Test_J2Swift2.java"
				//								"Legend.java",
				//				"AnimatedViewPortJob.java",
				//				"LineRadarRenderer.java"
				//				"ChevronDownShapeRender.java"
		};

		String path="/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/android/";

		for(String f:files){
			ASTParser parser = ASTParser.newParser(AST.JLS8);

			parser.setKind(ASTParser.K_COMPILATION_UNIT);

			parser.setResolveBindings(true);
			parser.setBindingsRecovery(true);

			Map options = JavaCore.getOptions();

			String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/testsrc" }; 
			String[] classpath = {"/Users/kijin/Desktop/android-13.jar"};		


			File file = new File(path+f);
			String str= readFileToString(file.getAbsolutePath());
			//		String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest/Test.java");
			classfileaName = f;

			parser.setKind(ASTParser.K_COMPILATION_UNIT);
			parser.setResolveBindings(true);
			//		parser.setKind(ASTParser.K_COMPILATION_UNIT);

			parser.setBindingsRecovery(true);

			options = JavaCore.getOptions();
			parser.setCompilerOptions(options);
			parser.setUnitName(file.getName());

			parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);

			parser.setSource(str.toCharArray());
			CompilationUnit	cu = (CompilationUnit) parser.createAST(null);
			cu.accept(new Android2Swift());

		}

		/*
		parser = ASTParser.newParser(AST.JLS8);
		str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/android/Legend.java");
//		String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest/Test.java");
		classfileaName = "Legend";
		tree = new GenericTree<ST>();
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
//		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		cu = (CompilationUnit) parser.createAST(null);
		cu.accept(new Android2Swift());


		parser = ASTParser.newParser(AST.JLS8);
		str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/android/XAxisRenderer.java");
//		String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest/Test.java");
		classfileaName = "XAxisRenderer";
		tree = new GenericTree<ST>();
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
//		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		cu = (CompilationUnit) parser.createAST(null);
		cu.accept(new Android2Swift());



		parser = ASTParser.newParser(AST.JLS8);
		str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/android/AnimatedViewPortJob.java");
//		String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest/Test.java");
		classfileaName = "AnimatedViewPortJob";
		tree = new GenericTree<ST>();
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
//		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		cu = (CompilationUnit) parser.createAST(null);
		cu.accept(new Android2Swift());


		parser = ASTParser.newParser(AST.JLS8);
		str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/android/LineRadarRenderer.java");
//		String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest/Test.java");
		classfileaName = "LineRadarRenderer";
		tree = new GenericTree<ST>();
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
//		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		cu = (CompilationUnit) parser.createAST(null);
		cu.accept(new Android2Swift());



		parser = ASTParser.newParser(AST.JLS8);
		str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/android/ChevronDownShapeRenderer.java");
//		String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest/Test.java");
		classfileaName = "ChevronDownShapeRender";
		tree = new GenericTree<ST>();
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
//		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		cu = (CompilationUnit) parser.createAST(null);
		cu.accept(new Android2Swift());
		 */

		/*
		Filewalker fw = new Filewalker();
		LinkedList<File> android_files = new LinkedList<File>();
		fw.walk("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest", android_files);
		System.out.println(android_files);
		Iterator<File> iter = android_files.iterator();
		while(iter.hasNext()){
//			k++;
			String javaFile = iter.next().getAbsolutePath();
//			if(k>50){
				System.out.println(javaFile);
				String str= readFileToString(javaFile);
				//			System.out.println(str);
				ASTParser parser = ASTParser.newParser(AST.JLS8);
				parser.setSource(str.toCharArray());
				parser.setKind(ASTParser.K_COMPILATION_UNIT);
				//			Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
				final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
				System.err.println(cu.toString());
				cu.accept(new Android2Swift());
//			}
		}



		/*

		Filewalker fw1 = new Filewalker();
		LinkedList<File> android_files = new LinkedList<File>();		
		fw1.walk("MPAndroidChart", android_files);
//		System.err.println(android_files);

		for(File f :android_files){
			System.err.println(f.getName());
			if(f.getName().equals("XAxisRenderer.java")){
				System.out.println(f.getAbsolutePath());

				ASTParser parser = ASTParser.newParser(AST.JLS8);
				parser.setResolveBindings(true);
				parser.setKind(ASTParser.K_COMPILATION_UNIT);
				parser.setBindingsRecovery(true);
				parser.setUnitName(f.getAbsolutePath());
				String str= readFileToString(f.getAbsolutePath());
//				String str= readFileToString("/Users/kijin/Documents/workspace_mars/GumTreeTester/testingFileName/grammarTest/Test.java");
				classfileaName = "test";
				GenericTree<ST> tree = new GenericTree<ST>();
				parser.setSource(str.toCharArray());
				parser.setKind(ASTParser.K_COMPILATION_UNIT);
//				Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
				CompilationUnit cu = (CompilationUnit) parser.createAST(null);
				cu.accept(new Android2Swift());

			}
		}
		 */
	}

}
