package recording.java;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;

public interface TemplateIfStatement {
	STGroup template = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates/if_statement.stg");
	ST if_statement = template.getInstanceOf("if_statement");
//	static GenericTree<ST> tree = new GenericTree<ST>();
//	static GenericTreeNode<ST> parent = new GenericTreeNode<ST>();
}
