package recording.java;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import net.vivin.GenericTreeTraversalOrderEnum;
import recording.MappingElement;

public class RecordFieldDeclaration2 extends ASTVisitor{

	//	GenericTreeNode<String> parent;
	//	GenericTreeNode<String> astNode;
	STGroup group = new STGroupFile("/Users/kijin/Documents/workspace_mars/GumTreeTester/cross_templates/if_statement.stg");

	public LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	public LinkedList<String> recordStr = new LinkedList<String> ();
	public GenericTree<ST> tree = new GenericTree<ST>();
	public Map<Integer,GenericTreeNode<ST>> treeMap = new HashMap<Integer,GenericTreeNode<ST>>();
	public RecordFieldDeclaration2(){
		
		GenericTreeNode<Integer> comp 			= new GenericTreeNode<Integer>(ASTNode.COMPILATION_UNIT);
		GenericTreeNode<Integer> field_decl 	= new GenericTreeNode<Integer>(ASTNode.FIELD_DECLARATION);
		GenericTreeNode<Integer> method_decl 	= new GenericTreeNode<Integer>(ASTNode.METHOD_DECLARATION);
		GenericTreeNode<Integer> return_stmt 	= new GenericTreeNode<Integer>(ASTNode.RETURN_STATEMENT);
		GenericTreeNode<Integer> if_stmt 		= new GenericTreeNode<Integer>(ASTNode.IF_STATEMENT);
//		GenericTreeNode<Integer> block 			= new GenericTreeNode<Integer>(ASTNode.BLOCK);
		
		
		comp.addChild(field_decl);
		comp.addChild(method_decl);
		method_decl.addChild(return_stmt);
		method_decl.addChild(if_stmt);
		if_stmt.addChild(return_stmt);
		if_stmt.addChild(if_stmt);
		
	}
	GenericTreeNode<ST> top;

	@Override
	public boolean visit(CompilationUnit node) {
		// TODO Auto-generated method stub
		System.out.println("CompilationUnit");
		ST data= group.getInstanceOf("program");
		data.add("className", "TestClass");
		top = new GenericTreeNode<ST>(data);

		treeMap.put(ASTNode.COMPILATION_UNIT, top);

		//		parent = childB;

		return super.visit(node);
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		// TODO Auto-generated method stub
		node.getType();
		//		node.modifiers().get(0);
		node.fragments();
		//		node.

		System.out.println("FieldDeclaration");
		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("var_decls"));
		GenericTreeNode<ST> childC = new GenericTreeNode<ST>(group.getInstanceOf("var_declaration"));
		ST data = childC.getData();
		VariableDeclarationFragment var_decl = (VariableDeclarationFragment)node.fragments().get(0);


		Connection connection;
		Statement stmt;
		ResultSet resultSet = null;
		String init = null;
		String modifier = null;
		try {
			connection  = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			connection.setAutoCommit(false);
			stmt 		= connection.createStatement();

			String sql = "SELECT DISTINCT var_swift.MODIFIER, var_swift.INIT FROM var_android, var_swift "
					+ "WHERE var_swift.ANDROID=var_android.ID and var_android.INIT"+"=\'"+var_decl.getInitializer()+"\'";
			data.add("log", sql);
			resultSet = stmt.executeQuery(sql);
			while(resultSet.next()){
				init = resultSet.getString("INIT");
				modifier  = resultSet.getString("MODIFIER");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




//		data.add("modifiers", node.modifiers().get(0).toString());
		data.add("name", var_decl.getName());
		if(init==null){
			data.add("init", var_decl.getInitializer());
		}else{
			data.add("init", init);
		}
		
		if(modifier==null){
			data.add("modifiers", node.modifiers().get(0).toString());
		}else{
			data.add("modifiers", modifier);
		}
		
		//data.add("tmp", data.getAttributes().toString());
		childC.setData(data);
		//		data.add("modifiers", node.modifiers().get(0).toString());
		GenericTreeNode<ST> parent = treeMap.get(ASTNode.COMPILATION_UNIT);
		childB.addChild(childC);
		parent.addChild(childB);

		treeMap.put(ASTNode.FIELD_DECLARATION, childB);

		return false;
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		// TODO Auto-generated method stub
		System.out.println("MethodDeclaration");

		GenericTreeNode<ST> childB = new GenericTreeNode<ST>(group.getInstanceOf("methods"));
//		ST temp = childB.getData();
//		temp.add("func_declaration", "func test()->float{}");
//		childB.setData(temp);

		GenericTreeNode<ST> childC = new GenericTreeNode<ST>(group.getInstanceOf("func_declaration"));
		ST temp = childC.getData();
		temp.add("modifiers", "public");
		temp.add("type", "CGFloat");
		temp.add("name", "nameTest");
		temp.add("statements","int a");
		childC.setData(temp);
		childB.addChild(childC);
		GenericTreeNode<ST> parent = treeMap.get(ASTNode.COMPILATION_UNIT);
		parent.addChild(childB);

		treeMap.put(ASTNode.METHOD_DECLARATION, childB);
		
//		List list = node.getBody().statements();
		

		return super.visit(node);
	}

	@Override
	public boolean visit(Block node) {
		// TODO Auto-generated method stub
		GenericTreeNode<Integer> block 	= new GenericTreeNode<Integer>(ASTNode.BLOCK);
		List list = node.statements();
		for(int i=0;i<list.size();i++){
			if (list.get(i) instanceof ReturnStatement){
				GenericTreeNode<Integer> return_stmt 	= new GenericTreeNode<Integer>(ASTNode.RETURN_STATEMENT);
				block.addChild(return_stmt);
			}
			
			if (list.get(i) instanceof IfStatement){
				GenericTreeNode<Integer> if_stmt 	= new GenericTreeNode<Integer>(ASTNode.IF_STATEMENT);
				block.addChild(if_stmt);
			}
		}
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ReturnStatement node) {
		// TODO Auto-generated method stub
		return super.visit(node);
	}
	
	@Override
	public void endVisit(CompilationUnit node) {
		// TODO Auto-generated method stub
		tree.setRoot(top);
		for(GenericTreeNode<ST> tt1:tree.build(GenericTreeTraversalOrderEnum.POST_ORDER)){
			System.err.println(tt1);

			ST data = tt1.getData();
			GenericTreeNode<ST> p= tt1.getParent();

			if(p!=null){
				ST pdata = p.getData();
				Map<String, Object> att = pdata.getAttributes();
				System.out.println("\n"+att+"->"+data.getName().replace("/", "")+","+data.render());
				//		System.err.println(data.getName().replace("/", "")+","+data.render());
				if(att.containsKey(data.getName().replace("/", ""))){
					pdata.add(data.getName().replace("/", ""), data.render());
				}
			}
		}
		System.out.println("final\n"+top.getData().render());
		super.endVisit(node);
	}
	//	visit


}
