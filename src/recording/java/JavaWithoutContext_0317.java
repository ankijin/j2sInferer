package recording.java;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.antlr.runtime.RecognitionException;
import org.antlr.v4.parse.ScopeParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.tool.Attribute;
import org.antlr.v4.tool.Grammar;
import org.apache.commons.lang3.StringUtils;

import alignment.DelcarationMapping;
import alignment.common.CommonOperators;
import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import alignment.ruleinfer.StructureMappingAntlr4_for_stmt;
import antlr_parsers.common_old.CommonBaseListener;
import antlr_parsers.common_old.CommonLexer;
import antlr_parsers.common_old.CommonParser;
import antlr_parsers.common_old.CommonParser.AssignContext;
import antlr_parsers.common_old.CommonParser.Ast_nodeContext;
import antlr_parsers.common_old.CommonParser.ExprContext;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.pcommon.PCommonBaseListener;
import antlr_parsers.pcommon.PCommonLexer;
import antlr_parsers.pcommon.PCommonParser;
import antlr_parsers.pcommon.PCommonParser.ExpressionContext;
import common.CommonParser.AssignmentContext;
import common.CommonParser.EnclosedContext;
import common.CommonParser.Keyword_withContext;
import common.CommonParser.ParseContext;
import recording.BaseSwiftRecorder;

public class JavaWithoutContext_0317 extends JavaBaseListener{
	public ParserRuleContext android;
	public List<String> 	ssnodes 					= new LinkedList<String>();
	public List<OpProperty> 	op_a_nodes 					= new LinkedList<OpProperty>();
	public List<OpProperty> 	a_nodes 					= new LinkedList<OpProperty>();
	public List<OpProperty> blacklist 					= new LinkedList<OpProperty>();
	//	public CommonTokenStream 		swiftstream;

	public List<OpProperty> op_a_precoding 		= new LinkedList<OpProperty>();
	String left="", right="",rright="";

	public List<TerminalNode> terms = new LinkedList<TerminalNode>();
	public Map<Interval, String> termsMap = new LinkedHashMap<Interval, String>();
	public Map<Interval, String> termsMapOnly = new LinkedHashMap<Interval, String>();
	public List<CommonOperators> termsMapOnlys = new LinkedList<CommonOperators>();
	public LineCharIndex leftIndex =null;
	public LineCharIndex blockIndex =null, rightIndex = null;
	//	LineCharIndex leftIndex =null;
	//	public Map<Interval, String> tokenMap = new LinkedHashMap<Interval, String>();
	public String type="none";
	public String opsequence="";
	Map<Interval, String> tokens;
	LinkedHashMap<String, Attribute> scope_list;
	public LinkedHashMap<String, List<Interval>> scope_constraint;
	public List<String> punc = new LinkedList<String>();

	public JavaWithoutContext_0317(ParserRuleContext android){
		scope_list 					= new  	LinkedHashMap<String, Attribute>();
		scope_constraint 			= new LinkedHashMap<String, List<Interval>>();
		a_nodes 					= new 	LinkedList<OpProperty>();
		opsequence="";
		op_a_nodes 					= new LinkedList<OpProperty>();
		op_a_precoding = new LinkedList<OpProperty>();
		tokens = new HashMap<Interval, String>();
		this.android = android;

		int a = android.start.getStartIndex();
		int b = android.stop.getStopIndex();
		Interval interval = new Interval(a,b);
		String context = android.start.getInputStream().getText(interval);
		//		List<TerminalNode> 	tokens 					= new LinkedList<TerminalNode>();
		//		System.out.println(context);

		//		String context = android.getText();

		ParseTreeWalker pwalker = new ParseTreeWalker();

		ANTLRInputStream stream2 = new ANTLRInputStream(context);
		Lexer plexer = new PCommonLexer((CharStream)stream2);
		antlr_parsers.pcommon.PCommonParser pparser	= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(plexer));


		antlr_parsers.pcommon.PCommonParser.ParseContext proot = pparser.parse();

		PCommonBaseListener plistener = new PCommonBaseListener(){
			int depth = 1000;
			@Override
			public void enterAssignment(PCommonParser.AssignmentContext ctx) {
				if(ctx.depth() < depth){
					type = "AssignmentContext";

					depth = ctx.depth();
					left = ctx.leftAssign().getText();
					rright = ctx.rightAssign().getText();
					//System.out.println("rright"+right);
					//super.enterAssignment(ctx);

					leftIndex = new LineCharIndex(
							ctx.leftAssign().start.getLine(), 
							ctx.leftAssign().start.getCharPositionInLine(), 
							ctx.leftAssign().stop.getLine(), 
							ctx.leftAssign().stop.getStopIndex());

					rightIndex = new LineCharIndex(
							ctx.rightAssign().start.getLine(), 
							ctx.rightAssign().start.getCharPositionInLine(), 
							ctx.rightAssign().stop.getLine(), 
							ctx.rightAssign().stop.getStopIndex());

				}//
				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 means one lower level of paranthesis
					int a 				= ctx.start.getStartIndex();
					int b 				= ctx.stop.getStopIndex();
					Interval interval 	= new Interval(a,b);
					String context 		= proot.start.getInputStream().getText(interval);

					//a_nodes.add(new OpProperty(null, context,"AssignmentContext",ctx.depth()));
					OpProperty op = new OpProperty(null, context,"AssignmentContext",ctx.depth());
					op.lineindex = LineCharIndex.contextToLineIndex(ctx);
					op.getxt = ctx.getText();
					a_nodes.add(op);
				}
				super.enterAssignment(ctx);
			}

			@Override
			public void enterEnclosed(PCommonParser.EnclosedContext ctx) {

				if(ctx.depth() < depth){
					type  = "EnclosedContext";

					depth = ctx.depth();
					left  = ctx.outer().getText();

					//					right = ctx.inner().getText();
					//					System.out.println("rright"+right);
					//					super.enterAssignment(ctx);

					if(ctx.outer().stop!=null && ctx.outer().start!=null){
						leftIndex = new LineCharIndex(
								ctx.outer().start.getLine(), 
								ctx.outer().start.getCharPositionInLine(), 
								ctx.outer().stop.getLine(), 
								ctx.outer().stop.getStopIndex());

					}
					if(ctx.inner().stop!=null && ctx.inner().start!=null){
						blockIndex = new LineCharIndex(
								ctx.inner().start.getLine(), 
								ctx.inner().start.getCharPositionInLine(), 
								ctx.inner().stop.getLine(), 
								ctx.inner().stop.getStopIndex());
					}

				}

				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 means one lower level of paranthesis
					//				if(ctx.depth()==6 && !ctx.outer().getText().equals("")){
					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval 	= new Interval(a,b);
					String context 		= proot.start.getInputStream().getText(interval);

					OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());
					op.getxt = ctx.getText();
					op.lineindex = LineCharIndex.contextToLineIndex(ctx);
					a_nodes.add(op);


				}
				super.enterEnclosed(ctx);
			}

			@Override
			public void enterExpression(ExpressionContext ctx) {


				// TODO Auto-generated method stub
				if(ctx.depth()==6){

					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
					String context = proot.start.getInputStream().getText(interval);
					//					System.err.println("ExpressionContext\n"+context+"   "+ctx.depth());
					//					a_nodes.add(new OpProperty(null, context,"ExpressionContext",ctx.depth()));

					OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
					op.getxt = ctx.getText();
					op.lineindex = LineCharIndex.contextToLineIndex(ctx);
					//System.out.println("ExpressionContext"+context);
					a_nodes.add(op);
					//					leftIndex = null;
				}
				super.enterExpression(ctx);
			}
		};



		pwalker.walk(plistener, proot);


	}

	/**
	 * only identifiers
	 */


	@Override
	public void visitTerminal(TerminalNode node) {
		// TODO Auto-generated method stub
		node.getSourceInterval();
		boolean disj = true;
		boolean notc = true;
		Interval it = null;



		//		LineCharIndex lineindex=new LineCharIndex(node.getSymbol().getLine(), node.getSymbol().getLine()+node.getSymbol().getCharPositionInLine(),
		//				node.getSymbol().getLine(),node.getSymbol().getCharPositionInLine()+node.getText().length()-1);

		LineCharIndex lineindex=new LineCharIndex(node.getSymbol().getLine(), node.getSymbol().getCharPositionInLine(),
				node.getSymbol().getLine(),node.getSymbol().getStopIndex());

		termsMap.put(node.getSourceInterval(), node.getText());
	
		if(StructureMappingAntlr4_for_stmt.common_operators_list.contains(node.getText())){
			if(!termsMapOnly.containsKey(node.getSourceInterval())){
				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
				termsMapOnly.put(node.getSourceInterval(), node.getText());
			}
		}
		//		System.out.println("node.getText()"+node.getText());
		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) 
				&& StructureMappingAntlr4_for_stmt.common_operators_list.contains(node.getText())){
			//		if(StructureMappingAntlr4.common_operators_list.contains(node.getText())){
			if(!termsMapOnly.containsKey(node.getSourceInterval())){
				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
				termsMapOnly.put(node.getSourceInterval(), node.getText());
			}
		} 
		else if (!type.equals("EnclosedContext")){
			if(StructureMappingAntlr4_for_stmt.common_operators_list.contains(node.getText())){
				if(!termsMapOnly.containsKey(node.getSourceInterval())){
					termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
					termsMapOnly.put(node.getSourceInterval(), node.getText());
				}
			}
		}




		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex))
		{

			Attribute decl = scope_list.get(node.getText());
			if(decl!=null){
				List<Interval> list = scope_constraint.get(decl.toString());
				if(list!=null){
					list.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), list);

				}else{
					List<Interval> ll = new LinkedList<Interval>();
					ll.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), ll);

				}
			}			
			//		if(type.equals("EnclosedContext")&&!left_interval.disjoint(node.getSourceInterval())){
			//			System.err.println(node.getSourceInterval()+"node.getText()"+node.getText());

			//			else if(StructureMappingAntlr4.swift_punctuations_list.contains(node.getText())){
			//				termsMapOnlys.add(new CommonOperators("PUNC", node.getSourceInterval()));
			//			}
			//			else{
			//				termsMapOnlys.add(new CommonOperators("OTHERS", node.getSourceInterval()));
			//			}
		}
		/*
		if(!type.equals("EnclosedContext") && StructureMappingAntlr4.common_operators_list.contains(node.getText())){
			termsMapOnly.put(node.getSourceInterval(), node.getText());
			termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
		}
		else if(type.equals("EnclosedContext") && blockIndex!=null){
			if(lineindex.isDisjoint(blockIndex) && StructureMappingAntlr4.common_operators_list.contains(node.getText())){
				termsMapOnly.put(node.getSourceInterval(), node.getText());
				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
			}
		}
		 */
		for(OpProperty t:op_a_nodes){



			//		terms.add(node);
			//			termsMap.put(node.getSourceInterval(), node.getText());
			if(StructureMappingAntlr4_for_stmt.java_punctuations_list.contains(node.getText())){
				punc.add(node.getText());
			}

			Iterator<Interval> keys = t.same_scope.keySet().iterator();
			while(keys.hasNext()){
				Interval ssc = keys.next();
				if(!ssc.equals(node.getSourceInterval())) return;
				//				
			}

			if(!t.interval.disjoint(node.getSourceInterval())) return;
			//			if(t.strASTNode.equals(node.getText()) && !t.interval.equals(node.getSourceInterval()) ) {
			//				System.out.println("same_scope");
			//				return;
			//			}

			if(t.strASTNode.equals(node.getText()) && t.interval.disjoint(node.getSourceInterval())) {
				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) && scope_list.get(node.getText())!=null){
					t.same_scope.put(node.getSourceInterval(), new OpProperty(node, node.getText(), "none"));
					//				System.out.println("adding scoping"+ctx.getSourceInterval()+":::"+ctx.getText()+"  >>"+t);
					return;
				}
			}

			if(t.node instanceof ParserRuleContext) it = ((ParserRuleContext)t.node).getSourceInterval();
			else if(t.node instanceof TerminalNode) it = ((TerminalNode)t.node).getSourceInterval();


			disj = it.disjoint(node.getSourceInterval()) & disj;
			//			System.out.println("swift contains"+t.strASTNode);
			//			t.node


			//			if(!disj) {
			//				System.out.println("not overlapped term	"+node.getText());
			//				notc = notc & false;
			//				return;
			//			}
		}
		/*
		for(OpProperty t:op_a_nodes){
			if(!t.interval.disjoint(node.getSourceInterval())){
//				System.out.println("not disjoint");
				return;
			}

		}
		 */
		//		if(!disj) return;



		//		if(node.getText().startsWith("(") && node.getText().endsWith(")")){

		//			System.out.println("enclosed()"+context);
		//						op_a_nodes.add(new OpProperty(node, node.getText(), "enclosed()"));
		//						return;
		//		}



		if(!StructureMappingAntlr4_for_stmt.java_keywords_list.contains(node.getText()) && !StructureMappingAntlr4_for_stmt.java_punctuations_list.contains(node.getText())){
			if(type.equals("EnclosedContext")){
				//				System.out.println("left.contains"+left+" "+ctx.getText());
				if(!leftIndex.isDisjoint(lineindex)){
					op_a_nodes.add(new OpProperty(node, node.getText(),"enclosed_outer"));
					//					System.out.println("adding in term"+node.getText()+"  "+blacklist);
				}else if(right.contains(node.getText())){
					//					op_a_nodes.add(new OpProperty(node, node.getText(),"enclosed_inner"));
				}
			}

			else if(type.equals("AssignmentContext")){
								System.out.println("right"+rightIndex+"  "+node.getText());
				if(!leftIndex.isDisjoint(lineindex)){
					//					System.out.println("AssignmentContext"+context);
					op_a_nodes.add(new OpProperty(node, node.getText(),"assign_left"));
				}else if(!rightIndex.isDisjoint(lineindex)){
					op_a_nodes.add(new OpProperty(node, node.getText(),"assign_right"));
				}
				else{
					//					op_a_nodes.add(new OpProperty(node, node.getText(),"none"));
				}
			}
			else{
				//				op_a_nodes.add(new OpProperty(ctx, context,"MMMnone",ctx.depth()));
				op_a_nodes.add(new OpProperty(node, node.getText(),"none"));
			}


			//		}

			super.visitTerminal(node);
		}
	}

	//one-to-one mappings
	/// how to know it is one to one
	/// structure? asfdasf.(dfafasf, asfasf, asdfasf) 
	//many-to-many mappings: binding case, grap
	//separates
	//enter examples
	//condition: keywords, puncs. 

	//collect statement context with tagging to stmts_list
	//collect ParserRuleContexts to stmts_list
	//
	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		// TODO Auto-generated method stub
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context="";

		try{
			//get orgin text of context
			context = ctx.start.getInputStream().getText(interval);
		} catch(Exception e){
			return;
		}



		for(OpProperty t:a_nodes){
			if(t.getxt.equals(ctx.getText())){
				t.node = ctx;
				t.interval = ctx.getSourceInterval();
				//				if(t.property.equals("EnclosedContext")){
				//					System.out.println("EnclosedContext");
				//				}
			}
		}

		for(OpProperty t:blacklist){
			//			System.out.println("blist"+t);
			//			System.err.println(ctx.getSourceInterval());
			if(t.interval.equals(ctx.getSourceInterval())) {
				//				System.out.println(ctx.getText()+"	android contains	"+t.strASTNode);
				return;

			}
		}


		/*
		LineCharIndex lineIndex=LineCharIndex.contextToLineIndex(ctx);
		for(OpProperty t:a_nodes){
//			System.out.println("lineIndexing");
			if(t.lineindex.equals(lineIndex)){

				t.node = ctx;	
				t.type = ctx.getRuleIndex();
			}	

		}
		 */

		LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
				ctx.getStop().getLine(),ctx.getStop().getStopIndex());



		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) 
				&& StructureMappingAntlr4_for_stmt.common_operators_list.contains(ctx.getText())){
			//		if(StructureMappingAntlr4.common_operators_list.contains(node.getText())){

			if(!termsMapOnly.containsKey(ctx.getSourceInterval())){
				termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval()));
				termsMapOnly.put(ctx.getSourceInterval(), ctx.getText());
			}
		}

		else if (!type.equals("EnclosedContext")){
			if(StructureMappingAntlr4_for_stmt.common_operators_list.contains(ctx.getText())){
				if(!termsMapOnly.containsKey(ctx.getSourceInterval())){
					termsMapOnlys.add(new CommonOperators(ctx.getText(), ctx.getSourceInterval()));
					termsMapOnly.put(ctx.getSourceInterval(), ctx.getText());
				}
			}
		}


		Grammar dummy = null;
		try {
			dummy = new Grammar("grammar T; a:'a';");
		} catch (RecognitionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LinkedHashMap<String, Attribute> attributes =  new LinkedHashMap<String, Attribute>();
		//		if(dummy!=null && context!=null && ScopeParser.parseTypedArgList(null, context, dummy)!=null)
		try{
			attributes = ScopeParser.parseTypedArgList(null, context, dummy).attributes;
		}
		catch(Exception e){

		}
		for (String arg : attributes.keySet()) {
			Attribute attr = attributes.get(arg);
			if(attr!=null&& attr.name!=null &&attr.type!=null && attr.initValue!=null && StringUtils.isAlphanumeric(attr.name) && StringUtils.isAlphanumeric(attr.type)  && !attr.initValue.contains(";")){
				//				out.add(attr.decl);
				scope_list.put(attr.name, attr);
			}
		}
		//		if(ctx.getText().equals(android.getText())) return;



		for(OpProperty t:op_a_nodes){

			//			if(t.strASTNode.equals(context) || t.strASTNode.contains(context)) {
			if(t.interval.equals(ctx.getSourceInterval())) {
				//				System.out.println(ctx.getText()+"	android contains	"+t.strASTNode);
				return;
			}

			//			Iterator<Interval> keys = t.same_scope.keySet().iterator();



			if(t.same_scope.containsKey(ctx.getSourceInterval())) return;
			if(t.strASTNode.equals(context) && t.interval.disjoint(ctx.getSourceInterval())) {
				//				System.out.println("scope:"+context+" "+interval);

				//				LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getLine()+ctx.getStart().getCharPositionInLine(),
				//						ctx.getStop().getLine(),ctx.getStop().getLine()+ctx.getStop().getCharPositionInLine()-1);



				//				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) && scope_list.get(context)!=null){
				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) && scope_list.get(context)!=null){

					t.same_scope.put(ctx.getSourceInterval(), new OpProperty(ctx, context, "none"));
					//					System.out.println("adding scoping"+ctx.getSourceInterval()+":::"+ctx.getText()+"  >>"+t);
					return;
				}
				//				t.same_scope.add(e);
				//			if(t.strASTNode.equals(context)|| t.node==null) {
				//				return;
			}

			//			Iterator<Interval> keys = t.same_scope.keySet().iterator();
			//			while(keys.hasNext()){
			//				Interval ssc = keys.next();
			//				if(ssc.equals(ctx.getSourceInterval())) return;
			//			}

			if(!t.interval.disjoint(ctx.getSourceInterval())) {
				//				System.out.println(ctx.getText()+"	android contains	"+t.strASTNode);
				return;
			}



			if(t.strASTNode.equals(ctx.getText()) && t.interval.disjoint(interval)) {
				//				System.out.println("scope:"+context+" "+interval);
				//				t.same_scope.put(ctx.getSourceInterval(), new OpProperty(ctx, context, "none", ctx.depth()));
				//				t.same_scope.add(e);
				//			if(t.strASTNode.equals(context)|| t.node==null) {
				//				return;
			}


			else if(t.property.equals("enclosed{}") && t.strASTNode.replaceAll("\n", "").replaceAll(" ", "").contains(ctx.getText())){
				return;
			}
			//			else if(t.property.equals("enclosed()") && t.strASTNode.replaceAll("\n", "").replaceAll(" ", "").contains(context)){
			//				return;
			//				op_a_nodes.add(new OpProperty(ctx, context, "enclosed()", ctx.depth()));
			//			}
		}


		/**op property write
		 *enclosed{} 
		 */


		if(context.replaceAll(" ", "").replaceAll("", "").startsWith("{") && context.replaceAll(" ", "").endsWith("}")){
			op_a_nodes.add(new OpProperty(ctx, context, "enclosed{}", ctx.depth()));
			return;
		}



		//		System.out.println("testing	"+context);



		//		if(context.equals("()")) return;
		//		if(context.replaceAll(" ", "").startsWith("(") && context.replaceAll(" ", "").endsWith(")") ){

		//			System.out.println("##enclosed()"+context);
		//			op_a_nodes.add(new OpProperty(ctx, context, "enclosed()", ctx.depth()));
		//			return;
		//		}

		for(String key:StructureMappingAntlr4_for_stmt.java_keywords_list){
			if(context.equals(key)) return;
			//			if(context.contains(key)) return;
		}

		for(String key:StructureMappingAntlr4_for_stmt.java_punctuations_list){
			if(context.equals(key)) return;
			//			if(context.contains(key)) return;
		}

		if(type.equals("EnclosedContext")){

			//			if(left.contains(ctx.getText())){
			if(!leftIndex.isDisjoint(lineindex)){
				op_a_nodes.add(new OpProperty(ctx, context,"enclosed_outer",ctx.depth()));
				//				System.out.println("adding "+ctx.getText()+"  "+blacklist);

			}
		}


		else if(type.equals("AssignmentContext")){
			System.out.println("cctright"+rightIndex+"  "+ctx.getText());
			if(!leftIndex.isDisjoint(lineindex)){
				//				System.out.println("AssignmentContext"+context);
				op_a_nodes.add(new OpProperty(ctx, context,"assign_left",ctx.depth()));
			}else if(!rightIndex.isDisjoint(lineindex)){
				op_a_nodes.add(new OpProperty(ctx, context,"assign_right",ctx.depth()));
			}
			else if(android.getText().equals(ctx.getText())){
				op_a_nodes.add(new OpProperty(ctx, context,"assign",ctx.depth()));
			}
			else{
				if(!op_a_nodes.contains(context)){
					//					op_a_nodes.add(new OpProperty(ctx, context,"WWWnone",ctx.depth()));
				}
			}
		}

		else{
			//			if(!op_a_nodes.contains(context))
			op_a_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
		}
		//				}
		super.enterEveryRule(ctx);
	}

}
