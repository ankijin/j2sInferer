package recording.java;
import java.util.LinkedList;


import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IfStatement;


import recording.MappingElement;

public class RecordExpression extends ASTVisitor{

	public LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	public LinkedList<String> recordStr = new LinkedList<String> ();
	StringBuffer buffer;
	
	public RecordExpression(StringBuffer buffer){
		this.buffer = buffer;
	}

	
	public boolean visit(ExpressionStatement node) {
		
		/*
		Expression expr = node.getExpression();
		if (expr instanceof Assignment){
			Assignment e = (Assignment) expr;
			buffer.append(e);
		}
		*/
		buffer.append("\t");
		buffer.append(node);
		
//		node.accept(new RecordIfStatement(buffer));
		return super.visit(node);
	}
	
	/*
	@Override
	public boolean visit(Block node) {
//		System.err.println(node);
//		buffer.append("{");
//		System.err.println("{");
		List sts = node.statements();
		for(int i=0; i<sts.size();i++){
			if( sts.get(i) instanceof ReturnStatement){
				System.err.println("ReturnStatement");
//				list.add(ReturnStatement.RETURN_STATEMENT);
			}
			else if( sts.get(i) instanceof SingleVariableDeclaration){
				System.err.println("SingleVariableDeclaration");
//				list.add(SingleVariableDeclaration.SINGLE_VARIABLE_DECLARATION);
			}
			else if( sts.get(i) instanceof ExpressionStatement){
				System.err.println("ExpressionStatement"+sts.get(i));
				buffer.append(sts.get(i));
//				list.add(ExpressionStatement.EXPRESSION_STATEMENT);
			}
			else if( sts.get(i) instanceof IfStatement){
//				System.err.println("IfStatement");
				((IfStatement)sts.get(i)).accept(new RecordIfStatement(buffer)); 
//				buffer.append("IfStatement");
//				list.add(IfStatement.IF_STATEMENT);
			}
			else if( sts.get(i) instanceof VariableDeclarationStatement){
				System.err.println("VariableDeclarationStatement"+sts.get(i));
//				System.err.println("VariableDeclarationStatement"+VariableDeclarationStatement.VARIABLE_DECLARATION_STATEMENT);
				
//				list.add(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT);
			}
			else if( sts.get(i) instanceof VariableDeclarationExpression){
				System.err.println("VariableDeclarationExpression");
//				list.add(VariableDeclaration.VARIABLE_DECLARATION_EXPRESSION);
			}
			else{
				System.err.println("others"+sts.get(i) );
			}
			
		}
//		System.err.println("}");
//		buffer.append("}");
		return super.visit(node);
	}
	*/

}
