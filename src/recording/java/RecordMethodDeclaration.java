package recording.java;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;

import org.stringtemplate.v4.ST;
import net.vivin.GenericTreeNode;
import recording.MappingElement;

public class RecordMethodDeclaration extends ASTVisitor{

	StringBuffer buffer;
	List<ST>template;
	GenericTreeNode<ST> tree;
	public LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	public LinkedList<String> recordStr = new LinkedList<String> ();

	public RecordMethodDeclaration(StringBuffer buffer, List<ST> template){
		this.buffer 	= buffer;
		this.template 	= template;
	}

	public RecordMethodDeclaration(GenericTreeNode<ST> tree){
		this.tree =tree;
	}
	
	public boolean visit(org.eclipse.jdt.core.dom.MethodDeclaration node) {
		System.out.println("MethodDeclaration");
		recordStr.add(node.modifiers().toString());
		recordStr.add(node.getReturnType2().toString());
		recordStr.add(node.getName().toString());
//		buffer.append(node.modifiers().toString());
//		buffer.append(node.getReturnType2().toString());
//		buffer.append(node.getName().toString());
//		buffer.append("{");
//		System.out.println(recordStr);
		if(node.getBody()!=null){
//			node.getBody().statements();
//			node.getBody().accept(new RecordStatements(buffer, template));
			node.getBody().accept(new RecordStatements(tree));
			//node.getBody().accept(new RecordExpression(buffer));
		}
//		buffer.append("}");
		return false;
	}

}
