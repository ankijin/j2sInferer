package recording.java;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import alignment.DelcarationMapping;
import alignment.common.OpProperty;
import antlr_parsers.common_old.CommonBaseListener;
import antlr_parsers.common_old.CommonLexer;
import antlr_parsers.common_old.CommonParser;
import antlr_parsers.common_old.CommonParser.AssignContext;
import antlr_parsers.common_old.CommonParser.Ast_nodeContext;
import antlr_parsers.common_old.CommonParser.ExprContext;
import common.CommonParser.AssignmentContext;
import common.CommonParser.EnclosedContext;
import common.CommonParser.Keyword_withContext;
import common.CommonParser.ParseContext;
import recording.BaseSwiftRecorder;

public class JavaListenerWithoutContext2 extends SwiftBaseListener{
	public ParserRuleContext android;
	public List<String> 	ssnodes 					= new LinkedList<String>();
	public List<OpProperty> 	op_a_nodes 					= new LinkedList<OpProperty>();
	public List<OpProperty> op_a_precoding = new LinkedList<OpProperty>();
	String left="", right="";
	public String type="none";
	public String opsequence="";
	Map<Interval, String> tokens;


	public JavaListenerWithoutContext2(ParserRuleContext android){

		opsequence="";
		op_a_nodes 					= new LinkedList<OpProperty>();
		op_a_precoding = new LinkedList<OpProperty>();
		tokens = new HashMap<Interval, String>();
		this.android = android;

		int az = android.start.getStartIndex();
		int bz = android.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = android.start.getInputStream().getText(interval);
		//		List<TerminalNode> 	tokens 					= new LinkedList<TerminalNode>();


		ANTLRInputStream stream1 = new ANTLRInputStream(context);
		Lexer lexer1  = new common.CommonLexer((CharStream)stream1);
		common.CommonParser parser1 = new common.CommonParser(new CommonTokenStream(lexer1));
		ParseContext root = parser1.parse();

		ParseTreeWalker walker = new ParseTreeWalker();

		/*

		CommonBaseListener listener = new CommonBaseListener(){
			int w=0;
			@Override
			public void enterAssign(AssignContext ctx) {
				type="AssignContext";
				// TODO Auto-generated method stub
				ctx.equal().getSourceInterval();
				ctx.equal().start.getStartIndex();
				ctx.equal().stop.getStopIndex();
				//				property = "AssignContext";
				//				property_start = ctx.equal().start.getStartIndex()+node.getStartPosition();
				//				property_end = ctx.equal().stop.getStopIndex()+node.getStartPosition();

				//				System.out.println("property  "+property+"  "+property_start+" "+property_end);
				super.enterAssign(ctx);
			}

			@Override
			public void exitExpr(ExprContext ctx) {
				type="ExprContext";
				// TODO Auto-generated method stub
				int az = ctx.start.getStartIndex();
				int bz = ctx.stop.getStopIndex();
				Interval interval = new Interval(az,bz);
				String context = ctx.start.getInputStream().getText(interval);

				System.out.println("sExprContext"+w+" "+ctx.getText()+"  "+context);
				op_s_precoding.add(new OpProperty(ctx, context, "ExprContext"+w));
				w++;
				if(ctx.mul()!=null){
					opsequence +="*";
				}
				if(ctx.div()!=null){
					opsequence +="/";
				}
				if(ctx.plus()!=null){
					opsequence +="+";
				}
				if(ctx.minus()!=null){
					opsequence +="-";
				}


				super.exitExpr(ctx);
			}

		};
		 */
		common.CommonBaseListener listener = new common.CommonBaseListener(){
			int depth = 1000;
			@Override
			public void enterEnclosed(EnclosedContext ctx) {
			

				// TODO Auto-generated method stub
				if(ctx.depth() < depth){
					type = "EnclosedContext";
//					System.err.println("enterE");
					depth = ctx.depth();
					left = ctx.outer().getText();
					right = ctx.inner().getText();
					//					ctx.outer();
					//					ctx.inner();
					System.out.println("javactx.outer()"+ctx.outer().getText());
				}

				super.enterEnclosed(ctx);
			}

			@Override
			public void enterAssignment(AssignmentContext ctx) {
			
				// TODO Auto-generated method stub
				if(ctx.depth() < depth){
					type = "AssignmentContext";
//					System.err.println("enterA");
					depth = ctx.depth();
					left = ctx.leftAssign().getText();
					right = ctx.rightAssign().getText();
					super.enterAssignment(ctx);
				}
			}
			
			@Override
			public void enterKeyword_with(Keyword_withContext ctx) {
				
				// TODO Auto-generated method stub
				if(ctx.depth() < depth){
					type = "Keyword_withContext";
//					System.err.println("enterK");
					depth = ctx.depth();
//					left = ctx.keyword().getText();
//					right = ctx.block().getText();
					super.enterKeyword_with(ctx);

				}
			}
		};

		walker.walk(listener, root);



		if(context.contains("=")){

			String[] spt = context.split("=");
			if(spt.length==2){
				left 	= spt[0];
				right 	= spt[1];
			}
		}

	}


	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		// TODO Auto-generated method stub
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = ctx.start.getInputStream().getText(interval);
		if(op_a_precoding.size()>0)
			System.out.println("aa_precoding"+context+"  "+op_a_precoding);
		//		if(Character.isUpperCase(ctx.getText().charAt(0))) return;
		//				ctx.depth();
		//				System.out.println();
		//with depth?? sub-tree?


		int index=op_a_precoding.indexOf(new OpProperty(ctx, ctx.getText()));
		if(index !=-1){
			System.out.println("op_ss_precoding"+op_a_precoding.get(index).property);
			op_a_nodes.add(new OpProperty(ctx, context, op_a_precoding.get(index).property, ctx.depth()));
			return;
		}
		System.out.println("same?"+ctx.getText()+" "+android.getText().replaceAll(" ", "")+" "+ctx.getText().replaceAll(" ", "").equals(android.getText().replaceAll(" ", "")));
		if(ctx.getText().replaceAll(" ", "").equals(android.getText().replaceAll(" ", ""))) return;

		for(OpProperty t:op_a_nodes){
			//			System.out.println("swift contains"+t.strASTNode);
			if(t.strASTNode.equals(context)) {
				return;
			}
		}
		/*
		for(String t:ssnodes){
			if(t.equals(context)) {
				System.out.println(t.replace("\n", "")+"	equals	"+context.replace("\n", ""));
				return;
			}
		}
		 */
		/**op property write
		 *enclosed{} 
		 */
		if(context.replaceAll(" ", "").startsWith("{") && context.replaceAll(" ", "").endsWith("}")){

			System.out.println("enclosed{}"+context);
			op_a_nodes.add(new OpProperty(ctx, context, "enclosed{}", ctx.depth()));
			//			return;
		}
		/**op property write
		 *AST seperated by ,  
		 */		

		int az1 = ctx.start.getStartIndex();
		int bz1 = ctx.stop.getStopIndex();
		Interval interval1 = new Interval(az1+1,az+1);
		Interval interval2 = new Interval(az1-1,az-1);
		String adj1 = android.start.getInputStream().getText(interval1);
		String adj2 = android.start.getInputStream().getText(interval2);

		Interval srcItv = ctx.getSourceInterval();
		Iterator<Interval> it = tokens.keySet().iterator();
		//	System.out.println("java srcItv    "+ ctx.getText()  +"    "+srcItv+" "+tokens);
		/*
		if(!context.contains("=")){

			ssnodes.add(context);

			if( left.contains(context)){
				op_a_nodes.add(new OpProperty(ctx, context, "left_assign"));
				System.out.println("llll"+type);
			}
			else if(right.contains(context)){
				op_a_nodes.add(new OpProperty(ctx, context, "right_assign"));
			}

			//			else 

			else{
				op_a_nodes.add(new OpProperty(ctx, context));
			}

			//			tssnodes.add(ctx);


		}
		 */
		//		if(ctx.getText().startsWith("float") || ctx.getText().endsWith("var")) return;
		if(ctx.getText().startsWith("new") || ctx.getText().startsWith("=")|| ctx.getText().endsWith("=") || ctx.getText().contains("=")) return;

		if(type.equals("EnclosedContext")){
			System.out.println("left.contains"+left+" "+ctx.getText());
			if(left.contains(ctx.getText())){
				System.out.println("#####"+context);
				op_a_nodes.add(new OpProperty(ctx, context,"enclosed_outer",ctx.depth()));
			}else if(right.contains(ctx.getText())){
				op_a_nodes.add(new OpProperty(ctx, context,"enclosed_inner",ctx.depth()));
			}
		}
		
		else if(type.equals("AssignmentContext")){
			System.out.println("right"+right+"  "+ctx.getText());
			if(left.contains(ctx.getText())){
				System.out.println("AssignmentContext"+context);
				op_a_nodes.add(new OpProperty(ctx, context,"assign_left",ctx.depth()));
			}else if(right.contains(context)){
				op_a_nodes.add(new OpProperty(ctx, context,"assign_right",ctx.depth()));
			}
			else{
				op_a_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			}
		}
		else{
			op_a_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
		}
		//				}
		super.enterEveryRule(ctx);
	}

}
