package recording.java;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.antlr.runtime.RecognitionException;
import org.antlr.v4.parse.ScopeParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.tool.Attribute;
import org.antlr.v4.tool.Grammar;
import org.apache.commons.lang3.StringUtils;

import alignment.common.CommonOperators;
import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import antlr_parsers.common_old.CommonBaseListener;
import antlr_parsers.common_old.CommonLexer;
import antlr_parsers.common_old.CommonParser;
import antlr_parsers.common_old.CommonParser.AssignContext;
import antlr_parsers.common_old.CommonParser.Ast_nodeContext;
import antlr_parsers.common_old.CommonParser.ExprContext;
import antlr_parsers.pcommon.PCommonBaseListener;
import antlr_parsers.pcommon.PCommonLexer;
import antlr_parsers.pcommon.PCommonListener;
import antlr_parsers.pcommon.PCommonParser;
import antlr_parsers.pcommon.PCommonParser.ExpressionContext;
import common.CommonParser.AssignmentContext;
import common.CommonParser.EnclosedContext;
import common.CommonParser.Keyword_withContext;
import common.CommonParser.ParseContext;
import migration.StructureMappingAntlr4;
import recording.BaseSwiftRecorder;

public class SwiftListenerWithoutContext2 extends SwiftBaseListener{
	public ParserRuleContext swift;
	Interval swift_interval;
	public List<String> 	ssnodes 							= new LinkedList<String>();
	public List<OpProperty> 	op_s_nodes 						= new LinkedList<OpProperty>();
	public List<OpProperty>  	s_nodes 						= new LinkedList<OpProperty>();
	public List<OpProperty> 	common_nodes 					= new LinkedList<OpProperty>();
	public List<OpProperty> 	blacklist 						= new LinkedList<OpProperty>();
	public CharStream inputstream = null;
	public List<OpProperty> op_s_precoding = new LinkedList<OpProperty>();
	String left="", right="",rright="";
	//	String left_interval=;
	public String type="none";

	public Map<Interval, String> termsMap = new LinkedHashMap<Interval, String>();
	public Map<Interval, String> termsMapOnly = new LinkedHashMap<Interval, String>();
	public List<CommonOperators> termsMapOnlys = new LinkedList<CommonOperators>();
	Interval left_interval= new Interval(0,0);
	
	Interval right_interval;
	public String opsequence="";
	Map<Interval, String> tokens;
	public List<String> punc_swift = new LinkedList<String>();
	
	LinkedHashMap<String, Attribute> scope_list;
	public LinkedHashMap<String, List<Interval>> scope_constraint;
	
	public LineCharIndex leftIndex =null, blockIndex = null;



	public SwiftListenerWithoutContext2(ParserRuleContext swift){
		
		scope_list 					= new  	LinkedHashMap<String, Attribute>();
		scope_constraint 			= new LinkedHashMap<String, List<Interval>>();
		
		s_nodes 					= new LinkedList<OpProperty>();
		opsequence="";
		op_s_nodes 					= new LinkedList<OpProperty>();
		op_s_precoding = new LinkedList<OpProperty>();
		tokens = new HashMap<Interval, String>();
		this.swift = swift;
//		inputstream = swift.start.getInputStream();
		//		swift_interval = swift.getSourceInterval();
		int az = swift.start.getStartIndex();
		int bz = swift.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = swift.start.getInputStream().getText(interval);
		//		List<TerminalNode> 	tokens 					= new LinkedList<TerminalNode>();




		ANTLRInputStream stream2 = new ANTLRInputStream(context);


		Lexer plexer = new PCommonLexer((CharStream)stream2);
		antlr_parsers.pcommon.PCommonParser pparser	= new antlr_parsers.pcommon.PCommonParser(new CommonTokenStream(plexer));

		antlr_parsers.pcommon.PCommonParser.ParseContext proot = pparser.parse();
		ParseTreeWalker pwalker = new ParseTreeWalker();
		//		int rootd = proot.depth();
		//		Interval interval1 = new Interval(proot.start.getStartIndex(),proot.stop.getStopIndex());
		//		String context1 = proot.start.getInputStream().getText(interval1);
		//		System.out.println("proot	"+rootd+" "+context1);





		PCommonBaseListener plistener = new PCommonBaseListener(){
			int depth = 1000;
			@Override
			public void enterAssignment(PCommonParser.AssignmentContext ctx) {


				if(ctx.depth() < depth){
					type = "AssignmentContext";
					//					System.err.println("enterA");
					depth = ctx.depth();
					left = ctx.leftAssign().getText();

					rright = ctx.rightAssign().getText().replace(";", "").replace(" ", "").replace("\n", "");
					//					System.out.println("rright"+right);
					super.enterAssignment(ctx);
				}


				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 mean one lower level of paranthesis

					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
					String context = proot.start.getInputStream().getText(interval);
					OpProperty op = new OpProperty(null, context,"AssignmentContext",ctx.depth());
					op.getxt = ctx.getText();
					s_nodes.add(op);
				}
				super.enterAssignment(ctx);
			}

			@Override
			public void enterEnclosed(PCommonParser.EnclosedContext ctx) {

				if(ctx.depth() < depth && ctx.outer()!=null && ctx.inner()!=null){
					type = "EnclosedContext";
//										System.err.println("EnclosedContext");
					depth = ctx.depth();
					left = ctx.outer().getText();
					right = ctx.inner().getText();
					left_interval =  ctx.outer().getSourceInterval();
					//					right = ctx.inner().getText();
					//					System.out.println(left_interval+"rright"+right);
					//					super.enterAssignment(ctx);

					if(ctx.outer().stop!=null && ctx.outer().start!=null)
//						leftIndex = new LineCharIndex(
//								ctx.outer().start.getLine(), 
//								ctx.outer().start.getCharPositionInLine(), 
//								ctx.outer().stop.getLine(), 
//								ctx.outer().stop.getCharPositionInLine()+1);

						leftIndex = new LineCharIndex(
								ctx.outer().start.getLine(), 
								ctx.outer().start.getCharPositionInLine(), 
								ctx.outer().stop.getLine(), 
								ctx.outer().stop.getStopIndex());
						
					//					ctx.start.getLine();
					//					ctx.start.getCharPositionInLine();
					//					ctx.stop.getLine();
					//					ctx.stop.getCharPositionInLine();

				}
				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 mean one lower level of paranthesis
					//				if(ctx.depth()==6 &&!ctx.outer().getText().equals("")){ // 6 mean one lower level of paranthesis

					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
					String context = proot.start.getInputStream().getText(interval);

					//					s_nodes.add(new OpProperty(ctx, context,"EnclosedContext",ctx.depth()));

					OpProperty op = new OpProperty(null, context,"EnclosedContext",ctx.depth());
					//					op.getxt = ctx.getText();
					op.getxt = ctx.getText().replaceAll("\n", "");
					s_nodes.add(op);
					
					if(ctx.outer().stop!=null && ctx.outer().start!=null){
						blockIndex = new LineCharIndex(
								ctx.outer().start.getLine(), 
								ctx.outer().start.getCharPositionInLine(), 
								ctx.outer().stop.getLine(), 
								ctx.outer().stop.getStopIndex());
					}
				}
				super.enterEnclosed(ctx);
			}

			@Override
			public void enterExpression(ExpressionContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.depth()==6){ // 6 mean one lower level of paranthesis

					int a = ctx.start.getStartIndex();
					int b = ctx.stop.getStopIndex();
					Interval interval = new Interval(a,b);
					String context = proot.start.getInputStream().getText(interval);

					//					s_nodes.add(new OpProperty(ctx, context,"ExpressionContext",ctx.depth()));
					OpProperty op = new OpProperty(null, context,"ExpressionContext",ctx.depth());
					op.getxt = ctx.getText();
					s_nodes.add(op);
				}

				super.enterExpression(ctx);
			}
		};

		//		walker.walk(listener, root);

		pwalker.walk(plistener, proot);

		//		s_nodes = common_nodes;
		//		System.out.println("common_nodes"+common_nodes);
	}

	/**
	 * only identifiers	
	 */

	@Override
	public void visitTerminal(TerminalNode node) {
		// TODO Auto-generated method stub
		//		node.getSourceInterval();
		boolean disj = true;
		Interval sourceInterval = node.getSourceInterval();
		//		node.getSourceInterval();;
		//		swift.getStart().getInputStream().getText(arg0);
		//		Token firstToken = swift.start.getTokenSource().getInputStream().
		//		int line = firstToken.getLine();

		//		node.
		//		swift.start.getLine();
		//		node.getSymbol().getLine();
		//		node.getSymbol().getCharPositionInLine();
		//		node.getSymbol().getStartIndex();
		LineCharIndex lineindex=new LineCharIndex(node.getSymbol().getLine(), node.getSymbol().getCharPositionInLine(),
				node.getSymbol().getLine(),node.getSymbol().getStopIndex());
	
//		LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
//				ctx.getStop().getLine(),ctx.getStop().getStopIndex());
		
		termsMap.put(node.getSourceInterval(), node.getText());
		//		System.out.println("node.getText()"+node.getText());
		if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex)
				)
		{
			
			Attribute decl = scope_list.get(node.getText());
			if(decl!=null){
				List<Interval> list = scope_constraint.get(decl.toString());
				if(list!=null){
					list.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), list);

				}else{
					List<Interval> ll = new LinkedList<Interval>();
					ll.add(node.getSourceInterval());
					scope_constraint.put(decl.toString(), ll);

				}
			}	
			//		if(type.equals("EnclosedContext")&&!left_interval.disjoint(node.getSourceInterval())){
			//			System.out.println(node.getSourceInterval()+" node.getText()"+node.getText());
//			if(StructureMappingAntlr4.common_operators_list.contains(node.getText())){
//				termsMapOnly.put(node.getSourceInterval(), node.getText());
//				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
//			} 
//			else if(StructureMappingAntlr4.swift_punctuations_list.contains(node.getText())){
//				termsMapOnlys.add(new CommonOperators("PUNC", node.getSourceInterval()));
//			}
//			else{
//				termsMapOnlys.add(new CommonOperators("OTHERS", node.getSourceInterval()));
//			}
		} 

		
		if(!type.equals("EnclosedContext") && StructureMappingAntlr4.common_operators_list.contains(node.getText())){
			termsMapOnly.put(node.getSourceInterval(), node.getText());
			termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
		}
		else if(type.equals("EnclosedContext") && blockIndex!=null){
			if(lineindex.isDisjoint(blockIndex) && StructureMappingAntlr4.common_operators_list.contains(node.getText())){
				termsMapOnly.put(node.getSourceInterval(), node.getText());
				termsMapOnlys.add(new CommonOperators(node.getText(), node.getSourceInterval()));
			}
		}
//		node.getSourceInterval();
		Interval it = null;
		for(OpProperty t:op_s_nodes){
			if(t.node instanceof ParserRuleContext) it = ((ParserRuleContext)t.node).getSourceInterval();
			else if(t.node instanceof TerminalNode) it = ((TerminalNode)t.node).getSourceInterval();


			if(!it.disjoint(node.getSourceInterval())) return;
			if(t.strASTNode.equals(node.getText()) && !t.interval.equals(node.getSourceInterval()) ) {
				//				System.out.println("same_scope");
				return;
			}
			//			if(!StructureMappingAntlr4.swift_punctuations_list.contains(node.getText())){
			//				if(t.getxt.equals(node.getText())) System.out.println("aaaaaaa	"+node.getText());
			//			}
			//			if(!it.disjoint(node.getSourceInterval())) return;
			disj = it.disjoint(node.getSourceInterval()) & disj;
			//			System.out.println("swift contains"+t.strASTNode);
			//			t.node
			//			if(!t.strASTNode.equals(node.getText()) && !t.strASTNode.contains(node.getText())) {
			//				System.out.println("not overlapped term	"+node.getText());
			//			}
			//			if(t.interval.equals(node.getSourceInterval()))
			//				return;
		}


		//		if(!disj) return;

		if(!StructureMappingAntlr4.swift_keywords_list.contains(node.getText()) && !StructureMappingAntlr4.swift_punctuations_list.contains(node.getText())){
			//		if(disj){
			//			System.out.println("not overlapped term	"+node.getText());
			//			op_a_nodes.add(new OpProperty(node, node.getText(),""));

			if(type.equals("EnclosedContext")){
				//				System.out.println("left.contains"+left+" "+ctx.getText());
				if(left.contains(node.getText())){
					//					System.out.println("#####"+context);
					op_s_nodes.add(new OpProperty(node, node.getText(),"enclosed_outer"));
				}else if(right.contains(node.getText())){
					op_s_nodes.add(new OpProperty(node, node.getText(),"enclosed_inner"));
				}
				else{
					op_s_nodes.add(new OpProperty(node, node.getText(),"none"));
				}

			}

			else if(type.equals("AssignmentContext")){
				//				System.out.println("right"+right+"  "+ctx.getText());
				if(left.contains(node.getText())){
					//					System.out.println("AssignmentContext"+context);
					op_s_nodes.add(new OpProperty(node, node.getText(),"assign_left"));
				}else if(rright.equals(node.getText())){
					op_s_nodes.add(new OpProperty(node, node.getText(),"assign_right"));
				}
				else{
					op_s_nodes.add(new OpProperty(node, node.getText(),"none"));
				}
			}
			else{
				op_s_nodes.add(new OpProperty(node, node.getText(),"none"));
			}


		}

		super.visitTerminal(node);
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {

		// TODO Auto-generated method stub
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context="";
		try{
			context = ctx.start.getInputStream().getText(interval);
		} catch(Exception e){
		}

//		System.out.println("t      "+context);
		/** 
		 * mapping statements, to be changed to LineCharIndex(line position)
		 */
		//if equal to .. chilren , should add line #
		for(OpProperty t:s_nodes){
			if(t.getxt.equals(ctx.getText())){
				t.node = ctx;
			}
		}

		
		
		Grammar dummy = null;
		try {
			dummy = new Grammar("grammar T; a:'a';");
		} catch (RecognitionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LinkedHashMap<String, Attribute> attributes =  new LinkedHashMap<String, Attribute>();
		//		if(dummy!=null && context!=null && ScopeParser.parseTypedArgList(null, context, dummy)!=null)
		try{
			attributes = ScopeParser.parseTypedArgList(null, context, dummy).attributes;
		}
		catch(Exception e){

		}
			for (String arg : attributes.keySet()) {
			Attribute attr = attributes.get(arg);
			if(attr!=null&& attr.name!=null &&attr.type!=null  && StringUtils.isAlphanumeric(attr.name) && StringUtils.isAlphanumeric(attr.type)){
				//				out.add(attr.decl);
				scope_list.put(attr.name, attr);
				//				System.err.println(ScopeParser.parse(null, attr.decl, ';', dummy).attributes);
				//		System.out.println("attr.decl	"+ScopeParser.parse(null, attr.decl, ';', dummy).attributes+"  "+context);
//				System.out.println("test scope swift"+attr.type+" "+attr.name);
				//				attributes_new.put(arg, attr);
				//					System.err.println((String)next[1]);
			}
		}
		

		if(ctx.getText().equals(swift.getText())) {return;}

		//goto next candidates
		for(OpProperty t:blacklist){
			if(t.interval.equals(ctx.getSourceInterval()) || t.property.equals("11enclosed{}")) {
				return;
			}
		}

		boolean disj = true;
		int idx = 0;
		for(OpProperty t:op_s_nodes){

//			disj = disj&t.interval.disjoint(ctx.getSourceInterval());
			//			System.out.println("swift contains"+t.strASTNode);
			//			if(t.strASTNode.contains(context)) return;

			if(t.interval.equals(interval)) {
				return;
			} 

			if(!t.interval.disjoint(ctx.getSourceInterval())) {
				return;
			}

			
			if(t.strASTNode.equals(context) && t.interval.disjoint(ctx.getSourceInterval())) {
				//				System.out.println("scope:"+context+" "+interval);

				LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
						ctx.getStop().getLine(),ctx.getStop().getStopIndex());
	
				
//				LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getLine()+ctx.getStart().getCharPositionInLine(),
//						ctx.getStop().getLine(),ctx.getStop().getLine()+ctx.getStop().getCharPositionInLine()-1);

//				LineCharIndex lineindex=new LineCharIndex(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
//						ctx.getStop().getLine(),ctx.getStop().getStopIndex());
				
				if(type.equals("EnclosedContext")&& leftIndex!=null && !leftIndex.isDisjoint(lineindex) && scope_list.get(context)!=null){

					t.same_scope.put(ctx.getSourceInterval(), new OpProperty(ctx, context, "none"));
					System.out.println("swft adding scoping"+ctx.getSourceInterval()+":::"+ctx.getText()+"  >>"+t);
					return;
					//					}
					//				}
				}
				//				t.same_scope.add(e);
				//			if(t.strASTNode.equals(context)|| t.node==null) {
				//				return;
			}
			
//			if(t.strASTNode.equals(ctx.getText()) && t.interval.disjoint(interval)) {
				//				System.out.println("scope:"+context+" "+interval);
//				if(type.equals("EnclosedContext")){
//					if(left.contains(ctx.getText())){
//						op_s_nodes.add(new OpProperty(ctx, context,"enclosed_outer",ctx.depth()));
//						t.same_scope.put(ctx.getSourceInterval(), new OpProperty(ctx, context, "none", ctx.depth()));
//					}
//				}
				//				t.same_scope.add(e);
				//			if(t.strASTNode.equals(context)|| t.node==null) {
				//				return;
//			}


			//			else if(t.property.equals("enclosed{}") && t.strASTNode.contains(context)){
			//				return;
			//			}
//			else if(t.property.equals("enclosed()") && t.strASTNode.contains(context)){
				//							return;
//			}
			idx++;
		}
		//		if(op_s_nodes.size() >0 && disj)


		if(context.replaceAll(" ", "").startsWith("(") && context.replaceAll(" ", "").endsWith(")")){
//			op_s_nodes.add(new OpProperty(ctx, context, "enclosed()", ctx.depth()));
//			return;
		}
		if(context.replaceAll(" ", "").startsWith("{") && context.replaceAll(" ", "").endsWith("}")){
			op_s_nodes.add(new OpProperty(ctx, context, "enclosed{}", ctx.depth()));
			return;
		}

		//		if(context.replaceAll(" ", "").startsWith("(") && context.replaceAll(" ", "").endsWith(")")){

		if(context.replaceAll(" ", "").startsWith("(") && context.replaceAll(" ", "").endsWith(")") && !right.contains(ctx.getText())){
			//			System.out.println("ttt"+swift.getText()+"  "+swift.getText().subSequence(1, swift.getText().length()-2));
			//			if(swift.getText().subSequence(1, swift.getText().length()-1).equals(ctx.getText())) return;
			//			op_s_nodes.add(new OpProperty(ctx, context, "enclosed()", ctx.depth()));
			//			return;
		}


		for(String key:StructureMappingAntlr4.swift_keywords_list){
			if(context.equals(key)) return;
		}
		for(String key:StructureMappingAntlr4.swift_punctuations_list){
			if(context.equals(key)) return;
		}




		if(type.equals("EnclosedContext")){
			if(left.contains(ctx.getText())){
				op_s_nodes.add(new OpProperty(ctx, context,"enclosed_outer",ctx.depth()));
				
			}else if(right.contains(ctx.getText())){
				//				op_s_nodes.add(new OpProperty(ctx, context,"enclosed_inner",ctx.depth()));
			}
			else{
				//				op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			}
		}

		else if(type.equals("AssignmentContext")){
			//			swift_interval.a;
			//			System.out.println("rrrright"+rright);
			if(left.contains(context)){
				//				System.out.println("AssignmentContext"+context);
				op_s_nodes.add(new OpProperty(ctx, context,"assign_left",ctx.depth()));
			}else if(rright.contains(context)){
				op_s_nodes.add(new OpProperty(ctx, context,"assign_right",ctx.depth()));
			}
			//			else if(swift.getText().equals(ctx.getText()) && !op_s_nodes.contains(context)){

			else if(!op_s_nodes.contains(context)){
				//				op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
			}
		}

		else{
			//			if(!op_s_nodes.contains(context))
			op_s_nodes.add(new OpProperty(ctx, context,"none",ctx.depth()));
		}
		//				}
		super.enterEveryRule(ctx);
	}
}
