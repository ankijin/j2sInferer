import java.util.Map;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import recording.MappingElement;



public class RuleMethodDecl extends ASTVisitor{

	Map<MappingElement, MappingElement> mappingElements;
	//			Set names = new HashSet();
	String swiftstr ="(generated Method)";

	public void setMap(Map<MappingElement, MappingElement> map){
		mappingElements = map;	
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		// TODO Auto-generated method stub
		return super.visit(node);
	}
	@Override
	public void endVisit(MethodDeclaration node) {
//		System.out.println("endVisitMethod REPLAY TEST\n");
//		System.out.println(swiftstr);
	}


	@Override
	public boolean visit(Modifier node) {
		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		//		MappingElement e = new MappingElement(node.getKeyword().toString(), node.getNodeType());
		//		record.add(e);
		swiftstr +=mappingElements.get(e);
		//		swiftstr += "public";


		swiftstr +=" ";
		return true;
	}
	MappingElement ePrimitiveType;
	public boolean visit(org.eclipse.jdt.core.dom.PrimitiveType node) {
		ePrimitiveType = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getNodeType());
		//		record.add(e);
		
		if(node.getParent().getNodeType()==31){//context: function decl
//			System.out.println("org.eclipse.jdt.core.dom.PrimitiveType");
			ePrimitiveType = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getParent().getNodeType());
//			record.add(e);	
		}
		
		
		return true;
	}
	@Override
	public void endVisit(SimpleName node) {
		// TODO Auto-generated method stub
		if(node.getParent().getNodeType()==31){
//			MappingElement e = new MappingElement(node.toString(), node.getNodeType());
			swiftstr += ") "+mappingElements.get(ePrimitiveType);
		}
		super.endVisit(node);
	}
	public boolean visit(SimpleName node) {
		if(node.getParent().getNodeType()==31){
			//			System.out.println("visitSimpleName	"+node.getParent().getNodeType());
			//			MappingElement e = new MappingElement(node.getIdentifier().toString(), node.getNodeType());
			//			record.add(e);
			swiftstr += "func "+node.getIdentifier().toString() +"(";
			//			swiftstr +=" ";
		}

		//		MappingElement e = new MappingElement(node.getIdentifier().toString(), node.getNodeType());
		//		record.add(e);
		return true;
	}
	@Override
	public boolean visit(Block node) {
		// TODO Auto-generated method stub
//		System.out.println("visit Block");
		//		node.
		swiftstr += "{";
		swiftstr +=" ";

		return true;
	}
	@Override
	public void endVisit(Block node) {
//		System.out.println("endVisit Block");
		swiftstr += "}";
		swiftstr +=" ";

	}


	//return 40@@mContentRect.left
	//	public boolean visit(org.eclipse.jdt.core.dom.QualifiedName node) {			
	//		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
	//		record.add(e);
	//		return false;
	//	}

	//return statement
	public boolean visit(org.eclipse.jdt.core.dom.ReturnStatement node) {
		//		Expression sss = node.getExpression();
		//		MappingElement e = new MappingElement("", node.getNodeType());	
//		System.out.println("ReturnStatement\n"+node.getExpression());
		Expression expr = node.getExpression();
		MappingElement e = new MappingElement(expr.toString(), expr.getNodeType());
		//			
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		swiftstr +="return "+mappingElements.get(e);
		return true;
	};

	@Override
	public boolean visit(ExpressionStatement node) {
//		System.out.println("ExpressionStatement\n"+node.toString());

		return super.visit(node);
	}


	/*
	@Override
	public boolean visit(MethodDeclaration node) {
		// TODO Auto-generated method stub
		return super.visit(node);
	}

	@Override
	public void endVisit(MethodDeclaration node) {
		// TODO Auto-generated method stub

	}

	public boolean visit(FieldDeclaration node) {
		//				SimpleName name = node.getName();
		//			System.out.println("FieldDeclaration\n");
		//			System.out.println(node.fragments());
		//			System.out.println(node.modifiers()+""+node.getType());
		return true; // do not continue 
	}

	public void endVisit(FieldDeclaration node) {
	//  System.out.println("end FieldDeclaration\n");
//		System.out.println(swiftstr);
	};

	public boolean visit(org.eclipse.jdt.core.dom.Modifier node) {
		//			System.out.println("Modifier\n"+node.getNodeType());
		if(node.getKeyword().toString().equals("final")){
			return false;
		}

		MappingElement e = new MappingElement(node.getKeyword().toString(), node.getNodeType());
		//			
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;	
	};

	public boolean visit(org.eclipse.jdt.core.dom.PrimitiveType node) {
		MappingElement e = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getNodeType());
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	}


	@Override
	public boolean visit(QualifiedName node) {
		// TODO Auto-generated method stub

		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		//			System.out.println(e);
		//			System.out.println(mappingElements.get(e));
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	}

	public boolean visit(SimpleName node) {

		swiftstr +=node.getIdentifier();
		if(node.getParent().getNodeType()!=43)
			swiftstr +="=";
		else{
			swiftstr +=" ";
		}

		return true;
	}

	@Override
	public boolean visit(ArrayCreation node) {
		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	}


	public boolean visit(org.eclipse.jdt.core.dom.ClassInstanceCreation node) {

		MappingElement e = new MappingElement(node.toString(), node.getNodeType());
		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";
		return false;
	};



	public boolean visit(org.eclipse.jdt.core.dom.Assignment node) {
		//				System.out.println("Assignment\n");
		return true;
	}


	public void endVisit(VariableDeclarationFragment node) {
		//				System.out.println("END VariableDeclarationFragment\n");
		//				System.out.println(node.getExtraDimensions());
		//				System.out.println(node.getInitializer());
		//				swiftstr +="=";

	}

	public boolean visit(org.eclipse.jdt.core.dom.VariableDeclarationExpression node) {
		//				System.out.println("END VariableDeclarationExpression\n");
		return true;
	}

	public boolean visit(org.eclipse.jdt.core.dom.NumberLiteral node) {
		MappingElement e = new MappingElement(node.getToken().toString(), node.getNodeType());
		//				System.out.println("NumberLiteral"+mapping.get(node.getToken().toString()));
		//				System.out.println(node.getToken().toString()+node.getFlags());
		//			System.err.println(mappingElements);

		swiftstr +=mappingElements.get(e);
		swiftstr +=" ";


		return true;
	}
	 */
}
