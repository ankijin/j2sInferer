package treeTemplate;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import libcore.java.lang.reflect.MethodTest.MethodTestHelper;

public class TestingMethodDecl {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		ASTParser parser = ASTParser.newParser(AST.JLS8);

		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();


		parser.setResolveBindings(true);
		//		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		parser.setBindingsRecovery(true);

		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);

		//		String unitName = "XAxisRenderer.java";
		//		System.out.println("F	"+file.getName());
//		parser.setUnitName(name);

		String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/testsrc" }; 
		String[] classpath = {"/Users/kijin/Desktop/android-13.jar","/Users/kijin/Downloads/android.jar","/Users/kijin/Desktop/android-17-api.jar","/Users/kijin/Downloads/android_2.jar"  };

		String str = "class Test{public void methodA(){int a = 1;}}";
		
		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
		parser.setSource(str.toCharArray());
		
		CompilationUnit method = (CompilationUnit) parser.createAST(null);
		
		System.out.println(method.toString()+" "+method.getNodeType());
		method.accept(new ASTVisitor() {
			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
				System.out.println("MethodDeclaration"+node);
				return super.visit(node);
			}

		});
		
	}

}
