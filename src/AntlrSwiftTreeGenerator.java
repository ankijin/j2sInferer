
import com.github.gumtreediff.gen.TreeGenerator;
import com.github.gumtreediff.gen.antlr.AbstractAntlrTreeGenerator;
//import com.github.gumtreediff.gen.jdt.AbstractJdtVisitor;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.TreeContext;

import gumtree_tester.SwiftBaseVisitor;
import gumtree_tester.SwiftLexer;
import gumtree_tester.SwiftParser;

import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
//import org.antlr.v4.runtime.tree.
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.IOException;
import java.io.Reader;
import java.util.Deque;
import java.util.ArrayDeque;

public class AntlrSwiftTreeGenerator extends AbstractAntlrTreeGenerator{	

	
	
/*
    @Override

    protected CommonTree getStartSymbol(Reader r) throws RecognitionException, IOException {
    	ANTLRInputStream stream = new ANTLRInputStream(r);
    	//ANTLRStringStream stream = new ANTLRReaderStream(r);

        Lexer lexer = new SwiftLexer((CharStream)stream);
        parser = new SwiftParser(new CommonTokenStream(lexer));
        
        
      //  parser.
   //    ParseTree tree = parser.top
    //    CommonTree aaa = getSubtrees(tree);
        ParseTree top =parser.top_level();
        System.err.println(top.toStringTree());
     //   org.antlr.runtime.tree.ParseTree aaa = (org.antlr.runtime.tree.ParseTree) top;
     //  parser.top_level().getRuleContext();
       // System.err.println(parser.getRuleNames()[41]+parser.getRuleIndex(parser.getRuleNames()[41]));
      //  System.err.println(parser.getTokenType("SwiftParser$DeclarationContext"));
        CommonTree commonTree = new CommonTree ();
        
      //  setRecursiveDescendants(commonTree, top); 
        CommonTree ct = getSubtrees(top);
        System.out.println(ct.toStringTree());
        return ct;
    }
     
 */   

	private TreeContext context;
	SwiftParser _parser;
	SwiftBaseVisitor visitor;
	/*
	@Override
	protected TreeContext generate(Reader r) throws IOException {
		// TODO Auto-generated method stub
		
    	ANTLRInputStream stream = new ANTLRInputStream(r);

        Lexer lexer = new SwiftLexer((CharStream)stream);
     //   lexer.removeErrorListeners();
    	_parser = new SwiftParser(new CommonTokenStream(lexer));
    	
        ParseTree top =_parser.top_level();
        System.out.println(top.toStringTree(_parser));
   //    _parser.
       visitor = new SwiftBaseVisitor(_parser);
    //    System.out.println(visitor.visit(top));
    //	top.accept(visitor);
    
    //	System.out.println(visitor);
    //	visitor.visitChildren(arg0)
    //	Object aa = visitor.visit(top);
    	context = new TreeContext();
    	CommonTree commonTree = new CommonTree();
    	setRecursiveDescendants(commonTree, top);
    	System.out.println(commonTree.toStringTree());
    	//parser.visi
//        Visitor eval = new EvalVisitor();
         	
      //  eval.visit(tree);
        
        // SwiftParser.Top_levelContext aaa =parser.top_level();
       
      //  context = toTree(new TreeContext(), top, null);
 //       System.err.println("context"+context.toString());
    //    ITree t = context.createTree(aaa.getRuleIndex(), ITree.NO_LABEL, aaa.getClass().getTypeName());
    //    t.setPos( aaa.start.getLine());
    //    t.setLength(aaa.stop.getLine()-aaa.start.getLine());
    //    System.out.println(t.toShortString());
     //   int type = parser.getRuleIndex(top.
      //  		getClass().getSimpleName().toLowerCase().replace("context", ""));
     //   String name = top.getClass().getSimpleName().toLowerCase().replace("context", "");
     //   ITree tree = buildTree(top, type, name);
      
    //    context.setRoot(t);   
     //   SwiftBaseListener lst = new SwiftBaseListener();
     //   ParseTreeWalker.DEFAULT.walk(lst, top);
		return context;
	}
	*/
	
	
	/*
	   protected CommonTree getStartSymbol(Reader r) throws RecognitionException, IOException {
	    	ANTLRInputStream stream = new ANTLRInputStream(r);

	        Lexer lexer = new SwiftLexer((CharStream)stream);
	     //   lexer.removeErrorListeners();
	    	_parser = new SwiftParser(new CommonTokenStream(lexer));
	    	
	        ParseTree top =_parser.top_level();
	        System.out.println(top.toStringTree(_parser));
	   //    _parser.
	       visitor = new SwiftBaseVisitor(_parser);
	    //    System.out.println(visitor.visit(top));
	    //	top.accept(visitor);
	    
	    //	System.out.println(visitor);
	    //	visitor.visitChildren(arg0)
	    //	Object aa = visitor.visit(top);
	    	context = new TreeContext();
	    	CommonTree commonTree = new CommonTree();
	    	setRecursiveDescendants(commonTree, top);
	    	System.out.println(commonTree.toStringTree());
	    	return commonTree;
	   }
	  */ 

//	private Deque<ITree> trees = new ArrayDeque<>();
	
    private TreeContext toTree(TreeContext ctx, ParseTree n, ITree parent) {
    	
    	
        int type = _parser.getRuleIndex(n.getClass().getSimpleName().toLowerCase().replace("context", ""));
        String label = null;
       
        String typeLabel =null;
  //      label = n.getText();
        ITree t = null;
        
        int num = n.getChildCount();
        if(type ==-1){
        //	System.out.println("text "+n.getText());
        	label = n.getText();
        }
        else{
        	typeLabel = SwiftParser.ruleNames[type];
        }
        
        System.out.println(type+", "+typeLabel+", "+label+": "+ ctx.getTypeLabel(type)+"+"+n.getChildCount());

        
    //    if(n.getParent()!=null)
    //    label = n.getParent().getText();
//        if (parent != null)
//        	System.out.println(parent.toShortString());
        
      //  System.out.println(label);
       // int type = n.getClass().getModifiers();
     //   int type = 0;
      //  System.out.println(type);
//        t = ctx.createTree(type, label, typeLabel);
//        if (parent == null)
//            ctx.setRoot(t);
//        else
//            t.setParentAndUpdateChildren(parent);
        
        //int pos = n.getPosition().getStartOffset();
        //int length = n.getPosition().getEndOffset() - n.getPosition().getStartOffset();
        //t.setPos(0);
        //t.setLength(1);
    //    if(type==-1) type = 0;
      //  if(type != -1)	
        
        
        t = ctx.createTree(type, label, typeLabel);   
        
        if (parent == null)
            ctx.setRoot(t);
        else
        	t.setParent(parent);
          //  t.setParentAndUpdateChildren(parent);

        for (int i = 0; i < num; i++) {
        	toTree(ctx, n.getChild(i), t);
        	//System.err.println(n.getChild(i).getParent().getText());
        }
        
    ///    for (Node c: n.childNodes())
    //        toTree(ctx, c, t);
        return ctx;
    }
	
    
    private CommonTree setRecursiveDescendants(CommonTree commonTree,ParseTree parseTree){
        int n = parseTree.getChildCount();
        
        
        
            for (int i = 0; i < n; i++) {
         //   System.out.println(parseTree.accept(new SwiftVisitor()));
           
          //  System.out.println();
            
            String name = parseTree.getChild(i).getClass().getSimpleName().toLowerCase().replace("context", "");
            int type = _parser.getRuleIndex(parseTree.getChild(i).
            		getClass().getSimpleName().toLowerCase().replace("context", ""));
            String label = null;
          //  String		typeLabel = SwiftParser.ruleNames[type];
            	if(type ==-1){
                //	System.out.println("text "+n.getText());
                	label = parseTree.getText();
                }
            	org.antlr.runtime.CommonToken token = new org.antlr.runtime.CommonToken(type, label);
            	CommonTree root = new CommonTree(token);
            	root.parent = commonTree;
                commonTree.insertChild(i, root);
                setRecursiveDescendants(commonTree, parseTree.getChild(i));
            }
            return commonTree;
    }


	@Override
	protected CommonTree getStartSymbol(Reader arg0) throws RecognitionException, IOException {
    	ANTLRInputStream stream = new ANTLRInputStream(arg0);

        Lexer lexer = new SwiftLexer((CharStream)stream);
     //   lexer.removeErrorListeners();
    	_parser = new SwiftParser(new CommonTokenStream(lexer));
    	
        ParseTree top =_parser.top_level();
      //  System.out.println(top.toStringTree(_parser));
   //    _parser.
       visitor = new SwiftBaseVisitor(_parser);
    //    System.out.println(visitor.visit(top));
    	top.accept(visitor);
      context = toTree(new TreeContext(), top, null);
    //	System.out.println(visitor);
    //	visitor.visitChildren(arg0)
    //	Object aa = visitor.visit(top);
    	context = new TreeContext();
    	CommonTree commonTree = new CommonTree();
    	setRecursiveDescendants(commonTree, top);
    	//System.out.println(commonTree.toStringTree());
    	return commonTree;
	}


	@Override
	protected String[] getTokenNames() {
		// TODO Auto-generated method stub
		return SwiftParser.tokenNames;
	}


	
		

}
