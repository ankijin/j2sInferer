package gumtree_tester;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;

import com.github.gumtreediff.actions.ActionGenerator;
import com.github.gumtreediff.client.Run;
import com.github.gumtreediff.gen.Generators;
import com.github.gumtreediff.matchers.Matcher;
import com.github.gumtreediff.matchers.Matchers;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.Tree;
import com.github.gumtreediff.tree.TreeContext;
import com.github.gumtreediff.actions.model.Action;
import com.github.gumtreediff.actions.model.Addition;
import com.github.gumtreediff.actions.model.Insert;


public class TreeDiffJava {
	Map<ITree, ITree> parentMap = new HashMap<ITree, ITree>();
	/*	
	INS 	26@@
				40@@android.location.Location
			to 15@@ at 1

	INS 	26@@
				40@@android.location.LocationListener
			to 15@@ at 2

	INS 	26@@
				40@@android.location.LocationManager
			to 15@@ at 3
	 */

	/*
	 * 15 CompilationUnit 0 274
26 ImportDeclaration 0 48
40 QualifiedName android.support.v7.app.AppCompatActivity7 40
26 ImportDeclaration 49 25
40 QualifiedName android.os.Bundle56 17
55 TypeDeclaration 77 195
83 Modifier public77 6
42 SimpleName MainActivity90 12
43 SimpleType AppCompatActivity111 17
42 SimpleName AppCompatActivity111 17
31 MethodDeclaration 133 137
83 Modifier protected133 9
39 PrimitiveType void143 4
42 SimpleName onCreate148 8
44 SingleVariableDeclaration 157 25
43 SimpleType Bundle157 6
42 SimpleName Bundle157 6
42 SimpleName savedInstanceState164 18
8 Block 184 86
21 ExpressionStatement 188 35
48 SuperMethodInvocation 188 34
42 SimpleName onCreate194 8
42 SimpleName savedInstanceState203 18
21 ExpressionStatement 226 40
32 MethodInvocation 226 39
42 SimpleName setContentView226 14
40 QualifiedName R.layout.activity_main2241 23
15 CompilationUnit 0 818
26 ImportDeclaration 0 48
40 QualifiedName android.support.v7.app.AppCompatActivity7 40
26 ImportDeclaration 49 33
40 QualifiedName android.location.Location56 25
26 ImportDeclaration 83 41
40 QualifiedName android.location.LocationListener90 33
26 ImportDeclaration 125 40
40 QualifiedName android.location.LocationManager132 32
26 ImportDeclaration 167 25
40 QualifiedName android.os.Bundle174 17
55 TypeDeclaration 194 624
83 Modifier public194 6
42 SimpleName MainActivity207 12
43 SimpleType AppCompatActivity228 17
42 SimpleName AppCompatActivity228 17
23 FieldDeclaration 252 40
83 Modifier private252 7
43 SimpleType LocationManager260 15
42 SimpleName LocationManager260 15
59 VariableDeclarationFragment 276 15
42 SimpleName locationManager276 15
31 MethodDeclaration 297 50
83 Modifier public297 6
39 PrimitiveType void304 4
42 SimpleName onProviderEnabled309 17
44 SingleVariableDeclaration 328 15
43 SimpleType String328 6
42 SimpleName String328 6
42 SimpleName provider335 8
8 Block 345 2
31 MethodDeclaration 352 50
83 Modifier public352 6
39 PrimitiveType void359 4
42 SimpleName onProviderEnabled364 17
44 SingleVariableDeclaration 383 15
43 SimpleType String383 6
42 SimpleName String383 6
42 SimpleName provider390 8
8 Block 400 2
31 MethodDeclaration 407 75
83 Modifier public407 6
39 PrimitiveType void414 4
42 SimpleName onStatusChanged419 15
44 SingleVariableDeclaration 436 15
43 SimpleType String436 6
42 SimpleName String436 6
42 SimpleName provider443 8
44 SingleVariableDeclaration 453 10
39 PrimitiveType int453 3
42 SimpleName status457 6
44 SingleVariableDeclaration 465 13
43 SimpleType Bundle465 6
42 SimpleName Bundle465 6
42 SimpleName extras472 6
8 Block 480 2
31 MethodDeclaration 488 328
83 Modifier protected488 9
39 PrimitiveType void498 4
42 SimpleName onCreate503 8
44 SingleVariableDeclaration 513 25
43 SimpleType Bundle513 6
42 SimpleName Bundle513 6
42 SimpleName savedInstanceState520 18
8 Block 540 276
21 ExpressionStatement 550 72
7 Assignment =550 71
42 SimpleName locationManager550 15
11 CastExpression 568 53
43 SimpleType LocationManager569 15
42 SimpleName LocationManager569 15
32 MethodInvocation 586 35
42 SimpleName getSystemService586 16
42 SimpleName LOCATION_SERVICE604 16
21 ExpressionStatement 631 84
32 MethodInvocation 631 83
42 SimpleName locationManager631 15
42 SimpleName requestLocationUpdates647 22
40 QualifiedName LocationManager.GPS_PROVIDER671 28
34 NumberLiteral 500701 3
34 NumberLiteral 0706 1
52 ThisExpression 709 4
21 ExpressionStatement 724 36
48 SuperMethodInvocation 724 35
42 SimpleName onCreate730 8
42 SimpleName savedInstanceState740 18
21 ExpressionStatement 769 41
32 MethodInvocation 769 40
42 SimpleName setContentView769 14
40 QualifiedName R.layout.activity_main2785 23
	 */
	
	Map<ITree, ArrayList<ITree>> targetMap = new HashMap<ITree, ArrayList<ITree>>();

	Map<ArrayList<ITree>, ArrayList<ITree>> aa = new HashMap<ArrayList<ITree>, ArrayList<ITree>>();
	
	void compute() throws IOException{


//		ITree aSrc = new JdtTreeGenerator().generateFromFile("Empty.java").getRoot();
//		ITree aDst = new JdtTreeGenerator().generateFromFile("GPSAdded.java").getRoot();
	//	System.out.println("test"+new JdtTreeGenerator().generateFromFile("Empty2.java").getTypeLabel(55));
		ITree iosSrc = new SwiftTreeGen().generateFromFile("Empty2.swift").getRoot();
		ITree iosDrc = new SwiftTreeGen().generateFromFile("GPSAdded2.swift").getRoot();
		System.out.println("220 is "+new SwiftTreeGen().generateFromFile("GPSAdded2.swift").getTypeLabel(220));
		System.out.println("221 is "+new SwiftTreeGen().generateFromFile("GPSAdded2.swift").getTypeLabel(221));
		System.out.println("233 is "+new SwiftTreeGen().generateFromFile("GPSAdded2.swift").getTypeLabel(233));
		System.out.println("231 is "+new SwiftTreeGen().generateFromFile("GPSAdded2.swift").getTypeLabel(231));

		//		6:INS 						231@@
//		220@@
//			221@@
//				233@@CLLocationManagerDelegate
//to 231@@ at 1
		
		//Get Java Tree difference(Actions)
//    	Matcher m = Matchers.getInstance().getMatcher(aSrc, aDst); // retrieve the default matcher
//        m.match();
//        ActionGenerator g = new ActionGenerator(aSrc, aDst, m.getMappings());
//        g.generate();
//        List<Action> actionsA = g.getActions(); // return the actions


      //Get Swift Tree difference(Actions)
    	Matcher mo = Matchers.getInstance().getMatcher(iosSrc, iosDrc); // retrieve the default matcher
        mo.match();
        ActionGenerator go = new ActionGenerator(iosSrc, iosDrc,mo.getMappings());
        go.generate();
        List<Action> actionsIO = go.getActions(); // return the actions
   
     /*   
        //mapping import declaration for Locale 
        mapping((Insert)actionsA.get(0), (Insert)actionsIO.get(0));
        mapping((Insert)actionsA.get(1), (Insert)actionsIO.get(0));
        mapping((Insert)actionsA.get(2), (Insert)actionsIO.get(0));
        //mapping Listner extend, implements
        mapping((Insert)actionsA.get(6), (Insert)actionsIO.get(6));
        System.out.println("SIX"+((Insert)actionsIO.get(6)).getParent());
        //mapping LocationManager Declarationpa
        mapping((Insert)actionsA.get(7), (Insert)actionsIO.get(7));
        
        mapping((Insert)actionsA.get(8), (Insert)actionsIO.get(8));
        //mapping initialzation LocationManager
        mapping((Insert)actionsA.get(14), (Insert)actionsIO.get(18));
        //mapping setListener
        mapping((Insert)actionsA.get(15), (Insert)actionsIO.get(19));
       */ 
        System.out.println("parentMap"+parentMap);
        System.out.println("targetMap"+targetMap);
        
      //	  ASTNode aa;
      //  ASTNode.` 
       // aa.type

//    	List<Action> list = new ArrayList<Action>();
    	/*
       	List<MyAction> iosmappinglist = new ArrayList<MyAction>();
    	
       	int numA = 0;
		for (Action a:actionsA){
			if (a instanceof Insert){
				System.out.println(numA+":"+a);
				numA++;
				List<MyAction> iosMapped = getChangesFromAndroid(a);
				for(MyAction im:iosMapped){
					if(!iosmappinglist.contains(im)){
						iosmappinglist.add(im);
					}
				}
			}
		}
		*/
		int numiO = 0;
		for (Action oa:actionsIO){
			if (oa instanceof Insert){
//				System.out.println(numiO+":"+numiO);
				System.err.println(numiO+":"+oa);
				//getChangesFromAndroid(oa);
				numiO++;
			}
		}
		System.out.println("ACTIONS");
//		for(Action aa:iosmappinglist){
//			System.out.println(aa);
//			
//		}
	
				
	}
	
	
	void mapping(Action actionA, Action actioniO){
		Insert iA = (Insert) actionA;
		Insert iO = (Insert) actioniO;
		parentMap.put(iA.getParent(), iO.getParent());
		updateTargetMap(iA.getNode(), iO.getNode());
	}
	
	void mapping(Insert actionA, List<Insert> actioniOList){

		parentMap.put(actionA.getParent(), actioniOList.get(0).getParent());
		ArrayList<ITree> list = new ArrayList<ITree>();
		for(Insert a:actioniOList){
			list.add(a.getNode());
		}
		updateTargetMap(actionA.getNode(), list);
	}
	
	void updateTargetMap(ITree key, ITree item){
		ArrayList<ITree> list = targetMap.get(key);
		if(list ==null) {list = new ArrayList<ITree>();}
		list.add(item);
//		if(!targetMap.containsValue(list))
		targetMap.put(key, list);
	}
	void updateTargetMap(ITree key, ArrayList<ITree> item){
	//	ArrayList<ITree> list = targetMap.get(key);
//		if(!targetMap.containsValue(item))
		targetMap.put(key, item);
	}
	
	void makeMap(){
		TreeContext  context = new TreeContext();
		//	Tree node = new Tree(40, "android.location.Location");
		ITree node = context.createTree(40, "android.location.Location","QualifiedName");
		ITree node1 = context.createTree(26, ITree.NO_LABEL,"ImportDeclaration");
		//		ITree node2 = context.createTree(15, ITree.NO_LABEL,"CompilationUnit");
		context.setRoot(node1);
		//node2.addChild(node1);
		node1.addChild(node);
		ArrayList<ITree> list = new ArrayList<ITree>();
		targetMap.put(node1, list);
		
		//ITree node2 = context.createTree(40, "android.location.Location","QualifiedName");
		///ITree node21 = context.createTree(26, ITree.NO_LABEL,"ImportDeclaration");

		//node.setParentAndUpdateChildren(node1);
		//node1.setParentAndUpdateChildren(node2);
		//System.err.println(context.getRoot().toTreeString());

	}

	Object getChangesFromAndroid(Action androidAction){
//		List<MyAction> actions = new ArrayList<>();
		if (androidAction instanceof Insert){
//			System.out.println("test"+targetMap);
			// Addition(ITree node, ITree parent, int pos)
			Insert additionAndroid = (Insert) androidAction;
			ITree androidParent = additionAndroid.getParent();
			ITree iosParent = parentMap.get(androidParent);
			
//			if(iosParent!=null)
//			System.out.println("iosParent "+iosParent.toShortString());
			ITree androidTarget = additionAndroid.getNode();
			ArrayList<ITree> iosTargets = targetMap.get(androidTarget);
//			if(iosTargets!=null)
//			System.out.println("androidTarget "+iosTargets.size());
//			List<Action> ww;
			
			if(iosParent!=null && iosTargets!=null){
				for(int j=0;j<iosTargets.size();j++){
//				MyAction target = new MyAction(iosTargets.get(j), iosParent, 0);
//				actions.add(target);
//				System.out.println("TARGET\n"+target);
				}
			}
			return null;
		}
		return null;
	}

	ITree getImportiOS(String label){		
		return null;
	}
	public static void main(String[] args) throws UnsupportedOperationException, IOException {
		// TODO Auto-generated method stub
		TreeDiffJava diff = new TreeDiffJava();
		//		diff.makeMap();
		diff.compute();
		/**
		 *  toString of Action in GumTree
		 *  getName() + " " + node.toTreeString() + " to " + parent.toShortString() + " at " + pos;}
		 *  node.toTreeString() 
		 *  String.format("%d%s%s", getType(), SEPARATE_SYMBOL, getLabel());
		 *  
		 * name(INS, UPS, ..., )  a tree (type@@label(.. to the parent tree (tree@@label(...
		 * 
		 * 26: IMPORT_DECLARATION 
		 * 40: QUALIFIED_NAME
		 * 15: COMPILATION_UNIT
		 * .. 
INS 	26@@
			40@@android.location.Location
 to 15@@ at 1

 INSERT (IMPORT_DECLARATION (QUALIFIED_NAME android.location.Location)) to COMPILATION_UNIT line 1
====================== 3

INS 	26@@
			40@@android.location.LocationListener
 to 15@@ at 2
  INSERT (IMPORT_DECLARATION (QUALIFIED_NAME android.location.LocationListener)) to COMPILATION_UNIT line 1
====================== 5

INS 	26@@
			40@@android.location.LocationManager
 to 15@@ at 3


====================== 7
INSERT (QUALIFIED_NAME android.location.Location) to IMPORT_DECLARATION line 0
INS 		40@@android.location.Location
 to 26@@ at 0
====================== 2

INS 		40@@android.location.LocationListener
 to 26@@ at 0
 INSERT (QUALIFIED_NAME android.location.LocationListener) to IMPORT_DECLARATION line 0
====================== 4

INS 		40@@android.location.LocationManager
 to 26@@ at 0
====================== 6

INS 		23@@
				83@@private
				43@@LocationManager
					42@@LocationManager
				59@@
					42@@locationManager
 to 55@@ at 3
		 * 
		 * 		
		 */


		/*	
		Map<Integer, ArrayList<String>> test = new HashMap<Integer, ArrayList<String>> ();
		ArrayList <String> list= new ArrayList<String>();
		list.add("list1"); list.add("list2");
		test.put(1, list);
	//	test.put(1, "test2");
		System.err.println(test.get(1));
	}
		 */
	}
}
