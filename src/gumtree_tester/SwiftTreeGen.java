package gumtree_tester;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import com.github.gumtreediff.gen.TreeGenerator;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.TreeContext;

public class SwiftTreeGen extends TreeGenerator{
	//protected Map<ITree, ParserRuleContext> mappings;
	
	@SuppressWarnings("rawtypes")
	SwiftBaseVisitor  visitor;
	SwiftParser parser;
	
	@SuppressWarnings("unchecked")
	protected Map<ITree, ParserRuleContext> getMappings()
	{
		return visitor.mappings;
	}
	@Override
	protected TreeContext generate(Reader r) throws IOException {
		// TODO Auto-generated method stub
	   	ANTLRInputStream stream = new ANTLRInputStream(r);

        Lexer lexer = new SwiftLexer((CharStream)stream);
     //   lexer.removeErrorListeners();
        parser = new SwiftParser(new CommonTokenStream(lexer));
        ParseTree root =parser.top_level();
    //    ParserRuleContext root1 = parser.top_level();
     //   System.out.println("test			"+root.toStringTree(parser));
   //    _parser.
        visitor = new SwiftBaseVisitor(parser);
        
  //  	root.accept(visitor);
    	visitor.visit(root);
        //root1.accept(parser);
    //    System.out.println(visitor.visit(top));
    //	top.accept(visitor);
    
    //	System.out.println(visitor);
    //	visitor.visitChildren(arg0)
    //	Object aa = visitor.visit(top);
      //  TreeContext context = new TreeContext();
      //  System.err.println(context);
		return visitor.getTree();
	}

}
