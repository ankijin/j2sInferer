package j2swift;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import alignment.common.Filewalker;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Very basic Java to Swift syntax converter.
 * See test/Test.java and test/Test.swift for an idea of what this produces.
 * This is a work in progress...
 * @author Pat Niemeyer (pat@pat.net)
 */
public class J2Swift2
{
	public static void main( String [] args ) throws Exception
    {
		
		Filewalker fw = new Filewalker("mcharts/MPAndroidChart-master", "mcharts/Charts-master");
		Map<File, File> maps = fw.getClassMapping();
		Set<File> androidcodes = maps.keySet();
		Iterator<File> iter = androidcodes.iterator();
		InputStream is = null;
		/*
		while(iter.hasNext()){
			String javaFile = iter.next().getAbsolutePath();
	        if ( javaFile!=null ) { is = new FileInputStream(javaFile); }
	        ANTLRInputStream input = new ANTLRInputStream(is);
	        Java8Lexer lexer = new Java8Lexer(input);
	        CommonTokenStream tokens = new CommonTokenStream(lexer);
	        Java8Parser parser = new Java8Parser(tokens);
	        ParseTree tree = parser.compilationUnit();
	        ParseTreeWalker walker = new ParseTreeWalker();
	        J2SwiftListener swiftListener = new J2SwiftListener(tokens);
	        walker.walk(swiftListener, tree);
	        System.out.println( swiftListener.rewriter.getText() );
	        break;
		}
		*/
		
		String protocol = "";
//		String javaFile = "/Users/kijin/Documents/workspace_mars/GumTreeTester"
//				+ "/testingFileName/grammarTest/Test_J2Swift2.java";
//		String javaFile = "/Users/kijin/Documents/workspace_mars/GumTreeTester"
//				+ "/testingFileName/android/TestingM1.java";
		String javaFile = "test1_0.java";
        if ( javaFile!=null ) { is = new FileInputStream(javaFile); }
        ANTLRInputStream input = new ANTLRInputStream(is);
        Java8Lexer lexer = new Java8Lexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Java8Parser parser = new Java8Parser(tokens);
        ParseTree tree = parser.compilationUnit();
        parser.getTokenStream();
        
        ParseTreeWalker walker = new ParseTreeWalker();
        J2SwiftListener_c swiftListener = new J2SwiftListener_c(tokens);
        System.out.println("before"+swiftListener.rewriter.getText() );
        System.out.println( swiftListener.rewriter.getText() );
        System.err.println( swiftListener.rewriter2.getText() );
        walker.walk(swiftListener, tree);

        // This boilerplate largely from the ANTLR example
        
	}
}
