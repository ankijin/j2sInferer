package antlr_parsers.pcommon;

// Generated from PCommon.g4 by ANTLR 4.6.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PCommonParser}.
 */
public interface PCommonListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PCommonParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(PCommonParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(PCommonParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(PCommonParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(PCommonParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(PCommonParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(PCommonParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(PCommonParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(PCommonParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#leftAssign}.
	 * @param ctx the parse tree
	 */
	void enterLeftAssign(PCommonParser.LeftAssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#leftAssign}.
	 * @param ctx the parse tree
	 */
	void exitLeftAssign(PCommonParser.LeftAssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#rightAssign}.
	 * @param ctx the parse tree
	 */
	void enterRightAssign(PCommonParser.RightAssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#rightAssign}.
	 * @param ctx the parse tree
	 */
	void exitRightAssign(PCommonParser.RightAssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#enclosed}.
	 * @param ctx the parse tree
	 */
	void enterEnclosed(PCommonParser.EnclosedContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#enclosed}.
	 * @param ctx the parse tree
	 */
	void exitEnclosed(PCommonParser.EnclosedContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#outer}.
	 * @param ctx the parse tree
	 */
	void enterOuter(PCommonParser.OuterContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#outer}.
	 * @param ctx the parse tree
	 */
	void exitOuter(PCommonParser.OuterContext ctx);
	/**
	 * Enter a parse tree produced by {@link PCommonParser#inner}.
	 * @param ctx the parse tree
	 */
	void enterInner(PCommonParser.InnerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PCommonParser#inner}.
	 * @param ctx the parse tree
	 */
	void exitInner(PCommonParser.InnerContext ctx);
}