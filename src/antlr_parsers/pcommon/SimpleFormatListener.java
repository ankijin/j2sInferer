package antlr_parsers.pcommon;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import alignment.common.LineCharIndex;
import alignment.common.OpProperty;
import antlr_parsers.pcommon.PCommonParser.ExpressionContext;

public class SimpleFormatListener extends PCommonBaseListener{
	CommonTokenStream tokens;
	TokenStreamRewriter rewriter;
	int depth = 1000;
	public SimpleFormatListener( CommonTokenStream tokens )
	{
		this.tokens = tokens;
		this.rewriter = new TokenStreamRewriter( tokens );
	}




	@Override
	public void enterEnclosed(PCommonParser.EnclosedContext ctx) {


		//out most block
//		System.err.println("EnclosedContext\n"+ctx.getText()+"  \n\n"+ctx.depth());
		// TODO Auto-generated method stub
		if(ctx.depth()==3){ // 6 mean one lower level of paranthesis
			//				if(ctx.depth()==6 &&!ctx.outer().getText().equals("")){ // 6 mean one lower level of paranthesis

//			System.err.println("EnclosedContext\n"+ctx.getText());
			depth = ctx.depth();
			int a = ctx.inner().start.getStartIndex();
			int b = ctx.inner().stop.getStopIndex();
			Interval interval = new Interval(a,b);
			int max =ctx.inner().getText().length();
			if(max>5)
				max = Math.min(5, ctx.inner().getText().length());
//			replace(ctx.inner(), "\n//"+ctx.inner().getStart().getInputStream().getText(interval).substring(0, max).replaceAll("\n", " ")+"...\n");

//			replace(ctx.inner(), (ctx.inner().getStart().getInputStream().getText(interval).substring(0, max)+"...").replaceAll("\n", ""));
			replace(ctx.inner(), "int a;");


		}
		super.enterEnclosed(ctx);
	}


	private void replace( ParserRuleContext ctx, String s ) {
		rewriter.replace( ctx.start, ctx.stop, s );
	}

	//		walker.walk(listener, root);

	//	pwalker.walk(plistener, proot);
}
