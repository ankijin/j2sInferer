grammar PCommon;		
parse : statement* EOF
 ;


statement : 
    ('\n')+|(enclosed |assignment |expression) 
 ;

expression 
 : (Identifier|String)+
 ;
assignment
 : leftAssign '=' rightAssign
 ;


leftAssign :
 Identifier+ 
;

rightAssign :
 (Identifier|String)+
;

enclosed
 : outer ('\n')* '{' inner '}' 
 ;


outer : (Identifier|'=')*
 ;
inner : statement*
 ;
Identifier
 : [\][@]*[a-zA-Z_0-9()->,:.?&;!%|"'/^#~]+('['|']')*
 ;


WS  :  [ \t\r\u000C]+ -> channel(HIDDEN)
    ;


String
 : ["] (~["\r\n] | '\\\\' | '\\"')* ["]
 | ['] (~['\r\n] | '\\\\' | '\\\'')* [']
 ;


COMMENT
    :   '/*' .*? '*/' -> channel(HIDDEN)
    ;


LINE_COMMENT
    :   '//' ~[\r\n]*  -> channel(HIDDEN)
    ;
 