package antlr_parsers.common_old;
// Generated from Common.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CommonParser}.
 */
public interface CommonListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CommonParser#ast_node}.
	 * @param ctx the parse tree
	 */
	void enterAst_node(CommonParser.Ast_nodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#ast_node}.
	 * @param ctx the parse tree
	 */
	void exitAst_node(CommonParser.Ast_nodeContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#none}.
	 * @param ctx the parse tree
	 */
	void enterNone(CommonParser.NoneContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#none}.
	 * @param ctx the parse tree
	 */
	void exitNone(CommonParser.NoneContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#enclosed_p}.
	 * @param ctx the parse tree
	 */
	void enterEnclosed_p(CommonParser.Enclosed_pContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#enclosed_p}.
	 * @param ctx the parse tree
	 */
	void exitEnclosed_p(CommonParser.Enclosed_pContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#enclosed_rb}.
	 * @param ctx the parse tree
	 */
	void enterEnclosed_rb(CommonParser.Enclosed_rbContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#enclosed_rb}.
	 * @param ctx the parse tree
	 */
	void exitEnclosed_rb(CommonParser.Enclosed_rbContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#fun_call}.
	 * @param ctx the parse tree
	 */
	void enterFun_call(CommonParser.Fun_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#fun_call}.
	 * @param ctx the parse tree
	 */
	void exitFun_call(CommonParser.Fun_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#colon_list}.
	 * @param ctx the parse tree
	 */
	void enterColon_list(CommonParser.Colon_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#colon_list}.
	 * @param ctx the parse tree
	 */
	void exitColon_list(CommonParser.Colon_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#seperted_colon}.
	 * @param ctx the parse tree
	 */
	void enterSeperted_colon(CommonParser.Seperted_colonContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#seperted_colon}.
	 * @param ctx the parse tree
	 */
	void exitSeperted_colon(CommonParser.Seperted_colonContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(CommonParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(CommonParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#left_assign}.
	 * @param ctx the parse tree
	 */
	void enterLeft_assign(CommonParser.Left_assignContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#left_assign}.
	 * @param ctx the parse tree
	 */
	void exitLeft_assign(CommonParser.Left_assignContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#right_assign}.
	 * @param ctx the parse tree
	 */
	void enterRight_assign(CommonParser.Right_assignContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#right_assign}.
	 * @param ctx the parse tree
	 */
	void exitRight_assign(CommonParser.Right_assignContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(CommonParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(CommonParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#mul}.
	 * @param ctx the parse tree
	 */
	void enterMul(CommonParser.MulContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#mul}.
	 * @param ctx the parse tree
	 */
	void exitMul(CommonParser.MulContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#div}.
	 * @param ctx the parse tree
	 */
	void enterDiv(CommonParser.DivContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#div}.
	 * @param ctx the parse tree
	 */
	void exitDiv(CommonParser.DivContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#plus}.
	 * @param ctx the parse tree
	 */
	void enterPlus(CommonParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#plus}.
	 * @param ctx the parse tree
	 */
	void exitPlus(CommonParser.PlusContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#minus}.
	 * @param ctx the parse tree
	 */
	void enterMinus(CommonParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#minus}.
	 * @param ctx the parse tree
	 */
	void exitMinus(CommonParser.MinusContext ctx);
	/**
	 * Enter a parse tree produced by {@link CommonParser#equal}.
	 * @param ctx the parse tree
	 */
	void enterEqual(CommonParser.EqualContext ctx);
	/**
	 * Exit a parse tree produced by {@link CommonParser#equal}.
	 * @param ctx the parse tree
	 */
	void exitEqual(CommonParser.EqualContext ctx);
}