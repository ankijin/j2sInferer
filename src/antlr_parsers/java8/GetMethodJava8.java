package antlr_parsers.java8;

import java.io.IOException;

//set path antlr4
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;

import antlr_parsers.java8.Java8Parser.MethodDeclarationContext;
import antlr_parsers.java8.Java8Parser.MethodDeclaratorContext;

public class GetMethodJava8 extends Java8BaseVisitor{
	static Interval interval;
	private String str;
	static String methodName;

	public GetMethodJava8(String name, String file) throws IOException{
		methodName = name;

		ANTLRFileStream input = new ANTLRFileStream(file);
		Java8Lexer lexer = new Java8Lexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParserRuleContext tree = parser.compilationUnit();
		visit(tree);
	}
	public String getResult(){
		return str;
	}
	
	private boolean existMethod(String name, MethodDeclarationContext ctx){
		for(int i =0; i< ctx.getChildCount();i++){
			if(ctx.getChild(i) instanceof Java8Parser.MethodHeaderContext){
				for(int j=0; j<ctx.getChild(i).getChildCount();j++){
					if(ctx.getChild(i).getChild(j) instanceof Java8Parser.MethodDeclaratorContext){
						Java8Parser.MethodDeclaratorContext m = (MethodDeclaratorContext) ctx.getChild(i).getChild(j);
						return m.Identifier().toString().equals(name);
					}
				}
			}

		}

		return false;
	}


	private String getMethodBodyStream(MethodDeclarationContext ctx){
		for(int i =0; i< ctx.getChildCount();i++){


			if(ctx.getChild(i) instanceof Java8Parser.MethodBodyContext){
				for(int j=0; j<ctx.getChild(i).getChildCount();j++){
					if(ctx.getChild(i).getChild(j) instanceof Java8Parser.BlockContext){
						//	System.out.println("method		"+ctx.getChild(i).getChild(j).getClass());
						Java8Parser.BlockContext ct = (Java8Parser.BlockContext) ctx.getChild(i).getChild(j);
						int a = ct.start.getStartIndex();
						int b = ct.stop.getStopIndex();
						interval = new Interval(a,b);
						//	System.err.println(interval);
						//	System.err.println(ct.getText());
						return ct.getText();

					}
				}
				return null;
			}

		}
		return null;
	}

	private Interval getMethodBody(MethodDeclarationContext ctx){
		for(int i =0; i< ctx.getChildCount();i++){


			if(ctx.getChild(i) instanceof Java8Parser.MethodBodyContext){
				for(int j=0; j<ctx.getChild(i).getChildCount();j++){
					if(ctx.getChild(i).getChild(j) instanceof Java8Parser.BlockContext){
						//		System.out.println("method		"+ctx.getChild(i).getChild(j).getClass());
						Java8Parser.BlockContext ct = (Java8Parser.BlockContext) ctx.getChild(i).getChild(j);
						int a = ct.start.getStartIndex();
						int b = ct.stop.getStopIndex();
						interval = new Interval(a,b);
						//	System.err.println(interval);
						// System.err.println(ct.getText());
						return interval;

					}
				}

				return interval;
			}

		}
		return null;
	}
	@Override
	public Object visitMethodDeclaration(MethodDeclarationContext ctx) {

		if(existMethod(methodName, ctx)){
			interval= getMethodBody(ctx);
			str = getMethodBodyStream(ctx);
		}
		return super.visitMethodDeclaration(ctx);
	}


	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		ANTLRFileStream input = new ANTLRFileStream("GetFaceService.java");
		Java8Lexer lexer = new Java8Lexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
		ParserRuleContext root = parser.compilationUnit();
		System.out.println(root.getText());
		System.out.println(root.toStringTree(parser));
	
		GetMethodJava8 visitor = new GetMethodJava8("run", "GetFaceService.java");
		System.out.println(visitor.getResult());

	}

}
