package antlr_parsers.java8;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.stringtemplate.v4.ST;

import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaParser;
import antlr_parsers.javaparser.JavaParser.CompilationUnitContext;
import antlr_parsers.javaparser.JavaParser.ModifierContext;
import j2swift.Java8Parser.ExpressionContext;
import j2swift.Java8Parser.MethodDeclarationContext;
import templates.MatchingTemplate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Very basic Java to Swift syntax converter.
 * See test/Test.java and test/Test.swift for an idea of what this produces.
 * This is a work in progress...
 *
 * TODO:
 *  Class (static) method invocations should be qualified.
 *  Method and fields with the same name should be disambiguated.
 *  Final vars should become "let" vars.
 *
 * @author Pat Niemeyer (pat@pat.net)
 */
public class Java2SwiftMigrationListener extends JavaBaseListener
{
	CommonTokenStream tokens;
	TokenStreamRewriter rewriter;
	public TokenStreamRewriter rewriter2;

	boolean inConstructor;
	int formalParameterPosition;

	// Some basic type mappings
	static Map<String,String> typeMap = new HashMap<>();
	static {
		/*
        typeMap.put("float", "Float");
        typeMap.put("Float", "Float");
        typeMap.put("int", "Int");
        typeMap.put("Integer", "Int");
        typeMap.put("long", "Int64");
        typeMap.put("Long", "Int64");
        typeMap.put("boolean", "Bool");
        typeMap.put("Boolean", "Bool");
        typeMap.put("Map", "Dictionary");
        typeMap.put("HashSet", "Set");
        typeMap.put("HashMap", "Dictionary");
        typeMap.put("List", "Array");
        typeMap.put("ArrayList", "Array");
		 */
	}

	// Some basic modifier mappings (others in context)
	static Map<String,String> modifierMap = new HashMap<>();
	static {

		//modifierMap.put("protected", "internalinternalinternal");
		//modifierMap.put("volatile", "/*volatile*/");"

	}

	public Java2SwiftMigrationListener( CommonTokenStream tokens )
	{
//		System.out.println("Java2SwiftMigrationListener");
		this.tokens = tokens;
		this.rewriter = new TokenStreamRewriter( tokens );
//		rewriter.
		this.rewriter2 = new TokenStreamRewriter( tokens );
	
	}
	/*
	@Override
	public void exitCompilationUnit(CompilationUnitContext ctx) {
		// TODO Auto-generated method stub
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = ctx.start.getInputStream().getText(interval);
//		System.out.println("final"+rewriter.getText(interval));
		
		super.exitCompilationUnit(ctx);
	}

	@Override
	public void enterModifier(ModifierContext ctx) {
		// TODO Auto-generated method stub
		System.err.println("enterModifier");
		
		super.enterModifier(ctx);
	}
	*/
	@Override
	public void exitModifier(ModifierContext ctx) {
		// TODO Auto-generated method stub
		System.err.println("exitModifier");
		replace(ctx,"prrrrrivate");
		super.exitModifier(ctx);
	}
	/*
	@Override
	public void exitMethodDeclaration(javaparser.JavaParser.MethodDeclarationContext ctx) {
		// TODO Auto-generated method stub
		System.out.println("exitMethodDeclaration");
		replace(ctx, ctx.getText()+"#######");
		super.exitMethodDeclaration(ctx);
	}
	*/
	public void enterEveryRule(ParserRuleContext ctx) {
//		System.out.println("every"+ctx.getRuleContext().getText());
		/*
		if(ctx.getRuleIndex() == 57){


			//            unannType = ctx.unannType();
			int az = ctx.start.getStartIndex();
			int bz = ctx.stop.getStopIndex();
			Interval interval = new Interval(az,bz);
			String context = ctx.start.getInputStream().getText(interval);
			if(context.equals("new RectF()")){
				replace2(ctx, "CGRect()");
//				delete(ctx);
				System.err.println("ctx.getRuleIndex() == 57"+ctx.getText());
			}
		}
		*/
	};

	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
		// TODO Auto-generated method stub
		
		int az = ctx.start.getStartIndex();
		int bz = ctx.stop.getStopIndex();
		Interval interval = new Interval(az,bz);
		String context = ctx.start.getInputStream().getText(interval);
		/**
		 * new <arg1>, <arg1>
		 */
		if(context.equals("new RectF()")){
			System.out.println(ctx.getRuleIndex()+"wwww");
		}
		if(ctx.getRuleIndex() == 35){
			//0f, 1f, 2f,...
			Map<String, String> argmap = MatchingTemplate.getAttributesMap("<arg0>f", getText(ctx));
			ST st_android = new ST("CGFloat(<arg0>.0)");
			Iterator<String> iterator = argmap.keySet().iterator();
			System.out.println("argmap"+argmap);
			while(iterator.hasNext()){
				String key=iterator.next();
				st_android.add(key, argmap.get(key));
//				st_android2.add(key, argmap.get(key));
			}
			replace( ctx, st_android.render());
		}
		if(ctx.getRuleIndex() == 19){
//			replace(ctx, "//test\n"+ctx.getText());
			
			Map<String, String> argmap = MatchingTemplate.getAttributesMap("<arg0> <arg1> <arg2>=<arg3>;", getText(ctx));
			Iterator<String> iterator = argmap.keySet().iterator();
			ST st_android = new ST("<arg0> var <arg2> = <arg3>");
			ST st_android2 = new ST("<arg0> var <arg2>:<arg1> = <arg3>");

			while(iterator.hasNext()){
				String key=iterator.next();
				st_android.add(key, argmap.get(key));
				st_android2.add(key, argmap.get(key));
			}

			System.out.println("st_android"+st_android.render()+"//generated"+st_android.render()+"  "+getText(ctx));

			System.out.println(MatchingTemplate.getAttributesMap("<arg0> <arg1> <arg2>=<arg3>;", context)+" "+context.indexOf("0f")+" "+context.indexOf("new RectF()"));
			//<arg0> var <arg2> = <arg3> 
			
			replace( ctx, st_android.render()+"	\n//generated\n"+st_android2.render()+"\n"+"return CGPoint(x: _contentRect.origin.x + _contentRect.size.width / 2.0, y: _contentRect.origin.y + _contentRect.size.height / 2.0)");
//			replace( ctx, getText(ctx));
		}
		super.exitEveryRule(ctx);
	}

	
	//
	private void deleteFirst( ParserRuleContext ctx, int token ) {
		List<TerminalNode> tokens = ctx.getTokens( token );
		rewriter.delete( tokens.get(0).getSymbol().getTokenIndex() );
	}
	private void replaceFirst( ParserRuleContext ctx, int token, String str ) {
		List<TerminalNode> tokens = ctx.getTokens( token );
		if ( tokens == null || tokens.isEmpty() ) { return; }
		rewriter.replace( tokens.get( 0 ).getSymbol().getTokenIndex(), str );
	}


	// Get possibly rewritten text
	private String getText( ParserRuleContext ctx ) {
		if ( ctx == null ) { return ""; }
		return rewriter.getText( new Interval( ctx.start.getTokenIndex(), ctx.stop.getTokenIndex() ) );
	}

	private void replace( ParserRuleContext ctx, String s ) {
		rewriter.replace( ctx.start, ctx.stop, s );
	}
	private void replace2( ParserRuleContext ctx, String s ) {
		rewriter2.replace( ctx.start, ctx.stop, s );
	}

	// remove context and hidden tokens to right
	private void removeRight( ParserRuleContext ctx )
	{
		rewriter.delete( ctx.start, ctx.stop );
		List<Token> htr = tokens.getHiddenTokensToRight( ctx.stop.getTokenIndex() );
		for (Token token : htr) { rewriter.delete( token ); }
	}

	public String mapType( ParserRuleContext ctx )
	{
		//if ( ctx instanceof Java8Parser.UnannArrayTypeContext ) { }
		//String text = ctx.getText();
		String text = getText(ctx);
		return mapType( text );
	}
	public String mapType( String text )
	{
		String mapText = typeMap.get( text );
		return mapText == null ? text : mapText;
	}

	public String mapModifier( ParserRuleContext ctx )
	{
		//if ( ctx instanceof Java8Parser.UnannArrayTypeContext ) { }
		//String text = ctx.getText();
		String text = getText( ctx );
		return mapModifier( text );
	}
	public String mapModifier( String text )
	{
		String mapText = modifierMap.get( text );
		return mapText == null ? text : mapText;
	}


}

