package com.google.code.javakbest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author David Miguel Antunes <davidmiguel [ at ] antunes.net>
 */
public class Test {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		long start = System.currentTimeMillis();
		double[][] costMat = new double[10][4];
		System.err.println(costMat.length+" "+costMat[0].length);
		if(costMat.length > costMat[0].length){
			costMat = transpose(costMat);
		}
//		if(costMat.length)
		for (int l = 0; l < costMat.length; l++) {
			for (int c = 0; c < costMat[0].length; c++) {
				costMat[l][c] = Math.random();
			}
		}

		System.out.println("Cost matrix:");
		for (int l = 0; l < costMat.length; l++) {
			for (int c = 0; c < costMat[0].length; c++) {
				System.out.format("%04f\t", costMat[l][c]);
			}
			System.out.println("");
		}

		List<int[]> rowsols = Murty.solve(costMat, 4);

		System.out.println("");

		int sol = 0;
		for (int[] solution : rowsols) {
			String s = "";
			double cost = 1;
			for (int i = 0; i < solution.length; i++) {
				s += (solution[i] + "\t");
				cost *= costMat[i][solution[i]];
			}
			System.out.println("Solution " + (sol++) + " (" + cost + "):");
			System.out.println(s);
		}


		System.out.println("Total time: " + (System.currentTimeMillis() - start) + " millis");
		
		double[][] matrixData = { {1.5d,2d}, {7.3d,-13.5d}};
		//        RealMatrix m = MatrixUtils.createRealMatrix(matrixData);
		double[][] a = transpose(matrixData);

		//        int aaa = matrixData.length;

		for (int i = 0; i < a.length; i++) {
			System.err.print("[");
			for (int y = 0; y < a[0].length; y++) {
				System.err.print(a[i][y] + ",");
			}
			System.err.print("]\n");
		} 
	}

	private static void testDataset(String path) throws FileNotFoundException, IOException {

		for (int i = 0; i < 10; i++) {
			String file = path + "mat" + String.format("%03d", i) + ".m";
			String s;
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			s = br.readLine();
			int nLines = 1;
			int nCols = s.split("\\t").length;
			while ((line = br.readLine()) != null) {
				s += line;
				nLines++;
			}

			String[] cells = s.split("\\t");

			double[][] costMat = new double[nLines][nCols];

			for (int j = 0; j < cells.length; j++) {
				costMat[j / nCols][j % nCols] = Double.parseDouble(cells[j]);
			}

			String msg = "";
			msg += "== cost matrix ==" + "\n";

			for (int l = 0; l < costMat.length; l++) {
				for (int c = 0; c < costMat[0].length; c++) {
					msg += costMat[l][c] + "\t";
				}
				msg += "\n";
			}

			//        System.out.println(msg);

			List<int[]> rowsols = Murty.solve(costMat, 5);

			for (int[] rowsol : rowsols) {
				System.out.print(i + ": ");
				for (int j = 0; j < rowsol.length; j++) {
					int col = rowsol[j];
					System.out.print((col + 1) + " ");
				}
				System.out.println("");
			}

			//            System.out.format("Cost=%f", cost);

		}



		//        for(matrixData.length)
	}
	public static double[][] transpose (double[][] array) {
		  if (array == null || array.length == 0)//empty or unset array, nothing do to here
		    return array;

		  int width = array.length;
		  int height = array[0].length;

		  double[][] array_new = new double[height][width];

		  for (int x = 0; x < width; x++) {
		    for (int y = 0; y < height; y++) {
		      array_new[y][x] = array[x][y];
		    }
		  }
		  return array_new;
		}

}
