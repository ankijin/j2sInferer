package delete_astlevel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Expression;

public class ASTMapping {
	
	public void exprTest() throws FileNotFoundException, IOException{
		
        ASTParser parser = ASTParser.newParser(AST.JLS8);
        parser.setKind(ASTParser.K_COMPILATION_UNIT);
        Map pOptions = JavaCore.getOptions();
        pOptions.put(JavaCore.COMPILER_COMPLIANCE, JavaCore.VERSION_1_8);
        pOptions.put(JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM, JavaCore.VERSION_1_8);
        pOptions.put(JavaCore.COMPILER_SOURCE, JavaCore.VERSION_1_8);
        pOptions.put(JavaCore.COMPILER_DOC_COMMENT_SUPPORT, JavaCore.ENABLED);
        parser.setCompilerOptions(pOptions);
        parser.setSource(readerToCharArray(new FileReader("charts/android/ViewPortHandler_V0.java")));
//        AbstractJdtVisitor v = createVisitor();
//        parser.createAST(null).accept(v);
         ASTNode root = parser.createAST(null).getRoot();
         
         System.out.println(root);
         System.out.println();
         
//        parser.setSource(readerToCharArray(r));
//        AbstractJdtVisitor v = createVisitor();
//        parser.createAST(null).accept(v);
	}
	
    private static char[] readerToCharArray(Reader r) throws IOException {
        StringBuilder fileData = new StringBuilder(1000);
        BufferedReader br = new BufferedReader(r);

        char[] buf = new char[10];
        int numRead = 0;
        while ((numRead = br.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        br.close();

        return  fileData.toString().toCharArray();
    }
    
	public static void main(String[] args) throws FileNotFoundException, IOException{
		
		new ASTMapping().exprTest();
	}
}
