package sqldb;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.regex.Matcher;
//import org.springframework.jdbc.core.JdbcTemplate;
import java.util.regex.Pattern;

import org.stringtemplate.v4.ST;

import templates.ConTextTemplate;



public class SQLiteJDBC
{
	static Connection c;

	public static Statement getStatementSQLiteJDBC() throws ClassNotFoundException, SQLException{
		//	  Connection c = null;
		//      Class.forName("org.sqlite.JDBC");
		//      c = DriverManager.getConnection("jdbc:sqlite:test1.db");
		//      c.setAutoCommit(false);
		System.out.println("Opened database successfully");
		return c.createStatement();
	}

	public static Connection getConnection(String db) throws ClassNotFoundException, SQLException{
		Connection c = null;
		//      Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection(db);
		//      c.setAutoCommit(false);
		System.out.println("Opened database successfully");
		return c;
	}

	public static String createStmtsAndroidTable(){
		String sql = "CREATE TABLE IF NOT EXISTS stmts_android "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" STATEMENTS           TEXT  , " + 
				" TEMPLATE            TEXT  , " + 
				" PARENT_ID         INT, " +
				" UNIQUE (ID) "
				+ ")"; 

		return sql;
	}
	
	public static String insertStmtsAndroidTable(int id, String stmts, String stmts_tmp){
		String sql = "INSERT OR IGNORE INTO stmts_android (ID,STATEMENTS,TEMPLATE) "
					+"VALUES ("+id+",\'"+stmts+"\',\'"+stmts_tmp+"\');"; 
		return sql;
	}
	
	
	
	public static String createStmtsSwiftTable(){
		String sql = "CREATE TABLE IF NOT EXISTS stmts_swift "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" STATEMENTS           TEXT  , " + 
				" TEMPLATE            TEXT  , " + 
				" PARENT_ID         INT, " +
				" ANDROID      ID, " +
				" FOREIGN KEY(ANDROID) REFERENCES stmts_android(ID), "+
				" UNIQUE (ID) "
				+ ")";
		return sql;
	}
	
	public static String insertStmtsSwiftTable(int id, String stmts, String stmts_tmp, int android){
		String sql = "INSERT OR IGNORE INTO stmts_swift (ID,STATEMENTS,TEMPLATE, ANDROID) "
					+"VALUES ("+id+",\'"+stmts+"\',\'"+stmts_tmp+"\',"+android+");"; 
		return sql;
	}
	
	public static String createTable(String tableName){
		String sql = "CREATE TABLE IF NOT EXISTS " +tableName+ " "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" CLASSNAME           TEXT  , " + 
				" VARNAME           TEXT  , " + 
				" MODIFIER            TEXT  , " + 
				" TYPE        TEXT, " + 
				" INIT         TEXT, " +
				" UNIQUE (ID) "
				+ ")"; 

		return sql;
	}

	public static String createAndroidTemplateTable(){
		String sql = "CREATE TABLE IF NOT EXISTS androidTemplate "+
				"(CONTEXT TEXT PRIMARY KEY     NOT NULL," +
				" TYPE           INT    NOT NULL, " +
				" TEMPLATE           TEXT    NOT NULL, " + 
				" UNIQUE (CONTEXT, TEMPLATE) "
				+ ")";

		return sql;
	}


	public static String createAndroidTemplateTable2(){
		String sql = "CREATE TABLE IF NOT EXISTS androidTemplate2 "+
				"(CONTEXT TEXT PRIMARY KEY     NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" TEMPLATE           TEXT    NOT NULL, " + 
				" UNIQUE (CONTEXT) "
				+ ")";

		return sql;
	}


	public static String createSwiftTemplateTable(){
		String sql = "CREATE TABLE IF NOT EXISTS swiftTemplate "+
				"(CONTEXT TEXT PRIMARY KEY     NOT NULL," +
				" TEMPLATE           TEXT    NOT NULL, " + 
				" ANDROID      TEXT, " +
				" FOREIGN KEY(ANDROID) REFERENCES androidTemplate(CONTEXT), "+
				" UNIQUE (CONTEXT) "
				+ ")";

		return sql;
	}

	public static String createSwiftTemplateTable2(){
		String sql = "CREATE TABLE IF NOT EXISTS swiftTemplate2 "+
				"(CONTEXT TEXT PRIMARY KEY     NOT NULL," +
				" TEMPLATE           TEXT    NOT NULL, " + 
				" AST_TYPE           INT    NOT NULL, " +
				" ANDROID      INT NOT NULL, " +
				" FOREIGN KEY(ANDROID) REFERENCES androidTemplate(CONTEXT), "+
				" UNIQUE (CONTEXT, TEMPLATE) "
				+ ")";

		return sql;
	}

	public static String insertAndroidTemplateTable(String context, int type, String template){
		String sql = "INSERT OR IGNORE INTO androidTemplate (CONTEXT,TYPE, TEMPLATE) " +
				"VALUES ("+
				"\'"+context+"\',"+ 
				" "+type+","+ 
				"\'"+template+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}
	
	public static String insertAndroidTemplateTable2(String context, int type, String template){
		String sql = "INSERT OR IGNORE INTO androidTemplate2 (CONTEXT,AST_TYPE, TEMPLATE) " +
				"VALUES ("+
				"\'"+context+"\',"+ 
				" "+type+","+ 
				"\'"+template+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}
	

	public static String insertSwiftTemplateTable(String context, String template, String android){
		String sql = "INSERT OR IGNORE INTO swiftTemplate (CONTEXT,TEMPLATE,ANDROID) " +
				"VALUES ("+
				"\'"+context+"\',"+ 
				"\'"+template+"\',"+ 
				"\'"+android+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}
	
	public static String insertSwiftTemplateTable2(String context, int AST_TYPE, String template, int android){
		String sql = "INSERT OR IGNORE INTO swiftTemplate2 (CONTEXT,AST_TYPE, TEMPLATE,ANDROID) " +
				"VALUES ("+
				"\'"+context+"\',"+ 
				""+AST_TYPE+","+ 
				"\'"+template+"\',"+ 
				+android+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}

	public static String createTableSwift(String tableName){
		String sql = "CREATE TABLE " +tableName+ " "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" CLASSNAME           TEXT  , " + 
				" VARNAME           TEXT  , " + 
				" MODIFIER            TEXT , " + 
				" TYPE        TEXT, " + 
				" INIT         TEXT, " +
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES var_android(AID), "+
				" UNIQUE(ID, VARNAME, MODIFIER, TYPE, INIT) "
				+ ")"; 
		return sql;
	}

	//modi, type, name, value
	public static String createTableMethod(String tableName){
		String sql = "CREATE TABLE IF NOT EXISTS " +tableName+ " "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" METHODNAME           TEXT    NOT NULL, " + 
				" MODIFIER            TEXT , " + 
				" TYPE        TEXT, " + 
				" RETURN         TEXT, "+
				" UNIQUE(ID, METHODNAME, MODIFIER, TYPE, RETURN) "
				+ ")"; 

		return sql;
	}

	public static String createTableMethodParams(){
		String sql = "CREATE TABLE IF NOT EXISTS mmethod_android "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" METHODNAME           TEXT    NOT NULL, " + 
				" MODIFIER            TEXT , " + 
				" TYPE        TEXT, " + 
				" PARAMETERS        TEXT, " + 
				" STATEMENTS         TEXT, "+
				" UNIQUE(ID) "
				+ ")"; 

		return sql;
	}
	
	public static String createTableMethodParamsSwift(){
		String sql = "CREATE TABLE IF NOT EXISTS mmethod_swift "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" METHODNAME           TEXT    NOT NULL, " + 
				" MODIFIER            TEXT , " + 
				" TYPE        TEXT, " + 
				" PARAMETERS        TEXT, " + 
				" STATEMENTS         TEXT, "+
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES mmethod_android(ID), "+
				" UNIQUE(ID) "
				+ ")"; 

		return sql;
	}
	
	
	public static String createTableMethodSwift(String tableName){
		String sql = "CREATE TABLE IF NOT EXISTS " +tableName+ " "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" METHODNAME           TEXT    NOT NULL, " + 
				" MODIFIER            TEXT     NOT NULL, " + 
				" TYPE        TEXT, " + 
				" RETURN         TEXT, "+
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES method_android(ID), "+
				" UNIQUE(ID, METHODNAME, MODIFIER, TYPE, RETURN) "
				+ ")"; 

		return sql;
	}

	public static String dropTable(String tableName){
		String sql ="DROP TABLE IF EXISTS "+tableName;
		return sql;
	}
	public static String createExprTables(){
		String sql = "CREATE TABLE IF NOT EXISTS expr_android "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" TEMP_EXPR TEXT  NOT NULL," +
				" SEQ_VARS           TEXT    NOT NULL, " + 
				" SEQ_VARS_ABS           TEXT, " + 
				" UNIQUE(ID, TEMP_EXPR, SEQ_VARS) "
				+ ")"; 


		return sql;
	}
	
	public static String createStatementTablesA(){
		String sql = "CREATE TABLE IF NOT EXISTS statement_android "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" STATEMENT TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " + 
				" UNIQUE(ID, STATEMENT) "
				+ ")"; 
		return sql;
	}
	
	
	public static String creategrammarAndroid(){
		String sql = "CREATE TABLE IF NOT EXISTS grammar_android "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT    NOT NULL, " +
				" EXAMPLE TEXT  NOT NULL," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	
	public static String creategrammarAndroidStat(){
		String sql = "CREATE TABLE IF NOT EXISTS grammar_android_stat "+
//				"(ID INT PRIMARY KEY     NOT NULL," +
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT, " +
				" COUNT           INT , " +
				" EXAMPLE TEXT  NOT NULL," +
				" RFORM TEXT  ," +
				" AFILENAME TEXT," +
				" ALOC TEXT," +
				" SFILENAME TEXT," +
				" SLOC TEXT," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	public static String createBindingStat(){
		String sql = "CREATE TABLE IF NOT EXISTS grammar_binding_stat "+
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT, " +
				" COUNT           INT , " +
				" EXAMPLE TEXT  NOT NULL," +
				" AFILENAME TEXT," +
				" ALOC TEXT," +
				" SFILENAME TEXT," +
				" SLOC TEXT," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	public static String createConstantStat(){
		String sql = "CREATE TABLE IF NOT EXISTS constant_stat "+
//				"(ID INT PRIMARY KEY     NOT NULL," +
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT, " +
				" COUNT           INT , " +
				" EXAMPLE TEXT  NOT NULL," +
				" AFILENAME TEXT," +
				" ALOC TEXT," +
				" SFILENAME TEXT," +
				" SLOC TEXT," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	public static String createUnsucessStat(){
		String sql = "CREATE TABLE IF NOT EXISTS unsucess_stat "+
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT, " +
				" COUNT           INT , " +
				" EXAMPLE TEXT  NOT NULL," +
				" AFILENAME TEXT," +
				" ALOC TEXT," +
				" SFILENAME TEXT," +
				" SLOC TEXT," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	public static String createMethod(){
		String sql = "CREATE TABLE IF NOT EXISTS method_table "+
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT, " +
//				" COUNT           INT , " +
				" EXAMPLE TEXT  NOT NULL," +
				" AFILENAME TEXT," +
				" ALOC TEXT," +
				" SFILENAME TEXT," +
				" SLOC TEXT," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	public static String createBindingAndroid(){
		String sql = "CREATE TABLE IF NOT EXISTS binding_android_stat "+
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
				" TEMPLATE TEXT ," +
				" EXAMPLE TEXT  ," +
				" COUNT           INT , " +
				" VARS TEXT  ," +
				" CONSTS TEXT  ," +
				" AFILENAME TEXT," +
				" ALOC TEXT," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	public static String createBindingSwift(){
		String sql = "CREATE TABLE IF NOT EXISTS binding_swift_stat "+
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
				" FILENAME TEXT," +
				" LINE INT," +
				" COLUMN INT, " +
				" EXAMPLE TEXT," +
				" BINDING TEXT," +
				" CONSTS TEXT," +
				" TYPE TEXT," +
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
//	" VARS TEXT  ," +
//	" CONSTANTS TEXT  ," +
	public static String insertTableBindingAndroid(String template, String example, String vars, String conts, String afile, String aloc){
		String sql = "INSERT OR IGNORE INTO binding_android_stat (TEMPLATE, EXAMPLE,VARS, CONSTS, AFILENAME, ALOC) " +
				"VALUES ("+
				"\'"+template+"\',"+ 
				"\'"+example+"\',"+
				"\'"+vars+"\',"+ 
				"\'"+conts+"\',"+
				"\'"+afile+"\',"+
				"\'"+aloc+"\'"+
				");";
		return sql;
	}	
	
//	" FILENAME TEXT," +
//	" LINE INT," +
//	" COLUMN INT, " +
//	" EXAMPLE TEXT," +
//	" TYPE TEXT," +
//	" BINDING TEXT," +
	
	public static String insertTableBindingSwift(String filename, int line, int column, String example, String type, String binding, String consts){
		String sql = "INSERT OR IGNORE INTO binding_swift_stat (FILENAME, LINE,COLUMN, EXAMPLE, BINDING, CONSTS, TYPE) " +
				"VALUES ("+
				"\'"+filename+"\',"+ 
				+line+","+
				+column+","+ 
				"\'"+example+"\',"+
				"\'"+binding+"\',"+
				"\'"+consts+"\',"+
				"\'"+type+"\'"+

				
				");";
		return sql;
	}	
	
	
	public static String insertTableGrammarAndroid(int id, String template, int ast, int parent, String example){
		String sql = "INSERT OR IGNORE INTO grammar_android (ID,TEMPLATE,AST_TYPE, PARENT_TYPE, EXAMPLE) " +
				"VALUES ("+
				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+ 
				"\'"+example+"\'"+
				");";
		return sql;
	}	
	
	
	public static String updateTableGrammarAndroid(int id, String template, int ast, int parent, String example){
		String sql = "UPDATE grammar_android_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		sql+= "INSERT INTO grammar_android_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE) " +
				"select "+
//				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	
	public static String updateTableGrammarAndroid(int id, String template, int ast, int parent, String example, String rform){
		String sql = "UPDATE grammar_android_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\' and RFORM="+"\'"+rform+"\';\n"; 
		sql+= "INSERT INTO grammar_android_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, RFORM) " +
				"select "+
//				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\',"+
				"\'"+rform+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	

	
	public static String updateTableGrammarAndroid(int id, String template, int ast, int parent, String example, String rform, String afile, String sfile, String aloc, String sloc){
		String sql = "UPDATE grammar_android_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template 
				+"\' and EXAMPLE="+"\'"+example +"\' and RFORM="+"\'"+rform+"\' and AFILENAME="+"\'"+afile+"\' and SFILENAME="+"\'"+sfile+"\'"+" and ALOC="+"\'"+aloc+"\'"+" and SLOC="+"\'"+sloc+"\'"
						+ ";\n"; 
		sql+= "INSERT INTO grammar_android_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, RFORM, AFILENAME, SFILENAME, ALOC, SLOC) " +
				"select "+
//				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\',"+
				"\'"+rform+"\',"+
				"\'"+afile+"\',"+
				"\'"+sfile+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sloc+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	
	
	public static String updateTableBinding(int id, String template, int ast, int parent, String example){
		String sql = "UPDATE grammar_binding_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		sql+= "INSERT INTO grammar_binding_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE) " +
				"select "+
//				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	

/*
	public static String updateTableBinding2(int id, String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = "UPDATE grammar_binding_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		sql+= "INSERT INTO grammar_binding_stat (ID,TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
				"select "+
				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	*/
	
	public static String updateTableBinding2(int id, String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = "UPDATE grammar_binding_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		sql+= "INSERT INTO grammar_binding_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
				"select "+
//				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	
	//id auto increasement
	public static String updateTableConstant(int id, String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = "UPDATE constant_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\' and AFILENAME="+"\'"+afilename +"\' and SFILENAME="+"\'"+sfilename+"\'"
				+ ";\n"; 
		sql+= "INSERT INTO constant_stat (TEMPLATE, AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
				"select "+
//				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	


	public static String insertTableMethod(String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = 
//				"UPDATE constant_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		"INSERT OR IGNORE INTO method_table (TEMPLATE,AST_TYPE, PARENT_TYPE, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
		"VALUES ("
//				+id + ","+
				+"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
//				id+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				");";
		return sql;
	}
	
	public static String insertTableConstant(int id, String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = 
//				"UPDATE constant_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
	
		"INSERT OR IGNORE INTO constant_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
		"VALUES ("
//				+id + ","+
				+"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				id+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				");";
		return sql;
	}
	
	public static String insertTableUS(int id, String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = 
//				"UPDATE constant_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
	
		"INSERT OR IGNORE INTO unsucess_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
		"VALUES ("
//				+id + ","+
				+"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				id+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				");";
		return sql;
	}
	

	public static String insertTableUnsucess(int id, String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = 
//				"UPDATE constant_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
	
		"INSERT OR IGNORE INTO unsucess_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
		"VALUES ("
//				+id + ","+
				+"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				id+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				");";
		return sql;
	}
	
	
	
	public static String updateTableGrammarSwift2(int id, String template, int ast, int parent, String example, String afilename, String aloc, String sfilename, String sloc){
		String sql = "UPDATE grammar_swift_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		sql+= "INSERT INTO grammar_swift_stat (TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE, AFILENAME, ALOC, SFILENAME, SLOC) " +
				"select "+
//				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				id+","+ 
				"\'"+example+"\',"+
				"\'"+afilename+"\',"+
				"\'"+aloc+"\',"+
				"\'"+sfilename+"\',"+
				"\'"+sloc+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	
	public static String updateTableGrammarSwift(int id, String template, int ast, int parent, String example){
		String sql = "UPDATE grammar_swift_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT=COUNT+1"+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		sql+= "INSERT INTO grammar_swift_stat (ID,TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE) " +
				"select "+
				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				1+","+ 
				"\'"+example+"\'"+
				" where (select Changes()=0);";
		return sql;
	}	
	
	
	public static String updateTableGrammarSwiftIID(int id, String template, int ast, int parent, String example, int iid){
//		String sql = "UPDATE grammar_swift_stat SET "+ " TEMPLATE="+"\'"+template+"\', AST_TYPE="	+ast+", PARENT_TYPE="	+parent+",COUNT= "+iid+" where TEMPLATE="+"\'"+template +"\' and EXAMPLE="+"\'"+example +"\';\n"; 
		String sql = "INSERT INTO grammar_swift_stat (ID,TEMPLATE,AST_TYPE, PARENT_TYPE, COUNT, EXAMPLE) " +
				"select "+
				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+
				iid+","+ 
				"\'"+example+"\'"+
				" ;";
		return sql;
	}	
	
	
	public static String insertTableStatementAndroid(int id, String statement, int ast){
		String sql = "INSERT OR IGNORE INTO statement_android (ID,STATEMENT,AST_TYPE) " +
				"VALUES ("+
				id + ","+
				"\'"+statement+"\',"+ 
				 +ast+ 
				");";
		return sql;
	}	
	
	public static String createStatementTablesS(){
		String sql = "CREATE TABLE IF NOT EXISTS statement_swift "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" STATEMENT TEXT  NOT NULL," +
				" AST_TYPE           TEXT    NOT NULL, " + 
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES statment_android(ID), "+
				" UNIQUE(ID, STATEMENT) "
				+ ")"; 
		return sql;
	}
	
	
	public static String creategrammarSwift(){
		String sql = "CREATE TABLE IF NOT EXISTS grammar_swift "+
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"(ID INT PRIMARY KEY     NOT NULL," +
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT    NOT NULL, " +
				" EXAMPLE TEXT  NOT NULL," +
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES grammar_android(ID), "+
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	
	/*
	public static String creategrammarSwiftStat(){
		String sql = "CREATE TABLE IF NOT EXISTS grammar_swift_stat "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT    NOT NULL, " +
				" COUNT           INT    NOT NULL, " +
				" EXAMPLE TEXT  NOT NULL," +
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES grammar_android(ID), "+
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	*/
	
	public static String creategrammarSwiftStat(){
		String sql = "CREATE TABLE IF NOT EXISTS grammar_swift_stat "+
				"(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
				" TEMPLATE TEXT  NOT NULL," +
				" AST_TYPE           INT    NOT NULL, " +
				" PARENT_TYPE           INT    NOT NULL, " +
				" COUNT           INT    NOT NULL, " +
				" EXAMPLE TEXT  NOT NULL," +
				" AFILENAME TEXT," +
				" ALOC TEXT," +
				" SFILENAME TEXT," +
				" SLOC TEXT," +
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES grammar_android(ID), "+
				" UNIQUE(ID) "
				+ ")"; 
		return sql;
	}
	
	
	
	public static String insertTableGrammarSwiftLOC(int id, String template, int ast, int parent, String example, String filename, String loc, int aid){
		String sql = "INSERT OR IGNORE INTO grammar_swift (ID,TEMPLATE,AST_TYPE, PARENT_TYPE, EXAMPLE, FILENAME, LOC, ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+ 
				"\'"+example+"\',"+
				"\'"+filename+"\',"+
				"\'"+loc+"\',"+
				+aid+ 
				");";
		return sql;
	}	
	
	
	public static String insertTableGrammarSwift(int id, String template, int ast, int parent, String example, int aid){
		String sql = "INSERT OR IGNORE INTO grammar_swift (ID,TEMPLATE,AST_TYPE, PARENT_TYPE, EXAMPLE, ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+template+"\',"+ 
				+ast+","+ 
				parent+","+ 
				"\'"+example+"\',"+
				+aid+ 
				");";
		return sql;
	}	
	
	
	
	public static String insertTableStatementSwift(int id, String statement, int ast, int aid){
		String sql = "INSERT OR IGNORE INTO statement_swift (ID,STATEMENT,AST_TYPE, ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+statement+"\',"+ 
				+ast+","+ 
				+aid+ 
				");";
		return sql;
	}	
	

	public static String createExprTablesSwift(){
		String sql= "CREATE TABLE IF NOT EXISTS expr_swift "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" S_TEMP_EXPR TEXT  NOT NULL," +
				" S_SEQ_VARS           TEXT    NOT NULL, " + 
				" S_SEQ_VARS_ABS           TEXT, " + 
				" VARS_RELATION           TEXT, " + 
				" ANDROID      INT, " +
				" FOREIGN KEY(ANDROID) REFERENCES expr_android(ID), "+
				" UNIQUE(S_TEMP_EXPR, S_SEQ_VARS) "
				+ ")"; 

		return sql;
	}

	public static String insertTableExprAndroid(int id, String expr, String vars){
		String sql = "INSERT OR IGNORE INTO expr_android (ID,TEMP_EXPR,SEQ_VARS) " +
				"VALUES ("+
				id + ","+
				"\'"+expr+"\',"+ 
				"\'"+vars+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	

	public static String insertTableExprAndroid2(int id, String expr, String vars, String abs){
		String sql = "INSERT OR IGNORE INTO expr_android (ID,TEMP_EXPR,SEQ_VARS, SEQ_VARS_ABS) " +
				"VALUES ("+
				id + ","+
				"\'"+expr+"\',"+ 
				"\'"+vars+"\',"+
				"\'"+abs+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	

	public static String insertTableExprSwift(int id, String expr, String vars, int android){
		String sql = "INSERT OR IGNORE INTO expr_swift (ID,S_TEMP_EXPR,S_SEQ_VARS, ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+expr+"\',"+ 
				"\'"+vars+"\',"+ 
				"\'"+android+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	


	public static String insertTableExprSwift2(int id, String expr, String vars, String abs, int android){
		String sql = "INSERT OR IGNORE INTO expr_swift (ID,S_TEMP_EXPR,S_SEQ_VARS, S_SEQ_VARS_ABS, ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+expr+"\',"+ 
				"\'"+vars+"\',"+ 
				"\'"+abs+"\',"+ 
				"\'"+android+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	
//	"update "+Table_name+" set availability='0' where product_name like 'bar'";
	
	public static String insertTableExprSwift3(int id, String relation){
		String sql = "UPDATE expr_swift SET VARS_RELATION= " +"\'"+relation+"\'"+
				" WHERE expr_swift.id = "+
				id +
				" ";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	
	
	
	public static String insertTableMethod(String tableName, int id, String varname, String modi, String type, String returnExpr){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,METHODNAME,MODIFIER,TYPE,RETURN) " +
				"VALUES ("+
				id + ","+
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				"\'"+type+"\',"+
				"\'"+returnExpr+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	
/*
	public static String createTableMethodParams(){
		String sql = "CREATE TABLE IF NOT EXISTS mmethod_android "+
				"(ID INT PRIMARY KEY     NOT NULL," +
				" METHODNAME           TEXT    NOT NULL, " + 
				" MODIFIER            TEXT , " + 
				" TYPE        TEXT, " + 
				" PARAMETERS        TEXT, " + 
				" STATEMENTS         TEXT, "+
				" UNIQUE(ID) "
				+ ")"; 

		return sql;
	}
	
*/	
	public static String insertTableMMethod(String tableName, int id, String varname, String modi, String type, String params, String stmts){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,METHODNAME,MODIFIER,TYPE,PARAMETERS, STATEMENTS) " +
				"VALUES ("+
				id + ","+
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				"\'"+type+"\',"+
				"\'"+params+"\',"+
				"\'"+stmts+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	

	
	public static String insertTableMMethodSwift(String tableName, int id, String varname, String modi, String type, String params, String stmts, int android){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,METHODNAME,MODIFIER,TYPE,PARAMETERS, STATEMENTS, ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				"\'"+type+"\',"+
				"\'"+params+"\',"+
				"\'"+stmts+"\',"+
				android +
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	
	
	public static String insertTableMethodSwift(String tableName, int id, String varname, String modi, String type, String returnExpr,int android){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,METHODNAME,MODIFIER,TYPE,RETURN, ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				"\'"+type+"\',"+
				"\'"+returnExpr+"\', "+
				android +
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	


	public static String insertTable(String tableName, int id, String varname, String modi, String type, String init){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,VARNAME,MODIFIER,TYPE,INIT) " +
				"VALUES ("+
				id + ","+
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				"\'"+type+"\',"+
				"\'"+init+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	

	
	public static String insertTableAndroid(String tableName, int id, String className, String varname, String modi, String type, String init){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,CLASSNAME, VARNAME,MODIFIER,TYPE,INIT) " +
				"VALUES ("+
				id + ","+
				"\'"+className+"\',"+ 
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				"\'"+type+"\',"+
				"\'"+init+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	

	
	public static String insertTableAndroidM(String tableName, int id, String className, String varname, String modi){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,CLASSNAME, VARNAME,MODIFIER) " +
				"VALUES ("+
				id + ","+
				"\'"+className+"\',"+ 
				"\'"+varname+"\',"+ 
				"\'"+modi+"\'"+ 
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	
	
	public static String insertTableSwift(String tableName,int id, String className, String varname, String modi, String type, String init, int aid){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,CLASSNAME, VARNAME,MODIFIER,TYPE,INIT,ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+className+"\',"+ 
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				"\'"+type+"\',"+
				"\'"+init+"\',"+ 
				aid +
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	
	
	public static String insertTableSwiftM(String tableName,int id, String className, String varname, String modi, int aid){

		String sql = "INSERT OR IGNORE INTO "+tableName+" (ID,CLASSNAME, VARNAME,MODIFIER,ANDROID) " +
				"VALUES ("+
				id + ","+
				"\'"+className+"\',"+ 
				"\'"+varname+"\',"+ 
				"\'"+modi+"\',"+ 
				aid +
				");";

		//	  JdbcTemplate template = new JdbcTemplate();
		//	  template.update("insert into METHOD values (?,?,?,?,?)", null, null, null, null, null);
		//	template.update(psc)  
		return sql;
	}	
	

	public static String selectDistinct(String name, String value){
		String ss = "SELECT DISTINCT var_swift."+name +" FROM var_android, var_swift where var_swift.ANDROID=var_android.ID and var_android."+name+
				"=\'"+value+"\'";

		return ss;
	}

	public static String selectTemplate(String context){
		String ss = "SELECT DISTINCT swiftTemplate.TEMPLATE FROM androidTemplate, swiftTemplate where swiftTemplate.ANDROID="+"\'"+context+"\'";

		return ss;
	}



	public static int matchingVarSequence(StringJoiner joiner, String input, String var){
		String str = input;
		Pattern p = Pattern.compile(var);
		Matcher m = p.matcher(str);
		int i=0;
		//		StringJoiner joiner = new StringJoiner(",");
		while (m.find()){
			//		    System.out.println(str);
			joiner.add(var);
			i++;
		}
		return i;
	}


	public static void main( String args[] )
	{
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			c.setAutoCommit(false);
//			System.out.println("Opened database successfully");
			//      System.out.println("Opened database successfully");

			//      System.out.println(insertTable(1, "_nameX","public","Rect","new Rect()"));


			//      stmt = c.createStatement();
			stmt = c.createStatement();
			stmt.execute(dropTable("swiftTemplate"));
			stmt.execute(dropTable("androidTemplate"));
			stmt.execute(dropTable("swiftTemplate2"));
			stmt.execute(dropTable("androidTemplate2"));
			stmt.execute(dropTable("expr_swift"));
			stmt.execute(dropTable("expr_android"));
//			System.out.println(createAndroidTemplateTable());
//			System.out.println(createSwiftTemplateTable());

			stmt.executeUpdate(createAndroidTemplateTable());
			stmt.executeUpdate(createSwiftTemplateTable());
			stmt.executeUpdate(createAndroidTemplateTable2());
			stmt.executeUpdate(createSwiftTemplateTable2());
			stmt.executeUpdate(createExprTables());
			stmt.executeUpdate(createExprTablesSwift());
			
			
			//			stmt.executeUpdate(dropTable("VarDecl_Android"));
			//			stmt.executeUpdate(createTable("VarDecl_Android"));
			String i1 = insertSwiftTemplateTable("variable_declaration", "<modifier> <type> <varname> = <init>","TypeDeclaration");
			String i2 = insertSwiftTemplateTable("method_declaration", "<modi> var <name>:<type> {return <expr>}", "MethodDeclaration");
			String i3 = insertSwiftTemplateTable("constant_declaration", "let <pattern> = <expression>", "Assignment");

			//			
//			System.out.println(i1);
//			System.out.println(i2);
			//			System.out.println(i2);
			String a1 = insertAndroidTemplateTable("TypeDeclaration",31 ,"<modifier> <type> <varname> = <init>;");
			String a2 = insertAndroidTemplateTable("MethodDeclaration", 55,"<modi> <type> <name>(){return expr;}");
			String a3 = insertAndroidTemplateTable("Assignment", 1,"<name> = <expr>;");

			
			stmt.executeUpdate(insertAndroidTemplateTable2("TypeDeclaration",55 ,"<modifier> <type> <varname> = <init>;"));
			stmt.executeUpdate(insertAndroidTemplateTable2("MethodDeclaration",31 ,"<modi> <type> <name>(<args;separator= >){<statements; separator=;>;}"));
			stmt.executeUpdate(insertAndroidTemplateTable2("return_statement",41 ,"return <expr>;"));
			stmt.executeUpdate(insertAndroidTemplateTable2("if_statement",25 ,"if (<expr>) {<statements; separator=;>;} else{<statements; separator=;>};"));
//			stmt.executeUpdate(insertAndroidTemplateTable2("aaaa_statement",21 ,"<name> = <init>;"));
			stmt.executeUpdate(insertAndroidTemplateTable2("SingleVariableDeclaration",44 ,"<type> <name>"));
				
			stmt.executeUpdate(insertSwiftTemplateTable2("variable_declaration",160 ,"<modifier> <type> <varname> = <init>",55));
//			stmt.executeUpdate(insertSwiftTemplateTable2("variable_declaration_with_return",160 ,"<modifier> <type> <varname> = <init>",31));
			stmt.executeUpdate(insertSwiftTemplateTable2("function_declaration",97 ,"<modifier> func <fuction_name>(<parameter_list;separator= >)-><type> {<statements; separator= >}",31));
			stmt.executeUpdate(insertSwiftTemplateTable2("return_statement",36 ,"return <expression>",41));
			stmt.executeUpdate(insertSwiftTemplateTable2("if_statement",18 ,"if (<expression>) {<statements; separator= >} else{<statements; separator= >};",25));
			stmt.executeUpdate(insertSwiftTemplateTable2("constant_declaration",76 ,"let <pattern> = <expression>",21));
			stmt.executeUpdate(insertSwiftTemplateTable2("parameter",106 ,"<name>:<type>",44));
						
			
			String aaaaa = "<type> <name>(<args; separator=\",\">)"
					+ "{"
					+ "<statements; separator=\";\">;"
					+ "<return_s>;"
					+ "}";
			
			String a4 = insertAndroidTemplateTable("MethodDeclaration11", 1,aaaaa);
//			System.err.println(aaaaa);
			
			
//			System.out.println(a1);
//			System.out.println(a2);

			stmt.executeUpdate(a1);
			stmt.executeUpdate(a2);
			stmt.executeUpdate(a3);
			stmt.executeUpdate(a4);
			
			stmt.executeUpdate(i1);
			stmt.executeUpdate(i2);
			stmt.executeUpdate(i3);
			String q11 = SQLiteJDBC.selectTemplate("TypeDeclaration");
			ResultSet rrr = stmt.executeQuery(q11);

			String template1 = rrr.getString("TEMPLATE");
//			System.err.println("TEMPLATE test"+template1);
			ST st1 = new ST(template1);

			String a22="float", a11="private", a44 = "1f";


			String se1 = "SELECT DISTINCT var_swift.MODIFIER "+
					" FROM var_android, var_swift "
					+ "where var_swift.ANDROID=var_android.ID and "
					+ "var_android.MODIFIER="+"=\'"+a11+"\'";

			ResultSet rrrr = stmt.executeQuery(se1);

			while(rrrr.next()){
				//				System.err.println("SQL"+rrrr.getString("MODIFIER"));
			}


			se1 = "SELECT DISTINCT var_swift.INIT "+
					" FROM var_android, var_swift "
					+ "where var_swift.ANDROID=var_android.ID and "
					+ "var_android.INIT="+"=\'"+a44+"\'";


			rrrr = stmt.executeQuery(se1);

			while(rrrr.next()){
				//				System.err.println("SQL"+rrrr.getString("INIT"));
			}


			se1 = "SELECT DISTINCT var_swift.TYPE "+
					" FROM var_android, var_swift "
					+ "where var_swift.ANDROID=var_android.ID and "
					+ "var_android.TYPE="+"=\'"+a22+"\'";


			rrrr = stmt.executeQuery(se1);

			while(rrrr.next()){
				//				System.err.println("SQL"+rrrr.getString("TYPE"));
			}

			a22= "mContentRect.left";
			se1 = "SELECT DISTINCT method_swift.RETURN "+
					" FROM method_android, method_swift "
					+ "where method_swift.ANDROID=method_android.ID and "
					+ "method_android.RETURN="+"=\'"+a22+"\'";


			rrrr = stmt.executeQuery(se1);

			while(rrrr.next()){
				//				System.err.println("SQL"+rrrr.getString("RETURN"));
			}


			se1 = "SELECT DISTINCT var_android.VARNAME, var_android.TYPE "+
					" FROM var_android";


			rrrr = stmt.executeQuery(se1);
			StringBuffer buffer = new StringBuffer();
			String e1= "mChartWidth - mContentRect.right ";

			int var_num=0;
			String var_seq ="";
			StringJoiner joiner = new StringJoiner(",");
			StringJoiner joiner2 = new StringJoiner(",");
			while(rrrr.next()){
				var_seq ="avar_"+var_num;
				String varname=rrrr.getString("VARNAME");
				String typename=rrrr.getString("TYPE");
				//				System.err.println("SSQL"+varname);
				//				System.err.println("SSQL"+typename);
				if(e1.contains(varname)){
					matchingVarSequence(joiner, e1, varname);
					e1 = e1.replace(varname, "{"+typename+"}");
					joiner2.add(var_seq);
					var_num++;
				}
				//				if(e2.contains(varname)){
				//					joiner = matchingVarSequence(joiner, e1, varname);
				//					e2 = e2.replace(varname, "{"+typename+"}");
				//				}

			}
//			System.err.println(e1);
//			System.out.println("Joiner	"+joiner.toString());
			String e11 = insertTableExprAndroid2(1, e1, joiner.toString(), joiner2.toString());
			stmt.executeUpdate(e11);
			//			System.err.println(e2);

			//			stmt.executeUpdate(i2);


			se1 = "SELECT DISTINCT var_swift.VARNAME, var_swift.INIT "+
					" FROM var_swift";


			rrrr = stmt.executeQuery(se1);
			buffer = new StringBuffer();
			e1= "_chartWidth - _contentRect.size.width - _contentRect.origin.x ";
			var_num=0;

			joiner = new StringJoiner(",");
			joiner2 = new StringJoiner(",");
			while(rrrr.next()){
				var_seq ="svar_"+var_num;
				String varname=rrrr.getString("VARNAME");
				String typename=rrrr.getString("INIT");
				//				System.err.println("SSQL"+varname);
				//				System.err.println("SSQL"+typename);
				if(e1.contains(varname)){
					int k =matchingVarSequence(joiner, e1, varname);
					e1 = e1.replace(varname, "{"+typename+"}");
					while(k>0){
						joiner2.add(var_seq);
						k--;
					}
					var_num++;
				}
				//				if(e2.contains(varname)){
				//					joiner = matchingVarSequence(joiner, e1, varname);
				//					e2 = e2.replace(varname, "{"+typename+"}");
				//				}

			}
			System.err.println(e1);
			System.out.println("Joiner	"+joiner.toString());
			System.out.println("var seq	"+joiner2.toString());
			e11 = insertTableExprSwift2(1, e1, joiner.toString(),joiner2.toString(), 1);
			stmt.executeUpdate(e11);



			se1 = "SELECT DISTINCT expr_android.SEQ_VARS, expr_swift.S_SEQ_VARS, expr_android.SEQ_VARS_ABS, expr_swift.S_SEQ_VARS_ABS"+
					" FROM expr_android, expr_swift "
					+ "where expr_swift.ANDROID=expr_android.ID";
			rrrr = stmt.executeQuery(se1);
			String[] list_aa = null, list_aaaa=null, list_ss=null, list_ssss=null;
			//			ArrayList<String> aaa = new ArrayList<String>(Arrays.asList(list_aa));

			String modi = "<modi>";
			while(rrrr.next()){
				String aa = rrrr.getString("SEQ_VARS");
				String ss = rrrr.getString("S_SEQ_VARS");
				String aaaa = rrrr.getString("SEQ_VARS_ABS");
				String ssss = rrrr.getString("S_SEQ_VARS_ABS");
				list_aa = aa.split(",");
				list_ss = ss.split(",");
				list_aaaa = aaaa.split(",");
				list_ssss = ssss.split(",");

//				System.err.println("SQL"+aa +aaaa);
//				System.err.println("SQL"+ss +ssss);

			}

			ArrayList<String> aalist = new ArrayList<String>(Arrays.asList(list_aa));
			ArrayList<String> sslist = new ArrayList<String>(Arrays.asList(list_ss));
			ArrayList<String> aaaalist = new ArrayList<String>(Arrays.asList(list_aaaa));
			ArrayList<String> sssslist = new ArrayList<String>(Arrays.asList(list_ssss));

			System.out.println(aalist);
			System.out.println(sslist);

			System.out.println(aaaalist);
			System.out.println(sssslist);
			/*
			for(int i=0;i<list_aa.length;i++){
				for(int j=0;j<list_ss.length;j++){
					System.err.println(list_ssss[i]+":"+list_ss[i]+","+list_aaaa[i]+":"+list_aa[j]);
				}
			}
			 */
			se1 = "SELECT DISTINCT var_swift.VARNAME, var_android.VARNAME "+
					" FROM var_android, var_swift "
					+ "where var_swift.ANDROID=var_android.ID";

			rrrr = stmt.executeQuery(se1);
			joiner2 = new StringJoiner(",");
/*
			while(rrrr.next()){
				String aa = rrrr.getString(2);
				String ss = rrrr.getString(1);
				System.out.println("SQL	"+aa+"	"+ss+"  :"+sslist);
				for(int i=0;i<sslist.size();i++){
					if(ss.equals(sslist.get(i))){
						String mapping = aaaalist.get(aalist.indexOf(aa));
						joiner2.add(mapping);
					}
				}
				if(sslist.contains(ss)){

					System.out.println(sslist.indexOf(ss));
					System.out.println(aalist.indexOf(aa));

				}
				//				sssslist.get(sslist.indexOf(ss));
				//				aaaalist.get(aalist.indexOf(aa));
				//				System.out.println("SQL"+aalist.get(aaaalist.indexOf(aa))+":"+sslist.get(sssslist.indexOf(ss)));
			}

			System.out.println("mapping test "+joiner2.toString());
			//			stmt.execute(dropTable("expr_swift"));
			//			stmt.execute(dropTable("expr_android"));
			//			stmt.executeUpdate(createExprTables());
			//			stmt.executeUpdate(createExprTablesSwift());
*/
			e11 = insertTableExprSwift3(1, joiner2.toString());
			stmt.executeUpdate(e11);
			
			c.commit();
			DBTablePrinter.printTable(c, "swiftTemplate");
			DBTablePrinter.printTable(c, "androidTemplate");
			
			DBTablePrinter.printTable(c, "swiftTemplate2");
			DBTablePrinter.printTable(c, "androidTemplate2");
			
			
			DBTablePrinter.printTable(c, "expr_swift");
			DBTablePrinter.printTable(c, "expr_android");
			DBTablePrinter.printTable(c, "var_swift");


			String[] qr ={"TypeDeclaration","MethodDeclaration" };
			//			String[] qr ={"TypeDeclaration"};



			for(int i=0;i<qr.length;i++){
				String q1 = selectTemplate(qr[i]);
				ResultSet r = stmt.executeQuery(q1);

				String template = r.getString("TEMPLATE");
				System.err.println(template);
				ST st = new ST(template);

				if(qr[i].equals("TypeDeclaration")){
					st.add("modifier", "public");
					st.add("type", "Matrix");
					st.add("varname", "mMatrx");
					st.add("init", "new Matrix()");
					System.out.println("(generated) "+st.render());
				}
				else if(qr[i].equals("MethodDeclaration")){
					st.add("modi", "public");
					st.add("type", "CFloat");
					st.add("name", "getOffset");
					st.add("expr", "xmoffset.x");
					System.out.println("(generated) "+st.render());
				}	
			}
			//			String q1 = selectTemplate("MethodDeclaration");
			//			String q2 = selectTemplate("TypeDeclaration");
			//			stmt.close();

			//			ResultSet r = stmt.executeQuery(q1);
			//			ResultSet r2 = stmt.executeQuery(q2);
			//			System.err.println(r.getString("TEMPLATE"));
			//			System.out.println(r2.getString("TEMPLATE"));
			//						System.out.println("MODIFIER	"+r.toString().getString(1));
			//						System.out.println("RRRResultSet"+r.getString("MODIFIER"));
			//						System.out.println("RRRResultSet"+r1.getString(1));
			//						System.out.println("RRRResultSet"+r2.getString(1));
			//						System.out.println("RRRResultSet"+r4.getString(1));
			//						System.out.println("RRRResultSet"+r4.getString("INIT"));
			//						System.out.println(SQLiteJDBC.selectDistinct("MODIFIER", a1)+"#"+r.next());

			//			while(r.next()){
			//				System.err.println(r.getString("TEMPLATE"));
			//							System.out.println("RRRResultSet"+r.getString("TYPE"));
			//							System.out.println("RRRResultSet"+r.getString("MODIFIER"));
			//							System.out.println("RRRResultSet"+r.getString("INIT"));
			//	System.out.println("(generated swift by DB)\n"+ConTextTemplate.varDeclSwift(r.getString("MODIFIER"), r.getString("TYPE"), s3, r.getString("INIT")));
			//			}

			c.close();

			//			DBTablePrinter.printTable(c, "swiftTemplate");


			/*
			 * 
			 * 
			 * 
			 * 

//      stmt = c.createStatement();
      String sql = "CREATE TABLE VARDECL " +
                   "(ID INT PRIMARY KEY     NOT NULL," +
                   " VARNAME           TEXT    NOT NULL, " + 
                   " MODIFIER            TEXT     NOT NULL, " + 
                   " TYPE        TEXT, " + 
                   " INIT         TEXT)"; 
      stmt.executeUpdate(sql);

      String sql = "INSERT INTO COMPANY (ID,VARNAME,MODIFIER,TYPE,INIT) " +
                   "VALUES (1, 'mMatrixTouch', 'public', 'Matrix', 'new Matrix()');"; 
      stmt.executeUpdate(sql);



      sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
            "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );"; 
      stmt.executeUpdate(sql);

      sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
            "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );"; 
      stmt.executeUpdate(sql);


      String sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
              "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );"; 
        stmt.executeUpdate(sql);
			 */     

		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Records created successfully");

	}
}