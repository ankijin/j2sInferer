package rulesapp;

//import migration.MigrationExamples.TemplateEDB;

public class TemplateEDB{
	int db_id;
	public String template;
	int count=0;
	public TemplateEDB(int id, String temp){
		db_id = id;
		template = temp;
	}
	
	public TemplateEDB(int id, String temp, int c){
		db_id = id;
		template = temp;
		count = c;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		TemplateEDB entry = (TemplateEDB)obj;
		// TODO Auto-generated method stub
		return entry.db_id==db_id && entry.template.equals(template);
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return db_id;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return template;
	}
}
