

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.Statement;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.metrics.Jaccard;
import org.simmetrics.metrics.JaroWinkler;
import org.simmetrics.simplifiers.Simplifiers;

import gumtree_tester.SwiftParser;

//import recordSwiftParser;

public class AssignmentList {
	List<String> list1;
	List<String> list2;

	public AssignmentList(List<String> l1, List<String> l2){
		list1= l1;
		list2= l2;
	}

	public AssignmentList(){

	}
	static public Map<Integer, Integer> assignmentStatement(List<Statement> alist, List<SwiftParser.StatementContext> slist){
		//		List<Statement> alist = a_cluster.get((Integer)a_cluster_keys[a]);
		//		List<SwiftParser.StatementContext> slist = s_cluster.get((Integer)s_cluster_keys[s]);

		StringMetric metric =
				with(new JaroWinkler())
				.simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
				//	            .simplify(Simplifiers.removeAll("Chart"))
				//	            .simplify(Simplifiers.removeAll("Activity"))
				.build();
		double[][] costMatrix = new double[alist.size()][slist.size()];
		for(int i=0;i<alist.size();i++){
			for(int k=0;k<slist.size();k++){
				double score = metric.compare(alist.get(i).toString(), slist.get(k).getText());
				costMatrix[i][k] = 1- score;
			}
		}


		Map<Integer, Integer> hclassmapping = new HashMap<Integer, Integer>();
		HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
		int[] result = hung.execute();
		for(int p=0; p<result.length;p++){
			if(result[p]!=-1){
				//				if(costMatrix[p][result[p]] < 0.9){
				//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
				hclassmapping.put(p, result[p]);
				//				}
			}
			//			System.out.println(p+",, "+result[p]);
			//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
		}




		return hclassmapping;
	}


	static public Map<Expression, ParserRuleContext> assignmentAst(List<Expression> alist, List<ParserRuleContext> slist){
		//		List<Statement> alist = a_cluster.get((Integer)a_cluster_keys[a]);
		//		List<SwiftParser.StatementContext> slist = s_cluster.get((Integer)s_cluster_keys[s]);

		//		List<ASTNode> compA_ast = new LinkedList<ASTNode>();
		//		List<ParserRuleContext> compS_ast = new LinkedList<ParserRuleContext>();

		StringMetric  metric = StringMetrics.blockDistance();
		//	    StringMetric metric =
		//	            with(new JaroWinkler())
		//	            .simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
		//	            .simplify(Simplifiers.removeAll("Chart"))
		//	            .simplify(Simplifiers.removeAll("Activity"))
		//	            .build();
		double[][] costMatrix = new double[alist.size()][slist.size()];
		for(int i=0;i<alist.size();i++){
			for(int k=0;k<slist.size();k++){

				alist.get(i).toString();

				slist.get(k).getText();

				double score = metric.compare(alist.get(i).toString(), slist.get(k).getText());
				costMatrix[i][k] = 1- score;
			}
		}


		Map<Expression, ParserRuleContext> hclassmapping = new HashMap<Expression, ParserRuleContext>();
		if(alist.size() >0 && slist.size() >0){
			HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
			int[] result = hung.execute();
			for(int p=0; p<result.length;p++){
				if(result[p]!=-1){
					//				if(costMatrix[p][result[p]] < 0.9){
					System.out.println("&&"+alist.get(p).toString()+"::::::"+slist.get(result[p]).getText());
					hclassmapping.put(alist.get(p), slist.get(result[p]));
					//				}
				}
				//			System.out.println(p+",, "+result[p]);
				//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
			}
		}



		return hclassmapping;
	}


	public Map<String, String> assignment(){

		StringMetric metric =
				with(new JaroWinkler())
				.simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
				//	            .simplify(Simplifiers.removeAll("Chart"))
				//	            .simplify(Simplifiers.removeAll("Activity"))
				.build();
		double[][] costMatrix = new double[list1.size()][list2.size()];
		for(int i=0;i<list1.size();i++){
			for(int k=0;k<list2.size();k++){
				double score = metric.compare(list1.get(i), list2.get(k));
				costMatrix[i][k] = 1- score;
			}
		}

		Map<String, String> hclassmapping = new HashMap<String, String>();
		HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
		int[] result = hung.execute();
		for(int p=0; p<result.length;p++){
			if(result[p]!=-1){
				if(costMatrix[p][result[p]] < 0.9){
					//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
					hclassmapping.put(list1.get(p), list2.get(result[p]));
				}
			}
			//			System.out.println(p+",, "+result[p]);
			//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
		}


		return hclassmapping;
	}
}
