package templatechecker;

// Generated from TemplateC.g4 by ANTLR 4.6.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TemplateCParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TemplateCVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#template}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemplate(TemplateCParser.TemplateContext ctx);
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#unit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnit(TemplateCParser.UnitContext ctx);
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg(TemplateCParser.ArgContext ctx);
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(TemplateCParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#java_keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_keyword(TemplateCParser.Java_keywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#swift_keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwift_keyword(TemplateCParser.Swift_keywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#java_punc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_punc(TemplateCParser.Java_puncContext ctx);
	/**
	 * Visit a parse tree produced by {@link TemplateCParser#swift_punc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwift_punc(TemplateCParser.Swift_puncContext ctx);
}