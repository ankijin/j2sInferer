grammar TemplateC;		
template : unit*  EOF
 ;

unit : java_keyword |swift_keyword | arg | block |java_punc|swift_punc; 

arg: '<arg'NUM*'>' ;

block: '{...}' ;

NUM : [0-9] ;


java_keyword : ('abstract'|	'continue'|	'for'|	'new'|	'switch'|
			'assert'| 'default'| 'package'| 'synchronized'|
			'boolean'|	
			'do'|	'if'|	'private'|	'this'|
			'break'|	
			'double'|	
			'implements'|	'protected'|	'throw'|
			'byte'|	'else'|	'import'|	'public'|	'throws'|
			'case'|	'enum'| 	'instanceof'|	'return'|	'transient'|
			'catch'|	'extends'|	
						'int'|	
			'short'|	'try'|
						'char'|	
			'final'| 
			'interface'| 'static'|	
			'void'|
			'class'| 'finally'|	
						'long'|	
			'strictfp'|	'volatile'| 'float'|	
			'native'|	'super'|	'while'|
			'@Override'|'Override'|'@'|'T')
			;

swift_keyword : ('associatedtype'| 'class'| 'deinit'| 'enum'| 
			'extension'| 'fileprivate'| 'func'| 'import'| 
			'init'| 'inout'| 'internal'| 'let'| 'open'|
			'operator'| 'private'| 'protocol'| 'self'|
			'public'| 'static'| 'struct'| 'subscript'| 'typealias'| 'private(set)'| 'convenience'
			'var'| 'break'| 'case'| 'continue'| 'default'| 'defer'| 
			'do'| 'else'| 'fallthrough'| 'for'| 'guard'| 'if'| 'in'| 
			'repeat'| 'return'| 'switch'| 'where'| 'while' | 'weak' | 'override' |'as' |'through' |'by'|'stride'|'to'|'NSObject'|'NSObjectProtocol'|'Hashable')
			;

java_punc : '('|')'| '{'|'}'| '['|']'| ';'|'='|'>'|'<'|'&'|':'|'=='|','|'+'|'-'|'*'|'/'|'.'|'|'
			; 

swift_punc : '('|')'|'()'| '{'|'}'|'['|']'| ';'|'='|'->'|':'|'<'|'>'|'&'|'-'|'..<'|'..'|','|'!'|'?'|'..'|'.'|'|'|'_'
			;			

WS  :  [ \t\r\u000C]+ -> skip
    ;
			