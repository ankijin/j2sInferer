package migration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import recording.MappingElement;

public class RercordMethodDeclAndroid extends ASTVisitor{

	LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	LinkedList<String> recordStr = new LinkedList<String> ();
	Map<String, String> recordParams = new HashMap<String, String>();
	
	@Override
	public void endVisit(MethodDeclaration node) {
		//				System.out.println("RECORD TEST return type\n"+node.getReturnType2().toString());
	}
	
	
	@Override
	public boolean visit(SingleVariableDeclaration node) {
		// TODO Auto-generated method stub
//		System.out.println("SingleVariableDeclaration");
//		System.out.println(node.getType());
//		System.out.println(node.getName());
		recordParams.put(node.getName().getIdentifier(), node.getType().toString());
		
		return super.visit(node);
	}
	
	
	

	@Override
	public boolean visit(Modifier node) {
		MappingElement e = new MappingElement(node.getKeyword().toString(), node.getNodeType());
		record.add(e);
		recordStr.add(node.getKeyword().toString());
		return super.visit(node);
	}
	public boolean visit(org.eclipse.jdt.core.dom.PrimitiveType node) {
		if(node.getParent().getNodeType()==31){//context: function decl
//			System.out.println("org.eclipse.jdt.core.dom.PrimitiveType");
			MappingElement e = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getParent().getNodeType());
			record.add(e);
			recordStr.add(node.getPrimitiveTypeCode().toString());
		}
		//				MappingElement e = new MappingElement(node.getPrimitiveTypeCode().toString(), node.getNodeType());
		//				record.add(e);
		return false;
	}

	public boolean visit(SimpleName node) {
		if(node.getParent().getNodeType()==31){
//			System.out.println("visitSimpleName	"+node.getParent().getNodeType());
			MappingElement e = new MappingElement(node.getIdentifier().toString(), node.getNodeType());
			record.add(e);
			recordStr.add(node.getIdentifier().toString());

		}

		//				MappingElement e = new MappingElement(node.getIdentifier().toString(), node.getNodeType());
		//				record.add(e);
		return true;
	}

	//return 40@@mContentRect.left
	//			public boolean visit(org.eclipse.jdt.core.dom.QualifiedName node) {			
	//				MappingElement e = new MappingElement(node.toString(), node.getNodeType());
	//				record.add(e);
	//				return false;
	//			}
	//return statement
	public boolean visit(org.eclipse.jdt.core.dom.ReturnStatement node) {
		Expression sss = node.getExpression();

		MappingElement e = new MappingElement(sss.toString(), sss.getNodeType());
		record.add(e);
		recordStr.add(sss.toString());
		//				System.out.println("ReturnStatement\n"+node.getExpression());
		return true;
	};

	@Override
	public boolean visit(ExpressionStatement node) {
		//				System.out.println("ExpressionStatement\n"+node.toString());

		return super.visit(node);
	}

}
