package migration;
import java.util.HashMap;
import java.util.Map;

import recording.MappingElement;



//migration mContentRect.left
//find type of mContentRect : Rect
//find mappings after making form-> Rect:left
//Rect:left = CRect:origin.x
//find linking variable of mContentRect : _contentRect
//substition variable CRect:origin.x
//finally: _contentRect.origin.x
//recording them altogether
//Breno

public class MappingElementExpr {
	public String expression;
	public String className;
	public String methodName;
	public String fieldName;
	static Map<MappingElementExpr, MappingElementExpr> mappingExprElements = new HashMap<MappingElementExpr, MappingElementExpr>();


	public static String getVarName(String expr){
		String aa[] = expr.split("\\.");
		if(aa.length>0)
			return aa[0];
		else return "null";
	}

	public static String substitionVarName(String var, String expr){
		String aa[] = expr.split(":");
		return var+"."+aa[1];
	}
	
	public static String getType(String expr){
		String aa[] = expr.split(":");
		return aa[0];
	}
	

	public static String getMethod(String expr){
		String aa[] = expr.split("\\.");
		if(aa.length==2){
			return aa[1];
		}
		if(aa.length>2){

			String m =aa[1];
			//	m += aa[0];
			for(int i=2;i<aa.length;i++){
				m += "."+aa[i];
			}
			return m;
		}
		else return "null";
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return className+":"+methodName;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return className.equals(((MappingElementExpr)obj).className)
				&&methodName.equals(((MappingElementExpr)obj).methodName)
				;
	}

	public int hashCode() {
		// TODO Auto-generated method stub
		return className.length()+methodName.length();
	}

	public static void main(String[] args){
		System.out.println(MappingElementExpr.getVarName("mContentRect.left"));
		System.out.println(MappingElementExpr.getVarName("_contentRect.origin.x"));
		System.out.println(MappingElementExpr.getMethod("mContentRect.left"));

		MappingElementExpr exprA = new MappingElementExpr();
		MappingElementExpr exprS = new MappingElementExpr();

		//get api structure
		exprA.className = "Rect";
		exprA.methodName = "left";
		exprS.className = "CRect";
		exprS.methodName = "origin.x";

		mappingExprElements.put(exprA, exprS);
		MappingElementExpr gett = new MappingElementExpr();
		System.err.println(gett);
		gett.className="Rect";
		gett.methodName="left";
		//get api structure
		//Rect.left
		//get Mapping CRect.orgin.x
		//get linked varname of vardecl
		//checking type
		//substition of varname
		
		System.out.println(MappingElementExpr.getMethod("_contentRect.origin.x"));
		//		mContentRect.left
		//		_contentRect.origin.x		
		System.out.println(mappingExprElements.get(gett));
		//CRect:origin.x -> _contentRect.orgin.x
		//if it has same types
		if(1==1)
			System.err.println(MappingElementExpr.substitionVarName("_contentRect", "CRect:origin.x"));
		
		Map<MappingElement, MappingElement> mappingElements1 = new HashMap<MappingElement, MappingElement>();
		mappingElements1.put(new MappingElement("RectF:top"), new MappingElement("CGRect():origin.y"));
		System.out.println(mappingElements1);
		String Str = new String("mChartWidth - mContentRect.right");
		String str2 ="_chartWidth - _contentRect.size.width - _contentRect.origin.x";
//		System.out.println(Str.replaceAll("mChartWidth", "{float}"));
		Str= Str.replaceAll("mChartWidth", "{float}");
		Str = Str.replaceAll("mContentRect", "{RectF}");
		System.out.println(Str);
		str2 = str2.replaceAll("_chartWidth", "{CGFloat(0.0)}");
		str2 = str2.replaceAll("_contentRect", "{CGRect()}");
		System.out.println(str2);
		//mChartWidth:_chartWidth
		//mContentRect:_contentRect
	}
}
