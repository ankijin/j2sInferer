package migration;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
//import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
//import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
//import java.util.List;
import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
import java.util.Vector;
import org.eclipse.jdt.internal.compiler.parser.TerminalTokens;
//import javax.lang.model.type.DeclaredType;

//import org.abego.treelayout.NodeExtentProvider;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.stringtemplate.v4.ST;

import alignment.common.Filewalker;
import alignment.common.HungarianAlgorithm;
//import SwiftParser.Class_declarationContext;
//import SwiftParser.Pattern_initializerContext;
//import SwiftParser.Variable_declarationContext;
import javassist.compiler.Parser;
import mapping.MappingMethods;
import mapping.MethodStructAndroid;
import mapping.MethodStructSwift;
//import recording.SwiftParser;
//import recording.MappingDeclaration;
//import recording.MappingElement;
//import recording.RecordVarDeclAndroid;
import sqldb.DBTablePrinter;
import sqldb.ProtoSQLiteJDBC;
import sqldb.SQLiteJDBC;
import templates.ConTextTemplate;
import org.eclipse.jdt.internal.compiler.parser.TerminalTokens;

public class Main {
	static ASTParser parser;
	static Map<String, String> 					mappingKeysVarDecl = new HashMap<String, String>();
	static Map<MappingElement, MappingElement> 	mappingElements = new HashMap<MappingElement, MappingElement>();

	public static Map<String, String> typeMapAndroid 	= new HashMap<String, String>();
	public static Map<String, String> typeMapSwift 	= new HashMap<String, String>();

	static Map<Integer, Integer> mapStatement = new HashMap<Integer, Integer>();

	public static String javaSource;
	
	static Connection ccc1= null;
	static java.sql.Statement ccc_sql_stmt;

	public Main(){

		//return
		mapStatement.put(41, 36);
		mapStatement.put(21, 68);
		mapStatement.put(25, 18);
		mapStatement.put(21, 180);
		/*
		try {
			Class.forName("org.sqlite.JDBC");
//			ccc = DriverManager.getConnection("jdbc:sqlite:test_record111.db");
//			ccc.setAutoCommit(true);
//			ccc_sql_stmt = ccc.createStatement();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 */
	}
	public static <T> Map<String, ParserRuleContext> getVarDeclSwift(String path) throws FileNotFoundException, IOException, IndexOutOfBoundsException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.Top_levelContext root1 = parser1.top_level();

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitPattern_initializer(SwiftParser.Pattern_initializerContext ctx) {
				if(ctx.initializer()!=null && ctx.initializer().expression()!=null)
					typeMapSwift.put(ctx.pattern().getText(), ctx.initializer().expression().getText().replaceAll("\\([^\\(]*\\)", ""));
				// TODO Auto-generated method stub
				return super.visitPattern_initializer(ctx);
			}

			@Override
			public T visitClass_declaration(SwiftParser.Class_declarationContext ctx) {
				// TODO Auto-generated method stub
				swiftClassName = ctx.class_name().getText();
				return super.visitClass_declaration(ctx);
			}

		};
		root1.accept(visitor);
		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getMethodDeclSwift(String path) throws FileNotFoundException, IOException{

		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		SwiftParser.Top_levelContext root1;
		try{

			ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
			Lexer lexer1 = new SwiftLexer((CharStream)stream1);
			SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
			root1 = parser1.top_level();
		} catch (Exception e){
			return nodeSwift;
		}
		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitPattern(SwiftParser.PatternContext ctx) {
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {	
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_name(SwiftParser.Variable_nameContext ctx) {
				// TODO Auto-generated method stub
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitVariable_name(ctx);
			}	
		};
		root1.accept(visitor);
		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getMethodVarDeclSwift2(String path) throws FileNotFoundException, IOException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.Top_levelContext root1 = parser1.top_level();

		SwiftBaseListener lst = new SwiftBaseListener(){

			@Override
			public void enterVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				super.enterVariable_declaration(ctx);
			}

			@Override
			public void exitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				super.exitVariable_declaration(ctx);
			}

			@Override
			public void enterReturn_statement(SwiftParser.Return_statementContext ctx) {
				// TODO Auto-generated method stub
				super.enterReturn_statement(ctx);
			}
		};

		return nodeSwift;
	}

	public static <T> Map<String, ParserRuleContext> getMethodVarDeclSwift(String path) throws FileNotFoundException, IOException{
		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		SwiftParser.Top_levelContext root1 = parser1.top_level();

		VariableDeclarion<T> visitor = new VariableDeclarion<T>(nodeSwift){
			@Override
			public T visitPattern(SwiftParser.PatternContext ctx) {
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {	
				return visitChildren(ctx);
			}
			@Override
			public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
				nodeSwift.put(ctx.getText(), ctx.getParent());
				return super.visitFunction_name(ctx);
			}

			@Override
			public T visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				return super.visitVariable_declaration(ctx);
			}

			@Override
			public T visitReturn_statement(SwiftParser.Return_statementContext ctx) {
				// TODO Auto-generated method stub

				//				ctx.getParent().getParent().getParent().getParent().getParent().
				//ctx.getParent().getParent().getParent().getParent().getParent().Identifier().toString()
				return super.visitReturn_statement(ctx);
			}
		};
		root1.accept(visitor);
		return nodeSwift;
	}


	public static String androidClassName ="";
	public static String swiftClassName ="";

	public static Map<String, ASTNode> getVarDeclAndroid(String str, String name) {
		parser = ASTParser.newParser(AST.JLS8);

		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();


		parser.setResolveBindings(true);
		//		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		parser.setBindingsRecovery(true);

		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);

		//		String unitName = "XAxisRenderer.java";
		//		System.out.println("F	"+file.getName());
		parser.setUnitName(name);

		String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/testsrc" }; 
		String[] classpath = {"/Users/kijin/Desktop/android-13.jar"};

		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
		parser.setSource(str.toCharArray());
		
		//		parser.
		CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		
		cu.accept(new ASTVisitor() {

			@Override
			public boolean visit(TypeDeclaration node) {
				// TODO Auto-generated method stub
				androidClassName = "";
				androidClassName +=node.getName();
				System.err.println(androidClassName);
				return super.visit(node);
			}



			@Override
			public boolean visit(PackageDeclaration node) {
				// TODO Auto-generated method stub
				//				System.err.println(node.getName());
				//				androidClassName +=node.getName()+".";
				return super.visit(node);
			}

			public boolean visit(VariableDeclarationFragment node) {
				if(node.getParent().getParent().getNodeType()==55){
					FieldDeclaration parent = (FieldDeclaration) node.getParent();
					SimpleName name = node.getName();
					nodeAndroid.put(name.getIdentifier(), node.getParent());
					typeMapAndroid.put(name.toString(), parent.getType().toString());
				}
				return false; // do not continue 
			}
			/*			
			public boolean visit(FieldDeclaration node) {
				VariableDeclarationFragment var_decl = (VariableDeclarationFragment)node.fragments().get(0);
				nodeAndroid.put(name.getIdentifier(), node.getParent());
				typeMap.put(var_decl.getName().toString(), node.getType().toString());
				return false;
			}
			 */
		});

		return nodeAndroid;

	}

	public static Map<String, ASTNode> getMethodDeclAndroid(String str, String name) {
		//		parser = ASTParser.newParser(AST.JLS8);
		//		parser.setSource(str.toCharArray());
		//		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();
		//		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		parser = ASTParser.newParser(AST.JLS8);

		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		//		Map<String, ASTNode> nodeAndroid = new HashMap<String, ASTNode>();


		parser.setResolveBindings(true);
		//		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		parser.setBindingsRecovery(true);

		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);

		//		String unitName = "XAxisRenderer.java";
		//		System.out.println("F	"+file.getName());
		parser.setUnitName(name);

		String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/testsrc" }; 
		String[] classpath = {"/Users/kijin/Desktop/android-13.jar"};

		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
		parser.setSource(str.toCharArray());

		javaSource = str;
		

		CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		
		cu.accept(new ASTVisitor() {

			public boolean visit(org.eclipse.jdt.core.dom.MethodDeclaration node) {
				//				System.out.println(node.modifiers());
				//				System.out.println(node.getName());
				//				System.out.println(node.parameters());
				//				System.out.println(node.getBody());
				//				System.out.println(node.getReturnType2());
				nodeAndroid.put(node.getName().toString(), node);
				return false;
			}

		});
		return nodeAndroid;
	}

	public static Map<String, MethodStructAndroid> getMethodDeclAndroid2(String str) {
		parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(str.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		Map<String, MethodStructAndroid> nodeAndroid = new HashMap<String, MethodStructAndroid>();
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		cu.accept(new ASTVisitor() {
			@Override
			public boolean visit(MethodDeclaration node) {
				// TODO Auto-generated method stub
				//				list.clear();
				ArrayList<Integer> list = new ArrayList<Integer>();
				if(node.getBody()==null || node.getBody().statements()==null) return false;
				List sts = node.getBody().statements();
				for(int i=0; i<sts.size();i++){
					if( sts.get(i) instanceof ReturnStatement){
						//						System.err.println("ReturnStatement"+ReturnStatement.RETURN_STATEMENT);
						list.add(ReturnStatement.RETURN_STATEMENT);
					}
					if( sts.get(i) instanceof SingleVariableDeclaration){
						//						System.err.println("SingleVariableDeclaration");
						list.add(SingleVariableDeclaration.SINGLE_VARIABLE_DECLARATION);
					}
					if( sts.get(i) instanceof ExpressionStatement){
						//						System.err.println("ExpressionStatement"+ExpressionStatement.EXPRESSION_STATEMENT);
						list.add(ExpressionStatement.EXPRESSION_STATEMENT);
					}
					if( sts.get(i) instanceof IfStatement){
						//						System.err.println("IfStatement");
						list.add(IfStatement.IF_STATEMENT);
					}
					if( sts.get(i) instanceof VariableDeclarationStatement){
						//						System.err.println("IfStatement");
						//						System.err.println("VariableDeclarationStatement"+VariableDeclarationStatement.VARIABLE_DECLARATION_STATEMENT);

						list.add(VariableDeclaration.VARIABLE_DECLARATION_STATEMENT);
					}
					if( sts.get(i) instanceof VariableDeclarationExpression){
						//						System.err.println("IfStatement");
						list.add(VariableDeclaration.VARIABLE_DECLARATION_EXPRESSION);
					}
				}
				nodeAndroid.put(node.getName().toString(), new MethodStructAndroid(node, list));
				return false;
			}
		});
		return nodeAndroid;
	}

	public static <T> Map<String, MethodStructSwift> getMethodVarDeclSwift3(String path) throws FileNotFoundException, IOException{
		//		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		Map<String, MethodStructSwift> node = new HashMap<String, MethodStructSwift>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));

		SwiftParser.Top_levelContext root1 = parser1.top_level();

		SwiftBaseListener listener = new SwiftBaseListener(){


			@Override
			public void enterFunction_declaration(SwiftParser.Function_declarationContext ctx) {
				// TODO Auto-generated method stub
				ArrayList<Integer> list  = new ArrayList<Integer>();
				if(ctx.function_body()!=null && ctx.function_body().code_block()!=null && ctx.function_body().code_block().statements()!=null){
					List<SwiftParser.StatementContext> stmts = ctx.function_body().code_block().statements().statement();
					for(int i=0; i< stmts.size();i++){
						SwiftParser.StatementContext stmt = stmts.get(i);
						if(stmt.expression()!=null){
							list.add(SwiftParser.RULE_expression);
							//								System.err.println("RULE_expression"+SwiftParser.RULE_expression);
						}
						if(stmt.branch_statement() !=null && stmt.branch_statement().if_statement()!=null){
							list.add(SwiftParser.RULE_if_statement);
						}
						if(stmt.control_transfer_statement() !=null && stmt.control_transfer_statement().return_statement()!=null){
							list.add(SwiftParser.RULE_return_statement);
						}
						if(stmt.declaration() !=null && stmt.declaration().constant_declaration()!=null){
							list.add(SwiftParser.RULE_constant_declaration);
							//								System.err.println("RULE_constant_declaration"+SwiftParser.RULE_constant_declaration);
						}
						if(stmt.declaration() !=null && stmt.declaration().variable_declaration()!=null){
							list.add(SwiftParser.RULE_variable_declaration);
							//								System.err.println("RULE_variable_declaration"+SwiftParser.RULE_variable_declaration);
						}
					}
				}
				node.put(ctx.function_name().getText(), new MethodStructSwift(ctx, list));
				super.enterFunction_declaration(ctx);
			}

			@Override
			public void enterVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
				// TODO Auto-generated method stub
				if(ctx.code_block()!=null && ctx.code_block().statements()!=null){
					ArrayList<Integer> list  = new ArrayList<Integer>();
					List<SwiftParser.StatementContext> stmts = ctx.code_block().statements().statement();
					for(int i=0; i< stmts.size();i++){
						SwiftParser.StatementContext stmt = stmts.get(i);
						if(stmt.expression()!=null){
							list.add(SwiftParser.RULE_expression);
						}
						if(stmt.branch_statement() !=null && stmt.branch_statement().if_statement()!=null){
							list.add(SwiftParser.RULE_if_statement);
						}
						if(stmt.control_transfer_statement() !=null && stmt.control_transfer_statement().return_statement()!=null){
							list.add(SwiftParser.RULE_return_statement);
						}
						if(stmt.declaration() !=null && stmt.declaration().constant_declaration()!=null){
							list.add(SwiftParser.RULE_declaration);
						}
					}

					node.put(ctx.variable_name().getText(), new MethodStructSwift(ctx, list));
				}
				super.enterVariable_declaration(ctx);
			}
		};


		ParseTreeWalker walker = new ParseTreeWalker();
		//			    AntlrDrinkListener listener = new AntlrDrinkListener();
		walker.walk(listener, root1);
		return node;
	}

	//read file content into a string
	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}

		reader.close();

		return  fileData.toString();	
	}

	/*
	public static Map<String, String> mappingVarDec(Map<String, ASTNode> nodeAndroid, Map<String, ParserRuleContext> nodeSwift, boolean toPrint, String pp){

		//		alignment of VariableDeclartion
		Object[] keyA =  nodeAndroid.keySet().toArray();
		Object[]  keyS = nodeSwift.keySet().toArray();

		ArrayList androids = new ArrayList (Arrays.asList(keyA));
		ArrayList swifts = new ArrayList (Arrays.asList(keyS));


		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		Vector<Integer> iii = new Vector<Integer>();
		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				if (score > max_score && score > 0.75 && !iii.contains(j)) {
					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
				}
			}
			iii.add(swifts.indexOf(fmapping.get(androids.get(i))));
			max_score = -1;
		}
		return fmapping;
	}

	 */


	public static Map<String, String> mappingVarDec(Map<String, ASTNode> nodeAndroid, Map<String, ParserRuleContext> nodeSwift, boolean toPrint, String pp){

		//		alignment of VariableDeclartion
		Object[] keyA =  nodeAndroid.keySet().toArray();
		Object[]  keyS = nodeSwift.keySet().toArray();

		ArrayList androids = new ArrayList (Arrays.asList(keyA));
		ArrayList swifts = new ArrayList (Arrays.asList(keyS));


		Map<String, String> fmapping = new HashMap<String, String>();
		Map<String, String> hmapping = new HashMap<String, String>();
		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		Vector<Integer> iii = new Vector<Integer>();
		Vector<Integer> jjj = new Vector<Integer>();
		double[][] costMatrix = new double[keyA.length][keyS.length];
		//		if(keyA.length>=keyS.length){
		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			int m =0, k=0;
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());
				costMatrix[i][j]=1-score;
				if (score > max_score && score > 0.75 && !iii.contains(i) && !jjj.contains(j)) {
					max_score = score;
					fmapping.put(keyA[i].toString(), keyS[j].toString());
					m=i; //store max index
					k=j;
				}
			}
			iii.add(m);
			jjj.add(k);
			//				iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));
			//prunning
			max_score = 0;
		}

		if(keyA.length >0 &&  keyS.length>0){
			HungarianAlgorithm hung = new HungarianAlgorithm(costMatrix);
			int[] result = hung.execute();
			for(int p=0; p<result.length;p++){
				if(result[p]!=-1){
					if(costMatrix[p][result[p]] < 0.25){
						//					System.err.println(android_files.get(p).getName()+", "+swift_files.get(result[p]).getName());
						hmapping.put(keyA[p].toString(), keyS[result[p]].toString());
					}
				}
				//			System.out.println(p+",, "+result[p]);
				//			System.out.println(android_files.get(result[p])+", "+swift_files.get(p));
			}
		}

		/*
//		}else{

//			System.out.println("for(int i=0; i< keyS.length;i++){");
			for(int i=0; i< keyS.length;i++){
				//			fmapping.put(keyA[i].toString(), keyS[0].toString());
				int m =0;
				for(int j=0; j< keyA.length;j++){
					float score = metric.compare(keyS[i].toString(), keyA[j].toString());

					if (score > max_score && score > 0.95 && !iii.contains(i)) {
						max_score = score;
							fmapping.put(keyA[j].toString(), keyS[i].toString());
						m=i; //store max index
					}
				}
				iii.add(m);
				//				iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));
				//prunning
				max_score = 0;
			}
		 */

		//		}
		return hmapping;
	}


	static LinkedList<VarDeclElement> alist = new LinkedList<VarDeclElement>();
	static LinkedList<VarDeclElement> slist = new LinkedList<VarDeclElement>();

	static Map<String, VarDeclElement> amapping = new HashMap<String, VarDeclElement>();
	static Map<String, VarDeclElement> smapping = new HashMap<String, VarDeclElement>();



	public static Map<String, String> mapping(Map<String, String> nodeAndroid, Map<String, String> nodeSwift){

		//		System.out.println("mapping"+nodeAndroid.toString() + nodeSwift.toString());
		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();

		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);

		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				if (score > max_score && score > 0.8) {
					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
				}

			}
			max_score = -1;
		}
		return fmapping;
	}



	public static boolean hasSameStructure(ArrayList<Integer> android, ArrayList<Integer> swift){
		if(android.size()!=swift.size()) return false;
		ArrayList<Integer> mappedSwift = new ArrayList<Integer>();
		for(int i=0; i< android.size();i++){
			if(mapStatement.containsKey(android.get(i))){
				mappedSwift.add(mapStatement.get(android.get(i)));
			}else{
				mappedSwift.add(-1);
			}
		}
		//		System.out.println("hasSameStructure"+android+swift+mappedSwift);
		return mappedSwift.containsAll(swift);
	}

	public static Map<String, String> mappingMethodwo(Map<String, MethodStructAndroid> nodeAndroid, Map<String, MethodStructSwift> nodeSwift){

		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();

		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);

		Vector<String> iii = new Vector<String>();
		String setting ="";
		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				MethodStructAndroid methodA = nodeAndroid.get(keyA[i]);
				MethodStructSwift 	methodS = nodeSwift.get(keyS[j]);
				//				System.out.println("TEST"+hasSameStructure(methodA.statements, methodS.statements));


				if (!iii.contains(keyS[j].toString()) && score > max_score && score > 0.75 && hasSameStructure(methodA.statements, methodS.statements)) {

					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
					setting = keyS[j].toString();
				}

				iii.add(setting);

			}
			max_score = -1;
		}
		return fmapping;
	}


	public static Map<String, String> mappingMethod(Map<String, MethodStructAndroid> nodeAndroid, Map<String, MethodStructSwift> nodeSwift){

		//		alignment of VariableDeclartion
		Object[] keyA = nodeAndroid.keySet().toArray();
		Object[] keyS = nodeSwift.keySet().toArray();

		Map<String, String> fmapping = new HashMap<String, String>();

		StringMetric metric = StringMetrics.jaroWinkler();
		//		StringMetrics.jaroWinkler();
		float max_score =-1;
		//		System.out.println(pp);
		Vector<String> iii = new Vector<String>();
		String setting ="";
		for(int i=0; i< keyA.length;i++){
			//			fmapping.put(keyA[i].toString(), keyS[0].toString());
			for(int j=0; j< keyS.length;j++){
				float score = metric.compare(keyA[i].toString(), keyS[j].toString());

				MethodStructAndroid methodA = nodeAndroid.get(keyA[i]);
				MethodStructSwift methodS = nodeSwift.get(keyS[j]);
				//				System.out.println("TEST"+hasSameStructure(methodA.statements, methodS.statements));


				if (!iii.contains(keyS[j].toString()) && score > max_score && score > 0.75) {

					max_score = score;
					if(!fmapping.containsValue(keyA[i].toString()) && !fmapping.containsValue(keyS[j].toString()))
						fmapping.put(keyA[i].toString(), keyS[j].toString());
					//					System.out.println(keyA[i]+":"+keyS[j]+"="+score);
					setting = keyS[j].toString();
				}

				iii.add(setting);

			}
			max_score = -1;
		}
		return fmapping;
	}


	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, InterruptedException {


		new Main();
		//recording process
		//1. automatic mapping by using name similarity of cross-platform repository
		//				Filewalker fw = new Filewalker("charts/android_test3", "charts/swift_test3");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("mcharts/MPAndroidChart-master", "mcharts/Charts-master");
		alignment.common.Filewalker fw = new alignment.common.Filewalker("testsrc", "mcharts/Charts-master");
		//		recording.java.Filewalker fw = new recording.java.Filewalker("testsrc", "/Users/kijin/Documents/repo/cross-app-repo/new_charts/Charts");

		//		/GumTreeTester/testsrc
		//		Filewalker fw = new Filewalker("charts/android_test2", "charts/swift_test2");


		Map<File, File> classmap = fw.getClassMapping();
		Filewalker.staticPrintMapping(classmap);

		//get VariableDeclation of each android and swift
		Map<String, ASTNode> varDeclAndroid = null;
		Map<String, ParserRuleContext> varDeclSwift 	= null;

		Map<String, String> mapper_varDec 				= null;
		Map<String, String> mapper_methodDec 			= null;
		Map<String, ParserRuleContext> methodDecliOS 	= null;
		Map<String, ASTNode> methodDeclA = null;

		Class.forName("org.sqlite.JDBC");
		Connection connection = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(SQLiteJDBC.dropTable("statement_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("statement_swift"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("expr_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("expr_swift"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("mmethod_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("mmethod_swift"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("grammar_swift"));

		stmt.executeUpdate(ProtoSQLiteJDBC.createExprTablesAndroid());
		stmt.executeUpdate(ProtoSQLiteJDBC.createExprTablesSwift());
		stmt.executeUpdate(SQLiteJDBC.dropTable("var_android"));
		stmt.executeUpdate(SQLiteJDBC.createTable("var_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("var_swift"));
		stmt.executeUpdate(SQLiteJDBC.createTableSwift("var_swift"));

		stmt.executeUpdate(SQLiteJDBC.dropTable("stmts_android"));
		stmt.executeUpdate(SQLiteJDBC.createStmtsAndroidTable());
		stmt.executeUpdate(SQLiteJDBC.dropTable("stmts_swift"));
		stmt.executeUpdate(SQLiteJDBC.createStmtsSwiftTable());

		stmt.executeUpdate(SQLiteJDBC.createStatementTablesA());
		stmt.executeUpdate(SQLiteJDBC.createStatementTablesS());

		//		System.err.println(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarAndroid());
		stmt.executeUpdate(SQLiteJDBC.creategrammarSwift());

		stmt.executeUpdate(SQLiteJDBC.createTableMethodParams());
		stmt.executeUpdate(SQLiteJDBC.createTableMethodParamsSwift());
		stmt.executeUpdate(SQLiteJDBC.dropTable("un_expr_android"));
		stmt.executeUpdate(SQLiteJDBC.dropTable("un_expr_swift"));
		stmt.executeUpdate(ProtoSQLiteJDBC.createUnmatchedExprTablesAndroid());
		stmt.executeUpdate(ProtoSQLiteJDBC.createUnmatchedExprTablesSwift());
		//		connection.commit();

		//get variable and method declarations class by class
		Iterator<File> a_classes = classmap.keySet().iterator();
		Connection connectionM = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		int b =0;
		while(a_classes.hasNext()){
			//			Math.max(a, b);
			b++;
			//						if(b==5) break;
			File aclass_path = a_classes.next();
			//			System.out.println("================="+b+"		"+aclass_path);
//			if(1==1){
//										if(aclass_path.getName().equals("YAxisRendererHorizontalBarChart.java")||aclass_path.getName().equals("XAxisRendererHorizontalBarChart.java")){
				//							if(aclass_path.getName().equals("X`AxisRendererHorizontalBarChart.java")){

//							if(aclass_path.getName().equals("HorizontalBarChartRenderer.java")){
	
			if(aclass_path.getName().equals("ViewPortHandler.java")){

//				if(aclass_path.getName().equals("ViewPortHandler.java")||aclass_path.getName().equals("Transformer.java")){

					File sclass_path = classmap.get(aclass_path);

					try{
						//			get variable declarations of aligned class by brief visit of ASTs
						varDeclSwift   = getVarDeclSwift(sclass_path.getAbsolutePath());
						varDeclAndroid = getVarDeclAndroid(readFileToString(aclass_path.getAbsolutePath()), aclass_path.getName());
					} catch(Exception e){

					}

					MappingDeclaration.varDeclAndroid 	= varDeclAndroid;
					MappingDeclaration.varDeclSwift 	= varDeclSwift;

					//			get variable declarations of aligned class by brief visit of ASTs
					methodDecliOS 	= getMethodDeclSwift(sclass_path.getAbsolutePath());
					methodDeclA 	= getMethodDeclAndroid(readFileToString(aclass_path.getAbsolutePath()), aclass_path.getName());

					//			System.out.println("methodDeclSwift\n"+methodDecliOS);
					boolean toPrint = true;

					//aligning of variable declaration by score.(score : name similarities of var_name ..)
					//fmapping: variable name pairs are stored in it.
					mapper_varDec 		= mappingVarDec(varDeclAndroid, varDeclSwift, toPrint, "var declaration mapping"+aclass_path.getName()+":"+sclass_path.getName());
					mapper_methodDec 	= mappingVarDec(methodDeclA, methodDecliOS, toPrint, "method declaration mapping"+aclass_path.getName()+":"+sclass_path.getName());

					System.out.println("var declartion mapping\n"+mapper_varDec);
					System.err.println("method declartion mapping\n"+mapper_methodDec);

					/**
					 * --------
					 * --------
					 * --------
					 * --------
					 */

					//record mapping of variable declarations 
					Iterator<String> iterator_varDec = mapper_varDec.keySet().iterator();
					while(iterator_varDec.hasNext()){
						String key_a=iterator_varDec.next();
						if(varDeclAndroid.get(key_a) instanceof FieldDeclaration && varDeclSwift.get(mapper_varDec.get(key_a)) instanceof SwiftParser.Variable_declarationContext){
							MappingDeclaration mm = new MappingDeclaration(
									(FieldDeclaration) 	varDeclAndroid.get(key_a), 
									(SwiftParser.Variable_declarationContext)		varDeclSwift.get(mapper_varDec.get(key_a))
									);
							mm.mappingVarDecl();
						}
					}//iteration of aligned variable declarations

					//record mapping of method declarations 
					Iterator<String> it_f2 = mapper_methodDec.keySet().iterator();
					while(it_f2.hasNext()){
						String key_a=it_f2.next();
						MappingDeclaration mmm = new MappingDeclaration();
						mmm.setElementMethodDecl(
								methodDeclA.get(key_a), 
								methodDecliOS.get(mapper_methodDec.get(key_a))
								);
						//				System.out.println("Functionlist\n"+methodDeclA.get(key_a));
					}//iteration of aligned variable declarations

					//			stmt = connectionM.createStatement();




				}//iteration of aligned classes
			}
			DBTablePrinter.printTable(connectionM, "grammar_android");
			DBTablePrinter.printTable(connectionM, "grammar_swift");


			DBTablePrinter.printTable(connectionM, "var_android");
			DBTablePrinter.printTable(connectionM, "var_swift");
			//			DBTablePrinter.printTable(connectionM, "method_android");
			//			DBTablePrinter.printTable(connectionM, "method_swift");
			//			DBTablePrinter.printTable(connectionM, "mmethod_android");
			//			DBTablePrinter.printTable(connectionM, "mmethod_swift");
			DBTablePrinter.printTable(connectionM, "expr_android");
			DBTablePrinter.printTable(connectionM, "expr_swift");




			//		DBTablePrinter.printTable(connectionM, "stmts_android");
			//		DBTablePrinter.printTable(connectionM, "stmts_swift");
			//		DBTablePrinter.printTable(connectionM, "un_expr_android");
			//		DBTablePrinter.printTable(connectionM, "un_expr_swift");
			connectionM.close();
		}//equal


	}