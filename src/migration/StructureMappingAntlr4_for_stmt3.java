package migration;

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.InputMismatchException;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.IntervalSet;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.compiler.ITerminalSymbols;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.internal.compiler.parser.Scanner;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.metrics.JaccardSimilarity;
import org.simmetrics.metrics.JaroWinkler;
import org.simmetrics.simplifiers.Simplifiers;

import com.google.code.javakbest.Murty;
import com.google.common.collect.Sets;

import alignment.DelcarationMapping;
import alignment.common.CommonOperators;
import alignment.common.CommonSeqToClustering;
import alignment.common.CostLCS;
import alignment.common.HungarianAlgorithm;
import alignment.common.JavaWithoutContext;
import alignment.common.LineCharIndex;
import alignment.common.MappingOutput;
import alignment.common.OpProperty;
import alignment.common.SwiftListenerWithoutContext;
import alignment.ruleinfer.QueryExamples;
import antlr_parsers.javaparser.JavaBaseListener;
import antlr_parsers.javaparser.JavaBaseVisitor;
import antlr_parsers.javaparser.JavaLexer;
import antlr_parsers.javaparser.JavaParser;
import j2swift.Java8Lexer;
import j2swift.Java8Parser;
import recording.java.JDTVisitorWithoutContext;
import recording.java.JavaListenerWithoutContext;
import sqldb.ProtoSQLiteJDBC;
import sqldb.SQLiteJDBC;
import templatechecker.DSLErrorListener;
//import templatechecker.DSLErrorListener;
import templatechecker.ExceptionErrorStrategy;
import templatechecker.TemplateCBaseVisitor;
import templatechecker.TemplateCLexer;
import templatechecker.TemplateCParser;
import templatechecker.TemplateCParser.ArgContext;

public class StructureMappingAntlr4_for_stmt3 {
	public boolean 					templateSolver 						= false;
	public Map<Integer, Integer> 	android_to_swift 					= new HashMap<Integer, Integer>();
	public Map<Pair, Double> 		android_to_swift_point 				= new HashMap<Pair, Double>();

	public CommonTokenStream 		swiftCommonStream					= null;
	public CommonTokenStream 		androidCommonStream					= null;

	public Map<Interval, Interval> 	constraint 							= new HashMap<Interval, Interval>();
	public Map<Interval, List<Interval>> constraintRefAndroid 			= new HashMap<Interval, List<Interval>>();

	public String afilename, sfilename;

	public String example_sql="", template_sql="", binding_sql="";

	//ssmap, aamap, nextMapping, template_a, template_s



	public static String[] java_keywords={
			"abstract",	"continue",	"for",	"new",	"switch",
			"assert", "default", "package", "synchronized",
			//			"boolean",	
			"do",	"if",	"private",	"this",
			"break",
			//			"double",	
			"implements",	"protected",	"throw",
			"byte",	"else",	"import",	"public",	"throws",
			"case",	"enum", 	"instanceof",	"return",	"transient",
			"catch",	"extends",	
			//			"int",	
			"short",	"try",
			//			"char",	
			"final", 
			"interface", "static",	
			"void",
			"class", "finally",	
			//			"long",	
			"strictfp",	"volatile",
			//			"float",	
			"native",	"super",	"while",
			"@Override","Override","@", "true", "false"
	};

	public static String[] common_keywords={
			"return", "if","else","class","for","while"
	};


	boolean toskip = false;

	public static List<String> java_keywords_list=Arrays.asList(java_keywords);

	public static String[] java_punctuations={"(",")", "{","}", 
			"[","]", 
			";","=",">","<","&&","&",":","==",",","-","+","/","*","?",".",">=","!=",".","<=","++","+=","()", "!", "==", "||","-="};

	public static List<String> java_punctuations_list=Arrays.asList(java_punctuations);

	public static String[] swift_keywords={"associatedtype", "class", "deinit", "enum", "to",
			"extension", "fileprivate", "func", "import", 
			"init", "inout", "internal", "let", "open",
			"operator", "private", "protocol", "self","init",
			"public", "static", "struct", "subscript", "typealias", 
			"var", "break", "case", "continue", "default", "defer", 
			"do", "else", "fallthrough", "for", "guard", "if", "in", 
			"repeat", "return", "switch", "where", "while", "override", "public override func", "public override","private func", "internal override", "internal override func","as","public func","private var","public var","stride", "through", "by","private weak var","weak","private weak","super"};
	//	"private var","public var","public func"};

	public static List<String> swift_keywords_list=Arrays.asList(swift_keywords); 
	public static String[] swift_punctuations={"(",")", "{","}","[","]", ";","=","->",":","<",">","&&","&","-","..<","..",",","!","?",">=","!=", "& &",".","+","??","==","||", "-=", "+=","_"};
	public static List<String> swift_punctuations_list=Arrays.asList(swift_punctuations);

	//	public static String[] common_operators={"+","-","/","*",".","<",">", "= =","=="};
	public static String[] common_operators={"+","-","/","*","."};
	//	public static String[] common_operators={"+","-","/","*",".",",","?",":","!",">","<","(",")"};
	public static List<String> common_operators_list = Arrays.asList(common_operators);

	public static int testnum = 0;
	public static int grammar_id=0;
	public static int ggrammar_id=0;
	ParserRuleContext android;
	ParserRuleContext swift;
	public SwiftListenerWithoutContext swift_listner;
	//	public JavaListenerWithoutContext wocontext;
	public JavaWithoutContext android_listener;

	Map<Interval, String> amap_ct 	= new HashMap<Interval, String>();
	Map<Interval, String> smap_ct 	= new HashMap<Interval, String>();

	private boolean stopped 	= false;
	public TokenStreamRewriter rewriter;
	public CommonTokenStream tokens;
	public StructureMappingAntlr4_for_stmt3(ParserRuleContext android, ParserRuleContext swift) throws FileNotFoundException, IOException{

		this.android 	= android;
		this.swift 		= swift;

		/**
		 * get main constraints, fed back to the .. more levels
		 * skipping common sequence operators in {...}
		 * checking common start end for better mapping 
		 */


		//		swift_listner.op_s_nodes.clear();
		//		android_listener.op_a_nodes.clear();

		swift_listner 			= new SwiftListenerWithoutContext(swift);
		android_listener 		= new JavaWithoutContext(android);

	}
	double cost = 0;
	double prev_cost = 0;
	int prev_id =0;
	public void waking(){




		cost = 0;
		android_to_swift_point.clear();
		android_to_swift.clear();

		swift_listner.termsMap.clear();
		android_listener.op_a_nodes.clear();
		swift_listner.op_s_nodes.clear();
		android_listener.termsMap.clear();
		swift_listner.termsMapOnlys.clear();
		android_listener.termsMapOnlys.clear();

		android_listener.blockIndex = null;
		swift_listner.blockIndex = null;
		//		android_listener.leftIndex = null;
		//		swift_listner.leftIndex = null;

		amap_ct.clear();
		smap_ct.clear();

		example_sql = "";
		template_sql=""; 
		binding_sql="";

		//		nextMapping.clear();
		android_listener.scope_constraint.clear();
		swift_listner.scope_constraint.clear();



		ParseTreeWalker walker_swift = new ParseTreeWalker();
		ParseTreeWalker walker_android = new ParseTreeWalker();
		//		try{
		walker_swift.walk(swift_listner, swift);
		walker_android.walk(android_listener, android);
		//		} catch (java.lang.NullPointerException e){
		//			System.out.println(android+"  "+swift);
		//		}

		//		System.err.println(android_listener.op_a_nodes+" "+android_listener.blacklist);

		constraint.clear();
		/*
		Map<Interval, String> terms = android_listener.termsMap;

		Map<Interval, String> terms_swft = swift_listner.termsMap;

		List<CommonOperators> lefttoright = ListUtils.longestCommonSubsequence(android_listener.termsMapOnlys, swift_listner.termsMapOnlys);
		List<CommonOperators> _android_ops_cluster = new LinkedList<CommonOperators>(), swift_ops_cluster = new LinkedList<CommonOperators>();



		//tokenList
		List<Interval> list = new ArrayList<Interval>(terms.keySet());
		Interval last = null;
		String ops_android = "";
		int j=0;
		//		System.out.println("aterms"+terms);
		int margin = 2;
		for(CommonOperators c:android_listener.termsMapOnlys){

			int i=list.indexOf(c.interval);
			if(i>0){
				//						if(){

				//						}
				if(last==null) last = list.get(i-1);
				//						System.out.println("last1	"+Math.abs(last.a-list.get(i-1).a));
				if(Math.abs(last.b-list.get(i-1).a) <=margin){
					//update last
					last=last.union(list.get(i-1));
					//							System.out.println("last1	"+last);
				} else{
					_android_ops_cluster.add(new CommonOperators(ops_android, last));
					last = list.get(i-1);
					ops_android ="";
					//							System.out.println("last1<	"+last);
				}
				//						newlefttoright.add(new CommonOperators(terms.get(list.get(i-1)), list.get(i-1))); //left
				//						last  = list.get(i-1);


				if(Math.abs(last.b-list.get(i).a) <=margin){
					last=last.union(list.get(i));
					ops_android +=terms.get(list.get(i));
					//						System.out.println("last2	"+last);
				} else{
					_android_ops_cluster.add(new CommonOperators(ops_android, last));
					ops_android ="";
					last = list.get(i);
					//						System.out.println("last2<	"+last);
				}

				if(Math.abs(last.b-list.get(i+1).a) <=margin){last=last.union(list.get(i+1));
				} else{
					_android_ops_cluster.add(new CommonOperators(ops_android, last));
					ops_android ="";
					last = list.get(i+1);
				}

				j++;
				if(j==lefttoright.size()){
					_android_ops_cluster.add(new CommonOperators(ops_android, last));
				}
			}
		}


		List<CommonOperators> righttoleft = ListUtils.longestCommonSubsequence(swift_listner.termsMapOnlys, android_listener.termsMapOnlys);

		list = new ArrayList<Interval>(terms_swft.keySet());
		last = null;
		ops_android = "";
		j=0;
		//		System.out.println("sterms"+terms_swft);
		for(CommonOperators c:swift_listner.termsMapOnlys){

			int i=list.indexOf(c.interval);
			if(i>0){
				//						if(){

				//						}
				if(last==null) last = list.get(i-1);
				//						System.out.println("last1	"+Math.abs(last.a-list.get(i-1).a));
				if(Math.abs(last.b-list.get(i-1).a) <=margin){
					//update last
					last=last.union(list.get(i-1));
					//							System.out.println("last1	"+last);
				} else{
					swift_ops_cluster.add(new CommonOperators(ops_android, last));
					last = list.get(i-1);
					ops_android ="";
					//							System.out.println("last1<	"+last);
				}
				//						newlefttoright.add(new CommonOperators(terms.get(list.get(i-1)), list.get(i-1))); //left
				//						last  = list.get(i-1);


				if(Math.abs(last.b-list.get(i).a) <=margin){
					last=last.union(list.get(i));
					ops_android +=terms_swft.get(list.get(i));
					//						System.out.println("last2	"+last);
				} else{
					swift_ops_cluster.add(new CommonOperators(ops_android, last));
					ops_android ="";
					last = list.get(i);
					//						System.out.println("last2<	"+last);
				}

				if(Math.abs(last.b-list.get(i+1).a) <=margin){last=last.union(list.get(i+1));
				} else{
					swift_ops_cluster.add(new CommonOperators(ops_android, last));
					ops_android ="";
					last = list.get(i+1);
				}
				j++;
				if(j==swift_listner.termsMapOnlys.size()){
					swift_ops_cluster.add(new CommonOperators(ops_android, last));
				}
			}
		}//get sequence operators in swift


		//		System.out.println("lefttoright"+lefttoright);
		//		System.out.println("righttoleft"+righttoleft);

				System.out.println("newlefttoright"+_android_ops_cluster+"   "+android_listener.termsMapOnlys);
				System.out.println("newrighttoleft"+swift_ops_cluster+"   "+swift_listner.termsMapOnlys+"   ");

//		Iterator<CommonOperators> and_it = newlefttoright.iterator();
//		Iterator<CommonOperators> swt_it = newrighttoleft.iterator();

		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		double[][] aecostMatrix = new double[_android_ops_cluster.size()][swift_ops_cluster.size()];
		CostLCS[][] ccMatrix = new CostLCS[_android_ops_cluster.size()][swift_ops_cluster.size()];


		//optimal alignment of operator sequence
		//common sub sequence of operator sequence
		for(int ai=0; ai<_android_ops_cluster.size();ai++){
			CommonOperators a_common = _android_ops_cluster.get(ai);
			String a_ops=a_common.text;
			String a_ctx = "a", s_ctx = "s";

			for(int si=0; si<swift_ops_cluster.size();si++){
				CommonOperators s_common = swift_ops_cluster.get(si);
				String s_ops=s_common.text;
				if(a_ops.equals(s_ops)){
					System.out.println("equals "+a_ops+"  and "+s_ops);
					a_ctx = androidCommonStream.getText(a_common.interval);
					s_ctx = swiftCommonStream.getText(s_common.interval);
					aecostMatrix[ai][si]= Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx));
					ccMatrix[ai][si] = new CostLCS(Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx)), a_common.interval, s_common.interval);

				} //just compute similarity




				else if((s_ops.length()> a_ops.length()) && s_ops.contains(a_ops)){
					double min_score = 1;
					int index = 0;
					System.out.println("contains "+a_ops+"  of "+s_ops);

					//a_ops is the most common sequence
					a_ctx = androidCommonStream.getText(a_common.interval);
					Interval maxInterval = s_common.interval;
					int c=a_common.opcount(); //.. : 2
					int s_size = s_common.opcount(); //....:4

					//windowing by the common op_seq ..
					for(int w=0; w< s_size-c+1; w++){
						List<CommonOperators> sublisting = swift_listner.termsMapOnlys.subList(w, w+c);
						sublisting = CommonSeqToClustering.clustering(sublisting, terms_swft);
						Interval aaaaa = sublisting.get(0).interval;
						String opsss=sublisting.get(0).text;
						//						CommonSeqToClustering.opText(list);

						if(opsss.equals(a_ops)){ //op_seq windowing matched

							s_ctx = swiftCommonStream.getText(aaaaa);

							double score = Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx));
							System.err.println("score"+" "+a_ctx+" "+s_ctx+"  "+opsss+"  "+score);
							if(min_score>=score){
								//								constraint.put(aaaaa, sssss);
								min_score = score;
								//store the maximum subseqence
								index = si;
								maxInterval = aaaaa;

							}

						}
					}
					aecostMatrix[ai][si]= min_score;
					ccMatrix[ai][si] 	= new CostLCS(min_score, a_common.interval, maxInterval);


				} //compute the most similar

				else if((s_ops.length() <  a_ops.length())){
//				else if((s_ops.length() <  a_ops.length()) && a_ops.contains(s_ops)){
					//					System.out.println("s_ops.length() <  a_");


					double min_score = 1;
					int index = 0;
					//					System.out.println("contains "+s_ops+"  of "+a_ops);

					//a_ops is the most common sequence
					s_ctx = swiftCommonStream.getText(s_common.interval);
					Interval maxInterval = s_common.interval;
					int c=s_common.opcount(); //. : 1
					int a_size = a_common.opcount(); //...:3


					//windowing by the common op_seq ..
					for(int w=0; w< a_size-c+1; w++){
						List<CommonOperators> sublisting = android_listener.termsMapOnlys.subList(w, w+c);
						sublisting = CommonSeqToClustering.clustering(sublisting, terms);
						Interval aaaaa = sublisting.get(0).interval;
						String opsss=sublisting.get(0).text;
						//						CommonSeqToClustering.opText(list);

						if(opsss.equals(s_ops)){ //op_seq windowing matched

							a_ctx = androidCommonStream.getText(aaaaa);

							double score = Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx));
							System.err.println("score"+" "+a_ctx+" "+s_ctx+"  "+opsss+"  "+score);
							if(min_score>=score){
								//								constraint.put(aaaaa, sssss);
								min_score = score;
								//store the maximum subseqence
								index = si;
								maxInterval = aaaaa;

							}

						}
					}

					aecostMatrix[ai][si]= min_score;
					ccMatrix[ai][si] 	= new CostLCS(min_score, maxInterval, s_common.interval);



				} //compute the most similar

				else{ //worse case no common ops
					//					System.out.println("no commons then make the cost maximum");
					aecostMatrix[ai][si]=1;
					ccMatrix[ai][si] 	= new CostLCS(1, a_common.interval, s_common.interval);
				} //no commons then make the cost maximum
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		//			int[] eresult_copy = new int[0];
		if(_android_ops_cluster.size() > 0 && swift_ops_cluster.size() >0){
			ehung = new HungarianAlgorithm(aecostMatrix);
			eresult = ehung.execute();
			//				eresult_copy = ehung.execute();
		}


		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1 && aecostMatrix[p][eresult[p]] !=1){

				System.out.println("final"+ _android_ops_cluster.get(p)+"  "+swift_ops_cluster.get(eresult[p]));
				System.out.println("finalcc	"+ccMatrix[p][eresult[p]]+"  "+androidCommonStream.getText(ccMatrix[p][eresult[p]].minIntvlA)+"    "+
						swiftCommonStream.getText(ccMatrix[p][eresult[p]].minItvlS));
				constraint.put(ccMatrix[p][eresult[p]].minIntvlA, ccMatrix[p][eresult[p]].minItvlS);
			}
		}

		//		constraint.put(new Interval(4, 4), new Interval(3, 3));
		//		constraint.put(new Interval(17, 17), new Interval(12, 12));

		//				constraint.put(new Interval(22, 30), new Interval(21, 29));
		//		16..26
		Iterator<Interval> constraint_keys = constraint.keySet().iterator();
		while(constraint_keys.hasNext()){
			Interval next_c = constraint_keys.next();
			System.out.println("const "+next_c+"    "+constraint.get(next_c));
			System.out.println("next_c"+androidCommonStream.getText(next_c)+"  "+swiftCommonStream.getText(constraint.get(next_c)));
		}
		//		List<Interval> listc = new LinkedList<Interval>();
		//		listc.add(new Interval(5, 5));
		//		listc.add(new Interval(14, 14));
		//		listc.add(new Interval(29, 29));

		//		constraintRefAndroid.put(new Interval(5, 5), listc);
		//		System.out.println();
	}
		
	*/	
		
		
		
		
		
		
		
		
		
		
		Map<Interval, String> terms = android_listener.termsMap;

		Map<Interval, String> terms_swft = swift_listner.termsMap;

		List<CommonOperators> lefttoright = ListUtils.longestCommonSubsequence(android_listener.termsMapOnlys, swift_listner.termsMapOnlys);
		List<CommonOperators> newlefttoright = new LinkedList<CommonOperators>(), newrighttoleft = new LinkedList<CommonOperators>();



		//tokenList
		List<Interval> list = new ArrayList<Interval>(terms.keySet());
		Interval last = null;
		String ops_android = "";
		//		List<Interval> oplist = new ArrayList<Interval>();
		int j=0;
		//		System.out.println("aterms"+terms);
		int margin = 2;
		for(CommonOperators c:android_listener.termsMapOnlys){

			int i=list.indexOf(c.interval);
			if(i>0){
				//						if(){

				//						}
				if(last==null) last = list.get(i-1);
				//						System.out.println("last1	"+Math.abs(last.a-list.get(i-1).a));
				if(Math.abs(last.b-list.get(i-1).a) <=margin){
					//update last
					last=last.union(list.get(i-1));
					//							System.out.println("last1	"+last);
				} else{

					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					newlefttoright.add(common);

					//					newlefttoright.add(new CommonOperators(ops_android, last));
					last = list.get(i-1);
					ops_android ="";
					//					oplist.clear();
					//							System.out.println("last1<	"+last);
				}
				//						newlefttoright.add(new CommonOperators(terms.get(list.get(i-1)), list.get(i-1))); //left
				//						last  = list.get(i-1);


				if(Math.abs(last.b-list.get(i).a) <=margin){
					last=last.union(list.get(i));
					ops_android +=terms.get(list.get(i));
					//					oplist.add(c.interval);

					//						System.out.println("last2	"+last);
				} else{
					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					newlefttoright.add(common);
					ops_android ="";
					//					oplist.clear();
					last = list.get(i);
					//						System.out.println("last2<	"+last);
				}

				if(Math.abs(last.b-list.get(i+1).a) <=margin){
					last=last.union(list.get(i+1));


				} else{
					//					newlefttoright.add(new CommonOperators(ops_android, last));
					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					newlefttoright.add(common);
					ops_android ="";
					//					oplist.clear();
					last = list.get(i+1);
				}

				j++;
				if(j==lefttoright.size()){
					//					newlefttoright.add(new CommonOperators(ops_android, last));
					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					newlefttoright.add(common);
				}
			}
		}

		//		oplist.clear();
		List<CommonOperators> righttoleft = ListUtils.longestCommonSubsequence(swift_listner.termsMapOnlys, android_listener.termsMapOnlys);
		//		System.out.println("righttoleft"+righttoleft);
				System.out.println("android_listener.termsMapOnlys"+android_listener.termsMapOnlys);
				System.out.println("swift_listner.termsMapOnlys"+swift_listner.termsMapOnlys);
				
		list = new ArrayList<Interval>(terms_swft.keySet());
		last = null;
		ops_android = "";
		j=0;
		//		oplist.clear();
		//		System.out.println("sterms"+terms_swft);
		for(CommonOperators c:swift_listner.termsMapOnlys){

			int i=list.indexOf(c.interval);
			if(i>0){
				//						if(){

				//						}
				if(last==null) last = list.get(i-1);
				//						System.out.println("last1	"+Math.abs(last.a-list.get(i-1).a));
				if(Math.abs(last.b-list.get(i-1).a) <=margin){
					//update last
					last=last.union(list.get(i-1));
					//							System.out.println("last1	"+last);
				} else{
					newrighttoleft.add(new CommonOperators(ops_android, last));

					//					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					//					newrighttoleft.add(common);
					ops_android ="";
					//					oplist.clear();

					last = list.get(i-1);
					ops_android ="";
					//							System.out.println("last1<	"+last);
				}
				//						newlefttoright.add(new CommonOperators(terms.get(list.get(i-1)), list.get(i-1))); //left
				//						last  = list.get(i-1);


				if(Math.abs(last.b-list.get(i).a) <=margin){
					last=last.union(list.get(i));
					ops_android +=terms_swft.get(list.get(i));
					//					oplist.add(c.interval);
					//						System.out.println("last2	"+last);
				} else{
					newrighttoleft.add(new CommonOperators(ops_android, last));
					ops_android ="";

					//					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					//					newrighttoleft.add(common);
					//					ops_android ="";
					//					oplist.clear();

					last = list.get(i);
					//						System.out.println("last2<	"+last);
				}

				if(Math.abs(last.b-list.get(i+1).a) <=margin){last=last.union(list.get(i+1));
				} else{
					newrighttoleft.add(new CommonOperators(ops_android, last));
					ops_android ="";

					//					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					//					newrighttoleft.add(common);
					//					ops_android ="";
					//					oplist.clear();

					last = list.get(i+1);
				}
				j++;
				if(j==swift_listner.termsMapOnlys.size()){
					newrighttoleft.add(new CommonOperators(ops_android, last));
					//					CommonOperators common = new CommonOperators(ops_android, last);
					//					common.ops_intervals = oplist;
					//					newrighttoleft.add(common);
				}
			}
		}//get sequence operators in swift


		//		System.out.println("lefttoright"+lefttoright);
		//		System.out.println("righttoleft"+righttoleft);

		//		System.out.println("newlefttoright"+newlefttoright+"   "+android_listener.termsMapOnlys);
		//		System.out.println("newrighttoleft"+newrighttoleft+"   "+swift_listner.termsMapOnlys+"   ");

		//		Iterator<CommonOperators> and_it = newlefttoright.iterator();
		//		Iterator<CommonOperators> swt_it = newrighttoleft.iterator();

		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		double[][] aecostMatrix = new double[newlefttoright.size()][newrighttoleft.size()];
		CostLCS[][] ccMatrix = new CostLCS[newlefttoright.size()][newrighttoleft.size()];


		//optimal alignment of operator sequence
		//common sub sequence of operator sequence
		for(int ai=0; ai<newlefttoright.size();ai++){
			CommonOperators a_common = newlefttoright.get(ai);
			String a_ops=a_common.text;
			String a_ctx = "a", s_ctx = "s";

			for(int si=0; si<newrighttoleft.size();si++){
				CommonOperators s_common = newrighttoleft.get(si);
				String s_ops=s_common.text;
				if(a_ops.equals(s_ops)){
					a_ctx = androidCommonStream.getText(a_common.interval);
					s_ctx = swiftCommonStream.getText(s_common.interval);
					aecostMatrix[ai][si]= Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx));
					ccMatrix[ai][si] = new CostLCS(Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx)), a_common.interval, s_common.interval);

				} //just compute similarity



				else if((s_ops.length()> a_ops.length()) && s_ops.contains(a_ops)){
					double min_score = 1;
					int index = 0;
					//					System.out.println("contains "+a_ops+"  of "+s_ops);

					//a_ops is the most common sequence
					a_ctx = androidCommonStream.getText(a_common.interval);
					Interval maxInterval = s_common.interval;
					int c=a_common.opcount(); //.. : 2
					int s_size = s_common.opcount(); //....:4

					//windowing by the common op_seq ..
					for(int w=0; w< s_size-c+1; w++){
						List<CommonOperators> sublisting = swift_listner.termsMapOnlys.subList(w, w+c);
						sublisting = CommonSeqToClustering.clustering(sublisting, terms_swft);
						Interval aaaaa = sublisting.get(0).interval;
						String opsss=sublisting.get(0).text;
						//						CommonSeqToClustering.opText(list);

						if(opsss.equals(a_ops)){ //op_seq windowing matched

							s_ctx = swiftCommonStream.getText(aaaaa);

							double score = Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx));
							//							System.err.println("score"+" "+a_ctx+" "+s_ctx+"  "+opsss+"  "+score);
							if(min_score>=score){
								//								constraint.put(aaaaa, sssss);
								min_score = score;
								//store the maximum subseqence
								index = si;
								maxInterval = aaaaa;

							}

						}
					}
					aecostMatrix[ai][si]= min_score;
					ccMatrix[ai][si] 	= new CostLCS(min_score, a_common.interval, maxInterval);


				} //compute the most similar


				else if((s_ops.length() <  a_ops.length()) && a_ops.contains(s_ops)){
					//					System.out.println("s_ops.length() <  a_");


					double min_score = 1;
					int index = 0;
					//					System.out.println("contains "+s_ops+"  of "+a_ops);

					//a_ops is the most common sequence
					s_ctx = swiftCommonStream.getText(s_common.interval);
					Interval maxInterval = s_common.interval;
					int c=s_common.opcount(); //. : 1
					int a_size = a_common.opcount(); //...:3


					//windowing by the common op_seq ..
					for(int w=0; w< a_size-c+1; w++){
						List<CommonOperators> sublisting = android_listener.termsMapOnlys.subList(w, w+c);
						sublisting = CommonSeqToClustering.clustering(sublisting, terms);
						Interval aaaaa = sublisting.get(0).interval;
						String opsss=sublisting.get(0).text;
						//						CommonSeqToClustering.opText(list);

						if(opsss.equals(s_ops)){ //op_seq windowing matched

							a_ctx = androidCommonStream.getText(aaaaa);

							double score = Math.min(0.999, 1- emetric.compare(a_ctx, s_ctx));
							//							System.err.println("score"+" "+a_ctx+" "+s_ctx+"  "+opsss+"  "+score);
							if(min_score>=score){
								//								constraint.put(aaaaa, sssss);
								min_score = score;
								//store the maximum subseqence
								index = si;
								maxInterval = aaaaa;

							}

						}
					}

					aecostMatrix[ai][si]= min_score;
					ccMatrix[ai][si] 	= new CostLCS(min_score, maxInterval, s_common.interval);



				} //compute the most similar

				else{ //worse case no common ops
					//					System.out.println("no commons then make the cost maximum");
					aecostMatrix[ai][si]=1;
					ccMatrix[ai][si] 	= new CostLCS(1, a_common.interval, s_common.interval);
				} //no commons then make the cost maximum
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		//			int[] eresult_copy = new int[0];
		if(newlefttoright.size() > 0 && newrighttoleft.size() >0){
			ehung = new HungarianAlgorithm(aecostMatrix);
			eresult = ehung.execute();
			//				eresult_copy = ehung.execute();
		}


		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1 && aecostMatrix[p][eresult[p]] !=1){

				//				System.out.println("final"+ newlefttoright.get(p)+"  "+newrighttoleft.get(eresult[p]));
				//				System.out.println("finalops_intervals"+ newlefttoright.get(p).ops_intervals);
				//				System.out.println("swiftfinalops_intervals"+ newrighttoleft.get(p).ops_intervals);
				List<Interval> alist = new LinkedList<Interval>();
				List<Interval> slist = new LinkedList<Interval>();
				for(CommonOperators c:android_listener.termsMapOnlys){

					if(ccMatrix[p][eresult[p]].minIntvlA.properlyContains(c.interval)){
						alist.add(c.interval);
					}
				}

				for(CommonOperators cs:swift_listner.termsMapOnlys){
					if(ccMatrix[p][eresult[p]].minItvlS.properlyContains(cs.interval)){
						slist.add(cs.interval);
					}
				}
				for(int jjj=0; jjj< alist.size(); jjj++){
					constraint.put(alist.get(jjj), slist.get(jjj));
				}
				//				


				System.out.println("finalcc	"+ccMatrix[p][eresult[p]]+"  "+androidCommonStream.getText(ccMatrix[p][eresult[p]].minIntvlA)+"    "+
						swiftCommonStream.getText(ccMatrix[p][eresult[p]].minItvlS));
				//				constraint.put(ccMatrix[p][eresult[p]].minIntvlA, ccMatrix[p][eresult[p]].minItvlS);
			}
		}

		//		constraint.put(new Interval(1, 1), new Interval(5, 5));
		//		constraint.put(new Interval(3, 3), new Interval(7, 7));
		//		constraint.put(new Interval(5, 5), new Interval(9, 9));
		//		constraint.put(new Interval(7, 7), new Interval(11, 11));

		//		constraint.put(new Interval(1, 1), new Interval(1, 1));
		//		constraint.put(new Interval(3, 3), new Interval(3, 3));
		//		constraint.put(new Interval(5, 5), new Interval(5, 5));
		//		constraint.put(new Interval(7, 7), new Interval(7, 7));
		//		const 0..8    4..12
		//		next_cmXBounds.range+mXBounds.min-z  _xBounds.range+_xBounds.min-z
		//		terms_android	{1..1=., 3..3=+, 5..5=., 7..7=-}
		//		terms_swft	{1..1=., 3..3=+, 5..5=., 7..7=+, 9..9=., 11..11=-}
		//		constraint.put(new Interval(4, 4), new Interval(3, 3));
		//		constraint.put(new Interval(17, 17), new Interval(12, 12));

		//				constraint.put(new Interval(22, 30), new Interval(21, 29));
		//		16..26
		Iterator<Interval> constraint_keys = constraint.keySet().iterator();
		while(constraint_keys.hasNext()){
			Interval next_c = constraint_keys.next();
//			System.out.println("const "+next_c+"    "+constraint.get(next_c));
			//			System.out.println("next_c"+androidCommonStream.getText(next_c)+"  "+swiftCommonStream.getText(constraint.get(next_c)));
		}
		//		List<Interval> listc = new LinkedList<Interval>();
		//		listc.add(new Interval(5, 5));
		//		listc.add(new Interval(14, 14));
		//		listc.add(new Interval(29, 29));

		//		constraintRefAndroid.put(new Interval(5, 5), listc);
		//		System.out.println();
	}


	/**
	 * With constraint map, checking relation interval-a and interval-s
	 * @param a
	 * @param s
	 * @return
	 */
	public boolean constraintMet(Interval a, Interval s){
		Iterator<Interval> a_anchors = constraint.keySet().iterator();
		boolean isMet = true;
		while(a_anchors.hasNext()){
			Interval a_anchor=a_anchors.next();
			Interval s_anchor=constraint.get(a_anchor);
			if(a_anchor.disjoint(a) && s_anchor.disjoint(s)){
				isMet = isMet && true;
			} else if((!a_anchor.disjoint(a) | !s_anchor.disjoint(s))){
				boolean a_valid = true, s_valid = true;
				if(a.length()> a_anchor.length()){
					a_valid = a.properlyContains(a_anchor);
				}else{
					a_valid = a_anchor.properlyContains(a);
				}

				if(s.length()> s_anchor.length()){
					s_valid = s.properlyContains(s_anchor);
				}else{
					s_valid = s_anchor.properlyContains(s);
				}
				isMet = a_valid && s_valid && isMet;
				//				isMet = a.properlyContains(a_anchor) && s.properlyContains(s_anchor) && isMet;
				//				isMet = a_anchor.properlyContains(a) && s_anchor.properlyContains(s) && isMet;
			}
		}
		return isMet;
	}
	/*
	public boolean constraintMet(Interval a, Interval s){
		Iterator<Interval> a_anchors = constraint.keySet().iterator();
		boolean isMet = true;
		while(a_anchors.hasNext()){
			Interval a_anchor=a_anchors.next();
			Interval s_anchor=constraint.get(a_anchor);
			if(a_anchor.disjoint(a) && s_anchor.disjoint(s)){
				isMet = isMet && true;
			} else if((!a_anchor.disjoint(a) || !s_anchor.disjoint(s))){
				isMet = a.properlyContains(a_anchor) && s.properlyContains(s_anchor) && isMet;
				//				isMet = a_anchor.properlyContains(a) && s_anchor.properlyContains(s) && isMet;
			}
		}
		return isMet;
	}
	 */
	//	public void compute() throws FileNotFoundException, IOException{
	public MappingOutput compute() throws FileNotFoundException, IOException{

		//		waking();

		int az = swift.start.getStartIndex(), bz = swift.stop.getStopIndex();
		Interval swift_interval = new Interval(az,bz);

		int az1 = android.start.getStartIndex(), bz1 = android.stop.getStopIndex();
		Interval android_interval = new Interval(az1,bz1);

		//				System.out.println("android_listener.a_nodes"+android_listener.a_nodes);
		//				System.err.println("swift_listner.s_node"+swift_listner.s_nodes);
		//children mappings
		//resolution {} blocks, = assignments and expressions
		if(android_listener.a_nodes.size()>0 && swift_listner.s_nodes.size()>0){

			//matching and save them
			//a_nodes or s_nodes stores statements
			double[][] aecostMatrix = new double[android_listener.a_nodes.size()][swift_listner.s_nodes.size()];

			StringMetric emetric =
					with(new JaroWinkler())
					.build();

			for(int i=0;i<android_listener.a_nodes.size();i++){
				for(int j=0;j<swift_listner.s_nodes.size();j++){
					if( android_listener.a_nodes.get(i).property.equals(swift_listner.s_nodes.get(j).property)) {
						//					if( android_listener.a_nodes.get(i).property.equals(swift_listner.s_nodes.get(j).property) && toCommon(android_listener.a_nodes.get(i).strASTNode,swift_listner.s_nodes.get(j).strASTNode)>=0.0) {
						aecostMatrix[i][j]	= Math.min(0.999, 1- emetric.compare(android_listener.a_nodes.get(i).strASTNode, swift_listner.s_nodes.get(j).strASTNode));
					}else{
						aecostMatrix[i][j] 	= 1;
					}


				}
			}

			HungarianAlgorithm ehung = null;
			int[] eresult = new int[0];
			//			int[] eresult_copy = new int[0];
			if(android_listener.a_nodes.size() > 0 && swift_listner.s_nodes.size() >0){
				ehung 	= new HungarianAlgorithm(aecostMatrix);
				eresult = ehung.execute();

				//				eresult_copy = ehung.execute();
			}


			for(int p=0; p<eresult.length;p++){
				if(eresult[p]!=-1 && eresult[p]<1){
					//				if(1==1){

					//					String a_exam = android_listener.a_nodes.get(p).strASTNode;
					//					String s_exam = swift_listner.s_nodes.get(eresult[p]).strASTNode;
					if(
							android_listener.a_nodes.get(p).node!=null 
							&& android_listener.a_nodes.get(p).node instanceof ParserRuleContext 
							&& swift_listner.s_nodes.get(eresult[p]).node!=null 
							&& swift_listner.s_nodes.get(eresult[p]).node instanceof ParserRuleContext
							)
					{
						StructureMappingAntlr4_for_stmt3 mapping = new StructureMappingAntlr4_for_stmt3((ParserRuleContext) android_listener.a_nodes.get(p).node, (ParserRuleContext)swift_listner.s_nodes.get(eresult[p]).node);
						mapping.afilename = afilename;
						mapping.sfilename = sfilename;
						mapping.androidCommonStream = this.androidCommonStream;
						mapping.swiftCommonStream = this.swiftCommonStream;
						//						System.out.println("examples"+mapping.android.getText()+" "+mapping.swift.getText());

						//						mapping.waking();
						//						mapping.compute();
					}
				}
			}

		} /**enclosed's children mappings using by {{...}}, = **/




		double[][] ecostMatrix = new double[android_listener.op_a_nodes.size()][swift_listner.op_s_nodes.size()];
		for(int k=0; k<android_listener.op_a_nodes.size();k++){
			Arrays.fill(ecostMatrix[k], 1);
		}

		//		MappingOutput mappingout = new MappingOutput();
		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		for(int i=0;i<android_listener.op_a_nodes.size();i++){
			for(int j=0;j<swift_listner.op_s_nodes.size();j++){
				boolean isNumA = NumberUtils.isNumber(android_listener.op_a_nodes.get(i).strASTNode);
				boolean isNumS = NumberUtils.isNumber(swift_listner.op_s_nodes.get(j).strASTNode);


				if( !swift_listner.op_s_nodes.get(j).property.equals("!none")	&& 
						!android_listener.op_a_nodes.get(i).property.equals("!none") && 
						android_listener.op_a_nodes.get(i).property.equals(swift_listner.op_s_nodes.get(j).property)
						//						&& toCommon(android_listener.op_a_nodes.get(i).strASTNode,swift_listner.op_s_nodes.get(j).strASTNode)>=0
						&& ((isNumA && isNumS) ||(!isNumA && !isNumS))
						) {

					if(constraintMet(android_listener.op_a_nodes.get(i).interval, swift_listner.op_s_nodes.get(j).interval)){
						ecostMatrix[i][j]	= Math.min(0.999, 1- emetric.compare(android_listener.op_a_nodes.get(i).strASTNode.replace(";", "").replace("this", ""), swift_listner.op_s_nodes.get(j).strASTNode.replace("var", "").replace("self", "")));
					}//if anchors(+/*-.{}= constraint met the syntax mapping score not to maxima, followed by the String similarity
					else{
						ecostMatrix[i][j] 	= 1;
					}//else the syntax mapping score not to maxima, cross-level anchoring
				}else{
					ecostMatrix[i][j] 	= 1;
				}

				//				System.out.println("ecostMatrix[i][j]"+ecostMatrix[i][j]+"  "+wocontext.op_a_nodes.get(i)+"  "+swictx.op_s_nodes.get(j));
			}
		} //cost matrix ecostMatrix[i][j]

		int[] eresult 		= new int[0];
		List<int[]> rowsols = new LinkedList<int[]>();
		//		int NUM_K_HUNG = 30;
		int NUM_K_HUNG = 1;

		if(android_listener.op_a_nodes.size() == 0 || swift_listner.op_s_nodes.size() == 0) return null;

		if(android_listener.op_a_nodes.size()<=swift_listner.op_s_nodes.size()){
			try{
				rowsols = Murty.solve(ecostMatrix,NUM_K_HUNG);
			} catch (java.lang.ArrayIndexOutOfBoundsException e){
				e.printStackTrace();
			}


			Comparator<OpProperty> comparator = new Comparator<OpProperty>() {
				public int compare(OpProperty o1, OpProperty o2) {
					if(o1.interval.length()>o2.interval.length()) return 1; 
					else if(o1.interval.length()==o2.interval.length()){
						if(o1.interval.a <= o2.interval.a) return 1;
						else return -1;
					}
					else return -1;
				}
			};

			//			System.out.println("rowsols.size()	"+rowsols.size());
			int k =0;
			//			boolean notfoundA = true, notfoundS = true;
			//iterations
			//			for (int r =0; r<rowsols.size() && notfoundA && notfoundS; r++) {

			for (int r =0; r<rowsols.size(); r++) {
				eresult = rowsols.get(r);
				cost = 0;
				for (int i = 0; i < eresult.length; i++) {

					android_to_swift.put(i, eresult[i]);
					android_to_swift_point.put(new Pair(i, eresult[i], android_listener.op_a_nodes.get(i).interval, swift_listner.op_s_nodes.get(eresult[i]).interval), ecostMatrix[i][eresult[i]]);
				}


				SortedSet<OpProperty> akeys = new TreeSet<OpProperty>(comparator);
				SortedSet<OpProperty> skeys = new TreeSet<OpProperty>(comparator);
				Map<OpProperty, String> aamap 	= new HashMap<OpProperty, String>();


				//			Map<String, String> smap 		= new HashMap<String, String>();
				Map<OpProperty, String> ssmap 				= new HashMap<OpProperty, String>();
				LinkedHashMap<OpProperty, OpProperty> nextMapping = new LinkedHashMap<OpProperty, OpProperty>();

				Map<Interval, String> terms = android_listener.termsMap;
				Map<Interval, String> terms_swft = swift_listner.termsMap;

				for(int p=0; p<eresult.length;p++){
					//					if(eresult[p]!=-1){
					//						System.out.println("rank "+k+" ecostMatrix"+"["+p+"]"+"["+eresult[p]+"]="+ecostMatrix[p][eresult[p]]+" "+android_listener.op_a_nodes.get(p).strASTNode+"  "+android_listener.op_a_nodes.get(p).property+"  "+swift_listner.op_s_nodes.get(eresult[p]).strASTNode+"  "+swift_listner.op_s_nodes.get(eresult[p]).property);
					//					}

					if(eresult[p]!=-1 && ecostMatrix[p][eresult[p]]<1){
						cost +=ecostMatrix[p][eresult[p]];
					} //get mapping cost 
				} //iteration of ...

				//				MappingOutput mappingout = new MappingOutput();
				int argnum = 0;
				List<Interval> listi = new LinkedList<Interval>();

				Iterator<Integer> ittt = android_to_swift.keySet().iterator();
				while(ittt.hasNext()){
					int aa = ittt.next();
					int ss = android_to_swift.get(aa);

					if(ecostMatrix[aa][eresult[aa]]<1){

						String a_p=android_listener.op_a_nodes.get(aa).property;
						String s_p=swift_listner.op_s_nodes.get(ss).property;

						if(a_p.equals("enclosed{}") && s_p.equals("enclosed{}")){
							android_to_swift_point.remove(new Pair(aa, ss));
							//						amap.put(a, "{...}");
							aamap.put(android_listener.op_a_nodes.get(aa), "{...}");
							//					amap_ct.put(wocontext.op_a_nodes.get(aa).interval, "{...}");
							//						smap.put(s, "{...}");
							ssmap.put(swift_listner.op_s_nodes.get(ss), "{...}");

						}

						else{

							aamap.put(android_listener.op_a_nodes.get(aa), "<arg"+argnum+">");
							//						amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
							//						smap.put(s, "<arg"+argnum+">");

							amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
							ssmap.put(swift_listner.op_s_nodes.get(ss), "<arg"+argnum+">");
							smap_ct.put(swift_listner.op_s_nodes.get(ss).interval, "<arg"+argnum+">");
							nextMapping.put(android_listener.op_a_nodes.get(aa),swift_listner.op_s_nodes.get(ss));
							cost += ecostMatrix[aa][eresult[aa]];
							Iterator<Interval> scopes = swift_listner.op_s_nodes.get(ss).same_scope.keySet().iterator();
							while(scopes.hasNext()){
								Interval sc = scopes.next();
								ssmap.put(swift_listner.op_s_nodes.get(ss).same_scope.get(sc), "<arg"+argnum+">");
							}

							Iterator<Interval> scopes_a = android_listener.op_a_nodes.get(aa).same_scope.keySet().iterator();

							//if the same scopes in the single statement, then .. linking it.
							while(scopes_a.hasNext()){
								Interval sc = scopes_a.next();
								amap_ct.put(android_listener.op_a_nodes.get(aa).same_scope.get(sc).interval, "<arg"+argnum+">");
								aamap.put(android_listener.op_a_nodes.get(aa).same_scope.get(sc), "<arg"+argnum+">");
								//								System.out.println("aamap.put++"+android_listener.op_a_nodes.get(aa).same_scope.get(sc)+" :"+aamap);
							}

							argnum++;
						}
						listi.add(android_listener.op_a_nodes.get(aa).interval);


						if(android_listener.op_a_nodes.get(aa).node instanceof ParserRuleContext && swift_listner.op_s_nodes.get(ss).node instanceof ParserRuleContext){
							ParserRuleContext anode = (ParserRuleContext)android_listener.op_a_nodes.get(aa).node;
							ParserRuleContext snode = (ParserRuleContext)swift_listner.op_s_nodes.get(ss).node;
							//							StructureMappingAntlr4 mapping = new StructureMappingAntlr4(anode, snode);
							nextMapping.put(android_listener.op_a_nodes.get(aa), swift_listner.op_s_nodes.get(ss));
							//							mapping.waking();
							//							mapping.compute();
							//							cost += ecostMatrix[aa][eresult[aa]];

						} 
					}

				}


				for(int p=0; p<eresult.length;p++){
					boolean disj= true;
					//			ParserRuleContext anode = (ParserRuleContext)wocontext.op_a_nodes.get(p).node;
					//			String anodeText = wocontext.op_a_nodes.get(p).strASTNode;
					Interval anodeInterval = android_listener.op_a_nodes.get(p).interval;
					for(int i=0; i<listi.size();i++){
						//								System.out.println("checking"+anode.getText()+""+listi.get(i).disjoint(anode.getSourceInterval())+" ");
						disj = disj & listi.get(i).disjoint(anodeInterval);
					}
					if(disj && !android_listener.op_a_nodes.get(p).property.equals("constant_a")){
						//					amap.put(android_listener.op_a_nodes.get(p).strASTNode, "<arg"+argnum+">");
						aamap.put(android_listener.op_a_nodes.get(p), "<arg"+argnum+">");
						amap_ct.put(android_listener.op_a_nodes.get(p).interval, "<arg"+argnum+">");
						argnum++;
					}
				}


				//				System.out.println("aamap"+aamap);
				//				System.out.println("ssmap"+ssmap);
				akeys.addAll(aamap.keySet());
				skeys.addAll(ssmap.keySet());

				Iterator<OpProperty> aait = akeys.iterator();
				Iterator<OpProperty> ssit = skeys.iterator();
				//			String android_ctxt = android_str;

				//								System.out.println("aamap\n"+aamap);
				//				System.out.println("terms_android	"+android_listener.termsMapOnly);
				//				System.out.println("op_s\n"+swift_listner.op_s_nodes);
				//								System.out.println("ssmap\n"+ssmap);
				//				System.out.println("terms_swft	"+swift_listner.termsMapOnly);

				//								System.out.println("terms	"+terms);

				while(aait.hasNext()){

					OpProperty key = aait.next();

					//				String expr = key;
					String arg = aamap.get(key);

					Iterator<Interval> itttt = terms.keySet().iterator();
					boolean isFirst = false;
					while(itttt.hasNext()){
						Interval next = itttt.next();
						//						if(!next.disjoint(key.interval) && !isFirst){
						//												terms.put(next, arg);
						//												isFirst = true;
						//						}
						if(!next.disjoint(key.interval) ){
							//					terms.put(next, arg);
							if(!isFirst){terms.put(next, arg); isFirst = true;}
							else{itttt.remove();}
						}
					}

					//			key.interval;
					//			buf.replace(start, end, str);

				} //making template for android
				terms_swft = new HashMap<Interval, String>();
				//				terms_swft.keySet().removeAll(swift_listner.termsMap.keySet());
				//				swift_listner.termsMap.putAll(terms_swft);
				//				terms_swft.keySet().removeAll(swift_listner.termsMap.keySet());
				terms_swft = swift_listner.termsMap;
				//				terms_swft.forEach(swift_listner.termsMap::putIfAbsent);
				//				System.out.println("before:"+terms_swft);
				while(ssit.hasNext()){
					OpProperty key = ssit.next();
					Iterator<Interval> scopes =null;
					//				String expr = key;
					String arg = ssmap.get(key);
					//
					Iterator<Interval> tokens = terms_swft.keySet().iterator();
					boolean isFirst = false;
					while(tokens.hasNext()){
						Interval next = tokens.next();
						//						if(!next.disjoint(key.interval) && !isFirst){
						//					terms.put(next, arg);
						//					isFirst = true;
						//						}
						if(!next.disjoint(key.interval) ){
							//					terms.put(next, arg);

							//adding <arg#>
							if(!isFirst){
								terms_swft.put(next, arg); isFirst = true;


							}
							else{
								tokens.remove();
							}

						}

					}


				} //making template for swift

				//		StringUtils.join(terms.values());
				ArrayList<String> aaaa = new ArrayList<String>(terms.values());
				ArrayList<String> ssss = new ArrayList<String>(terms_swft.values());



				String tempa=StringUtils.join(aaaa.toArray()," ");
				String temps=StringUtils.join(ssss.toArray()," ");



				System.out.println(QueryExamples.idd+"	templateeee\n"+tempa+"\n"+temps);
				aait=aamap.keySet().iterator();


				Iterator<Interval> constraint_iter = constraint.keySet().iterator();
				//				while(constraint_iter.hasNext()){
				//					constraint_iter.next();
				//				}
				/*
				//template token check
				Lexer lexer1 				= new TemplateCLexer((CharStream)new ANTLRInputStream(tempa));
				DSLErrorListener listner 	= new DSLErrorListener();
				lexer1.addErrorListener(listner);


				TemplateCParser parser 		= new TemplateCParser(new CommonTokenStream(lexer1));
				//				toskip = false;
				//				ExceptionErrorStrategy error_handler = new ExceptionErrorStrategy();
				//				parser.removeErrorListeners();
				//				parser.setErrorHandler(error_handler);

				TemplateCParser.TemplateContext comj = parser.template();
				List<String> argSwift = new LinkedList<String>(), argAndroid = new LinkedList<String>();
				comj.accept(new TemplateCBaseVisitor(){
					@Override
					public Object visitArg(ArgContext ctx) {
						// TODO Auto-generated method stub
						argAndroid.add(ctx.getText());
						return super.visitArg(ctx);
					}
				});

				if(listner.hasErrors()){
					//					System.out.println("error in"+tempa);
				}


				lexer1 = new TemplateCLexer((CharStream)new ANTLRInputStream(temps));
				DSLErrorListener listner_swift = new DSLErrorListener();
				lexer1.addErrorListener(listner_swift);



				parser = new TemplateCParser(new CommonTokenStream(lexer1));
				//				parser.setErrorHandler(error_handler);
				TemplateCParser.TemplateContext com = parser.template();

				//				System.out.println("comcom"+com.getText());
				com.accept(new TemplateCBaseVisitor(){
					@Override
					public Object visitArg(ArgContext ctx) {
						// TODO Auto-generated method stub
						//						System.out.println("visitArg");
						argSwift.add(ctx.getText());
						return super.visitArg(ctx);
					}
					public Object visitJava_punc(templatechecker.TemplateCParser.Java_puncContext ctx) {

						return super.visitJava_punc(ctx);
					};
				});

				//				System.out.println("args	"+argAndroid +" "+argSwift);
				if(listner_swift.hasErrors()){
					//					System.out.println("swift error in		"+temps);
				}


				ittt = android_to_swift.keySet().iterator();
				while(ittt.hasNext() && !listner.hasErrors() && !listner_swift.hasErrors()){
					int aa = ittt.next();
					int ss = android_to_swift.get(aa);

					if(ecostMatrix[aa][eresult[aa]]<1){

						if(android_listener.op_a_nodes.get(aa).node instanceof ParserRuleContext && swift_listner.op_s_nodes.get(ss).node instanceof ParserRuleContext){
							//						ParserRuleContext anode = (ParserRuleContext)wocontext.op_a_nodes.get(aa).node;
							StructureMappingAntlr4_for_stmt mapping = new StructureMappingAntlr4_for_stmt((ParserRuleContext)android_listener.op_a_nodes.get(aa).node, (ParserRuleContext)swift_listner.op_s_nodes.get(ss).node);
							if(!android_listener.op_a_nodes.get(aa).property.equals("enclosed{}") && !android_listener.op_a_nodes.get(aa).property.equals("enclosed()")){
								//							System.out.println(a+"<::>"+s);

								//								mapping.compute();
							}
						} 

					}
				}

				 */


				//				String e12 = "", example_sql="", template_sql="", binding_sql="";
				String binding = null;
				try{
					if(DelcarationMapping.methodBinding!=null)
						binding=DelcarationMapping.methodBinding.get(android.start.getLine()+":"+android.start.getCharPositionInLine());
				} catch (Exception e){
					//					e.printStackTrace();
				}

				if(binding !=null){
					//					binding_sql 	= SQLiteJDBC.updateTableBinding(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
					binding_sql = SQLiteJDBC.updateTableBinding2(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval),
							afilename, LineCharIndex.contextToLineIndex(android).toString(),
							sfilename, LineCharIndex.contextToLineIndex(swift).toString()
							);

//					template_sql 	= SQLiteJDBC.updateTableGrammarAndroid(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval));
					//					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval));
					template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, StringUtils.join(aaaa.toArray()," ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."), this.android.getRuleIndex(), swift.getRuleIndex(), StringUtils.join(ssss.toArray()," ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."));

					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
							afilename, LineCharIndex.contextToLineIndex(android).toString(),
							sfilename, LineCharIndex.contextToLineIndex(swift).toString());

				}
				else{
					template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, StringUtils.join(aaaa.toArray()," ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."), this.android.getRuleIndex(), swift.getRuleIndex(), StringUtils.join(ssss.toArray()," ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."));

					//					template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, "rank "+k+" "+StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " "), this.android.getRuleIndex(), swift.getRuleIndex(), StringUtils.join(ssss.toArray()," "));
					//					example_sql = SQLiteJDBC.updateTableGrammarSwift(grammar_id, android.start.getInputStream().getText(android_interval).replaceAll("\n", ""), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
					//					example_sql = SQLiteJDBC.updateTableGrammarSwift(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval));

					/*
					String query = "select id from grammar_android_stat where template=\'"+StringUtils.join(aaaa.toArray()," ") +" and example=\'"+StringUtils.join(ssss.toArray()," ") +"\';";

					Connection c;
					int iddd=0;
					try {
						c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
						java.sql.Statement sql_stmt = c.createStatement();
						c.setAutoCommit(true);
						ResultSet resultSet = sql_stmt.executeQuery(query);
						resultSet.next();
						String bbb = resultSet.getString("example");
						iddd=Integer.parseInt(resultSet.getString("id"));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					 */

					//					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
					//							StringUtils.join(aaaa.toArray()," "), LineCharIndex.contextToLineIndex(android).toString(),
					//							StringUtils.join(ssss.toArray()," "), LineCharIndex.contextToLineIndex(swift).toString());

					//					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
					//							StringUtils.join(aaaa.toArray()," "), nextMapping.keySet().toString(),
					//							StringUtils.join(ssss.toArray()," "), nextMapping.values().toString());

					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, android.start.getInputStream().getText(android_interval).replaceAll("\n", ""), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""), 
							StringUtils.join(aaaa.toArray()," ").replaceAll(" \\.", "\\."), nextMapping.keySet().toString().replaceAll("\n", ""),
							StringUtils.join(ssss.toArray()," ").replaceAll(" \\.", "\\."), nextMapping.values().toString().replaceAll("\n", ""));
				}

				if(tempa.equals("<arg0>") && temps.equals("<arg0>")){
//					template_sql  = "";
					//					template_sql  = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, android.start.getInputStream().getText(android_interval).replaceAll("\n", ""), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
					//					System.out.println(template_sql);
					//	return new MappingOutput(nextMapping, binding_sql, example_sql, template_sql, tempa, temps, cost);
//					if(Character.isUpperCase(android.start.getInputStream().getText(android_interval).replaceAll("\n", "").charAt(0))){
//						template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, android.start.getInputStream().getText(android_interval).replaceAll("\n", ""), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
//						return new MappingOutput(nextMapping, binding_sql, example_sql, template_sql, tempa, temps, cost);
//					}
					return null;
				}
				/*
				mappingout.nextMapping.clear();
				mappingout.nextMapping = nextMapping;
				mappingout.binding_sql = binding_sql;
				mappingout.example_sql = example_sql;
				mappingout.template_sql = template_sql;
				mappingout.template_a = tempa;
				mappingout.template_s= temps;
//				if(listner_swift.hasErrors() || listner.hasErrors())
					mappingout.hasError = true;
//				else 
					mappingout.hasError = false;
				mappingout.cost = cost;
				 */


				grammar_id++;
				return new MappingOutput(nextMapping, binding_sql, example_sql, template_sql, tempa, temps, cost);
				//				doupdate();
				/*
				Connection c;
				try {
					//			if(!isEqualTemplate){
					c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
					java.sql.Statement sql_stmt = c.createStatement();
					c.setAutoCommit(true);
					//			sql_stmt.executeUpdate(e12);
					//					if(!listner.hasErrors() && !listner_swift.hasErrors() && prev_cost!=cost){
					//					if(true){
					templateSolver = true;
					//					notfoundA = false;
					//					notfoundS = false;
					sql_stmt.executeUpdate(example_sql);
					sql_stmt.executeUpdate(template_sql);
					sql_stmt.executeUpdate(binding_sql);
					System.err.println("cost:"+cost+"\n"+StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ")+"\n"+StringUtils.join(ssss.toArray()," "));
					prev_cost = cost;
					grammar_id++;
					//					}else{
					templateSolver = false;
					//						sql_stmt.executeUpdate(example_sql);
					//						sql_stmt.executeUpdate(binding_sql);
					//						grammar_id++;
					//											}
					//					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.out.println(binding_sql);
					//			System.out.println(e11);
					//			System.out.println(e122);
					e1.printStackTrace();
				}

				 */
				//				k++;
			} 
		}/** 

			n	o t		r	a	n	s	p	o	s	e
		 *
		 */

		else{



			//			System.out.println("transpose");
			ecostMatrix=Murty.transpose(ecostMatrix);
			//			System.out.println("dim	"+ecostMatrix.length+" "+ecostMatrix[0].length);
			//			rowsols = Murty.solve(ecostMatrix, 5);
			try{
				rowsols = Murty.solve(ecostMatrix, NUM_K_HUNG);
				//				rowsols.add(eresult);
			} catch (java.lang.ArrayIndexOutOfBoundsException e){
				System.out.println(android_listener.op_a_nodes+",,"+swift_listner.op_s_nodes);
				e.printStackTrace();
			}


			//iterations
			for (int[] solution : rowsols) {
				eresult = solution;
				//			eresult = new HungarianAlgorithm(ecostMatrix).execute();
				//			Map<String, String> amap 		= new HashMap<String, String>();
				Map<OpProperty, String> aamap 	= new HashMap<OpProperty, String>();
				//			Map<Interval, String> amap_ct 	= new HashMap<Interval, String>();

				//			Map<String, String> smap 		= new HashMap<String, String>();
				Map<OpProperty, String> ssmap 	= new HashMap<OpProperty, String>();
				LinkedHashMap<OpProperty, OpProperty> nextMapping = new LinkedHashMap<OpProperty, OpProperty>();
				//				Map<Integer, Integer> android_to_swift 		= new HashMap<Integer, Integer>();
				//				Map<Pair, Double> android_to_swift_point 		= new HashMap<Pair, Double>();
				for(int a = 0; a< ecostMatrix[0].length;a++){
					android_to_swift.put(a, -1);
				}
				//			List<String> disset =  new LinkedList<String>();
				//find disjoint.
				//			boolean disjoint = false;
				int sol = 0;
				//				for (int[] solution : rowsols) {
				String s = "";
				//				double cost = 0;
				cost = 0;
				for (int i = 0; i < solution.length; i++) {
					s += (solution[i] + "\t");

					//0:2, 1:1, :2:3
					//						android_to_swift.put(solution[i], i);
					android_to_swift.put(solution[i], i);
					android_to_swift_point.put(new Pair(solution[i],i), ecostMatrix[i][solution[i]]);

				}


				int argnum = 0;
				List<Interval> listi = new LinkedList<Interval>();

				Iterator<Integer> ittt = android_to_swift.keySet().iterator();
				while(ittt.hasNext()){
					int aa = ittt.next();
					int ss = android_to_swift.get(aa);


					//			anode.getSourceInterval();


					if(ss!=-1 && ecostMatrix[ss][aa]<1){

						//					if(0<1){

						//						String a = android_listener.op_a_nodes.get(aa).strASTNode;
						//						String qs = swift_listner.op_s_nodes.get(ss).strASTNode;
						//						System.err.println(a+":::"+qs);s

						String a_p=android_listener.op_a_nodes.get(aa).property;
						String s_p=swift_listner.op_s_nodes.get(ss).property;
						//						System.out.println("android_to_swift_point"+android_to_swift_point);
						if(a_p.equals("enclosed{}") && s_p.equals("enclosed{}")){
							//						amap.put(a, "{...}");
							android_to_swift_point.remove(new Pair(aa,ss));
							//							android_to_swift_point.put(new Pair(aa,ss), ecostMatrix[ss][aa]);
							aamap.put(android_listener.op_a_nodes.get(aa), "{...}");
							//					amap_ct.put(wocontext.op_a_nodes.get(aa).interval, "{...}");
							//						smap.put(s, "{...}");
							ssmap.put(swift_listner.op_s_nodes.get(ss), "{...}");

						}




						else{

							//						amap.put(a, "<arg"+argnum+">");
							aamap.put(android_listener.op_a_nodes.get(aa), "<arg"+argnum+">");
							//						amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
							//						smap.put(s, "<arg"+argnum+">");

							Iterator<Interval> scopes_a = android_listener.op_a_nodes.get(aa).same_scope.keySet().iterator();
							while(scopes_a.hasNext()){
								Interval sc = scopes_a.next();
								//								amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
								amap_ct.put(android_listener.op_a_nodes.get(aa).same_scope.get(sc).interval, "<arg"+argnum+">");
								aamap.put(android_listener.op_a_nodes.get(aa).same_scope.get(sc), "<arg"+argnum+">");
//								System.out.println("aamap.put++"+android_listener.op_a_nodes.get(aa).same_scope.get(sc)+" :"+aamap);

							}

							amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
							ssmap.put(swift_listner.op_s_nodes.get(ss), "<arg"+argnum+">");
							nextMapping.put(android_listener.op_a_nodes.get(aa),swift_listner.op_s_nodes.get(ss));
							cost+=ecostMatrix[ss][aa];
							//							smap_ct.put(swift_listner.op_s_nodes.get(ss).interval, "<arg"+argnum+">");
							argnum++;
						}
						listi.add(android_listener.op_a_nodes.get(aa).interval);
						if(android_listener.op_a_nodes.get(aa).node instanceof ParserRuleContext && swift_listner.op_s_nodes.get(ss).node instanceof ParserRuleContext){
							ParserRuleContext anode = (ParserRuleContext)android_listener.op_a_nodes.get(aa).node;
							StructureMappingAntlr4_for_stmt3 mapping = new StructureMappingAntlr4_for_stmt3(anode, (ParserRuleContext)swift_listner.op_s_nodes.get(ss).node);
							//										if(StructureMappingAntlr4.testnum<100){
							//						StructureMappingAntlr4.testnum++;

							//transpose case
							//							mapping.waking();
							//							mapping.compute();
							//					}
							//					listi.add(anode.getSourceInterval());


						} 
					}else if(ss==-1 && !android_listener.op_a_nodes.get(aa).property.equals("constant_a")){
						aamap.put(android_listener.op_a_nodes.get(aa), "<arg"+argnum+">");
						amap_ct.put(android_listener.op_a_nodes.get(aa).interval, "<arg"+argnum+">");
						//						android_to_swift_point.put(new Pair(aa,ss), ecostMatrix[ss][aa]);
						argnum++;
					}

				}
				//				System.err.println("trans candidate"+android_listener.op_a_nodes+"\n   "+swift_listner.op_s_nodes);

				for(int p=0; p<eresult.length;p++){
					boolean disj= true;
					//			ParserRuleContext anode = (ParserRuleContext)wocontext.op_a_nodes.get(p).node;
					//			String anodeText = wocontext.op_a_nodes.get(p).strASTNode;
					Interval anodeInterval = android_listener.op_a_nodes.get(p).interval;
					for(int i=0; i<listi.size();i++){
						//								System.out.println("checking"+anode.getText()+""+listi.get(i).disjoint(anode.getSourceInterval())+" ");
						disj = disj & listi.get(i).disjoint(anodeInterval);
					}
					if(disj && !android_listener.op_a_nodes.get(p).property.equals("!!!!!none")){
						//					amap.put(android_listener.op_a_nodes.get(p).strASTNode, "<arg"+argnum+">");
						//						aamap.put(android_listener.op_a_nodes.get(p), "<arg"+argnum+">");
						//						argnum++;
					}
				}


				Iterator<OpProperty> aait = aamap.keySet().iterator();
				Iterator<OpProperty> ssit = ssmap.keySet().iterator();
				//			String android_ctxt = android_str;

				Map<Interval, String> terms = android_listener.termsMap;
				Map<Interval, String> terms_swft1 = swift_listner.termsMap;


				while(aait.hasNext()){

					OpProperty key = aait.next();

					//				String expr = key;
					String arg = aamap.get(key);

					Iterator<Interval> itttt = terms.keySet().iterator();
					boolean isFirst = false;
					while(itttt.hasNext()){
						Interval next = itttt.next();
						if(!next.disjoint(key.interval) && !isFirst){
							//					terms.put(next, arg);
							//					isFirst = true;
						}
						if(!next.disjoint(key.interval) ){
							//					terms.put(next, arg);
							if(!isFirst){terms.put(next, arg); isFirst = true;}
							else{itttt.remove();}
						}
					}

					//			key.interval;
					//			buf.replace(start, end, str);

				} //making template for android


				while(ssit.hasNext()){
					OpProperty key = ssit.next();
					//				String expr = key;
					String arg = ssmap.get(key);
					Iterator<Interval> itttt = terms_swft1.keySet().iterator();
					boolean isFirst = false;
					while(itttt.hasNext()){
						Interval next = itttt.next();
						//						if(!next.disjoint(key.interval) && !isFirst){
						//					terms.put(next, arg);
						//					isFirst = true;
						//						}
						if(!next.disjoint(key.interval) ){
							//					terms.put(next, arg);
							if(!isFirst){terms_swft1.put(next, arg); isFirst = true;}
							else{itttt.remove();}
						}
					}


				} //making template for swift

				//		StringUtils.join(terms.values());
				ArrayList<String> aaaa = new ArrayList<String>(terms.values());
				ArrayList<String> ssss = new ArrayList<String>(terms_swft1.values());

				//		System.err.println(android.getText()+" termlist "+StringUtils.join(aaaa.toArray()," "));
				//		System.err.println(swift.getText()+" termlist "+StringUtils.join(ssss.toArray()," "));












				ittt = android_to_swift.keySet().iterator();
				while(ittt.hasNext()){
					int aa = ittt.next();
					int ss = android_to_swift.get(aa);

					//					if(ecostMatrix[aa][eresult[aa]]<1){

					if(ss!=-1 &&  android_listener.op_a_nodes.get(aa).node instanceof ParserRuleContext && swift_listner.op_s_nodes.get(ss).node instanceof ParserRuleContext){
						//						ParserRuleContext anode = (ParserRuleContext)wocontext.op_a_nodes.get(aa).node;
						if(!android_listener.op_a_nodes.get(aa).property.equals("enclosed{}") && !android_listener.op_a_nodes.get(aa).property.equals("enclosed()")){
							//							System.out.println(a+"<::>"+s);
							//							StructureMappingAntlr4 mapping = new StructureMappingAntlr4((ParserRuleContext)android_listener.op_a_nodes.get(aa).node, (ParserRuleContext)swift_listner.op_s_nodes.get(ss).node);
							//							nextMapping.put((ParserRuleContext)android_listener.op_a_nodes.get(aa).node, (ParserRuleContext)swift_listner.op_s_nodes.get(ss).node);
							nextMapping.put(android_listener.op_a_nodes.get(aa),swift_listner.op_s_nodes.get(ss));

							//							mapping.waking();	
							//							mapping.compute();

						}
					} 

					//					}
				}




				String tempa=StringUtils.join(aaaa.toArray()," ");
				String temps=StringUtils.join(ssss.toArray()," ");

				System.out.println("	tr template\n"+tempa+"\n"+temps);
				//				ANTLRInputStream stream1 = new ANTLRInputStream("return <arg1> ; 1 ");
				//				example_sql="", template_sql="", binding_sql="";
				String binding = null;
				try{
					//
					if(DelcarationMapping.methodBinding!=null)
						binding=DelcarationMapping.methodBinding.get(android.start.getLine()+":"+android.start.getCharPositionInLine());

				} catch (Exception e){
					e.printStackTrace();
				}

				if(binding !=null){
					//					binding_sql = SQLiteJDBC.updateTableBinding(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
					binding_sql = SQLiteJDBC.updateTableBinding2(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval),
							afilename, LineCharIndex.contextToLineIndex(android).toString(),
							sfilename, LineCharIndex.contextToLineIndex(swift).toString()
							);
					//					template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, binding, this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
				//	example_sql = SQLiteJDBC.updateTableGrammarSwift(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval));
					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
							afilename, LineCharIndex.contextToLineIndex(android).toString(),
							sfilename, LineCharIndex.contextToLineIndex(swift).toString());
					template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."), this.android.getRuleIndex(), swift.getRuleIndex(), StringUtils.join(ssss.toArray()," ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."));
					
				}
				else{
					template_sql = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."), this.android.getRuleIndex(), swift.getRuleIndex(), StringUtils.join(ssss.toArray()," ").replaceAll(" \\.", "\\.").replaceAll("\\. ", "\\."));
					//					example_sql = SQLiteJDBC.updateTableGrammarSwift(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval));

					/*
					String query = "select id from grammar_android_stat where template=\'"+StringUtils.join(aaaa.toArray()," ") +" and example=\'"+StringUtils.join(ssss.toArray()," ") +"\';";

					Connection c;
					int iddd=0;
					try {
						c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
						java.sql.Statement sql_stmt = c.createStatement();
						c.setAutoCommit(true);
						ResultSet resultSet = sql_stmt.executeQuery(query);
						resultSet.next();
						String bbb = resultSet.getString("example");
						iddd=Integer.parseInt(resultSet.getString("id"));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					 */
					//					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(grammar_id, android.start.getInputStream().getText(android_interval), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval), 
					//							afilename, LineCharIndex.contextToLineIndex(android).toString(),
					//							sfilename, LineCharIndex.contextToLineIndex(swift).toString());

					example_sql 	= SQLiteJDBC.updateTableGrammarSwift2(QueryExamples.idd, android.start.getInputStream().getText(android_interval).replaceAll("\n", ""), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""), 
							StringUtils.join(aaaa.toArray()," "), nextMapping.keySet().toString().replaceAll("\n", ""),
							StringUtils.join(ssss.toArray()," "), nextMapping.values().toString().replaceAll("\n", ""));

				}
				if(tempa.equals("<arg0>") && temps.equals("<arg0>")){
					return null;
//					template_sql  = "";
					//					template_sql  = SQLiteJDBC.updateTableGrammarAndroid(grammar_id, android.start.getInputStream().getText(android_interval).replaceAll("\n", ""), this.android.getRuleIndex(), swift.getRuleIndex(), swift.start.getInputStream().getText(swift_interval).replaceAll("\n", ""));
					//					System.out.println(template_sql);
				}
				/*
//				mappingout = new 
				mappingout.nextMapping.clear();
				mappingout.nextMapping = nextMapping;
				mappingout.binding_sql = binding_sql;
				mappingout.example_sql = example_sql;
				mappingout.template_sql = template_sql;
				mappingout.template_a = tempa;
				mappingout.template_s= temps;
//				if(listner_swift.hasErrors() || listner.hasErrors())
//					mappingout.hasError = true;
				mappingout.cost = cost;
				 */
				grammar_id++;
				return new MappingOutput(nextMapping, binding_sql, example_sql, template_sql, tempa, temps, cost);
				//				doupdate();
			} 




		}
		return null;

	}

	public static List<String> getMatchingStrings(List<String> list, String regex) {

		ArrayList<String> matches = new ArrayList<String>();

		Pattern p = Pattern.compile(regex);

		for (String s:list) {
			if (p.matcher(s).matches()) {
				matches.add(s);
			}
		}

		return matches;
	}

	public static Set<Character> toSet(String s) {
		Set<Character> ss = new HashSet<Character>(s.length());
		for (char c : s.toCharArray())
			ss.add(Character.valueOf(c));
		return ss;
	}


	public static Set<String> toSet(List<String> s) {
		Set<String> ss = new HashSet<String>(s.size());
		for (String c : s)
			ss.add(c);
		return ss;
	}


	public static double toCommon(List<String> and, List<String> swft) {
		Set<String> aset = toSet(and);
		aset.retainAll(toSet(swft));

		return (double)aset.size()/(double)and.size();
	}

	public static double toCommon(String andS, String swftS) {
		List<String> lst = matchingList(andS);
		List<String> cplst = matchingList(andS);
		List<String> swlst = matchingList(swftS);
		List<String> cpswlst = matchingList(swftS);
		//		System.err.println(lst);
		//		System.err.println(swlst);
		int s = lst.size();
		int ss = swlst.size();
		//		Set<String> aset = toSet(lst);
		//		aset.retainAll(toSet(matchingList(swftS)));
		lst.retainAll(matchingList(swftS));
		//		System.out.println(lst);
		swlst.retainAll(cplst);
		//		System.out.println(swlst);
		if(s==0 && ss==0){
			return 1.0;
		}
		//		else if((double)lst.size()==0){
		//			return 0.0;
		//		}
		double s1 = s==0? 0:(double)lst.size()/(double)s;
		double s2 = ss==0? 0:(double)swlst.size()/(double)ss;
		//		System.out.println(s1);
		//		System.err.println(s2);
		return Math.min(s1, s2);
	}

	public static List<String> matchingList(String str){

		Pattern p = Pattern.compile("\\d+");
		List<String> numbers = new ArrayList<String>();
		Matcher m = p.matcher(str);
		while (m.find()) {  
			//			numbers.add(m.group());
		}   




		try {
			Pattern regex = Pattern.compile("[+*/&><={}]|(?<=\\s)-|if|else|while");
			Matcher regexMatcher = regex.matcher(str);
			while (regexMatcher.find()) {
				numbers.add(regexMatcher.group());
			} 
		} catch (PatternSyntaxException ex) {
			// Syntax error in the regular expression
		}


		//		System.err.println(str+": num"+numbers);
		return numbers;
	}

	public void doupdate(){

		Connection c;
		try {
			//			if(!isEqualTemplate){
			//					if(!listner_swift.hasErrors() && !listner.hasErrors() && cost!=prev_cost){

			//			c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			c = DriverManager.getConnection("jdbc:sqlite:var_0309.db");

			java.sql.Statement sql_stmt = c.createStatement();
			c.setAutoCommit(true);
			//			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(example_sql);
			sql_stmt.executeUpdate(template_sql);
			sql_stmt.executeUpdate(binding_sql);
			//			System.out.println("cost:"+cost+"\n"+StringUtils.join(aaaa.toArray()," ").replaceAll("\\s+", " ")+"\n"+StringUtils.join(ssss.toArray()," "));
			prev_cost = cost;
			grammar_id++;

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println("err in sql"+example_sql);
			System.out.println("err in sql"+template_sql);
			System.out.println("err in sql"+binding_sql);
			//			System.out.println(e11);
			//			System.out.println(e122);
			e1.printStackTrace();
		}

	}

	public static void main(String args []){
		/*
		boolean aa = true&false;
		System.out.println(aa+"main"+(StringUtils.countMatches("_chartWidth - _contentRect.size.width - _contentRect.origin.x", "-")==StringUtils.countMatches("_chartWidth - _contentRect.size.width", "-")));
		System.out.println(NumberUtils.isNumber("0"));
		//		NumberUtils.isNumber(arg0)
		System.out.println(NumberUtils.isNumber("0.0")+" "+ (NumberUtils.createDouble("0.0").equals(NumberUtils.createDouble("0"))));

		Pattern p = Pattern.compile("-?\\d+(,\\d+)*?\\.?\\d+?");
		List<String> numbers = new ArrayList<String>();
		Matcher m = p.matcher("Government has distributed 4.8 million textbooks to 2000 schools22");
		while (m.find()) {  
			numbers.add(m.group());
		}   
		System.err.println(numbers);


		List<String> matchList = new ArrayList<String>();
		try {
//			Pattern regex = Pattern.compile("[+/&><=]|(?<=\\s)-|if");
			Matcher regexMatcher = regex.matcher("if -test + aaa * aaaa/a22");
			while (regexMatcher.find()) {
				numbers.add(regexMatcher.group());
			} 
		} catch (PatternSyntaxException ex) {
			// Syntax error in the regular expression
		}
		System.out.println(numbers);

		String s1 = "Seychelles";
		String s2 = "sydney";
		Set<Character> ss1 = toSet(s1);
		ss1.retainAll(toSet(s2));

		 */

		System.out.println("\nSwiftParser.ruleNames[180]"+SwiftParser.ruleNames[214]+"  "+JavaParser.ruleNames[70]);
		System.out.println(toCommon("0f","CGFloat(0.0)"));


		/*
		System.out.println(matchingList("text != null && longest.length() < text.length();"));
		System.out.println(matchingList("text = null > !longest.length() && text.length();"));

		String t1="if ( yScale   == 0.f   )			{            yScale = Float.MAX_VALUE;        }";
		String t2="if ( newValue == 0.0 )         	{            newValue = CGFloat.max           }";
		String t3="if newValue   == 0.0             {            newValue = CGFloat.max           }";


		ANTLRInputStream stream1 = new ANTLRInputStream(t1);
		Lexer lexer1 = new JavaLexer((CharStream)stream1);
		CommonTokenStream androidcommon = new CommonTokenStream(lexer1);
		JavaParser parser = new JavaParser(new CommonTokenStream(lexer1));

		System.out.println(parser.getTokenStream().getTokenSource());
		String test= "let <arg0> = -width * (_scaleX - 1.0)";
		//		let <arg0> = -width * (_scaleX - 1.0) \-width \* \(_scaleX \- 1\.0\)
		//		test=test.replaceFirst("\\b"+"\\-width \\* \\(_scaleX \\- 1\\.0\\)"+"\\b", "<arg>");
		test=test.replaceFirst("\\-width \\* \\(_scaleX \\- 1\\.0\\)", "<arg>");
		//		<(.*?)>
		System.out.println(test);

		String in = "var <arg1>: <arg0> = <arg2>";

		Pattern pp = Pattern.compile("\\<(.*?)\\>");
		Matcher mm = pp.matcher(in);

		while(mm.find()) {
			System.out.println(mm.group(1));
			//		    System.out.println(mm);
		}
		 */
		StringBuffer buf = new StringBuffer("123456789");

		int start = 3;
		int end = 6;
		buf.replace(start, end, "foobar"); 
		System.out.println(buf);
	} 

	public class Pair{
		public int a,b;
		public Interval a_intval, b_intval;

		public Pair(int a, int b){
			this.a=a;
			this.b=b;
		}
		public Pair(int a, int b, Interval ai, Interval bi){
			this.a=a;
			this.b=b;
			this.a_intval = ai;
			this.b_intval = bi;
		}

		@Override
		public boolean equals(Object obj) {
			// TODO Auto-generated method stub
			Pair p=(Pair)obj;
			return a==p.a && b==p.b;
		}

		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return a*100+b*1;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return a+":"+b;
		}
	}
}
