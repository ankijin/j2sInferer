package migration;

//import greenblocks.util.Util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

public class ASTTester {

	public static String readFileToString(String filePath) throws IOException {
		StringBuilder fileData = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		char[] buf = new char[10];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}

		reader.close();
		return  fileData.toString();	
	}

	public static void main(String[] args) throws IOException {

		/*
		String path ="/Users/kijin/Documents/workspace_mars/GumTreeTester/src/snippet/mcharts_testing/MPAndroidChart-master/MPChartLib/src/com/github/mikephil/charting/renderer/XAxisRenderer.java";

		File file = new File(path);
		String str = readFileToString(file.getAbsolutePath());

		System.out.println(str);

		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		parser.setBindingsRecovery(true);

		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);

		String unitName = "XAxisRenderer.java";
		parser.setUnitName(unitName);

		String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/src/snippet/mcharts_testing/MPAndroidChart-master/MPChartLib/src" }; 
		String[] classpath = {"/Users/kijin/Desktop/mpandroidchartlibrary-2-2-4.jar"};

		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
		parser.setSource(str.toCharArray());

		CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		if (cu.getAST().hasBindingsRecovery()) {
			System.out.println("Binding activated.");
		}

		TypeFinderVisitor v = new TypeFinderVisitor();
		cu.accept(v);
		 */

		String path ="/Users/kijin/Documents/workspace_mars/GumTreeTester/testsrc/com/github/mikephil/charting/renderer/XAxisRenderer.java";
		File file = new File(path);
		String str = ASTTester.readFileToString(file.getAbsolutePath());

		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		parser.setBindingsRecovery(true);

		Map options = JavaCore.getOptions();
		parser.setCompilerOptions(options);

		String unitName = "XAxisRenderer.java";
//		System.out.println("F	"+file.getName());
		parser.setUnitName(file.getName());

		String[] sources = { "/Users/kijin/Documents/workspace_mars/GumTreeTester/testsrc" }; 
		String[] classpath = {};


		parser.setEnvironment(classpath, sources, new String[] { "UTF-8"}, true);
		parser.setSource(str.toCharArray());

		CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		if (cu.getAST().hasBindingsRecovery()) {
			System.out.println("Binding activated.");
		}

		TypeFinderVisitor v = new TypeFinderVisitor();
		cu.accept(v);

	}
}

class TypeFinderVisitor extends ASTVisitor{

	public boolean visit(VariableDeclarationStatement node){
		for (Iterator iter = node.fragments().iterator(); iter.hasNext();) {
			System.out.println("------------------");

			VariableDeclarationFragment fragment = (VariableDeclarationFragment) iter.next();
			IVariableBinding binding = fragment.resolveBinding();

			System.out.println("binding variable declaration: " +binding.getVariableDeclaration());
			System.out.println("binding: " +binding);
		}
		return true;
	}
	@Override
	public boolean visit(ExpressionStatement node) {
		// TODO Auto-generated method stub
		if(node.getExpression() instanceof MethodInvocation){

			System.out.println(node.getExpression());
			MethodInvocation mm = (MethodInvocation) node.getExpression();
			mm.accept(new ASTVisitor() {
				
				@Override
				public boolean visit(SimpleName node) {
					// TODO Auto-generated method stub
					
					 IBinding binding = node.resolveBinding();

//						System.out.println("binding variable declaration: " +binding.getVariableDeclaration());
//						System.out.println(node+"cccccinding: " +binding);
					
					return super.visit(node);
				}
				
				@Override
				public boolean visit(MethodInvocation node) {
					// TODO Auto-generated method stub
					
					if(node.getExpression() !=null && node.getExpression().resolveTypeBinding()!=null){
					ITypeBinding aa = node.getExpression().resolveTypeBinding();
					System.err.println("INNN"+mm.getExpression()+" "+aa.getName());
				}
					
					return super.visit(node);
				}
				
			});
//			if(mm.getExpression() !=null && mm.getExpression().resolveTypeBinding()!=null){
//				ITypeBinding aa = mm.getExpression().resolveTypeBinding();
//				System.err.println(mm.getExpression()+" "+aa.getName());
//			}
		}
		//		System.err.println(node.getExpression().resolve());
		return super.visit(node);
	}
	@Override
	public boolean visit(SimpleName node) {
		// TODO Auto-generated method stub
		 IBinding binding = node.resolveBinding();
//		 if(binding !=null){
//			 
//		 }
//		System.out.println("binding variable declaration: " +binding.getVariableDeclaration());
//		System.out.println(node+"bbbbbinding: " +binding);
		return super.visit(node);
	}
}
