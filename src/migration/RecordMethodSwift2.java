package migration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;

import recording.MappingElement;

//import SwiftParser.Type_annotationContext;
//import SwiftParser.Variable_declarationContext;
//import SwiftParser.Variable_declaration_headContext;
//import SwiftParser.Variable_nameContext;


//import SwiftParser.Function_declarationContext;

public class RecordMethodSwift2<T> extends BaseSwiftRecorder<T>{

	public Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
	LinkedList<MappingElement> record = new LinkedList<MappingElement> ();

	
	@Override
	public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {
		// TODO Auto-generated method stub
		return super.visitFunction_declaration(ctx);
	}
	
	@Override
	public T visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
		// TODO Auto-generated method stub
		SwiftParser.Variable_declaration_headContext h = ctx.variable_declaration_head();
		SwiftParser.Variable_nameContext n = ctx.variable_name();
		List<SwiftParser.Type_annotationContext> t = ctx.type_annotation();
		t.get(0).type().getText();
		//ctx.code_block().statements().
		return super.visitVariable_declaration(ctx);
	}
	
	@Override
	public T visitDeclaration_modifiers(SwiftParser.Declaration_modifiersContext ctx) {
		
		
		record.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		//ctx.getParent():variable_declration_head 
		//func
		record.add(new MappingElement(ctx.getParent().getChild(1).getText(), 0));
//		System.out.println("visitDeclaration_modifiers\n"+record);
		
		
		return super.visitDeclaration_modifiers(ctx);
	}
	
	@Override
	public T visitFunction_name(SwiftParser.Function_nameContext ctx) {
		record.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		
		// TODO Auto-generated method stub
		return super.visitFunction_name(ctx);
	}
	
	@Override
	public T visitPattern(SwiftParser.PatternContext ctx) {
		// TODO Auto-generated method stub
		
		record.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		
		
		return super.visitPattern(ctx);
	}
	
	@Override
	public T visitFunction_result(SwiftParser.Function_resultContext ctx) {
		// TODO Auto-generated method stub
//		System.out.println("visitFunction_result"+ctx.arrow_operator().getText());
//		System.out.println("visitFunction_result"+ctx.type().getText());
		
//		record.add(new M`appingElement(ctx.getText(), ctx.getRuleIndex()));
		record.add(new MappingElement(ctx.type().getText(), ctx.type().getRuleIndex()));
			
		return super.visitFunction_result(ctx);
	}
	@Override
	public T visitExpression(SwiftParser.ExpressionContext ctx) {
		// TODO Auto-generated method stub
		if(ctx.getParent() instanceof SwiftParser.Return_statementContext){
//			System.out.println("instanceof SwiftParser.Return_statementContext");
			record.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		}
		return super.visitExpression(ctx);
	}
	
}
