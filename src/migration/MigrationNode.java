package migration;

import java.util.HashMap;
import java.util.Map;

import org.stringtemplate.v4.ST;

public class MigrationNode {
	public ST srcTemp;
	public ST targetTemp;
	public String srcText;
	public String targetText;
	public String argID;
	//arg#, value
	public Map<String, String> argmap= new HashMap<String, String>();
	public MigrationNode(ST src, ST target){
		srcTemp   =  src;
		targetTemp = target;
	}
	
	public MigrationNode(ST src, ST target, String srcT, String targetT){
		srcTemp   	=  src;
		targetTemp 	= target;
		srcText 	= srcT;
		targetText 	= targetT;
	}
	
	public MigrationNode(){
	}
}

