package migration;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.metrics.JaroWinkler;
import org.simmetrics.simplifiers.Simplifiers;
import org.simmetrics.tokenizers.Tokenizers;

import static org.simmetrics.builders.StringMetricBuilder.with;

import com.google.common.io.Files;

public class Filewalker {

	
	public Filewalker(){
		
	}
	String _folder_a;
	String _folder_s;
	public Filewalker(String folder_a, String folder_s){

		_folder_a = folder_a;
		_folder_s = folder_s;

	}

	
	public void walk( String path ) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;

		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walk( f.getAbsolutePath() );
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(f.getName().contains(".java")){
					System.out.println( "JavaFile:" + f.getName() );
				}
			}
		}
	}
	

	public void walk( String path , LinkedList<File> files) {

		File root = new File( path );
		File[] list = root.listFiles();

		if (list == null) return;

		for ( File f : list ) {
			if ( f.isDirectory() ) {
				walk( f.getAbsolutePath() , files);
				//                System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				//                System.out.println( "File:" + f.getAbsoluteFile() );

				if(Files.getFileExtension(f.getAbsolutePath()).equals("java")
						|| Files.getFileExtension(f.getAbsolutePath()).equals("swift")){
					//                	  System.out.println( "JavaFile:" + f.getName() );
					//                	  System.err.println(Files.getFileExtension(f.getAbsolutePath()));
					files.add(f);
					//                	  Files.get
				}
			}
		}
	}

	public Map<File, File> getClassMapping() throws IOException{
		
		LinkedList<File> android_files = new LinkedList<File>();
		LinkedList<File> swift_files = new LinkedList<File>();
//		fw.walk("facebook/facebook-android-sdk-master", android_files);
//		fw.walk("facebook/facebook-sdk-swift-master", swift_files);
		
		walk(_folder_a, android_files);
		walk(_folder_s, swift_files);
		
		Map<File, File> classmapping = new HashMap<File, File>();
//		StringMetric
//		StringMetric metric = StringMetrics.jaroWinkler();
//		StringMetric metric = StringMetrics.jaroWinkler();
//		StringMetrics.
		
	    StringMetric metric =
	            with(new JaroWinkler())
	            .simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
	            .simplify(Simplifiers.removeAll("Chart"))
	            .simplify(Simplifiers.removeAll("Activity"))
	            .build();
		
		float max_score = -1;
		System.out.println(android_files);
		System.out.println(swift_files);
		Vector<Integer> iii = new Vector<Integer>();
		for (int i=0;i< android_files.size();i++ ) {
			File android_filePath = android_files.get(i);



			for (int j=0;j< swift_files.size();j++ ) {
				File swift_filePath = swift_files.get(j);

				String a = Files.getNameWithoutExtension(android_filePath.getName());				
				String s = Files.getNameWithoutExtension(swift_filePath.getName());

//				System.err.println(a+" : "+s);
				float score = metric.compare(a, s);

				if (score > max_score && score > 0.9 && !iii.contains(j)) {
					max_score = score;
					classmapping.put(android_files.get(i), swift_files.get(j));
				}

				
				
			}
			//prunning
			iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));
			max_score = -1;


		}

//		staticPrintMapping(classmapping);
		
		return classmapping;
		
		
	}
	
	public static void staticPrintMapping(Map<File, File> fmapping){

		System.out.println("class mapping: "+fmapping.size());

		Iterator<File> classmap = fmapping.keySet().iterator();
		while(classmap.hasNext()){

			File aclass_path = classmap.next();
			
			File sclass_path =fmapping.get(aclass_path);
			System.out.println(aclass_path.getName()+":"+sclass_path.getAbsolutePath());
		}
	}


	public static void main(String[] args) throws IOException {
		Filewalker fw = new Filewalker("mcharts/MPAndroidChart-master", "mcharts/Charts-master");
		
		//32
		//Filewalker fw = new Filewalker("facebook/facebook-android-sdk-master", "facebook/facebook-sdk-swift-master");
		//33
		//		Filewalker fw = new Filewalker("wordpress/WordPress-Android-develop", "wordpress/WordPress-iOS-develop");
//				Filewalker fw = new Filewalker("wire/wire-android-master", "wire/wire-ios-develop");
			
		Map<File, File> classmapping = fw.getClassMapping();
		staticPrintMapping(classmapping);
		/*
		LinkedList<File> android_files = new LinkedList<File>();
		LinkedList<File> swift_files = new LinkedList<File>();
//		fw.walk("facebook/facebook-android-sdk-master", android_files);
//		fw.walk("facebook/facebook-sdk-swift-master", swift_files);
		
		fw.walk("wechat/wechat-master", android_files);
		fw.walk("wechat/TSWeChat-master", swift_files);
		
//		fw.walk("wordpress/WordPress-Android-develop", android_files);
//		fw.walk("wordpress/WordPress-iOS-develop", swift_files);
		
//		fw.walk("wire/wire-android-master", android_files);
//		fw.walk("wire/wire-ios-develop", swift_files);

//		fw.walk("reactivex/RxJava-1.x", android_files);
//		fw.walk("reactivex/RxSwift-master", swift_files);
		
//		fw.walk("mcharts/MPAndroidChart-master", android_files);
//		fw.walk("mcharts/Charts-master", swift_files);
		
		System.out.println(android_files.size());
		System.out.println(swift_files.size());

		Map<File, File> classmapping = new HashMap<File, File>();
		StringMetric metric = StringMetrics.jaroWinkler();
		float max_score = -1;
		Vector<Integer> iii = new Vector<Integer>();
		for (int i=0;i< android_files.size();i++ ) {
			File android_filePath = android_files.get(i);



			for (int j=0;j< swift_files.size();j++ ) {
				File swift_filePath = swift_files.get(j);


				//						parseSwift(swift_filePath);
				//						System.out.println(android_files[i].getName().split(".java")[0]);
				//						System.out.println(swift_files[j].getName().split(".swift")[0]);
//				String a = android_files[i].getName().split(".java")[0];
				String a = Files.getNameWithoutExtension(android_filePath.getName());
				
				String s = Files.getNameWithoutExtension(swift_filePath.getName());
				//						String[] bb = "ViewPortHandler.java".split(".java");
				//						System.err.println(bb[0]);
				
				float score = metric.compare(a, s);

				if (score > max_score && score > 0.8 && !iii.contains(j)) {
					max_score = score;
					classmapping.put(android_files.get(i), swift_files.get(j));
				}

				iii.add(swift_files.indexOf(classmapping.get(android_files.get(i))));
				
				
			}
			//prunning
			max_score = -1;


		}
*/
//		staticPrintMapping(classmapping);
		Filewalker fw1 = new Filewalker();
		LinkedList<File> android_files = new LinkedList<File>();		
		fw1.walk("MPAndroidChart", android_files);
//		System.err.println(android_files);
		
		for(File f :android_files){
			System.err.println(f.getName());
			if(f.getName().equals("XAxisRenderer.java")){
				System.out.println(f.getAbsolutePath());
				
			}
		}
		
	}

}