package migration;
import static org.simmetrics.builders.StringMetricBuilder.with;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
//import java.util.Set;
import java.util.Vector;
//import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.ParserRuleContext;
//import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
//import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.CastExpression;
//import org.eclipse.jdt.core.dom.CharacterLiteral;
//import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
//import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;

import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;

//import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclaration;
//import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
//import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;

import alignment.common.HungarianAlgorithm;
import net.vivin.GenericTree;
import net.vivin.GenericTreeNode;
import sqldb.ProtoSQLiteJDBC;
import templates.MatchingTemplate;

public class MappingSingleStatement {



	//	static Class.forName("org.sqlite.JDBC");
	//	c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
	//	c.setAutoCommit(true);
	//System.out.println("Opened database successfully");

	//	sql_stmt = c.createStatement();

	static int expr_id=1;
	public org.eclipse.jdt.core.dom.Statement  android_stmt;
	public SwiftParser.StatementContext swift_stmt;
	//	static public MappingStatements stmtsMap = new MappingStatements();


	public MappingSingleStatement() throws ClassNotFoundException, SQLException{
		//		Class.forName("org.sqlite.JDBC");
		//		c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
		//		c.setAutoCommit(true);
		//		sql_stmt = c.createStatement();
	}

	public static String getBinding(Expression androidExpr){

		String mbinding ="";

		if(androidExpr instanceof CastExpression){
			Expression cst = ((CastExpression) androidExpr).getExpression();
			cst.accept(new ASTVisitor(){
				@Override
				public boolean visit(QualifiedName node) {
					// TODO Auto-generated method stub
					System.out.println("node.resolveBinding()"+((IVariableBinding)node.getQualifier().resolveBinding()).getVariableDeclaration());
					return super.visit(node);
				}
			});
		}
		if(androidExpr instanceof MethodInvocation){
		
			MethodInvocation mmmm = (MethodInvocation)androidExpr;
			IMethodBinding m11=null;
			if(mmmm.resolveMethodBinding()!=null)
				//					mbinding = mmmm.resolveMethodBinding().toString();
				m11= mmmm.resolveMethodBinding().getMethodDeclaration();

			if(m11!=null){
				mbinding=m11.getDeclaringClass().getName()+".";
				mbinding+=m11.getName()+"(";
				ITypeBinding[] pt = m11.getParameterTypes();
				StringUtils.join(pt, ", ");
				ArrayList<String> types = new ArrayList<String>();
				for(ITypeBinding b:pt){
					types.add(b.getName());
				}
				mbinding+= StringUtils.join(types, ", ");
				mbinding+=")";
				return mbinding;
			}
			//					m11.getDeclaredReceiverType();
			//					mbinding= mmmm.resolveMethodBinding().getMethodDeclaration();
			return androidExpr.toString();
		}else if(androidExpr instanceof SimpleName){
			ITypeBinding typeB = ((SimpleName)androidExpr).resolveTypeBinding();
			if(typeB !=null){

				IVariableBinding[] aa = typeB.getDeclaredFields().clone();
				for(IVariableBinding v :aa){
					System.out.println("tttypeB"+v.getDeclaringClass()+v);
				}
				System.out.println(typeB.getBound()+"ttypeB"+((SimpleName)androidExpr).getFullyQualifiedName()+typeB+"  "+typeB.getDeclaredFields().length+"  "+typeB.getDeclaredMethods().length+"  "+typeB.getDeclaringClass());
				return androidExpr.toString();
			}else{
				return androidExpr.toString();
			}
		}
		return androidExpr.toString();
	}

	public MappingSingleStatement(org.eclipse.jdt.core.dom.Statement a, SwiftParser.StatementContext s){
		android_stmt 	= a;
		swift_stmt 		= s;
	}

	static void compute(ASTNode android, int aid, ParserRuleContext swift, int sid) throws SQLException, ClassNotFoundException, InterruptedException{
		//		System.out.println("dfasfsdf");

		//		Connection c = null;
		//		java.sql.Statement sql_stmt = null;

		//		Semaphore mutex = new Semaphore(1);

		if(aid== ReturnStatement.RETURN_STATEMENT && sid == SwiftParser.RULE_return_statement){
			
//			StructureMapping strt = new StructureMapping(android, swift);
//			strt.compute();
			
			MappingExpression mappingExpr = new MappingExpression();
			mappingExpr.androidExpr = ((ReturnStatement) android).getExpression();
			GenericTree<ASTNode> tree = new GenericTree<ASTNode>();
			Vector<GenericTreeNode<ASTNode>> stack = new Vector<GenericTreeNode<ASTNode>>();
			Vector<String> expr_store_a = new Vector<String>();
			Vector<String> expr_store_s = new Vector<String>();

			//tokens, we don't need to parse data
			//recording features of return statement
			android.accept(new ASTVisitor() {	
				public void preVisit(ASTNode node) {
					if(node instanceof Expression){
						//						mappingExpr.androidExpr = (Expression) node;
						if(node instanceof MethodInvocation){
							MethodInvocation nn = (MethodInvocation)node;
							expr_store_a.add(nn.toString());
							List<ASTNode> aaa = nn.arguments();
							for(ASTNode a:aaa){
								preVisit(a);
							}
						}
						else{
							//							Expression nn = (Expression)node;
							//							GenericTreeNode<ASTNode> treee = new GenericTreeNode<ASTNode>(nn);
							//														System.out.println("instanceof Expression"+e);
							//							stack.add(treee);
							//							expr_store_a.add(nn.toString());
						}
					}
				};
			});

			BaseSwiftRecorder recorder = new BaseSwiftRecorder(){
				@Override
				public Object visitExpression(SwiftParser.ExpressionContext ctx) {
					//								System.out.println("visitExpression"+ctx.getText());
					expr_store_s.add(ctx.getText());
					mappingExpr.swiftExpr =(SwiftParser.ExpressionContext) ctx;
					return super.visitExpression(ctx);
				};
				public Object visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
					List<SwiftParser.Type_annotationContext> aa = ctx.type_annotation();
					aa.iterator();
					return ctx;
				};
			};

			swift.accept(recorder);

			int len_a = expr_store_a.size();
			int len_s = expr_store_s.size();
			int short_len = len_a > len_s? len_s:len_a;
			//			System.err.println("MappingDeclaration.typeMapAndroid"+Main.typeMapAndroid);



			for(int k=0;k<short_len;k++){
				String expra= expr_store_a.get(k);
				String exprs= expr_store_s.get(k);

				String texpra = expra;
				String texprs = exprs;

				texpra = makeTemplateAndroid(texpra);
				texprs=	makeTemplateSwift(texprs);


				String genVarsA, genVarsS;
				genVarsA = MatchingTemplate.getGeneralVarsStr(texpra, expra);
				genVarsS = MatchingTemplate.getGeneralVarsStr(texprs, exprs);


				String binding = "";
				if(mappingExpr.androidExpr.resolveTypeBinding()!=null && mappingExpr.androidExpr.resolveTypeBinding().getName()!=null)
					binding=	mappingExpr.androidExpr.resolveTypeBinding().getName();
				String bbb="";
				if(mappingExpr.androidExpr instanceof SimpleName){
					//					mappingExpr.androidExpr
					//					((SimpleName) mappingExpr.androidExpr).

					IBinding rBinding = ((SimpleName) mappingExpr.androidExpr).resolveBinding();
				}

				String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(expr_id, mappingExpr.androidExpr.toString(),binding, bbb, Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
				String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(expr_id,  mappingExpr.swiftExpr.getText(),  mappingExpr.swiftExpr.getText(), expr_id, genVarsS);


				Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
				java.sql.Statement sql_stmt = c.createStatement();
				sql_stmt.executeUpdate(e12);	
				sql_stmt.executeUpdate(e11);
				expr_id++;

			}


		}







		if(aid== IfStatement.IF_STATEMENT && sid == SwiftParser.RULE_if_statement){

			//			Vector<String> expr_store_a = new Vector<String>();
			//			Vector<String> expr_store_ab = new Vector<String>();
			//			Vector<String> expr_store_s = new Vector<String>();

			List<Expression> expr_aa = new LinkedList<Expression> ();
			List<ParserRuleContext> expr_ss = new LinkedList<ParserRuleContext> ();

			List<Block> blocks_aa = new LinkedList<Block> ();
			List<SwiftParser.Code_blockContext> blocks_ss = new LinkedList<SwiftParser.Code_blockContext> ();
			MappingExpression mm = new MappingExpression();
			android.accept(new ASTVisitor() {

				@Override
				public boolean visit(IfStatement node) {
					// TODO Auto-generated method stub
					Expression ee = node.getExpression();
					mm.androidExpr = ee;
					//					expr_store_a.add(ee.toString());
					expr_aa.add(ee);
					System.out.println("resolveTypeBinding"+ee+"_"+ee.resolveTypeBinding());
					Map<String, String> mm = new HashMap<String, String>();


					//store if(A) else if(B) ... A, B 
					Iterator<String> keys = mm.keySet().iterator();
					String expr ="";
					while(keys.hasNext()){
						String v = keys.next();
						mm.get(v);
						expr= ee.toString().replaceAll("\\b("+v+")\\b",mm.get(v));
					}
					expr=expr.replace(" ", "");
					//					expr_store_ab.add(expr);
					//					node.getElseStatement();
					return super.visit(node);
				}


				public boolean visit(org.eclipse.jdt.core.dom.Block node) {
					//					List<org.eclipse.jdt.core.dom.Statement> e = node.statements();
					if(node !=null)
						blocks_aa.add(node);
					return super.visit(node);
				};

			});

			BaseSwiftRecorder recorder = new BaseSwiftRecorder(){
				@Override
				public Object visitIf_statement(SwiftParser.If_statementContext ctx) {
					if(ctx.condition_clause().expression()!=null){
						mm.swiftExpr = ctx.condition_clause().expression();

						//						expr_store_s.add(ctx.condition_clause().expression().getText());
						//						mm.swiftExpr = ctx.condition_clause().expression();

						ctx.condition_clause().expression().accept(new BaseSwiftRecorder(){
							public Object visitParenthesized_expression(SwiftParser.Parenthesized_expressionContext ctx) {
								if(ctx.expression_element_list() !=null && ctx.expression_element_list().expression_element(0)!=null)
								{
									mm.swiftExpr = ctx.expression_element_list().expression_element(0).expression();
									mm.swiftExpr = ctx.expression_element_list().expression_element(0).expression();
								}
								return false;
							};
						});


						expr_ss.add(mm.swiftExpr);
					}
					return visitChildren(ctx);
				}
				public Object visitCode_block(SwiftParser.Code_blockContext ctx) {
					if(ctx !=null){
						blocks_ss.add(ctx);
					}
					return visitChildren(ctx);
				};
			};

			//			System.err.println("ExpressionContext	"+swift.getText());
			swift.accept(recorder);

			//			swift


			//			SwiftParser.ExpressionContext ssss


			StringMetric emetric =
					with(new JaroWinkler())
					.build();

			//				int m = blocks_a.size() < blocks_s.size()?blocks_a.size():  blocks_s.size();
			double[][] ecostMatrix = new double[expr_aa.size()][expr_ss.size()];

			//				int aaa = blocks_aa.size();
			//				int bbb = blocks_ss.size();
			System.out.println(expr_aa.size()+" @@***** "+expr_ss.size());
			for(int i=0;i<expr_aa.size();i++){
				for(int j=0;j<expr_ss.size();j++){
					ecostMatrix[i][j]= 1- emetric.compare(expr_aa.get(i).toString(), expr_ss.get(j).getText());
				}
			}
			HungarianAlgorithm ehung = null;
			int[] eresult = new int[0];
			if(expr_aa.size() > 0 && expr_ss.size() >0){
				ehung = new HungarianAlgorithm(ecostMatrix);
				eresult = ehung.execute();
			}

			for(int p=0; p<eresult.length;p++){
				if(eresult[p]!=-1){


					String aifexpr= expr_aa.get(p).toString().replace(" ", "");	
					String sifexpr=expr_ss.get(eresult[p]).getText().replace(" ", "");	

					mm.androidExpr = expr_aa.get(p);
					mm.swiftExpr = expr_ss.get(eresult[p]);
					mm.compute();
					Map<Expression, ParserRuleContext> exprMapping = mm.getASTMap();
					Iterator<Expression> iter = exprMapping.keySet().iterator();
					while(iter.hasNext()){
						Expression and = iter.next();
						ParserRuleContext swft = exprMapping.get(and); 
						String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(expr_id, and.toString(),  and.toString(),  getBinding(and), Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
						String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(expr_id,  swft.getText(),  swft.getText(), expr_id, "");

						Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
						java.sql.Statement sql_stmt = c.createStatement();
						sql_stmt.executeUpdate(e12);	
						sql_stmt.executeUpdate(e11);
						expr_id++;
					}
				}
			}

			StringMetric metric =
					with(new JaroWinkler())
					//				.simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
					//				.simplify(Simplifiers.removeAll("Chart"))
					//				.simplify(Simplifiers.removeAll("Activity"))
					.build();
			double[][] costMatrix = new double[blocks_aa.size()][blocks_ss.size()];

			System.out.println(blocks_aa.size()+" ****** "+blocks_ss.size());
			for(int i=0;i<blocks_aa.size();i++){
				for(int j=0;j<blocks_ss.size();j++){
					costMatrix[i][j]= 1- metric.compare(blocks_aa.get(i).toString(), blocks_ss.get(j).getText());
				}
			}
			HungarianAlgorithm hung = null;
			int[] result = new int[0];
			if(blocks_aa.size() > 0 && blocks_ss.size() >0){
				hung = new HungarianAlgorithm(costMatrix);
				result = hung.execute();
			}

			for(int p=0; p<result.length;p++){
				if(result[p]!=-1){

					MappingStatements ms = new MappingStatements();
					System.out.println("if_blocks");
					System.out.println(blocks_aa.get(p));
					System.err.println(blocks_ss.get(result[p]).getText());
					ms.android_stmts_list =  blocks_aa.get(p).statements();
					ms.swift_stmts_list = blocks_ss.get(result[p]).statements().statement();
					ms.mapping();
				}
			}
		}
		//		VariableDeclarationStatement

		if(aid== VariableDeclaration.VARIABLE_DECLARATION_STATEMENT && sid == SwiftParser.RULE_constant_declaration)
		{
			
//			StructureMapping strt = new StructureMapping(android, swift);
//			strt.compute();
			
			SwiftParser.Constant_declarationContext condecl=null;
			VariableDeclarationStatement vardecl = (VariableDeclarationStatement) android;
			List<VariableDeclarationFragment> vardel_frags = vardecl.fragments();
			SwiftParser.DeclarationContext aaa = ((SwiftParser.StatementContext) swift).declaration();
			List<SwiftParser.Pattern_initializerContext> condecl_inits=null;
			if(aaa!=null && aaa.constant_declaration()!=null && aaa.constant_declaration().pattern_initializer_list()!=null){
				condecl = aaa.constant_declaration();
				SwiftParser.Pattern_initializer_listContext condecl_expr = condecl.pattern_initializer_list();
				condecl_inits = condecl_expr.pattern_initializer();
			}

			if(condecl_inits==null) return;
			StringMetric metric =
					with(new JaroWinkler())
					//				.simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
					//				.simplify(Simplifiers.removeAll("Chart"))
					//				.simplify(Simplifiers.removeAll("Activity"))
					.build();

			//				int m = blocks_a.size() < blocks_s.size()?blocks_a.size():  blocks_s.size();
			double[][] costMatrix = new double[vardel_frags.size()][condecl_inits.size()];

			for(int i=0; i<vardel_frags.size();i++){
				for(int j=0; j<condecl_inits.size();j++){
					costMatrix[i][j] = metric.compare(vardel_frags.get(i).getName().toString(), condecl_inits.get(j).pattern().getText());
				}
			}


			HungarianAlgorithm hung = null;
			int[] result = new int[0];
			if(vardel_frags.size() > 0 && condecl_inits.size() >0){
				hung = new HungarianAlgorithm(costMatrix);
				result = hung.execute();
			}

			for(int p=0; p<result.length;p++){
				if(result[p]!=-1 && vardel_frags.get(p).getInitializer()!=null){
					String expra = vardel_frags.get(p).getInitializer().toString();
					Expression androidExpr = vardel_frags.get(p).getInitializer();
					//					System.out.println("androidExpr"+androidExpr);
					String exprs = condecl_inits.get(result[p]).initializer().expression().getText();

					String mbinding ="";
					if(androidExpr instanceof MethodInvocation){
						MethodInvocation mmmm = (MethodInvocation)androidExpr;
						IMethodBinding m11=null;
						if(mmmm.resolveMethodBinding()!=null)
							//					mbinding = mmmm.resolveMethodBinding().toString();
							m11= mmmm.resolveMethodBinding().getMethodDeclaration();

						if(m11!=null){
							mbinding=m11.getDeclaringClass().getName()+".";
							mbinding+=m11.getName()+"(";
							ITypeBinding[] pt = m11.getParameterTypes();
							StringUtils.join(pt, ", ");
							ArrayList<String> types = new ArrayList<String>();
							for(ITypeBinding b:pt){
								types.add(b.getName());
							}
							mbinding+= StringUtils.join(types, ", ");
							mbinding+=")";
						}
						//					m11.getDeclaredReceiverType();
						//					mbinding= mmmm.resolveMethodBinding().getMethodDeclaration();

					}



					String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(expr_id, expra.replaceAll(" ", ""), expra.replaceAll(" ", ""), mbinding, Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
					String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(expr_id,  exprs.replaceAll(" ", ""),  exprs.replaceAll(" ", ""), expr_id, "");	


					Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
					//			c.setAutoCommit(true);
					java.sql.Statement sql_stmt = c.createStatement();
					//			mutex.acquire();
					//			sql_stmt = c.createStatement();
					sql_stmt.executeUpdate(e12);	
					sql_stmt.executeUpdate(e11);
					expr_id++;
				}
			}

		} // VariableDeclarationStatement <->constant_declaration

		if(aid == ExpressionStatement.EXPRESSION_STATEMENT && sid == SwiftParser.RULE_expression){
			final String expra=null, exprs=null;

			MappingExpression mappingExpr = new MappingExpression();


			Expression exprofStmt = ((ExpressionStatement)android).getExpression();
//			StructureMapping strt = new StructureMapping(android, swift);
//			strt.compute();
			swift.accept(new BaseSwiftRecorder(){
				//context assignment
				public Object visitBinary_expression(SwiftParser.Binary_expressionContext ctx) {
					if(ctx.binary_operator()!=null && ctx.binary_operator().getText().equals("=")){
						System.out.println("Binary_expressionContext"+ctx.prefix_expression().getText());
						mappingExpr.swiftExpr = ctx.prefix_expression();
					}

					return null;
				};
			});

			//assignment context
			if(exprofStmt instanceof Assignment && mappingExpr.swiftExpr!=null) {
				mappingExpr.androidExpr = ((Assignment)exprofStmt).getRightHandSide();
				System.out.println("mappingExpr.androidExpr"+mappingExpr.androidExpr+"  "+exprofStmt);
			}
			else{
				mappingExpr.androidExpr = ((ExpressionStatement) android).getExpression();
				mappingExpr.swiftExpr 	= ((SwiftParser.StatementContext)swift).expression();
			}
			
			if(mappingExpr.swiftExpr!=null && mappingExpr.androidExpr!=null){
				mappingExpr.compute();
				/*
				mappingExpr.getASTMap();
				System.out.println("mappingExpr.compute()");
				Map<Expression, ParserRuleContext> exprMapping = mappingExpr.getASTMap();
				Iterator<Expression> iter = exprMapping.keySet().iterator();
				while(iter.hasNext()){
					Expression and = iter.next();
					ParserRuleContext swft = exprMapping.get(and);
					MappingExpression mapexr = new MappingExpression(and, swft);
					
				}
				*/
				/*
				while(iter.hasNext()){
					Expression and = iter.next();
					ParserRuleContext swft = exprMapping.get(and);

//					VariableTokenMapping vm = new VariableTokenMapping(and, swft);
//					vm.compute();

					String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(expr_id, and.toString(),  and.toString(),  getBinding(and), Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
					String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(expr_id,  swft.getText(),  mappingExpr.swiftTemplate, expr_id, "");

					Connection c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
					java.sql.Statement sql_stmt = c.createStatement();
					try{
						sql_stmt.executeUpdate(e12);	
						sql_stmt.executeUpdate(e11);
						expr_id++;
					} catch(java.sql.SQLException e){
						e.printStackTrace();
						System.out.println(e11);
						System.out.println(e12);
					}
				}
				*/
			}
		}


		if(aid== ForStatement.FOR_STATEMENT && sid == SwiftParser.RULE_for_in_statement){
			//			System.out.println("FOR_STATEMENT"+android+"\n"+swift.getText());

			MappingStatements ms = new MappingStatements();
			android.accept(new ASTVisitor() {
				@Override
				public boolean visit(Block node) {
					// TODO Auto-generated method stub
					ms.android_stmts_list = node.statements();
					//					System.out.println("forblock"+node.toString());
					return false;
				}
			});

			swift.accept(new BaseSwiftRecorder(){
				@Override
				public Object visitCode_block(SwiftParser.Code_blockContext ctx) {
					// TODO Auto-generated method stub
					//System.out.println("forblock"+ctx.getText());
					ms.swift_stmts_list = ctx.statements().statement();
					return null;
				}
			});
			ms.mapping();

		}



		//		c.commit();
		//		c.close();

	}


	static void computeAndroid(ASTNode android) throws SQLException, ClassNotFoundException{
		//		System.out.println("dfasfsdf");


		String genVarsA="", texpra="", expra="";
		if(android instanceof ReturnStatement){



			texpra = ((ReturnStatement)android).getExpression().toString();
			//				String texprs = exprs;
			expra = texpra;
			texpra = makeTemplateAndroid(texpra);
			genVarsA = MatchingTemplate.getGeneralVarsStr(texpra, expra);
		}







		if(android instanceof IfStatement){

			texpra = ((IfStatement) android).getExpression().toString();
			expra = texpra;
			texpra = makeTemplateAndroid(texpra);
			genVarsA = MatchingTemplate.getGeneralVarsStr(texpra, expra);


		}


		//		VariableDeclarationStatement

		if(android instanceof VariableDeclaration){
			texpra = ((VariableDeclaration)android).getInitializer().toString();
			expra = texpra;
			texpra = makeTemplateAndroid(texpra);

			genVarsA = MatchingTemplate.getGeneralVarsStr(texpra, expra);
		} // VariableDeclarationStatement <->constant_declaration

		if(android instanceof ExpressionStatement){
			//			System.out.println("android instanceof ExpressionStatement");
			texpra = ((ExpressionStatement)android).getExpression().toString();
			expra = texpra;
			texpra = makeTemplateAndroid(texpra);
			genVarsA = MatchingTemplate.getGeneralVarsStr(texpra, expra);
		}

		//		System.out.println("unmatched a"+s);
		if(!expra.equals("")){
			//			String sql= ProtoSQLiteJDBC.insertTableUnmatchedExprAndroidV(MappingStatements.umatched_a_id, expra.replaceAll("\n", ""), texpra.replaceAll("\n", ""),MappingStatements.stmts_id , genVarsA);
			//			sql_stmt.executeUpdate(sql);
			//			c.commit();
			//			MappingStatements.umatched_a_id++;
		}


	}

	static String makeTemplateAndroid(String expr){
		/*
		Iterator<String> androidVars = Main.typeMapAndroid.keySet().iterator();
		while(androidVars.hasNext()){
			String varname 	= androidVars.next();
			String type 	= Main.typeMapAndroid.get(varname);
			if(type!=null && expr!=null){
				expr= expr.replaceAll("\\b("+varname+")\\b",type);
				expr=expr.replace(" ", "");
			}


		}

		Iterator<String> androidVarsLocal = stmtsMap.localtypeMapAndroid.keySet().iterator();

		while(androidVarsLocal.hasNext() && expr!=null){
			expr = expr.replace("this", Main.androidClassName);
			String varname 	= androidVarsLocal.next();
			//			System.out.println("localvar"+varname);
			String type 	= stmtsMap.localtypeMapAndroid.get(varname);
			if(type!=null)
				expr= expr.replaceAll("\\b("+varname+")\\b",type);
			expr=expr.replace(" ", "");
			//			if(expr.contains(varname)){
			//			if(expr.contains(varname) && (expr.contains("."+varname) || expr.contains(varname+"."))){
			//				expr = expr.replace(varname, "${"+type+"}");
			//			}

		}
		 */
		return expr;
	}


	/*
	static String makeTemplateAndroid(String expr){
		Iterator<String> androidVars = Main.typeMapAndroid.keySet().iterator();
		while(androidVars.hasNext()){
			String varname 	= androidVars.next();
			String type 	= Main.typeMapAndroid.get(varname);
			if(type!=null && expr!=null){
				expr= expr.replaceAll("\\b("+varname+")\\b","\\${"+type+"}");
			}


		}

		Iterator<String> androidVarsLocal = stmtsMap.localtypeMapAndroid.keySet().iterator();

		while(androidVarsLocal.hasNext() && expr!=null){
			expr = expr.replace("this", Main.androidClassName);
			String varname 	= androidVarsLocal.next();
			System.out.println("localvar"+varname);
			String type 	= stmtsMap.localtypeMapAndroid.get(varname);
			if(type!=null)
				expr= expr.replaceAll("\\b("+varname+")\\b","\\${"+type+"}");
			//			if(expr.contains(varname)){
			//			if(expr.contains(varname) && (expr.contains("."+varname) || expr.contains(varname+"."))){
			//				expr = expr.replace(varname, "${"+type+"}");
			//			}

		}
		return expr;
	}
	 */
	static String makeTemplateSwift(String expr){
		/*
		Iterator<String> SwiftVars = Main.typeMapSwift.keySet().iterator();
		while(SwiftVars.hasNext()){
			expr = expr.replace("self", Main.swiftClassName);
			String varname 	= SwiftVars.next();
			String type 	= Main.typeMapSwift.get(varname);
			//			   myStr = myStr.replaceAll("\\b("+Pattern.quote("width") +")\\b","WIDTH");
			expr= expr.replaceAll("\\b("+varname+")\\b","\\${"+type+"}");
			//			expr = expr.replaceAll("\\.\\${"+type+"}", "\\."+varname);
			//			if(expr.contains(varname) &&  expr.contains(varname+".")){
			//				expr = expr.replace(varname, "${"+type+"}");
			//			}
		}

		Iterator<String> swiftVarsLocal = stmtsMap.localtypeMapSwift.keySet().iterator();

		while(swiftVarsLocal.hasNext()){
			String varname 	= swiftVarsLocal.next();
			String type 	= stmtsMap.localtypeMapSwift.get(varname);
			expr= expr.replaceAll("\\b("+varname+")\\b","\\${"+type+"}");
			//			if(expr.contains(varname) && (expr.contains(varname+"."))){
			//				expr = expr.replace(varname, "${"+type+"}");
			//			}
		}
		 */
		return expr;
	}
	public static void main(String[] args){
		
		System.err.println(StringUtils.left("public a=CFloat()", 1));
		String test1 = "public a+CFloat()";
		System.err.println(test1.split("="));
		String[] bb = test1.split("=");
		for(int i=0; i<bb.length;i++){
			System.err.println("bb[i]"+bb[i]);
		}
		String test = " x > c.x";
		String newtest = test;
		boolean a = test.matches("x");
		//		test.replaceAll("x", "${something}");
		while(test.contains("x") && test.contains(".x")){
			test = test.replaceFirst("x", "\\${something}");
			System.out.println(test);
		}

		System.err.println(test);

		String input = "I have a cat, but I like my dog better.";
		StringBuffer sb = new StringBuffer();

		Pattern p = Pattern.compile("x");

		//		Pattern p = Pattern.compile("x\\.");
		Matcher m = p.matcher(test);

		List<String> animals = new ArrayList<String>();
		while (m.find()) {
			System.out.println("Found a " + m.group() + ".");
			animals.add(m.group());
			//			m.appendReplacement(sb, "\\${something}.");

			m.appendReplacement(sb, "\\${something}");
		}

		m.appendTail(sb);

		//		System.out.println(sb.toString());

		String myStr = "chart!=nil&&point.x+width+offset.x>chart!.bounds.size.width ";
		//	    myStr= myStr.replaceAll("\\b("+ Pattern.quote("test1") +"\\.)\\b","####.");
		//		myStr=myStr.replaceAll("testt.", "AAAAA.");
		System.out.println(myStr);
		myStr = myStr.replaceAll("\\b("+Pattern.quote("width") +")\\b","WIDTH");
		System.out.println(myStr);
		System.err.println(myStr.replaceAll("\\.WIDTH", ".width"));///\@te\b/
	}
	//	Math.sqrt(tx * tx + ty * ty)

}
