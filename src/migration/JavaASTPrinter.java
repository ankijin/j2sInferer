package migration;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;

public class JavaASTPrinter {

    public void print(ASTNode ctx) {
        explore(ctx, 0);
    }

	public static List<ASTNode> getChildren(ASTNode node) {
	    List<ASTNode> children = new ArrayList<ASTNode>();
	    List list = node.structuralPropertiesForType();
	    for (int i = 0; i < list.size(); i++) {
	        Object child = node.getStructuralProperty((StructuralPropertyDescriptor)list.get(i));
//	        node.getStructuralProperty(property)
	        if (child instanceof ASTNode) {
	            children.add((ASTNode) child);
	        }
	    }
	    return children;
	}
    
    private void explore(ASTNode ctx, int indentation) {
//        String ruleName = SwiftParser.ruleNames[ctx.getRuleIndex()];
//        ctx.
        String ruleName = ASTNode.nodeClassForType(ctx.getNodeType()).getSimpleName();
//        String ruleName =  ctx.getNodeType().;
//    	 int ruleName = ctx.getNodeType();
        for (int i=0;i<indentation;i++) {
            System.out.print("  ");
        }
        System.out.println(ruleName);
        List<ASTNode> children = getChildren(ctx);
        for (int i=0;i<children.size();i++) {
        	ASTNode element = children.get(i);
            if (element instanceof ASTNode) {
                explore((ASTNode)element, indentation + 1);
            }
        }
    }
    
}