package migration;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;



//[public, var, offsetLeft, CGFloat, _contentRect.origin.x]
//import SwiftParser.Function_declarationContext;

public class RecordMethodSwift<T> extends BaseSwiftRecorder<T>{
	public List<SwiftParser.StatementContext> stmt_list = new ArrayList<SwiftParser.StatementContext>();
	public Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
	LinkedList<MappingElement> record = new LinkedList<MappingElement> ();
	String stmts ="";
	String params="";

	String params_swift="";
	String stms_swift ="";
	public static String name_swift ="";
	String modifiers_swift="";
	String type_swift="";
	String returnType="NULL";
	String func_decl ="";
	LinkedList<MappingElement> recordStatement = new LinkedList<MappingElement> ();
	Map<String, String> recordSwiftParams	= new HashMap<String, String>();
	@Override
	public T visitFunction_declaration(SwiftParser.Function_declarationContext ctx) {
		func_decl = ctx.function_name().getText()+ctx.function_signature().getText();
		// TODO Auto-generated method stub
		SwiftParser.Declaration_modifiersContext ms = ctx.function_head().declaration_modifiers();
		String aaa="";
		String modifier="";
		if(ctx.function_head().declaration_modifiers()!=null){
			modifier = ms.getText();
		}
		//		if(ms.getParent() !=null && ms.getParent().getChild(1) !=null){
		//			System.out.println(ms.getParent().getChild(1).getText());
		//			aaa = ms.getParent().getChild(1).getText();
		//		}
		//		System.out.println(ctx.getText());
		record.add(new MappingElement(modifier, 1));
		record.add(new MappingElement(aaa, 1));

		record.add(new MappingElement(ctx.function_name().getText(), 1));
		String func_result = "";
		if(ctx.function_signature().function_result() !=null){
			func_result = ctx.function_signature().function_result().type().getText();

		}
		record.add(new MappingElement(func_result, 1));

		if(ctx.function_body()!=null && ctx.function_body().code_block() !=null && ctx.function_body().code_block().statements() !=null){
			if(ctx.function_body().code_block().statements().statement()!=null){
				stmt_list	= ctx.function_body().code_block().statements().statement();
				stms_swift 	= ctx.function_body().code_block().statements().getText();
			}
		}
		name_swift = ctx.function_name().getText();
		type_swift = func_result;
		if(ctx.function_signature()!=null && ctx.function_signature().function_result()!=null){
			returnType = ctx.function_signature().function_result().type().getText();
		}
		return super.visitFunction_declaration(ctx);
	}

	@Override
	public T visitCode_block(SwiftParser.Code_blockContext ctx) {
		// TODO Auto-generated method stub
		if(ctx.statements()!=null)
			stmts = ctx.statements().getText();
		//		System.out.println("stms"+stmts);
		return super.visitCode_block(ctx);
	}
	//	
	//	@Override
	//	public T visitStatement(SwiftParser.StatementContext ctx) {
	// TODO Auto-generated method stub
	//		System.err.println("visitStatement\n"+ctx.getText());
	//		System.err.println("tree"+ ctx.getRuleIndex());
	//		return super.visitStatement(ctx);
	//	}

	/*
	@Override
	public T visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
		String modi="", name="", type="", init="", keyword ="";
//		System.out.println("visitVariable_declaration\n"+ctx.getText());
		ctx.variable_name();


		record.add(new MappingElement(modi, 1));
		record.add(new MappingElement(keyword, 1));
		record.add(new MappingElement(name, 1));
		record.add(new MappingElement(type, 1));


//		ctx.variable_declaration_head().declaration_modifiers().getText();
		if(ctx.pattern_initializer_list()!=null && !ctx.pattern_initializer_list().isEmpty()){
//			name = ctx.pattern_initializer_list().pattern_initializer().get(0).pattern().getText();
			name = ctx.variable_name().getText();
			type = ctx.pattern_initializer_list().pattern_initializer().get(0).initializer().expression().getText();
			record.add(new MappingElement(modi, 1));
			record.add(new MappingElement(keyword, 1));
			record.add(new MappingElement(name, 1));
			record.add(new MappingElement(type, 1));
		} else{
			SwiftParser.Declaration_modifiersContext ms = ctx.variable_declaration_head().declaration_modifiers();
			modi = ms.getText();
			keyword= ms.getParent().getChild(1).getText();
			name = ctx.variable_name().getText();
			type = ctx.type_annotation().get(0).type().getText();
			record.add(new MappingElement(modi, 1));
			record.add(new MappingElement(keyword, 1));
			record.add(new MappingElement(name, 1));
			record.add(new MappingElement(type, 1));
		}
	 */
	// TODO Auto-generated method stub
	//		ctx.variable_name().getText();
	//		System.out.println();
	//		ctx.variable_declaration_head().declaration_modifiers();




	//		return super.visitVariable_declaration(ctx);

	//		return null;
	//	}

	@Override
	public T visitVariable_declaration(SwiftParser.Variable_declarationContext ctx) {
		String modi="", name="", type="", init="", keyword ="";
		//		System.out.println("visitVariable_declaration\n"+ctx.getText());
		//		ctx.variable_name();
		//		ctx.variable_declaration_head().declaration_modifiers().getText();
		//		ctx.variable_name();
		//	ctx.type_annotation(1).type();
		if(ctx.code_block() !=null){
			if(ctx.code_block().statements().statement()!=null){
				stmt_list = ctx.code_block().statements().statement();
				stms_swift = ctx.code_block().statements().getText();
			}
		}
		if(ctx.pattern_initializer_list()!=null && (ctx.pattern_initializer_list().pattern_initializer()!=null)){
			name = ctx.pattern_initializer_list().pattern_initializer().get(0).pattern().getText();
			if(ctx.pattern_initializer_list().pattern_initializer().get(0)!=null
					&&
					ctx.pattern_initializer_list().pattern_initializer().get(0).initializer()!=null)
				type = ctx.pattern_initializer_list().pattern_initializer().get(0).initializer().expression().getText();

			modifiers_swift = modi;
			type_swift = type;
			//			name_swift = ctx.variable_name().getText();

			record.add(new MappingElement(modi, 1));
			record.add(new MappingElement(keyword, 1));
			record.add(new MappingElement(name, 1));
			record.add(new MappingElement(type, 1));
		} else if(ctx.variable_declaration_head().declaration_modifiers()!=null){
			SwiftParser.Declaration_modifiersContext ms = ctx.variable_declaration_head().declaration_modifiers();
			
			modi = ms.getText();
			keyword= ms.getParent().getChild(1).getText();
			name = ctx.variable_name().getText();
			type = ctx.type_annotation().get(0).type().getText();
			record.add(new MappingElement(modi, 1));
			record.add(new MappingElement(keyword, 1));
			record.add(new MappingElement(name, 1));
			record.add(new MappingElement(type, 1));

			modifiers_swift = modi;
			type_swift = type;
			name_swift = name;
		}


		// TODO Auto-generated method stub
		//		ctx.variable_name().getText();
		//		System.out.println();
		//		ctx.variable_declaration_head().declaration_modifiers();




		return super.visitVariable_declaration(ctx);

		//		return null;
	}






	@Override
	public T visitExpression(SwiftParser.ExpressionContext ctx) {
		// TODO Auto-generated method stub
		if(ctx.getParent() instanceof SwiftParser.Return_statementContext){
			//			System.out.println("instanceof SwiftParser.Return_statementContext");
			record.add(new MappingElement(ctx.getText(), ctx.getParent().getRuleIndex()));
			//			recordStatement.add(new MappingElement(ctx.getText(), ctx.getParent().getRuleIndex()));
		}
		else{
			//			recordStatement.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		}
		return super.visitExpression(ctx);
	}

	@Override
	public T visitReturn_statement(SwiftParser.Return_statementContext ctx) {
		// TODO Auto-generated method stub
		recordStatement.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		return super.visitReturn_statement(ctx);
	}

	//[private, float, mMaxScaleX, Float.MAX_VALUE]

	@Override
	public T visitParameter(SwiftParser.ParameterContext ctx) {
		// TODO Auto-generated method stub
		//		System.out.println("exitParameter");
		//		System.out.println(ctx.local_parameter_name().getText());
		//		System.out.println(ctx.type_annotation().type().getText());
//		System.out.println("visitParameter->"+ctx.getText());
		String types="";
		if(ctx.type_annotation()!=null){
			types = ctx.type_annotation().type().getText().replaceAll("\\([^\\(]*\\)", "");
		}
		//		recordSwiftParams.put(ctx.local_parameter_name().getText(), ctx.type_annotation().type().getText().replaceAll("\\([^\\(]*\\)", ""));
		recordSwiftParams.put(ctx.local_parameter_name().getText(), types);

		return super.visitParameter(ctx);
	}
	@Override
	public T visitParameter_list(SwiftParser.Parameter_listContext ctx) {
		// TODO Auto-generated method stub
		params = ctx.getText();
		params_swift = params;
		//		System.out.println("params"+params);
		return super.visitParameter_list(ctx);
	}

	@Override
	public T visitDeclaration(SwiftParser.DeclarationContext ctx) {
		// TODO Auto-generated method stub
		//		System.out.println("SwiftParser.DeclarationContext ");
		return super.visitDeclaration(ctx);
	}

	@Override
	public T visitConstant_declaration(SwiftParser.Constant_declarationContext ctx) {
		// TODO Auto-generated method stub
		//		System.out.println("visitConstant_declaration");
		recordStatement.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		return super.visitConstant_declaration(ctx);
	}

	@Override
	public T visitIf_statement(SwiftParser.If_statementContext ctx) {
		// TODO Auto-generated method stub
		recordStatement.add(new MappingElement(ctx.getText(), ctx.getRuleIndex()));
		return super.visitIf_statement(ctx);
	}

	public static void main(String[] args) throws FileNotFoundException, IOException{
		RecordMethodSwift rrr = new RecordMethodSwift<>();
		//		System.out.println("RECORD Swift\n"+s.toStringTree());
		//		s.accept(rrr);
		String path = "/Users/kijin/Documents/workspace_mars/GumTreeTester/charts/test_set/test2.swift";

		Map<String, ParserRuleContext> nodeSwift = new HashMap<String, ParserRuleContext>();
		ANTLRInputStream stream1 = new ANTLRInputStream(new FileReader(path));
		Lexer lexer1 = new SwiftLexer((CharStream)stream1);
		SwiftParser parser1 = new SwiftParser(new CommonTokenStream(lexer1));
		//		SwiftParser.StatementsContext root1 = parser1.statements();

		SwiftParser.Top_levelContext root1 = parser1.top_level();

		root1.accept(rrr);

		System.out.println(rrr.record);
		System.out.println(rrr.recordSwiftParams);
		//		System.err.println(rrr.recordStatement);

		//		for(int i=0; i < rrr.recordStatement.size(); i++){
		//			System.out.println(((MappingElement) rrr.recordStatement.get(i)).getType());
		//		}
		//[public, func, offsetLeft, CGFloat, _contentRect.origin.x]
		//[public, var, offsetLeft, CGFloat, _contentRect.origin.x]
		//[public, var, offsetLeft, _contentRect.origin.x]
		//[public, func, isInBoundsTop, _contentRect.origin.y<=y?true:false]
	}

}
