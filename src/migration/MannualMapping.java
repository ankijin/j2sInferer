package migration;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;

import com.github.gumtreediff.actions.ActionGenerator;
import com.github.gumtreediff.client.Run;
import com.github.gumtreediff.gen.Generators;
import com.github.gumtreediff.io.TreeIoUtils;
import com.github.gumtreediff.matchers.Matcher;
import com.github.gumtreediff.matchers.Matchers;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.Tree;
import com.github.gumtreediff.tree.TreeContext;
import com.github.gumtreediff.tree.TreeMap;
import com.google.common.collect.Lists;
import com.github.gumtreediff.actions.model.Action;
import com.github.gumtreediff.actions.model.Addition;
import com.github.gumtreediff.actions.model.Insert;


public class MannualMapping {
	Map<ITree, ITree> parentMap = new HashMap<ITree, ITree>();	
	Map<ITree, ArrayList<ITree>> targetMap = new HashMap<ITree, ArrayList<ITree>>();
	Map<ITree, ITree> grammarMap = new HashMap<ITree, ITree>();	
	Map<String, String>  migrationMap = new HashMap<String, String>();	
	Map<Integer, Integer>  typeMap = new HashMap<Integer, Integer>();	


	Map<ITree, ITree> grammarMapping = new HashMap<ITree, ITree>();	
	Map<ITree, ITree> treeMapping = new HashMap<ITree, ITree>();	

	Map<ArrayList<ITree>, ArrayList<ITree>> aa = new HashMap<ArrayList<ITree>, ArrayList<ITree>>();

	void compute() throws Exception{
		//		getID();
		//		MapMaker();
		//	TypeDeclMapp();

		//		migrationMap.put("boolean", "Bool");
		//		migrationMap.put("public", "Public");
		//		migrationMap.put("private", "private");
		//		migrationMap.put("protected", "internal");
		//		migrationMap.put("&gt;", "&gt;");
		//		migrationMap.put("observers.length", "_observers.count111111");
		//		parseXml();

		ITree aSrc = new JdtTreeGenerator().generateFromFile("charts/android/ViewPortHandler_V0.java").getRoot();
		ITree aDst = new JdtTreeGenerator().generateFromFile("charts/android/ViewPortHandler_V1.java").getRoot();


		//		System.out.println(aSrc.toTreeString());
		//		ITree aSrc = new JdtTreeGenerator().generateFromFile("Async_0.java").getRoot();
		//		ITree aDst = new JdtTreeGenerator().generateFromFile("Async_1.java").getRoot();
		//		ITree aSrc = new JdtTreeGenerator().generateFromFile("test1_0.java").getRoot();
		//		ITree aDst = new JdtTreeGenerator().generateFromFile("test1_1.java").getRoot();

		//		SwiftTreeGen swiftGen = new SwiftTreeGen();
		//		ITree iosSrc = new SwiftTreeGen().generateFromFile("BehaviorSubject_0.swift").getRoot();
		//		ITree iosDrc = new SwiftTreeGen().generateFromFile("BehaviorSubject_2.swift").getRoot();

		//get diff of Android
		Matcher m = Matchers.getInstance().getMatcher(aSrc, aDst); // retrieve the default matcher
		m.match();
		ActionGenerator g = new ActionGenerator(aSrc, aDst, m.getMappings());
		g.generate();
		List<Action> actionsA = g.getActions(); // return the actions

		int numA = 0;
		for(int i=0;i<actionsA.size();i++){
			Action a = actionsA.get(i);
			System.out.println(a);
			if (a instanceof Insert){
				Insert ia = (Insert) a;
				//								System.out.println("`"+ia.getNode().toTreeString());
			}
		}

		//		TreeContext context11 = new TreeContext();;
		//		context11.setRoot(((Insert) actionsA.get(0)).getNode());
		//		ByteArrayOutputStream bos11 = new ByteArrayOutputStream();
		//		TreeIoUtils.toXml(context11).writeTo(bos11);
		//		System.out.println("AAAAAAA"+bos11);

		//get corresponding diff of iOS
		//		getdiffiO(((Insert) actionsA.get(0)).getNode());





		/*

		Matcher mo = Matchers.getInstance().getMatcher(iosSrc, iosDrc); // retrieve the default matcher
		mo.match();
		ActionGenerator go = new ActionGenerator(iosSrc, iosDrc,mo.getMappings());
		go.generate();
		List<Action> actionsIO = go.getActions(); // return the actions
		int numiO = 0;

		for(int i=0;i<actionsIO.size();i++){
			Action a = actionsIO.get(i);
//			System.out.println("111111111\n"+a);
			if (a instanceof Insert){
				Insert ia = (Insert) a;
//				System.out.println("111111111"+ia.getNode().toTreeString());
			}
		}
		 */

		//mappingStart(((Insert) actionsA.get(0)).getNode(), ((Insert) actionsIO.get(0)).getNode());
		/*

		 */
		//		TreeContext context1 = new TreeContext();;
		//		context1.setRoot(((Insert) actionsIO.get(0)).getNode());
		//		ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
		//		TreeIoUtils.toXml(context1).writeTo(bos1);
		//		TreeContext tca1 = TreeIoUtils.fromXml().generateFromString(bos11.toString());
		//		System.out.println("XXXXXXxmlxml "+((Insert) actionsA.get(0)).getNode().toTreeString());
		//		System.out.println("IIIIII"+bos1);

		//parseXml();

	}

	public String getMigrationMap(String android){
		if(migrationMap.containsKey(android)){
			return migrationMap.get(android);
		} else{
			return android;
		}

	}



	public void manualmapping() throws Exception{
		TreeContext  aContext = new TreeContext();
		ITree aNode1 = aContext.createTree(41, "return","");
		ITree aNode2 = aContext.createTree(27, ITree.NO_LABEL,"");
		ITree aNode3 = aContext.createTree(40, ITree.NO_LABEL,"");
		ITree aNode4 = aContext.createTree(34, ITree.NO_LABEL,"");
		aContext.setRoot(aNode1);
		aNode1.addChild(aNode2);
		aNode2.addChild(aNode3);
		aNode2.addChild(aNode4);



		ByteArrayOutputStream nbos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(aContext).writeTo(nbos);
		TreeContext ntca = TreeIoUtils.fromXml().generateFromString(nbos.toString());
		ITree nca = ntca.getRoot();
		System.out.println(nca.toTreeString()); 

		//		aNode4.setParentAndUpdateChildren(aNode3);
		//		aNode3.setParentAndUpdateChildren(aNode2);
		//		aNode1.setParentAndUpdateChildren(aNode2);



		//	System.out.println(aNode1.toTreeString());
		/*
		//		ITree node1 = context.createTree(83, "public","Modifier");
		ITree nnode1 = ncontext.createTree(83, ITree.NO_LABEL,"Modifier");
		ncontext.setRoot(nnode);
		nnode.addChild(nnode1);
		//	node1.setParentAndUpdateChildren(node);
		ByteArrayOutputStream nbos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(ncontext).writeTo(nbos);
		TreeContext ntca = TreeIoUtils.fromXml().generateFromString(nbos.toString());
		ITree nca = ntca.getRoot();
		System.out.println(nca.toTreeString());   
		 */

	}

	public void setLabel() throws IOException{
		TreeContext treeA = TreeIoUtils.fromXml().generateFromFile("decl_a1.xml");
		Iterator<ITree> trees_a = treeA.getRoot().breadthFirst().iterator();
		List<ITree> aa = Lists.newArrayList(trees_a);

	}


	public void matchTree(ITree patree, String alabel, ITree pitree, String ilabel){

	}
	public boolean hasSibling(ITree tree){



		if(tree.getParent()==null)
			return false;

		else{
			System.out.println(tree.getParent().toShortString()+"hasSibling"+tree.getParent().getChildren().size());
			return tree.getParent().getChildren().size() >1;
		}
	}

	public void mappingExpression(ITree diffA, ITree diffiO) throws Exception{

		TreeContext treeA = new TreeContext();;
		//		diffA.setDepth(0);
		//		diffA.setPos(0);
		treeA.setRoot(diffA);

		TreeContext treeiOS = new TreeContext();;
		//		diffiO.setDepth(0);
		//		diffiO.setPos(0);
		treeiOS.setRoot(diffiO);

		ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
		TreeIoUtils.toXml(treeA).writeTo(bos1);


		System.out.println("mappingExpression");

		//		Iterator<ITree> trees_ios = treeiOS.getRoot().breadthFirst().iterator();
		//		Iterator<ITree> trees_a = treeA.getRoot().breadthFirst().iterator();

		//preorder walking
		List<ITree> list_ios = new ArrayList<ITree>();
		List<ITree> list_android = new ArrayList<ITree>();
		//preorder walking
		list_ios.add(treeiOS.getRoot());
		list_android.add(treeA.getRoot());
		list_ios.addAll(treeiOS.getRoot().getDescendants());
		list_android.addAll(treeA.getRoot().getDescendants());
		//		System.out.println("leaves"+treeA.getRoot().getLeaves());
		//		System.out.println("labels"+treeA.getRoot().getChildrenLabels());
		//		Spliterator<ITree> list = treeA.getRoot().breadthFirst().spliterator();
		Iterator<ITree> iterA = treeA.getRoot().breadthFirst().iterator();

		while(iterA.hasNext()){
			System.out.println("iterator"+iterA.next().toShortString());
		}

		for(int i=0;i<list_android.size();i++){
			for(int j=0;j<list_ios.size();j++){
				ITree itree_a = list_android.get(i);
				ITree itree_i = list_ios.get(j);
				//				System.err.println(itree_a.getType()+"has "+hasSibling(itree_a));

				String label_a = itree_a.getLabel();
				String label_i = itree_i.getLabel();

				System.err.println(label_a+":"+label_i);
				StringMetric metric = StringMetrics.jaroWinkler();

				//				float result = metric.compare(label_a, label_i); //0.4767

				//				if(metric.compare(label_i,">")>0.99 && metric.compare(label_a,">")>0.99){
				if(metric.compare(label_i,label_a)>0.99 && label_i!=""){
					//					System.out.println("expression "+label_i+" and "+label_a+" are "+result);
					//					System.out.println("types"+itree_a.toTreeString());
					//					System.out.println("types"+itree_i.toTreeString());
					//					System.out.println("tree parent java"+itree_a.getParent().toTreeString());
					System.out.println("parentMap"+grammarMap.get(itree_a.getParent()));
					ITree ptree = grammarMap.get(itree_a.getParent());
					ITree pp= itree_i.getParent();
					while(pp.getType()==ptree.getType()){
						pp = pp.getParent();
					}
					System.out.println("ios parent for"+label_i+":\n"+pp.toTreeString());
				}
				//				itree_a.isLeaf();
				/*
				if(metric.compare(label_i,">")>0.99 && metric.compare(label_a,">")>0.99){

					System.out.println("greaterthan "+label_i+" "+label_a+"is "+result);

					System.out.println("depth: "+itree_a.getDepth()+", "+itree_i.getDepth());
					System.out.println("type tracing ");
					ITree parent= itree_i.getParent();

					while(!hasSibling(parent)){
						parent = parent.getParent();
						System.out.println("type"+parent.getType());
					}
					grammarMap.put(itree_a, parent);
					System.out.println(parent.toTreeString());
				}
				 */
			}
		}
		System.out.println("grammarMap2"+grammarMap);
	}


	//mapping for aligned diff codes
	public void mappingStart(ITree diffA, ITree diffiO) throws Exception{


		//		TreeContext treeiOS = TreeIoUtils.fromXml().generateFromFile("sts_i2.xml");
		//		TreeContext treeA = TreeIoUtils.fromXml().generateFromFile("sts_a2.xml");
		TreeContext treeA = new TreeContext();;
		treeA.setRoot(diffA);

		TreeContext treeiOS = new TreeContext();;
		treeiOS.setRoot(diffiO);

		ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
		TreeIoUtils.toXml(treeA).writeTo(bos1);


		System.out.println("mappingStart");
		//		System.out.println("mappingStart leaves"+diffA.getLeaves());
		//		System.out.println("mappingStart leaves"+diffiO.getLeaves());

		List<ITree> list_ios = new ArrayList<ITree>();
		List<ITree> list_android = new ArrayList<ITree>();
		//preorder walking
		list_ios.add(treeiOS.getRoot());
		list_android.add(treeA.getRoot());
		list_ios.addAll(treeiOS.getRoot().getDescendants());
		list_android.addAll(treeA.getRoot().getDescendants());

		//		System.out.println("mmm"+list_ios);
		//		System.out.println("mmm"+list_android);
		for(int i=0;i<list_android.size();i++){
			for(int j=0;j<list_ios.size();j++){
				ITree itree_a = list_android.get(i);
				ITree itree_i = list_ios.get(j);
				//				System.err.println(itree_a.getType()+"has "+hasSibling(itree_a));

				String label_a = itree_a.getLabel();
				String label_i = itree_i.getLabel();



				//check same label: return public .. , set as similarity
				StringMetric metric = StringMetrics.jaroWinkler();

				float result = metric.compare(label_a, label_i); //0.4767
				//check return statements
				if(metric.compare(label_i,"return")>0.99 && metric.compare(label_a,"return")>0.99){
					//					System.out.println("returnS "+label_i+" "+label_a+"is "+result);
					grammarMap.put(itree_a, itree_i);
					grammarMap.put(itree_a.getChild(0), itree_i.getChild(0));
					//					System.out.println("labels"+itree_a.getChildrenLabels());
					//					System.out.println("labels"+itree_i.getChildrenLabels());
					//					System.out.println("ddddddddd leaves"+itree_i.getLeaves());
					mappingExpression(itree_a.getChild(0), itree_i.getChild(0));
				}
			}




		}
		System.out.println("grammarMap"+grammarMap);

	}

	/*

	public void getdiffiO(ITree diffA) throws Exception{

		//		migrationMap.put("boolean", "Bool");
		//		migrationMap.put("public", "Public");
		//		migrationMap.put("private", "private");
		//		migrationMap.put("protected", "internal");
		//		migrationMap.put("&gt;", "&gt;");
		//		migrationMap.put("observers.length", "_observers.count111111");

		//		System.out.println("migration map test");
		//		System.out.println(getMigrationMap("boolean"));
		//		System.out.println(getMigrationMap("hasObserver"));

		//		typeMap.put(83, 163);
		//loading rules: android

		//grammar mapping
		//api mapping



		TreeContext treeiOS = TreeIoUtils.fromXml().generateFromFile("sts_i2.xml");
		//		TreeContext treeA = TreeIoUtils.fromXml().generateFromFile("sts_a2.xml");
		TreeContext treeA = new TreeContext();;
		treeA.setRoot(diffA);
		ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
		TreeIoUtils.toXml(treeA).writeTo(bos1);


		System.out.println("parseXml");

		Iterator<ITree> trees_ios = treeiOS.getRoot().breadthFirst().iterator();
		Iterator<ITree> trees_a = treeA.getRoot().breadthFirst().iterator();

		//preorder walking
		List<ITree> list_ios = treeiOS.getRoot().getDescendants();
		List<ITree> list_android = treeA.getRoot().getDescendants();

		for(int i=0;i<list_android.size();i++){
			for(int j=0;j<list_ios.size();j++){
				ITree itree_a = list_android.get(i);
				ITree itree_i = list_ios.get(j);
				//				System.err.println(itree_a.getType()+"has "+hasSibling(itree_a));

				//check return tokens and get 
				if(itree_a.getLabel() == itree_i.getLabel() && itree_i.getLabel() =="return"){
					System.out.println("returnS "+itree_a.getType()+" "+itree_i.getType());
					System.out.println("android"+itree_a.getDescendants());
					System.out.println("ios"+itree_i.getDescendants());
				}
				//check same label: return public .. , set as similarity

				String label_a = itree_a.getLabel();
				String label_i = itree_i.getLabel();

				StringMetric metric = StringMetrics.jaroWinkler();

				float result = metric.compare(label_a, label_i); //0.4767
				//			    System.out.println("StringMetric"+result);
				//				if(itree_a.getLabel() == itree_i.getLabel() && itree_i.getLabel()!=""){
				if(label_a==label_i && label_a=="return"){
					System.out.println("returnS "+itree_a.getType()+" "+itree_i.getType());
					System.out.println("android"+itree_a.getChild(0).toTreeString());
					System.out.println("ios"+itree_i.getChild(0).toTreeString());
				}

				if(result >0.7 && label_a!=""){
					System.out.println(itree_a.getType()+" "+itree_i.getType());
					System.out.println(label_a+" and "+label_i+" :"+result);
					//					System.out.println("ios"+itree_i.getLabel()); 
				}

				if(itree_a.getType()==83 && itree_i.getType()==161){
					ITree itree_i_next = list_ios.get(j+1);
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	
					list_ios.set(j+1, itree_i_next);
				}

				if(itree_a.getType()==42 && itree_i.getType()==82){


					ITree itree_i_next = list_ios.get(j+1);
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	
					list_ios.set(j+1, itree_i_next);
				}

				if(itree_a.getType()==39 && itree_i.getType()==221){


					ITree itree_i_next = list_ios.get(j+1);
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	
					list_ios.set(j+1, itree_i_next);
				}

				if(itree_a.getType()==40 && itree_i.getType()==180){

					System.out.println("40 and 180");
					ITree itree_i_next = list_ios.get(j+1);
					System.out.println("40 and 180"+itree_i_next.getLabel()+itree_a.getLabel());
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	
					System.out.println("40 and 180"+itree_i_next.getType()+itree_i_next.getLabel());
					list_ios.set(j+1, itree_i_next);
				}


			}
		}




		while(trees_a.hasNext()){
			ITree tree_a = trees_a.next();

			while(trees_ios.hasNext()){

				ITree tree_ios = trees_ios.next();
	//			System.out.println("label"+tree_a.getType());
				//			System.out.println(tree1.getType()+":"+getTypeNameiO(tree1.getType()));

				//
				if(tree_a.getType()==83 && tree_ios.getType()==161){
					System.out.println(tree_a.getLabel());
					trees_ios.next().setLabel(migrationMap.get(tree_a.getLabel()));	
				}

				if(tree_a.getType()==42 && tree_ios.getType()==82){
					System.out.println("42 and 82 "+tree_a.getLabel()+trees_ios.next());

					//trees_ios.next().setLabel(migrationMap.get(tree_a.getLabel()));	
				}

			}
			trees_ios = treeiOS.getRoot().breadthFirst().iterator();

		}


		trees_ios = treeiOS.getRoot().breadthFirst().iterator();

		//	while(trees_ios.hasNext()){
		//	System.err.println("changed?"+trees_ios.next().getLabel());
		//	}

		TreeContext context11 = new TreeContext();
		//	list_ios.get(0).setParent(trees_ios.next());
		list_ios.get(0).setParentAndUpdateChildren(trees_ios.next());
		context11.setRoot(list_ios.get(0).getParent());
		ByteArrayOutputStream bos11 = new ByteArrayOutputStream();
		TreeIoUtils.toXml(context11).writeTo(bos11);
		//		TreeContext tca1 = TreeIoUtils.fromXml().generateFromString(bos11.toString());
		//		System.out.println("XXXXXXxmlxml "+((Insert) actionsA.get(0)).getNode().toTreeString());
		System.out.println("generated iosDiff\n"+bos11);


	}

	 */
	public void parseXml() throws Exception{

		//		migrationMap.put("boolean", "Bool");
		//		migrationMap.put("public", "Public");
		//		migrationMap.put("private", "private");
		//		migrationMap.put("protected", "internal");
		//		migrationMap.put("&gt;", "&gt;");
		//		migrationMap.put("observers.length", "_observers.count111111");

		//		System.out.println("migration map test");
		//		System.out.println(getMigrationMap("boolean"));
		//		System.out.println(getMigrationMap("hasObserver"));

		//		typeMap.put(83, 163);
		//loading rules: android

		//grammar mapping
		//api mapping

		TreeContext treeiOS = TreeIoUtils.fromXml().generateFromFile("sts_i2.xml");
		TreeContext treeA = TreeIoUtils.fromXml().generateFromFile("sts_a2.xml");
		System.out.println("parseXml");


		Iterator<ITree> trees_ios = treeiOS.getRoot().breadthFirst().iterator();
		Iterator<ITree> trees_a = treeA.getRoot().breadthFirst().iterator();

		//preorder walking
		List<ITree> list_ios = treeiOS.getRoot().getDescendants();
		List<ITree> list_android = treeA.getRoot().getDescendants();

		for(int i=0;i<list_android.size();i++){
			for(int j=0;j<list_ios.size();j++){
				ITree itree_a = list_android.get(i);
				ITree itree_i = list_ios.get(j);

				if(itree_a.getType()==83 && itree_i.getType()==161){
					ITree itree_i_next = list_ios.get(j+1);
					migrationMap.put(itree_a.getLabel(), itree_i_next.getLabel());
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	

					list_ios.set(j+1, itree_i_next);
				}

				if(itree_a.getType()==42 && itree_i.getType()==82){


					ITree itree_i_next = list_ios.get(j+1);
					migrationMap.put(itree_a.getLabel(), itree_i_next.getLabel());
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	
					list_ios.set(j+1, itree_i_next);
				}

				if(itree_a.getType()==39 && itree_i.getType()==221){


					ITree itree_i_next = list_ios.get(j+1);
					migrationMap.put(itree_a.getLabel(), itree_i_next.getLabel());
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	
					list_ios.set(j+1, itree_i_next);
				}

				if(itree_a.getType()==40 && itree_i.getType()==180){

					//					System.out.println("40 and 180");
					ITree itree_i_next = list_ios.get(j+1);
					//					System.out.println("40 and 180"+itree_i_next.getLabel()+itree_a.getLabel());
					migrationMap.put(itree_a.getLabel(), itree_i_next.getLabel());
					itree_i_next.setLabel(getMigrationMap(itree_a.getLabel()));	
					//					System.out.println("40 and 180"+itree_i_next.getType()+itree_i_next.getLabel());
					list_ios.set(j+1, itree_i_next);
				}


			}
		}


		/*

		while(trees_a.hasNext()){
			ITree tree_a = trees_a.next();

			while(trees_ios.hasNext()){

				ITree tree_ios = trees_ios.next();
	//			System.out.println("label"+tree_a.getType());
				//			System.out.println(tree1.getType()+":"+getTypeNameiO(tree1.getType()));

				//
				if(tree_a.getType()==83 && tree_ios.getType()==161){
					System.out.println(tree_a.getLabel());
					trees_ios.next().setLabel(migrationMap.get(tree_a.getLabel()));	
				}

				if(tree_a.getType()==42 && tree_ios.getType()==82){
					System.out.println("42 and 82 "+tree_a.getLabel()+trees_ios.next());

					//trees_ios.next().setLabel(migrationMap.get(tree_a.getLabel()));	
				}

			}
			trees_ios = treeiOS.getRoot().breadthFirst().iterator();

		}

		 */
		trees_ios = treeiOS.getRoot().breadthFirst().iterator();

		//	while(trees_ios.hasNext()){
		//	System.err.println("changed?"+trees_ios.next().getLabel());
		//	}

		TreeContext context11 = new TreeContext();
		//	list_ios.get(0).setParent(trees_ios.next());
		list_ios.get(0).setParentAndUpdateChildren(trees_ios.next());
		context11.setRoot(list_ios.get(0).getParent());
		ByteArrayOutputStream bos11 = new ByteArrayOutputStream();
		TreeIoUtils.toXml(context11).writeTo(bos11);
		//		TreeContext tca1 = TreeIoUtils.fromXml().generateFromString(bos11.toString());
		//		System.out.println("XXXXXXxmlxml "+((Insert) actionsA.get(0)).getNode().toTreeString());
		//		System.out.println("AAAAAAA"+bos11);

		System.err.println("migrationMap"+migrationMap);
	}

	String getTypeNameA(int type) throws IOException{
		return new JdtTreeGenerator().generateFromFile("BehaviorSubject_V0.java").getTypeLabel(type).toString();
	}

	String getTypeNameiO(int type) throws IOException{
		return new SwiftTreeGen().generateFromFile("BehaviorSubject_1.swift").getTypeLabel(type).toString();
	}

	void migrationMapping(){
		migrationMap.put("boolean", "Bool");
		migrationMap.put("public", "public");
		migrationMap.put("protected", "internal");


		typeMap.put(83, 163);
		//loading rules: android

		//grammar mapping

		//api mapping

	}



	void grammarMapping(Action actionA, Action actioniO){
		Insert iA = (Insert) actionA;
		Insert iO = (Insert) actioniO;
		//	parentMap.put(iA.getParent(), iO.getParent());
		//		iA.getNode().post
		grammarMap.put(iA.getNode(), iO.getNode());
		//updateTargetMap(iA.getNode(), iO.getNode());
	}

	void mapping(Action actionA, Action actioniO){
		Insert iA = (Insert) actionA;
		Insert iO = (Insert) actioniO;
		parentMap.put(iA.getParent(), iO.getParent());
		updateTargetMap(iA.getNode(), iO.getNode());
	}

	void mapping(Insert actionA, List<Insert> actioniOList){

		parentMap.put(actionA.getParent(), actioniOList.get(0).getParent());
		ArrayList<ITree> list = new ArrayList<ITree>();
		for(Insert a:actioniOList){
			list.add(a.getNode());
		}
		updateTargetMap(actionA.getNode(), list);
	}

	void updateTargetMap(ITree key, ITree item){
		ArrayList<ITree> list = targetMap.get(key);
		if(list ==null) {list = new ArrayList<ITree>();}
		list.add(item);
		//		if(!targetMap.containsValue(list))
		targetMap.put(key, list);
	}
	void updateTargetMap(ITree key, ArrayList<ITree> item){
		//	ArrayList<ITree> list = targetMap.get(key);
		//		if(!targetMap.containsValue(item))
		targetMap.put(key, item);
	}


	void MapMaker1() throws Exception{
		TreeContext  context = new TreeContext();
		ITree node = context.createTree(55, ITree.NO_LABEL,"TypeDeclaration");
		ITree node1 = context.createTree(83, "public","Modifier");
		context.setRoot(node);
		//		node.addChild(node1);
		node1.setParentAndUpdateChildren(node);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(context).writeTo(bos);
		TreeContext tca = TreeIoUtils.fromXml().generateFromString(bos.toString());
		ITree ca = tca.getRoot();
		System.out.println(ca.toTreeString());
	}
	void TypeDeclMapp() throws Exception{
		TreeContext  contextA = new TreeContext();
		//		ITree nodes[];
		int labelA [] ={31};
		//int labelA [] ={31,83, 39, 42,8};


		TreeContext  context = new TreeContext();
		ITree parent = null;
		for(int i=0;i<labelA.length;i++){
			ITree node = context.createTree(labelA[i], ITree.NO_LABEL, getTypeNameA(labelA[i]));
			if(context.getRoot()==null){
				context.setRoot(node);

			}
			if(parent !=null){
				node.setParentAndUpdateChildren(parent);
			}
			parent = node;
		}
		ITree node83 = context.createTree(83, "public", getTypeNameA(83));
		ITree node39 = context.createTree(39, "boolean", getTypeNameA(39));
		ITree node42 = context.createTree(42, "hasObservers", getTypeNameA(42));
		ITree node8 = context.createTree(8, ITree.NO_LABEL, getTypeNameA(8));

		node83.setParentAndUpdateChildren(parent);
		node39.setParentAndUpdateChildren(parent);
		node42.setParentAndUpdateChildren(parent);
		node8.setParentAndUpdateChildren(parent);

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(context).writeTo(bos);
		TreeContext tca = TreeIoUtils.fromXml().generateFromString(bos.toString());
		System.out.println("@@@@@@@@@@xml "+bos.toString());
		ITree ca = tca.getRoot();
		//		System.out.println("##########"+ca.toTreeString());

		Iterator<ITree> ccc = ca.breadthFirst().iterator();

		while(ccc.hasNext()){
			System.err.println("next"+ccc.next().toShortString());
		}

		int labeliO [] ={69, 68, 80, 81, 162, 161, 163};

		TreeContext  contextiO = new TreeContext();
		ITree parentiO = null;
		for(int i=0;i<labeliO.length;i++){
			ITree node = context.createTree(labeliO[i], ITree.NO_LABEL, getTypeNameA(labeliO[i]));
			if(contextiO.getRoot()==null){
				contextiO.setRoot(node);

			}
			if(parentiO !=null){
				node.setParentAndUpdateChildren(parentiO);
			}
			parentiO = node;
		}

		Iterator<ITree> treee = contextiO.getRoot().breadthFirst().iterator();

		while(treee.hasNext()){
			System.out.println("next "+treee.next().toShortString());
		}

		ByteArrayOutputStream ibos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(contextiO).writeTo(ibos);
		TreeContext itca = TreeIoUtils.fromXml().generateFromString(ibos.toString());
		ITree ica = itca.getRoot();
		System.out.println(ica.toTreeString());


	}

	void MapMaker() throws Exception{
		TreeContext  context = new TreeContext();
		ITree node = context.createTree(55, ITree.NO_LABEL,"TypeDeclaration");
		//		ITree node1 = context.createTree(83, "public","Modifier");
		ITree node1 = context.createTree(83, ITree.NO_LABEL,"Modifier");
		context.setRoot(node);
		node.addChild(node1);
		//	node1.setParentAndUpdateChildren(node);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(context).writeTo(bos);
		TreeContext tca = TreeIoUtils.fromXml().generateFromString(bos.toString());
		ITree ca = tca.getRoot();
		System.out.println(ca.toTreeString());

		TreeContext  ncontext = new TreeContext();
		ITree nnode = ncontext.createTree(55, ITree.NO_LABEL,"TypeDeclaration");
		//		ITree node1 = context.createTree(83, "public","Modifier");
		ITree nnode1 = ncontext.createTree(83, ITree.NO_LABEL,"Modifier");
		ncontext.setRoot(nnode);
		nnode.addChild(nnode1);
		//	node1.setParentAndUpdateChildren(node);
		ByteArrayOutputStream nbos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(ncontext).writeTo(nbos);
		TreeContext ntca = TreeIoUtils.fromXml().generateFromString(nbos.toString());
		ITree nca = ntca.getRoot();
		System.out.println(nca.toTreeString());        

		System.out.println("test EQ"+ca.equals(nca));

		TreeContext  contextiOS = new TreeContext();
		Tree i_node0 = (Tree) contextiOS.createTree(1, ITree.NO_LABEL,"statement");
		Tree i_node00 = (Tree) contextiOS.createTree(1, ITree.NO_LABEL,"statement");

		System.err.println(i_node0.equals(i_node00));

		ITree i_node1 = contextiOS.createTree(68, ITree.NO_LABEL,"declaration");
		ITree i_node2 = contextiOS.createTree(130, ITree.NO_LABEL,"class_declaration");
		//		ITree i_node3 = contextiOS.createTree(163, "public","access_level_modifier");
		ITree i_node3 = contextiOS.createTree(163, ITree.NO_LABEL,"access_level_modifier");

		contextiOS.setRoot(i_node0);

		i_node0.addChild(i_node1);
		i_node1.addChild(i_node2);
		i_node2.addChild(i_node3);

		ByteArrayOutputStream ibos = new ByteArrayOutputStream();
		TreeIoUtils.toXml(contextiOS).writeTo(ibos);
		TreeContext itca = TreeIoUtils.fromXml().generateFromString(ibos.toString());
		ITree ica = itca.getRoot();

		System.out.println(ca.getHash()+"\n"+ca.toTreeString());
		System.out.println(nca.getHash()+"\n"+nca.toTreeString());
		System.out.println(ca.equals(nca));
		System.out.println(ca.equals(nca));
		grammarMap.put(ca, ica);

		System.out.println("getget"+grammarMap.get(nca));
		//     System.out.println("grammar\n"+grammarMap.get(nca).toTreeString());


		//		TreeContext  context1 = new TreeContext();
		//	Tree node = new Tree(40, "android.location.Location");

		//		ITree node0 = context1.createTree(15, ITree.NO_LABEL,"Compilation");

		//		System.out.println(node.toTreeString());

		//		Insert aaa = new Insert(node, node0, 5);
		//		System.out.println(aaa);
		/*		
        TreeContext tc = new TreeContext();
        ITree a = tc.createTree(0, "a", "type0");
        tc.setRoot(a);

        ITree b = tc.createTree(1, "b", null);
        b.setParentAndUpdateChildren(a);
        ITree c = tc.createTree(3, "c", null);
        c.setParentAndUpdateChildren(b);
        ITree d = tc.createTree(3, "d", null);
        d.setParentAndUpdateChildren(b);
        ITree e = tc.createTree(2, null, null);
        e.setParentAndUpdateChildren(a);
        tc.validate();
		 */



	}

	void makeMap(){
		TreeContext  context = new TreeContext();
		//	Tree node = new Tree(40, "android.location.Location");
		ITree node = context.createTree(55, "android.location.Location","QualifiedName");
		ITree node1 = context.createTree(26, ITree.NO_LABEL,"ImportDeclaration");
		//		ITree node2 = context.createTree(15, ITree.NO_LABEL,"CompilationUnit");
		context.setRoot(node1);
		//node2.addChild(node1);
		node1.addChild(node);
		ArrayList<ITree> list = new ArrayList<ITree>();
		targetMap.put(node1, list);

		//ITree node2 = context.createTree(40, "android.location.Location","QualifiedName");
		///ITree node21 = context.createTree(26, ITree.NO_LABEL,"ImportDeclaration");

		//node.setParentAndUpdateChildren(node1);
		//node1.setParentAndUpdateChildren(node2);
		//System.err.println(context.getRoot().toTreeString());

	}

	List<MyAction> getChangesFromAndroid(Action androidAction){
		List<MyAction> actions = new ArrayList<>();
		if (androidAction instanceof Insert){
			//			System.out.println("test"+targetMap);
			// Addition(ITree node, ITree parent, int pos)
			Insert additionAndroid = (Insert) androidAction;
			ITree androidParent = additionAndroid.getParent();
			ITree iosParent = parentMap.get(androidParent);

			//			if(iosParent!=null)
			//			System.out.println("iosParent "+iosParent.toShortString());
			ITree androidTarget = additionAndroid.getNode();
			ArrayList<ITree> iosTargets = targetMap.get(androidTarget);
			//			if(iosTargets!=null)
			//			System.out.println("androidTarget "+iosTargets.size());
			//			List<Action> ww;

			if(iosParent!=null && iosTargets!=null){
				for(int j=0;j<iosTargets.size();j++){
					MyAction target = new MyAction(iosTargets.get(j), iosParent, 0);
					actions.add(target);
					//				System.out.println("TARGET\n"+target);
				}
			}
			return actions;
		}
		return null;
	}

	ITree getImportiOS(String label){		
		return null;
	}
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		MannualMapping diff = new MannualMapping();
		//		diff.makeMap();
		diff.compute();


		String str1 = "boolean";
		String str2 = "Bool";

		String ios_str[]={"_observers", "count","_observers.count", "&gt","0"};
		String a_str[]={"observers.length","&gt", "0"};

		StringMetric metric = StringMetrics.jaroWinkler();

		for(int i = 0;i<a_str.length;i++){
			for(int j=0;j<ios_str.length;j++){

				System.out.println(a_str[i]+" and "+ios_str[j]+" :"+metric.compare(a_str[i], ios_str[j]));
			}
		}

		float result = metric.compare(str1, str2); //0.4767
		System.out.println("StringMetric"+result);

		diff.manualmapping();

	}
}
