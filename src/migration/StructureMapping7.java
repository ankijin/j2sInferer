package migration;

import static org.simmetrics.builders.StringMetricBuilder.with;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.compiler.ITerminalSymbols;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.internal.compiler.parser.Scanner;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.JaroWinkler;

import alignment.common.HungarianAlgorithm;
import alignment.common.OpProperty;
import alignment.common.SwiftListenerWithoutContext;
import recording.java.JDTVisitorWithoutContext;
import sqldb.ProtoSQLiteJDBC;
import sqldb.SQLiteJDBC;

public class StructureMapping7 {
	public static int grammar_id=0;
	ASTNode android;
	ParserRuleContext swift;
	public SwiftListenerWithoutContext swictx;
	public JDTVisitorWithoutContext wocontext;

	private boolean stopped = false;

	public StructureMapping7(ASTNode android, ParserRuleContext swift){
		this.android = android;
		this.swift = swift;
		swictx = new SwiftListenerWithoutContext(swift);
		wocontext = new JDTVisitorWithoutContext(android);
		//		SwiftParser.Rule

	}

	public void compute(){

		List<String> 		aanodes 				= new LinkedList<String>();
		List<String> 		token_and 				= new LinkedList<String>();

		List<String> 	ssnodes 					= new LinkedList<String>();
		List<OpProperty> 	op_a_nodes 					= new LinkedList<OpProperty>();
		List<OpProperty> 	op_s_nodes 					= new LinkedList<OpProperty>();
		List<ParserRuleContext> 	tssnodes 		= new LinkedList<ParserRuleContext>();
		//		Scanner scanner = new Scanner();
		//		scanner.recordLineSeparator = true;
		//		char[] source = android.toString().toCharArray();
		int az = swift.start.getStartIndex();
		int bz = swift.stop.getStopIndex();
		Interval interval = new Interval(az,bz);

		final String swift_str2 = swift.start.getInputStream().getText(interval);
		String swift_str = swift_str2;
		//		StringBuffer b = new StringBuffer();
		//		scanner.setSource(source);

		ParseTreeWalker walker = new ParseTreeWalker();

		//		SwiftListenerWithoutContext swictx = new SwiftListenerWithoutContext(swift);


		walker.walk(swictx, swift);
		System.err.println(swift_str2);
		System.err.println("swift"+swictx.op_s_nodes);

		//		JDTVisitorWithoutContext wocontext = new JDTVisitorWithoutContext(android);

		android.accept(wocontext);
		System.err.println(android);
		System.out.println("androidandroid"+wocontext.op_a_nodes);


		double[][] ecostMatrix = new double[wocontext.op_a_nodes.size()][swictx.op_s_nodes.size()];


		StringMetric emetric =
				with(new JaroWinkler())
				.build();

		for(int i=0;i<wocontext.op_a_nodes.size();i++){
			for(int j=0;j<swictx.op_s_nodes.size();j++){
				//				float delta = 0;
				//				if(!wocontext.op_a_nodes.get(i).property.equals("none") && wocontext.op_a_nodes.get(i).property.equals(swictx.op_s_nodes.get(j).property)) {
				if( wocontext.op_a_nodes.get(i).property.equals(swictx.op_s_nodes.get(j).property)) {
					ecostMatrix[i][j]	= Math.min(0.9, 1- emetric.compare(wocontext.op_a_nodes.get(i).strASTNode, swictx.op_s_nodes.get(j).strASTNode));
						//if same: 1
						//identifiers, keywords
					System.err.println(emetric.compare("test","test")+"	ecostMatrix[i][j]"+ecostMatrix[i][j]+" "+wocontext.op_a_nodes.get(i).strASTNode+"  "+wocontext.op_a_nodes.get(i).property+"  "+swictx.op_s_nodes.get(j).property+" "+swictx.op_s_nodes.get(j).strASTNode);

				}else{
					ecostMatrix[i][j] 	= 1;
				}
				//method1: just compare if..  
			}
		}
		HungarianAlgorithm ehung = null;
		int[] eresult = new int[0];
		if(wocontext.op_a_nodes.size() > 0 && swictx.op_s_nodes.size() >0){
			ehung = new HungarianAlgorithm(ecostMatrix);
			eresult = ehung.execute();
			System.out.println("dim"+eresult.length);
		}

		Map<String, String> amap = new HashMap<String, String>();
		Map<String, String> smap = new HashMap<String, String>();
		int argnum = 0;
		for(int p=0; p<eresult.length;p++){
			if(eresult[p]!=-1){
				if(ecostMatrix[p][eresult[p]]>0){
				amap.put(wocontext.op_a_nodes.get(p).strASTNode.replace("\n", ""), "<arg"+argnum+">");
				smap.put(swictx.op_s_nodes.get(eresult[p]).strASTNode, "<arg"+argnum+">");
				String sssss = ((ParserRuleContext)swictx.op_s_nodes.get(eresult[p]).node).getText();

				//				if(!((ASTNode)wocontext.op_a_nodes.get(p).node instanceof MethodInvocation)){

				//				}
				//				}

				ASTNode c_android=(ASTNode)wocontext.op_a_nodes.get(p).node;
				ParserRuleContext c_swift=(ParserRuleContext)swictx.op_s_nodes.get(eresult[p]).node;

				System.out.println("%%%%%%%%%%%%"+ecostMatrix[p][eresult[p]]+c_android);
				//				int occurance = StringUtils.countOccurrencesOf("a.b.c.d", ".");
				//				if(StringUtils.countMatches(c_android.toString(), "+") == StringUtils.countMatches(c_swift.getText(), "+")
				//						&& 	StringUtils.countMatches(c_android.toString(), "-") == StringUtils.countMatches(c_swift.getText(), "-")	
				//						){


				StructureMapping7 mapping = new StructureMapping7((ASTNode)wocontext.op_a_nodes.get(p).node, (ParserRuleContext)swictx.op_s_nodes.get(eresult[p]).node);
				System.out.println("#########"+(ASTNode)wocontext.op_a_nodes.get(p).node+"    "+((ParserRuleContext)swictx.op_s_nodes.get(eresult[p]).node).getText());
				String ss = swictx.type;
				String oss = swictx.opsequence;
				System.out.println("swictx.opsequence"+ ss+"  "+oss +"  "+ wocontext.opsequence+"  "+wocontext.type);
				//					System.out.println(StringUtils.countMatches(c_android.toString(), "-"));
				//					System.out.println(StringUtils.countMatches(c_swift.getText(), "-")	);
				//				if(!Character.isUpperCase(sssss.charAt(0))){
				if(swictx.type.equals(wocontext.type) && swictx.opsequence.equals(wocontext.opsequence)){
					mapping.compute();
				}else{
					stopped = true;
				}
				//				}  
				argnum++;
				}
			}else{
				amap.put(wocontext.op_a_nodes.get(p).strASTNode.replace("\n", ""), "<arg"+argnum+">");
				argnum++;
			}
		}




		Comparator<String> comparator = new Comparator<String>() {
			public int compare(String o1, String o2) {
				if(o1.length()>=o2.length()) return -1; 
				else return 1;
			}
		};

		SortedSet<String> keys = new TreeSet<String>(comparator);
		keys.addAll(amap.keySet());

		SortedSet<String> skeys = new TreeSet<String>(comparator);
		skeys.addAll(smap.keySet());

		//		amap = sortByValues(amap, -1);  // Ascending order
		//		smap = sortByValues(smap, -1);  // Ascending order

		Iterator<String> ait = keys.iterator();
		Iterator<String> sit = skeys.iterator();

		//		Iterator<String> ait = amap.keySet().iterator();
		//		Iterator<String> sit = smap.keySet().iterator();
		swift.start.getInputStream();
		if(android instanceof FieldDeclaration){
			((FieldDeclaration)android).setJavadoc(null);
		}
		String android_str = android.toString().replace("\n", "");
		System.out.println("keys"+keys);
		int aaa=0;
		while(ait.hasNext()){
			String key = ait.next();

			String expr = key;
			String arg = amap.get(key);
			//			System.err.println(astnode+" "+arg);
			expr =expr.replace("\n", "");
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\!", "\\\\!");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
			expr=expr.replaceAll("\\+", "\\\\+");
			expr=expr.replaceAll("\\-", "\\\\-");
			//			expr=expr.replaceAll("\\ ", "\\\\ ");
			expr=expr.replaceAll("/", "\\\\/");
			//			expr=expr.replaceAll("\\s", "\\\\s");
			expr=expr.replaceAll("\\&", "\\\\&");
			expr=expr.replaceAll("\\>", "\\\\>");
			expr=expr.replaceAll("\\<", "\\\\<");
			//			android_str=android_str.replaceAll("\b"+expr+"\b", arg);
			android_str=android_str.replaceAll(expr, arg);
			System.err.println("amap	"+android_str+" "+expr+" "+amap+" "+aanodes);
			aaa++;
		}

		int sss=0;
		System.out.println("skeys"+skeys);
		while(sit.hasNext()){
			String key = sit.next();
			String expr = key;
			String arg = smap.get(key);
			//			expr =expr.replace("\n", "");
			expr=expr.replaceAll("\\(", "\\\\(");
			expr=expr.replaceAll("\\)", "\\\\)");
			expr=expr.replaceAll("\\.", "\\\\.");
			//			expr=expr.replaceAll("\\+", "\\\\+");
			//			expr=expr.replaceAll("\\-", "\\\\-");
			expr=expr.replaceAll("\\*", "\\\\*");
			expr=expr.replaceAll("\\!", "\\\\!");
			expr=expr.replaceAll("\\[", "\\\\[");
			expr=expr.replaceAll("\\]", "\\\\]");
			expr=expr.replaceAll("\\}", "\\\\}");
			expr=expr.replaceAll("\\{", "\\\\{");
			expr=expr.replaceAll("\\+", "\\\\+");
			expr=expr.replaceAll("\\-", "\\\\-");
			//			expr=expr.replaceAll("\\ ", "\\\\ ");
			expr=expr.replaceAll("/", "\\\\/");
			//			expr=expr.replaceAll("\\s", "\\\\s");
			expr=expr.replaceAll("\\&", "\\\\&");
			expr=expr.replaceAll("\\>", "\\\\>");
			expr=expr.replaceAll("\\<", "\\\\<");
			//			System.err.println(astnode+" "+arg);
			swift_str=swift_str.replaceAll(expr, arg);
			//			swift_str=swift_str.replaceFirst(expr, arg);
			//			swift_str=swift_str.replaceAll(expr, arg);
			System.out.println("smap	"+swift_str+" "+expr+" "+smap);
			sss++;
		}

		System.out.println("android_str	"+android+"   "+android_str);
		System.out.println("swift_str	"+swift_str2+"	"+swift_str);

		String e12 = "", e11="";
		if(stopped){

		e12 = SQLiteJDBC.insertTableGrammarAndroid(grammar_id, android.toString().replaceAll("\n", ""), android.getNodeType(), android.getParent().getNodeType(), android.toString().replaceAll("\n", ""));
		e11 = SQLiteJDBC.insertTableGrammarSwift(grammar_id, swift_str2.replaceAll("\n", ""), swift.getRuleIndex(), swift.getParent().getRuleIndex(), swift_str2.replaceAll("\n", ""), grammar_id);
		//		String e12 = ProtoSQLiteJDBC.insertTableExprAndroidV(grammar_id, mappingExpr.androidExpr.toString(),binding, bbb, Main.androidClassName+"."+MappingDeclaration.methodnameAndroid);
		//		String e11 = ProtoSQLiteJDBC.insertTableExprSwiftV(grammar_id,  mappingExpr.swiftExpr.getText(),  mappingExpr.swiftExpr.getText(), expr_id, genVarsS);
		}else{
			e12 = SQLiteJDBC.insertTableGrammarAndroid(grammar_id, android_str, android.getNodeType(), android.getParent().getNodeType(), android.toString().replaceAll("\n", ""));
			e11 = SQLiteJDBC.insertTableGrammarSwift(grammar_id, swift_str.replaceAll("\n", ""), swift.getRuleIndex(), swift.getParent().getRuleIndex(), swift_str2.replaceAll("\n", ""), grammar_id);

		}
		
		
		Connection c;
		try {

			c = DriverManager.getConnection("jdbc:sqlite:test_record.db");
			java.sql.Statement sql_stmt = c.createStatement();
			c.setAutoCommit(true);
			sql_stmt.executeUpdate(e12);	
			sql_stmt.executeUpdate(e11);

			grammar_id++;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println(e12);
			System.out.println(e11);
			e1.printStackTrace();
		}


		/*
		if(aaa==sss){
			BufferedWriter out;
			try {
				out = new BufferedWriter(new FileWriter("sample4.txt", true));
				//		out.write(android.toString()+"	"+swift.getText());
				out.write(android.toString()+android.getNodeType());
				out.write(swift.getText()+"\n"+swift.getRuleIndex());
				out.write(android_str.toString()+"\n");
				out.write(" "+swift_str.toString()+"\n");
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		 */
	}
	public static void main(String args []){
		System.out.println("main"+(StringUtils.countMatches("_chartWidth - _contentRect.size.width - _contentRect.origin.x", "-")==StringUtils.countMatches("_chartWidth - _contentRect.size.width", "-")));

	}
}
