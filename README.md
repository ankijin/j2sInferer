j2sInferer, tool for translating java to swift(Kijin An, ankijin@vt.edu)


``alignment.ExamplesAlignerTest``
<p align="center">
  <img src="examples/examples_align.png" width="1000"/>
</p>

//Query example from table, test_table and iteratively infer templates by reconstruction of AST from text and recorded type in the table

``alignment.ruleinfer.QueryExampleTest``
<p align="center">
  <img src="examples/templates.png" width="1000"/>
</p>

